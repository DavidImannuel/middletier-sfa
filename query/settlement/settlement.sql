-- insert db_sfa mysql to sql server
use d_sfa;
delete from d_sfa..t_sales_cost;
delete from d_sfa..t_somst;
delete from d_sfa..t_sodtl;
delete from d_sfa..t_domst;
delete from d_sfa..t_dodtl;
delete from d_sfa..t_billmst;
delete from d_sfa..t_billdtl;
delete from d_sfa..t_bankmst;
delete from d_sfa..t_bankdtl;
delete from d_sfa..t_paymentmst;
delete from d_sfa..t_paymentdtl;
delete from d_sfa..t_movebrgmst;
delete from d_sfa..t_movebrgdtl;
delete from d_sfa..t_tagmst;
delete from d_sfa..t_tagdtl;

-- sales cost
insert into d_sfa..t_sales_cost 
select * from [mysql]...[db_sfa.t_sales_cost] where format(fd_date,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd');
-- so
insert into d_sfa..t_somst 
select * from [mysql]...[db_sfa.t_somst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd');

insert into d_sfa..t_sodtl 
select * from [mysql]...[db_sfa.t_sodtl] 
where fc_sono in ( select fc_sono from [mysql]...[db_sfa.t_somst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd') );
-- do
insert into d_sfa..t_domst 
select * from [mysql]...[db_sfa.t_domst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd');

insert into d_sfa..t_dodtl 
select * from [mysql]...[db_sfa.t_dodtl] 
where fc_dono in ( select fc_dono from [mysql]...[db_sfa.t_domst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd') );
-- bill
insert into d_sfa..t_billmst 
select * from [mysql]...[db_sfa.t_billmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd');

insert into d_sfa..t_billdtl 
select * from [mysql]...[db_sfa.t_billdtl]
where fc_docno in ( select fc_docno from [mysql]...[db_sfa.t_billmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd') );  
-- bank
insert into d_sfa..t_bankmst 
select * from [mysql]...[db_sfa.t_bankmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd');
insert into d_sfa..t_bankdtl 
select * from [mysql]...[db_sfa.t_bankdtl] 
where fc_docno in ( select fc_docno from [mysql]...[db_sfa.t_bankmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd') );
--payment
insert into d_sfa..t_paymentmst 
select * from [mysql]...[db_sfa.t_paymentmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd');

insert into d_sfa..t_paymentdtl 
select * from [mysql]...[db_sfa.t_paymentdtl] 
where fc_docno in ( select fc_docno from [mysql]...[db_sfa.t_paymentmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd') );
-- movebrg
insert into d_sfa..t_movebrgmst 
select * from [mysql]...[db_sfa.t_movebrgmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd');

insert into d_sfa..t_movebrgdtl 
select * from [mysql]...[db_sfa.t_movebrgdtl] 
where fc_docno in  (select fc_docno from [mysql]...[db_sfa.t_movebrgmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd'));
-- tagmst
insert into d_sfa..t_tagmst 
select * from [mysql]...[db_sfa.t_tagmst] 
where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd') and fc_loktujuan = '101' AND fc_ket LIKE '%SFA TAG TURUN%';

insert into d_sfa..t_tagdtl 
select * from [mysql]...[db_sfa.t_tagdtl] 
where fc_tagno in ( select fc_tagno from [mysql]...[db_sfa.t_tagmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-10' as date) ,'yyyyMMdd') and fc_loktujuan = '101' AND fc_ket LIKE '%SFA TAG TURUN%' )

SET XACT_ABORT ON;

BEGIN TRY
	use d_transaksi;
	BEGIN tran transaksi
		-- OPT COST NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBankOPTCOSTNO', 'U') IS NOT NULL
		DROP TABLE #tempBankOPTCOSTNO; 
		CREATE TABLE #tempBankOPTCOSTNO ( cBankNo varchar(50) );
		-- SO NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempSONO', 'U') IS NOT NULL
		DROP TABLE #tempSONO; 
		CREATE TABLE #tempSONO ( cDocno varchar(50) );
		-- DO NO TO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempDONO', 'U') IS NOT NULL
		DROP TABLE #tempDONO; 
		CREATE TABLE #tempDONO ( cDocno varchar(50) );
		-- DO NO CAN TEMP
		IF OBJECT_ID('tempdb.dbo.#tempDONOCAN', 'U') IS NOT NULL
		DROP TABLE #tempDONOCAN; 
		CREATE TABLE #tempDONOCAN ( cDOno varchar(50) );
		-- GALON DO NO
		IF OBJECT_ID('tempdb.dbo.#tempGLNDO', 'U') IS NOT NULL
		DROP TABLE #tempGLNDO; 
		CREATE TABLE #tempGLNDO ( cDOno varchar(50) );
		-- BILL GALON NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBILLGALON', 'U') IS NOT NULL
		DROP TABLE #tempBILLGALON; 
		CREATE TABLE #tempBILLGALON ( cDocNo varchar(50) );
		-- BANK BI NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBANKBINO', 'U') IS NOT NULL
		DROP TABLE #tempBANKBINO; 
		CREATE TABLE #tempBANKBINO ( cDocno varchar(50) );
		-- BANK AR NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBANKARNO', 'U') IS NOT NULL
		DROP TABLE #tempBANKARNO; 
		CREATE TABLE #tempBANKARNO ( cDocno varchar(50) );
		-- PAYMENT NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempPAYMENTNO', 'U') IS NOT NULL
		DROP TABLE #tempPAYMENTNO; 
		CREATE TABLE #tempPAYMENTNO ( cDocno varchar(50) );
		-- TAG TURUN
		IF OBJECT_ID('tempdb.dbo.#tempTAGNO', 'U') IS NOT NULL
		DROP TABLE #tempTAGNO; 
		CREATE TABLE #tempTAGNO ( cDocno varchar(50) );


	DECLARE 
		@year varchar(10),@month varchar(10),@cBranch VARCHAR(10), 
		@cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30),
		-- opt cost
		@cArmada VARCHAR(20), @vSalescode VARCHAR(20),@cGL varchar(10),
		-- tipe untuk pembeda di do canvaser dan to
		@cTipe VARCHAR(30)
		;

		-- insert optCost
		print 'INSERT OPT COST'
		DECLARE cursorvar_optcost CURSOR LOCAL STATIC
			FOR select fc_branch,fc_userid,fc_armadacode,fc_GL ,year(fd_date),format(fd_date,'MM')
			from d_sfa..t_sales_cost (NOLOCK)
			group by fc_branch,fc_userid,fc_armadacode,fc_GL,year(fd_date),format(fd_date,'MM') ;
		OPEN cursorvar_optcost;

		FETCH NEXT FROM cursorvar_optcost INTO 
			@cBranch , @vSalescode,@cArmada,@cGL,@year,@month ;

		WHILE @@FETCH_STATUS = 0
		BEGIN

			insert into #tempBankOPTCOSTNO exec pr_nomor @cBranch,'BANKA',@year,@month;
			select @cDocNoTis=cBankNo from #tempBankOPTCOSTNO;

			print @cBranch+' '+@vSalescode+' '+@cArmada+' '+@cGL+' '+@year+' '+@month;
			print 'proses OPT COST SALES :  NO TIS : '+@cDocnoTis; 	
			update d_sfa..t_sales_cost set fc_costno = @cDocNoTis 
			where fc_branch = @cBranch and fc_userid = @vSalescode and fc_armadacode = @cArmada and fc_Gl = @cGL

			insert into t_bankmst
			SELECT '0002',fc_branch,year(fd_date),format(fd_date,'MM'),fc_costno,'GO','',cast(fd_date as datetime),cast(fd_date as datetime),getdate()
			,'MIDDLETIER' ,'IDR','1.00','111201','C',sum(fm_value),sum(fm_value),'','R','1','','','','','',0,0,0,0,0
			from d_sfa..t_sales_cost(NOLOCK)
			where fc_costno = @cDocNoTis
			group by fc_branch,fc_userid,fc_armadacode,fc_costno,fc_gl,year(fd_date),format(fd_date,'MM'),cast(fd_date as datetime);

			insert into t_bankdtl
			select 
			'0002',a.fc_branch,YEAR(a.fd_date),format(a.fd_date,'MM'),cast(a.fc_costno as varchar(12)), 
			RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_userid,a.fc_armadacode,fc_gl ORDER BY a.fd_date) ),2 ) cNo  ,'',
			d.cBusTran,'','','',d.cTipe,'GO','','','111201',a.fc_gl,'D',cast('1900-01-01 00:00:00.000' as datetime),
			substring(a.fc_cost_description,1,50),e.cIOCode,e.cCostctr,'IDR',a.fm_value,a.fm_value,'0','0','R','0','0','0',
			'','','','','','','0','','' 
			from  d_sfa..t_sales_cost (NOLOCK) a
			left join t_glacc1 b on a.fc_GL = b.cGL
			left join t_cashjrngl c on b.cGL=c.cGL 
			LEFT join t_cashjrn d on c.cCJCode=d.cCJCode and c.cNo=d.cNo 
			left join t_io e on a.fc_armadacode= e.cDesc and e.cbranch = a.fc_branch
			where a.fc_costno = @cDocNoTis
			order by a.fc_costno;
			EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
			
			update t_nomor set nDocno = nDocno + 1 where cDocument = 'BANKA' and cPeriode =@month and cyear=@year and cBranch = @cBranch;

			FETCH NEXT FROM cursorvar_optcost INTO 
				@cBranch , @vSalescode ,@cArmada,@cGL,@year,@month;

		END;

		CLOSE cursorvar_optcost;

		DEALLOCATE cursorvar_optcost;

		-- insert SO
		print 'INSERT SO';
		DECLARE cursor_so CURSOR LOCAL STATIC 
		FOR select fc_branch,fc_sono,fc_year,fc_periode from d_sfa..t_somst (NOLOCK);
		OPEN cursor_so;
				
		FETCH NEXT FROM cursor_so INTO @cBranch , @cDocNo,@year,@month ;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			insert into #tempSONO EXEC pr_nomor @cBranch,'SOA',@year,@month;
			select @cDocNoTis  = cDocno from #tempSONO;	
			print 'proses SO : '+@cDocno+' NO TIS : '+@cDocnoTis;
			update d_sfa..t_somst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;
			update d_sfa..t_sodtl set fc_sono = @cDocnoTis  where  fc_branch = @cBranch and fc_sono = @cDocNo;
			update d_sfa..t_domst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;

			insert into t_somst select * from d_sfa..t_somst(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;  	
			insert into t_sodtl 
			select * from d_sfa..t_sodtl(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;

			update t_nomor set nDocno = ndocno + 1 where cDocument = 'SOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
			insert into #tempSONO EXEC pr_nomor @cBranch,'SOA',@year,@month;
			select @cDocNoTis  = cDocno from #tempSONO;

			FETCH NEXT FROM cursor_so INTO @cBranch , @cDocno,@year,@month ;

		END;

		CLOSE cursor_so;
		DEALLOCATE cursor_so;

		-- insert do taking order
		print 'INSERT DO TO';
		DECLARE cursor_do CURSOR LOCAL STATIC
		FOR select fc_branch,fc_dono,fc_year,fc_periode,left(fc_dono,2) from d_sfa..t_domst(NOLOCK)  where fc_tipe != '631' ;
		OPEN cursor_do;
			FETCH NEXT FROM cursor_do INTO 
				@cBranch , @cDocNo,@year,@month,@cTipe ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				IF (@cTipe = 'TO')
				BEGIN
					insert into #tempDONO EXEC pr_nomor @cBranch,'DOA',@year,@month;
					select @cDocNoTis  = cDocno from #tempDONO;
					print @cBranch+' '+@cDocno+' '+@year+' '+@month;
					print 'proses DO-TO : '+@cDocno+' NO TIS : '+@cDocnoTis;

					update d_sfa..t_domst set fc_dono = @cDocnoTis where  fc_branch = @cBranch and fc_dono = @cDocNo ;
					update d_sfa..t_dodtl set fc_dono = @cDocnoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;

					insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;  	
					insert into t_dodtl 
					select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

					update t_domst set cStatus = 'R' where cdono = @cDocNoTis and cBranch = @cBranch;
					update t_domst set cStatus = 'I' where cdono = @cDocNoTis and cBranch = @cBranch;
		
					update t_nomor set nDocno = ndocno + 1 where cDocument = 'DOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				END
				ELSE
				BEGIN
					insert into #tempDONOCAN EXEC pr_nomor @cBranch,'BILLA',@year,@month;
					select @cDocNoTis  = cDOno from #tempDONOCAN;

					print @cbranch+' '+@cDocno+' '+@year+' '+@month;
					print 'proses DO & BILL CAN : '+@cDocno+' NO TIS : '+@cDocnoTis+' TIPE : '+@cTipe;
					
					update d_sfa..t_domst set fc_dono = @cDocNoTis, fc_sono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo;
					update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
					
					insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;  	
					insert into t_dodtl 
					select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

					update d_sfa..t_domst set fc_sono = @cDocNoTis where  fc_branch = @cBranch and fc_sono = @cDocNo;
						
					update t_domst set cStatus = 'R' where cdono = @cDocNoTis;
					update t_domst set cStatus = 'B' where cdono = @cDocNoTis;
					
					update d_sfa..t_billmst set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
					update d_sfa..t_billdtl set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
					
					insert into t_billmst select * from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
					insert into t_billdtl 
					select * from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
					
					--update t_billmst set cStatus = 'R' where cdocno = @cDocNoTis;

					EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis;	
		
					update d_sfa..t_billmst set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;
					update d_sfa..t_billdtl set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;

					update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;	

					update d_sfa..t_movebrgmst set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;	
					update d_sfa..t_movebrgdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDoCno;	

					update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILLA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				END

				FETCH NEXT FROM cursor_do INTO 
					@cBranch , @cDocno,@year,@month,@cTipe ;
			END;

		CLOSE cursor_do;
		DEALLOCATE cursor_do;

		--insert DO Gslon
		print 'INSERT DO Galon';
		DECLARE cursor_dogalon CURSOR LOCAL STATIC
		FOR select fc_branch,fc_dono,fc_year,fc_periode from d_sfa..t_domst(NOLOCK) where fc_tipe = '631' ;
		OPEN cursor_dogalon;

			FETCH NEXT FROM cursor_dogalon INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempGLNDO EXEC pr_nomor @cBranch,'RTNGLNA',@year,@month;
				select @cDocNoTis  = cDOno from #tempGLNDO;

				print @cbranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses DO Galon : '+@cDocno+' NO TIS : '+@cDocnoTis;
				
				
				update d_sfa..t_domst set fc_dono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo;
			
				update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
				
				insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;  	
				insert into t_dodtl 
				select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

				update t_domst set cStatus = 'R' where cdono = @cDocNoTis;

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'RTNGLNA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				
				FETCH NEXT FROM cursor_dogalon INTO 
					@cBranch , @cDocNo,@year,@month ;

			END;
		CLOSE cursor_dogalon;
		DEALLOCATE cursor_dogalon;

		--insert BILL GALON & MOVE BRG CANVASER
		print 'INSERT BILL GALON DAN MOVEBRG';
		DECLARE cursor_billgalon CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_billmst(NOLOCK) where fc_tipe in ('DG','DI');

		OPEN cursor_billgalon;

			FETCH NEXT FROM cursor_billgalon INTO 
				@cBranch , @cDocNo ,@year,@month;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempBILLGALON EXEC pr_nomor @cBranch,'BILGA',@year,@month;
				select @cDocNoTis  = cDocNo from #tempBILLGALON;
				print @cBranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses BILL GALON + MOVEBRG : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_billmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_billdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;

				insert into t_billmst select * from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
				insert into t_billdtl 
				select * from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;

				--update t_billmst set cStatus = 'R' where cdocno = @cDocNoTis;

				EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis;

				update d_sfa..t_movebrgmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_movebrgdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;

				insert into t_movebrgmst select * from d_sfa..t_movebrgmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
				insert into t_movebrgdtl 
				select * from d_sfa..t_movebrgdtl(NOLOCK) 
				where fc_branch = @cBranch and fc_docno = @cDocNoTis;	

				update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo ;	

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILGA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				insert into #tempBILLGALON EXEC pr_nomor @cBranch,'BILGA',@year,@month;
				select @cDocNoTis  = cDocNo from #tempBILLGALON;

				FETCH NEXT FROM cursor_billgalon INTO 
					@cBranch , @cDocNo ,@year,@month;

			END;

		CLOSE cursor_billgalon;
		DEALLOCATE cursor_billgalon;

		-- BANK TIPE BI
		print 'INSERT BANK BI';
		DECLARE cursor_bankbi CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) where fc_tipe = 'BI' ;

		OPEN cursor_bankbi;
			FETCH NEXT FROM cursor_bankbi INTO 
				@cBranch,@cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKBINO EXEC pr_nomor @cBranch,'BANKA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKBINO;

				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses BANK-BI : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_bankmst set fc_docno = @cDocNoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_bankdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;	
				insert into t_bankmst select * from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
				insert into t_bankdtl 
				select * from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;

				EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
				update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;
				
				update d_sfa..t_bankmst set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
				update d_sfa..t_bankdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_bankbi INTO 
					@cBranch , @cDocNo,@year,@month;

			END;
		CLOSE cursor_bankbi;
		DEALLOCATE cursor_bankbi;

		-- BANK TIPE AR
		print 'INSERT BANK AR';
		DECLARE cursor_bankar CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) where fc_tipe = 'AR';

		OPEN cursor_bankar;
			FETCH NEXT FROM cursor_bankar INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKARNO EXEC pr_nomor @cBranch,'ARDEPA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKARNO;
				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_bankmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_bankdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
				print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
				insert into t_bankmst select * from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;  	
				insert into t_bankdtl 
				select * from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;
				
				EXEC pr_glhis 'ARDEP','0002',@cBranch,@year,@month,@cDocNoTis;
				
				update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	
				
				update d_sfa..t_paymentdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'ARDEPA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_bankar INTO 
					@cBranch , @cDocno,@year,@month ;
					
			END;

		CLOSE cursor_bankar;
		DEALLOCATE cursor_bankar;
		-- PAYMENT
		print 'INSERT PAYMENT';
		DECLARE cursor_payment CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_paymentmst(NOLOCK);

		OPEN cursor_payment;
			FETCH NEXT FROM cursor_payment INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempPAYMENTNO EXEC pr_nomor @cBranch,'RECEIVEA',@year,@month;
				select @cDocNoTis  = cDocno from #tempPAYMENTNO;
				print @cBranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses PAYMENT : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_paymentmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_paymentdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;

				insert into t_paymentmst select * from d_sfa..t_paymentmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;  	
				insert into t_paymentdtl 
				select * from d_sfa..t_paymentdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;

				update t_paymentmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_payment INTO 
					@cBranch , @cDocno,@year,@month ;

			END;

		CLOSE cursor_payment;
		DEALLOCATE cursor_payment;
		-- TAG TURUN
		print 'TAG TURUN';
		DECLARE cursorvar_tagturun CURSOR LOCAL STATIC
		FOR select fc_branch,fc_tagno,fc_year,fc_periode from d_sfa..t_tagmst(NOLOCK) where fc_lokTujuan = '101';

		OPEN cursorvar_tagturun;
			FETCH NEXT FROM cursorvar_tagturun INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
								
				insert into #tempTAGNO EXEC pr_nomor @cBranch,'TAG',@year,@month;
				select @cDocNoTis  = cDocno from #tempTAGNO;

				update d_sfa..t_tagmst set fc_tagno = @cDocNoTis where  fc_branch = @cBranch and fc_tagno = @cDocNo ;
				update d_sfa..t_tagdtl set fc_tagno = @cDocnoTis  where  fc_branch = @cBranch and fc_tagno = @cDocNo;

				insert into t_tagdtl1 
				select 
				a.fc_branch,a.fc_Tagno,b.fc_lokAsal,a.fc_Brgcode,a.fc_Batch,
				RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) ),2 ) cNo
				,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
				from d_sfa..t_tagdtl a
				INNER JOIN d_sfa..t_tagmst b ON a.fc_branch = b.fc_branch AND a.fc_tagno = b.fc_tagno
				where a.fc_Tagno = @cDocNoTis and a.fc_branch = @cBranch;

				insert into t_tagdtl2
				select 
				a.fc_branch,a.fc_Tagno,a.fc_Gdcode,a.fc_Brgcode,a.fc_Batch,
				RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) + b.add_no )  ,2 ) cNo
				,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
				from d_sfa..t_tagdtl a
				left join ( select fc_branch,fc_tagno,count(fc_tagno) add_no from d_sfa..t_tagdtl group by fc_branch,fc_tagno ) b
				on a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_tagno
				where a.fc_Tagno = @cDocNoTis and a.fc_branch = @cBranch;

				insert into t_tagmst 
				select 
				fc_branch,fc_tagno,fc_tipe,fc_year,fc_periode,fc_costctr,
				fc_docref,fd_docdate,fd_postdate,fd_inputdate,'MDTIER' cINputby,
				fd_editdate,'MDTIER' cEditBy,fc_branchasal,fc_lokasal,fc_branchtujuan,
				fc_loktujuan,fc_ket,'R' cStatus,NULL ordertype
				from d_sfa..t_tagmst 
				where fc_tagno = @cDocnoTis and fc_branch = @cBranch;
	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'TAG' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				
				FETCH NEXT FROM cursorvar_tagturun INTO 
					@cBranch , @cDocno,@year,@month ;

			END;

		CLOSE cursorvar_tagturun;
		DEALLOCATE cursorvar_tagturun;


	COMMIT tran transaksi;
END TRY
BEGIN CATCH
	ROLLBACK tran transaksi;
	print 'QUERY ERROR, ROLLBACK TRANSACTION';
	THROW;
END CATCH