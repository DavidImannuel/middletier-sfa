use d_transaksi

IF OBJECT_ID('tempdb.dbo.#tempBankNO', 'U') IS NOT NULL
DROP TABLE #tempBankNO; 
CREATE TABLE #tempBankNO ( cBankNo varchar(50) )

DECLARE 
	@cBankNo varchar(20),@cArmada VARCHAR(20),@cBranch VARCHAR(20), @vSalescode VARCHAR(20),@cGL varchar(10),@year varchar(4),@month varchar(2);


select @year = FORMAT( cast( getdate() as date) ,'yyyy');
select @month = FORMAT( cast( getdate() as date),'MM');
insert into #tempBankNO exec pr_nomor '2001','BANKA',@year,@month;
select @cBankNo=cBankNo from #tempBankNO;

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_userid,fc_armadacode,fc_GL 
from [MYSQL]...[db_sfa.t_sales_cost] 
where format(cast(fd_date as date),'yyyyMMdd') = format(cast( getdate() as date),'yyyyMMdd') 
group by fc_branch,fc_userid,fc_armadacode,fc_GL ;

OPEN cursorvar;

FETCH NEXT FROM cursorvar INTO 
    @cBranch , @vSalescode,@cArmada,@cGL ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	BEGIN TRY

		update [mysql]...[db_sfa.t_sales_cost] set fc_costno = @cBankNo 
		where fc_branch = @cBranch and fc_userid = @vSalescode and fc_armadacode = @cArmada and fc_Gl = @cGL

		insert into t_bankmst
		SELECT '0002',fc_branch,year(getdate()),format(getdate(),'MM'),fc_costno,'GO','',cast(getdate() as date),cast(getdate() as date),getdate()
		,'MIDDLETIER' ,'IDR','1.00','111201','C',sum(fm_value),sum(fm_value),'','R','1','','','','','',0,0,0,0,0
		from [MYSQL]...[db_sfa.t_sales_cost] 
		where format(cast(fd_date as date),'yyyyMMdd') = format(cast( getdate() as date),'yyyyMMdd')
		and fc_costno = @cBankNo
		group by fc_branch,fc_userid,fc_armadacode,fc_costno;

		IF EXISTS ( select * from t_bankmst where cDocno = @cBankNo )
		begin
			insert into t_bankdtl
			select 
			'0002',a.fc_branch,YEAR(a.fd_date),format(a.fd_date,'MM'),cast(a.fc_costno as varchar(12)), 
			RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_userid,a.fc_armadacode,fc_gl ORDER BY a.fd_date) ),2 ) cNo  ,'',
			d.cBusTran,'','','',d.cTipe,'GO','','','111201',a.fc_gl,'D',cast('1900-01-01 00:00:00.000' as datetime),
			substring(a.fc_cost_description,1,50),e.cIOCode,e.cCostctr,'IDR',a.fm_value,a.fm_value,'0','0','R','0','0','0',
			'','','','','','','0','','' 
			from  [mysql]...[db_sfa.t_sales_cost] a
			left join t_glacc1 b on a.fc_GL = b.cGL
			left join t_cashjrngl c on b.cGL=c.cGL 
			LEFT join t_cashjrn d on c.cCJCode=d.cCJCode and c.cNo=d.cNo 
			left join t_io e on a.fc_armadacode= e.cDesc and e.cbranch = a.fc_branch
			where format(cast(a.fd_date as date),'yyyyMMdd') = format(cast( getdate() as date),'yyyyMMdd')
			and fc_costno = @cBankNo
			order by a.fc_costno;
			EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cBankNo;

			delete from [MYSQL]...[db_sfa.t_sales_cost] where format(cast( fd_date as date ),'yyyyMMdd') = format( getdate() ,'yyyyMMdd') and fc_costno = @cBankNo ;
		end

		update t_nomor set nDocno = nDocno + 1 where cDocument = 'BANKA' and cPeriode =@month and cyear=@year and cBranch = @cBranch;

	END TRY
	BEGIN CATCH

	  SELECT
	    ERROR_NUMBER() AS ErrorNumber,
	    ERROR_STATE() AS ErrorState,
	    ERROR_SEVERITY() AS ErrorSeverity,
	    ERROR_PROCEDURE() AS ErrorProcedure,
	    ERROR_LINE() AS ErrorLine,
	    ERROR_MESSAGE() AS ErrorMessage;

	END CATCH;


	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @vSalescode ,@cArmada,@cGL;
		insert into #tempBankNO exec pr_nomor '2001','BANKA',@year,@month;
		select @cBankNo=cBankNo from #tempBankNO;

END;

CLOSE cursorvar;

DEALLOCATE cursorvar;


