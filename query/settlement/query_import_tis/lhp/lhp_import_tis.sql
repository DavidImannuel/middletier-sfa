BEGIN TRY
	
	use d_transaksi;
	DECLARE 
		@cBranch VARCHAR(10), @tghRkpNo VARCHAR(30),@tghNo VARCHAR(30);
	DECLARE cursorvar CURSOR
	FOR select a.cBranch,a.cDocno,b.cDocref from t_tagihanrkpmst where format(dDocdate,'yyyyMMdd') = format(cast('2020-10-10' as date),'yyyyMMdd');

	OPEN cursorvar;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @tghRkpNo ,@tghNo ;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		IF EXISTS( select * from [mysql]...[db_sfa.t_lhpmst] where fc_docno = concat(@tghRkpNo,'/',@tghNo))
		BEGIN
			insert into [mysql]...[db_sfa.t_lhpmst]
			select 
			a.cBranch,concat(a.cDocno,'/',c.cDocno) cDocno,c.ccustcode,c.cYear,c.cPeriode,c.dDocdate,
			c.dPOstdate,c.dinputdate,a.cCollector,c.msisacn,'0',
			'F','',c.cBank,c.cNorek
			from t_tagihanrkpmst a
			left join t_tagihanrkpdtl b on a.cDocno = b.cDocno
			left join t_tagihanmst c on c.cDocno = b.cDocref
			where a.cDocno = @tghRkpNo and b.cdocref = @tghNo;

			insert into [mysql]...[db_sfa.t_lhpdtl] 
			select 
			a.cBranch,concat(a.cDocno,'/',c.cDocno) cDOcno,d.cDocref,d.cTipe,d.dDocdate,d.dPostdate,
			d.dInputdate,d.dDuedate,d.mSisaCN,'0','F'
			from t_tagihanrkpmst a
			left join t_tagihanrkpdtl b on a.cDocno = b.cDocno
			left join t_tagihanmst c on c.cDocno = b.cDocref
			left join t_tagihandtl d on c.cDocno = d.cDocno
			where a.cDocno = @tghRkpNo and b.cdocref = @tghNo;
		END

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @tghRkpNo ,@tghNo;

	END;

	CLOSE cursorvar;

	DEALLOCATE cursorvar;

END TRY
BEGIN CATCH
		
  THROW;
END CATCH;