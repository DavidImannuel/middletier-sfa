-- 3
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempBANKNO', 'U') IS NOT NULL
DROP TABLE #tempBANKNO; 
CREATE TABLE #tempBANKNO ( cDocno varchar(50) );

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30);

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_docno,fc_year,fc_periode from [MYSQL]...[db_sfa.t_bankmst] 
where fc_tipe = 'BI' and format(fd_inputdate,'yyyyMMdd') =  format(getdate(),'yyyyMMdd')  ;

OPEN cursorvar;
FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDocNo,@year,@month ;

WHILE @@FETCH_STATUS = 0
BEGIN
	insert into #tempBANKNO EXEC pr_nomor '2001','BANKA',@year,@month;
	select @cDocNoTis  = cDocno from #tempBANKNO;

	update [MYSQL]...[db_sfa.t_bankmst] set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
	update [MYSQL]...[db_sfa.t_bankdtl] set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;	
	insert into t_bankmst select * from [MYSQL]...[db_sfa.t_bankmst] where fc_branch = @cBranch and fc_docno = @cDocnoTis;  	
	insert into t_bankdtl select * from [MYSQL]...[db_sfa.t_bankdtl] where fc_branch = @cBranch and fc_docno = @cDocnoTis;
	delete from [MYSQL]...[db_sfa.t_bankmst] where fc_branch = @cBranch and fc_docno = @cDocnoTis;  	
	delete from [MYSQL]...[db_sfa.t_bankdtl] where fc_branch = @cBranch and fc_docno = @cDocnoTis;
		
	update [MYSQL]...[db_sfa.t_bankmst] set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
	update [MYSQL]...[db_sfa.t_bankdtl] set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
	
	update t_nomor set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempBANKNO EXEC pr_nomor '2001','BANKA',@year,@month;
	select @cDocNoTis  = cDocno from #tempBANKNO;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDocNo,@year,@month;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;