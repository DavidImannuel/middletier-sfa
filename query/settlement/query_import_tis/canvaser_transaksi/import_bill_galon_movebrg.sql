-- 2
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempDONO_CAN', 'U') IS NOT NULL
DROP TABLE #tempDONO_CAN; 
CREATE TABLE #tempDONO_CAN ( cDocNo varchar(50) );

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cNoSFA VARCHAR(30),@cNoTis VARCHAR(30);

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_docno,fc_year,fc_periode from [MYSQL]...[db_sfa.t_billmst] 
	where fc_tipe in ('DG','DI') and format(fd_inputdate,'yyyyMMdd') =  format(getdate(),'yyyyMMdd');

OPEN cursorvar;

FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cNoSFA ,@year,@month;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	insert into #tempDONO_CAN EXEC pr_nomor '2001','BILGA',@year,@month;
	select @cNoTis  = cDocNo from #tempDONO_CAN;

	update [MYSQL]...[db_sfa.t_billmst] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;
	update [MYSQL]...[db_sfa.t_billdtl] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;

	insert into t_billmst select * from [MYSQL]...[db_sfa.t_billmst] where fc_branch = @cBranch and fc_docno = @cNoTis;  	
	insert into t_billdtl select * from [MYSQL]...[db_sfa.t_billdtl] where fc_branch = @cBranch and fc_docno = @cNoTis;
	EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cNoTis	
	delete from [MYSQL]...[db_sfa.t_billmst] where fc_branch = @cBranch and fc_docno = @cNoTis;  	
	delete from [MYSQL]...[db_sfa.t_billdtl] where fc_branch = @cBranch and fc_docno = @cNoTis;

	update [MYSQL]...[db_sfa.t_movebrgmst] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;
	update [MYSQL]...[db_sfa.t_movebrgdtl] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;
	insert into t_movebrgmst select * from [MYSQL]...[db_sfa.t_movebrgmst] where fc_branch = @cBranch and fc_docno = @cNoTis;  	
	insert into t_movebrgdtl select * from [MYSQL]...[db_sfa.t_movebrgdtl] where fc_branch = @cBranch and fc_docno = @cNoTis;	
	delete from [MYSQL]...[db_sfa.t_movebrgmst] where fc_branch = @cBranch and fc_docno = @cNoTis;  	
	delete from [MYSQL]...[db_sfa.t_movebrgdtl] where fc_branch = @cBranch and fc_docno = @cNoTis;


	update [MYSQL]...[db_sfa.t_paymentdtl] set fc_docref = @cNoTis  where  fc_branch = @cBranch and fc_docref = @cNoSFA ;	
	

	update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILGA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempDONO_CAN EXEC pr_nomor '2001','BILGA',@year,@month;
	select @cNoTis  = cDocNo from #tempDONO_CAN;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cNoSFA ,@year,@month;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;

