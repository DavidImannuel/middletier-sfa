-- 5
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempPAYMENTNO', 'U') IS NOT NULL
DROP TABLE #tempPAYMENTNO; 
CREATE TABLE #tempPAYMENTNO ( cDocno varchar(50) );

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30);


DECLARE cursorvar CURSOR
FOR select fc_branch,fc_docno,fc_year,fc_periode from [MYSQL]...[db_sfa.t_paymentmst] 
where format(fd_inputdate,'yyyyMMdd') =  format(getdate(),'yyyyMMdd');

OPEN cursorvar;
FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDocNo,@year,@month ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	insert into #tempPAYMENTNO EXEC pr_nomor '2001','RECEIVEA',@year,@month;
	select @cDocNoTis  = cDocno from #tempPAYMENTNO;

	update [MYSQL]...[db_sfa.t_paymentmst] set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
	update [MYSQL]...[db_sfa.t_paymentdtl] set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;

	insert into t_paymentmst select * from [MYSQL]...[db_sfa.t_paymentmst] where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
	insert into t_paymentdtl select * from [MYSQL]...[db_sfa.t_paymentdtl] where fc_branch = @cBranch and fc_docno = @cDocNoTis;
	update t_paymentmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	
	delete from [MYSQL]...[db_sfa.t_paymentmst] where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
	delete from [MYSQL]...[db_sfa.t_paymentdtl] where fc_branch = @cBranch and fc_docno = @cDocNoTis;
	
	update t_nomor set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempPAYMENTNO EXEC pr_nomor '2001','RECEIVEA',@year,@month;
	select @cDocNoTis  = cDocno from #tempPAYMENTNO;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDocno,@year,@month ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;
