--1
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempDONO_CAN', 'U') IS NOT NULL
DROP TABLE #tempDONO_CAN; 
CREATE TABLE #tempDONO_CAN ( cDOno varchar(50) );
DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDOno VARCHAR(30),@cDOnoTis VARCHAR(30);


DECLARE cursorvar CURSOR
FOR select fc_branch,fc_dono,fc_year,fc_periode from [MYSQL]...[db_sfa.t_domst] where left(fc_dono,2) != 'TO' 
	and format(fd_inputdate,'yyyyMMdd') =  format(getdate(),'yyyyMMdd') ;

OPEN cursorvar;

FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDOno,@year,@month ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	insert into #tempDONO_CAN EXEC pr_nomor '2001','BILLA',@year,@month;
	select @cDOnoTis  = cDOno from #tempDONO_CAN;

	update [MYSQL]...[db_sfa.t_domst] set fc_dono = @cDOnoTis,fc_sono = @cDOnoTis where  fc_branch = @cBranch and fc_dono = @cDOno;
	update [MYSQL]...[db_sfa.t_dodtl] set fc_dono = @cDOnoTis  where  fc_branch = @cBranch and fc_dono = @cDOno;
	insert into t_domst select * from [MYSQL]...[db_sfa.t_domst] where fc_branch = @cBranch and fc_dono = @cDOnoTis;  	
	insert into t_dodtl select * from [MYSQL]...[db_sfa.t_dodtl] where fc_branch = @cBranch and fc_dono = @cDOnoTis;
	update t_domst set cStatus = 'R' where cdono = @cDOnoTis;
	update t_domst set cStatus = 'B' where cdono = @cDOnoTis;
	delete from [MYSQL]...[db_sfa.t_domst] where fc_branch = @cBranch and fc_dono = @cDOnoTis;  	
	delete from [MYSQL]...[db_sfa.t_dodtl] where fc_branch = @cBranch and fc_dono = @cDOnoTis;

	update [MYSQL]...[db_sfa.t_billmst] set fc_docno = @cDOnoTis,fc_docRef = @cDOnoTis  where  fc_branch = @cBranch and fc_docno = @cDOno;
	update [MYSQL]...[db_sfa.t_billdtl] set fc_docno = @cDOnoTis,fc_docRef = @cDOnoTis  where  fc_branch = @cBranch and fc_docno = @cDOno;
	insert into t_billmst select * from [MYSQL]...[db_sfa.t_billmst] where fc_branch = @cBranch and fc_docno = @cDOnoTis;  	
	insert into t_billdtl select * from [MYSQL]...[db_sfa.t_billdtl] where fc_branch = @cBranch and fc_docno = @cDOnoTis;
	EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cDOnoTis	
	delete from [MYSQL]...[db_sfa.t_billmst] where fc_branch = @cBranch and fc_docno = @cDOnoTis;  	
	delete from [MYSQL]...[db_sfa.t_billdtl] where fc_branch = @cBranch and fc_docno = @cDOnoTis;

	
	update [MYSQL]...[db_sfa.t_billmst] set fc_docRef = @cDOnoTis  where  fc_branch = @cBranch and fc_docRef = @cDOno ;
	update [MYSQL]...[db_sfa.t_billdtl] set fc_docRef = @cDOnoTis  where  fc_branch = @cBranch and fc_docRef = @cDOno ;

	update [MYSQL]...[db_sfa.t_paymentdtl] set fc_docref = @cDOnoTis  where  fc_branch = @cBranch and fc_docref = @cDOno;	

	update [MYSQL]...[db_sfa.t_movebrgmst] set fc_docref = @cDOnoTis  where  fc_branch = @cBranch and fc_docref = @cDOno;	
	update [MYSQL]...[db_sfa.t_movebrgdtl] set fc_docref = @cDOnoTis  where  fc_branch = @cBranch and fc_docref = @cDOno;	

	update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILLA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempDONO_CAN EXEC pr_nomor '2001','BILLA',@year,@month;
	select @cDOnoTis  = cDOno from #tempDONO_CAN;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDOno,@year,@month  ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;
