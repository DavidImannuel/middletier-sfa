-- 2
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempDONO', 'U') IS NOT NULL
DROP TABLE #tempDONO; 
CREATE TABLE #tempDONO ( cDocno varchar(50) );

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30);

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');

insert into #tempDONO EXEC pr_nomor '2001','DOA',@year,@month;
select @cDocNoTis  = cDocno from #tempDONO;

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_dono from [MYSQL]...[db_sfa.t_domst] 
where left(fc_dono,2) = 'TO' and format(fd_inputdate,'yyyyMMdd') =  format(getdate(),'yyyyMMdd') ;

OPEN cursorvar;
FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDocNo ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	update [MYSQL]...[db_sfa.t_domst] set fc_dono = @cDocnoTis where  fc_branch = @cBranch and fc_dono = @cDocNo ;
	update [MYSQL]...[db_sfa.t_dodtl] set fc_dono = @cDocnoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;

	insert into t_domst select * from [MYSQL]...[db_sfa.t_domst] where fc_branch = @cBranch and fc_dono = @cDocNoTis;  	
	insert into t_dodtl select * from [MYSQL]...[db_sfa.t_dodtl] where fc_branch = @cBranch and fc_dono = @cDocNoTis;

	-- update t_domst set cStatus = 'R' where cdono = @cDOnoTis;
	-- update t_domst set cStatus = 'P' where cdono = @cDOnoTis;

	
	delete from [MYSQL]...[db_sfa.t_domst] where fc_branch = @cBranch and fc_dono = @cDocNoTis;  	
	delete from [MYSQL]...[db_sfa.t_dodtl] where fc_branch = @cBranch and fc_dono = @cDocNoTis;
	
	update t_nomor set nDocno = ndocno + 1 where cDocument = 'DOA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempDONO EXEC pr_nomor '2001','DOA',@year,@month;
	select @cDocNoTis  = cDocno from #tempDONO;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDocno ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;
