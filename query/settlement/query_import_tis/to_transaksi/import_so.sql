-- 1
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempSONO', 'U') IS NOT NULL
DROP TABLE #tempSONO; 
CREATE TABLE #tempSONO ( cDocno varchar(50) );

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30);

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');

insert into #tempSONO EXEC pr_nomor '2001','SOA',@year,@month;
select @cDocNoTis  = cDocno from #tempSONO;

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_sono from [MYSQL]...[db_sfa.t_somst] where format(fd_inputdate,'yyyyMMdd') =  format(getdate(),'yyyyMMdd');

OPEN cursorvar;
FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDocNo ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	update [MYSQL]...[db_sfa.t_somst] set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;
	update [MYSQL]...[db_sfa.t_sodtl] set fc_sono = @cDocnoTis  where  fc_branch = @cBranch and fc_sono = @cDocNo;
	update [MYSQL]...[db_sfa.t_domst] set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;
	
	insert into t_somst select * from [MYSQL]...[db_sfa.t_somst] where fc_branch = @cBranch and fc_sono = @cDocNoTis;  	
	insert into t_sodtl select * from [MYSQL]...[db_sfa.t_sodtl] where fc_branch = @cBranch and fc_sono = @cDocNoTis;

	delete from [MYSQL]...[db_sfa.t_somst] where fc_branch = @cBranch and fc_sono = @cDocNoTis;  	
	delete from [MYSQL]...[db_sfa.t_sodtl] where fc_branch = @cBranch and fc_sono = @cDocNoTis;

	update t_nomor set nDocno = ndocno + 1 where cDocument = 'SOA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempSONO EXEC pr_nomor '2001','SOA',@year,@month;
	select @cDocNoTis  = cDocno from #tempSONO;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDocno ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;
