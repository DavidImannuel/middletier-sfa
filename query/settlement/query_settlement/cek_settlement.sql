use d_sfa
truncate table t_sales_cost
truncate table t_somst
truncate table t_sodtl
truncate table t_domst
truncate table t_dodtl
truncate table t_billmst
truncate table t_billdtl
truncate table t_movebrgmst
truncate table t_movebrgdtl
truncate table t_bankmst
truncate table t_bankdtl
truncate table t_paymentmst
truncate table t_paymentdtl
truncate table t_tagmst
truncate table t_tagdtl

select * from t_sales_cost
select * from t_somst
select * from t_sodtl
select * from t_domst
select * from t_dodtl
select * from t_billmst
select * from t_billdtl
select * from t_movebrgmst
select * from t_movebrgdtl
select * from t_bankmst
select * from t_bankdtl
select * from t_paymentmst
select * from t_paymentdtl
select * from t_tagmst
select * from t_tagdtl

exec pr_settlementSales '2001','3617','2020-11-12'

select * from d_transaksi..t_bankmst where cDOcno  = 'BN2011-01396' and cbranch = '2001'

select * from d_sfa..t_domst where fc_Branch = '2053' and format(fd_docdate,'yyyyMMdd') = '20201125' 
select fc_dono from d_sfa..t_domst where fc_Branch = '2053' and format(fd_docdate,'yyyyMMdd') = '20201125' group by fc_dono


exec d_sfa.dbo.pr_settlementSales '2053','5321','2020-11-25'
select * from d_transaksi..t_nomor where cBranch = '2053' and cDocument = 'DOA' and cyear = '2020' and cperiode = '11'

select * from d_sfa..t_sales_cost where fc_branch  = '2053' and fc_userid = '5395'

select * from d_transaksi..t_bankmst where cBranch = '2053' 
and format(dDocdate,'yyyyMMdd') = '20201125' and cinputby = '5395'

select * from d_transaksi..t_bankmst where cBranch = '2053' 
and format(dDocdate,'yyyyMMdd') = '20201125' and cdocno in ('BN2011-02570','BN2011-02571')

select * from d_transaksi..t_bankdtl where cBranch = '2053' 
and cdocno in ('BN2011-02570','BN2011-02571')

select 
'0002',a.fc_branch,YEAR(a.fd_date),format(a.fd_date,'MM'),cast(a.fc_TISDocno as varchar(12)), 
RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_userid,a.fc_armadacode,fc_gl ORDER BY a.fd_date) ),2 ) cNo  ,'',
d.cBusTran,'','','',d.cTipe,'GO','','','111201',a.fc_gl,'D',cast('1900-01-01 00:00:00.000' as datetime),
substring(a.fc_cost_description,1,50),e.cIOCode,e.cCostctr,'IDR',a.fm_value,a.fm_value,'0','0','R','0','0','0',
'','','','','','','0','','' 
from  d_sfa..t_sales_cost (NOLOCK) a
left join d_transaksi..t_glacc1 b on a.fc_GL = b.cGL
left join d_transaksi..t_cashjrngl c on b.cGL=c.cGL 
LEFT join d_transaksi..t_cashjrn d on c.cCJCode=d.cCJCode and c.cNo=d.cNo 
left join d_transaksi..t_io e on a.fc_armadacode= e.cDesc and e.cbranch = a.fc_branch and e.cStatus = 1
where a.fc_TISDocno = 'BN2011-02570' 
order by a.fc_TISDocno;

select * from d_transaksi..t_io where cDesc = 'F 8675 HA' and cBranch = '2053'

-- LHP
use d_Sfa
select * from t_bankmst where fc_branch = '2001' and fc_inputby = 'B040' and convert(char(8),fd_docdate,112) = '20210104'
select b.* 
from t_bankmst a 
inner join t_bankdtl b on a.fc_branch = b.fc_branch and a.fc_docno  = b.fc_docno 
where a.fc_branch = '2001' and a.fc_inputby = 'B040' and convert(char(8),a.fd_docdate,112) = '20210107'
select *  from t_paymentmst where fc_branch = '2001' and fd_inputby = 'B040' and convert(char(8),fd_docdate,112) = '20210107'
select b.* 
from t_paymentmst a 
inner join t_paymentdtl b on a.fc_branch = b.fc_branch and a.fc_docno  = b.fc_docno 
where a.fc_branch = '2001' and a.fd_inputby = 'B040' and convert(char(8),a.fd_docdate,112) = '20210108'

select * from t_lhpmst where fc_Branch = '2025'
select * from t_lhpdtl where fc_Docno = 'RTG2101-013/TGH2101-0213' and fc_Branch = '2025'

select b.* 
from t_paymentmst a 
inner join t_paymentdtl b on a.fc_branch = b.fc_branch and a.fc_docno  = b.fc_docno 
where a.fc_branch = '2025' and convert(char(8),a.fd_docdate,112) = '20210109' and a.fd_inputby = '3087' order by fc_docno,fc_cr

use d_sfa

select b.* 
from t_paymentmst a 
inner join t_paymentdtl b on a.fc_branch = b.fc_branch and a.fc_docno  = b.fc_docno 
where a.fc_branch = '2001' and convert(char(8),a.fd_docdate,112) = '20210113' and a.fd_inputby = 'B041' order by fc_docno,fc_cr

use d_transaksi

select b.* 
from t_paymentmst a 
inner join t_paymentdtl b on a.cbranch = b.cbranch and a.cdocno  = b.cDocno 
where a.cbranch = '2001' and convert(char(8),a.ddocdate,112) = '20210113' and a.dinputby = 'B040' order by cdocno,cCR






