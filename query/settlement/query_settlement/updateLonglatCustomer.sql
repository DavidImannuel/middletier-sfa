USE [d_sfa]
GO

/****** Object:  StoredProcedure [dbo].[pr_updateLonglatCustomer]    Script Date: 11/12/2020 14:13:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[pr_updateLonglatCustomer] @cBranch char(4),@cSalesCode char(4),@dTransactionDate date 
AS
BEGIN
	
	DECLARE  
	@vCustcode VARCHAR(30),
	@vLongtitude varchar(75),
	@vLatitude varchar(75);

DECLARE cursor_custlonglat CURSOR
FOR select fc_custcode,fc_latitude,fc_longtitude from [MYSQL]...[db_sfa.t_customer_edit] 
	where fc_salescode = @cSalesCode and fc_branch = @cBranch and convert(char(8),fd_editdate,112) = convert(char(8),@dTransactionDate,112);

OPEN cursor_custlonglat;

FETCH NEXT FROM cursor_custlonglat INTO 
	@vCustcode ,
	@vLatitude ,
	@vLongtitude ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	IF EXISTS (
            select * from d_transaksi..t_customerCoordinate where cBranch = @cBranch and vCustcode = @vCustcode
            )
    BEGIN
				print 'update '+@vCustcode+' latitude:'+ @vLatitude +' longitude:'+@vLongtitude
        UPDATE d_transaksi..t_customerCoordinate
        SET vLatitude = @vLatitude, vLongitude = @vLongtitude
        WHERE cBranch = @cBranch and vCustcode = @vCustcode
    END
    ELSE
    BEGIN
				print 'insert '+@vCustcode+' latitude:'+ @vLatitude +' longitude:'+@vLongtitude
        INSERT INTO d_transaksi..t_customerCoordinate
        select fc_branch,fc_custcode,fc_latitude,fc_longtitude from [MYSQL]...[db_sfa.t_customer_edit]
				where fc_branch = @cBranch and fc_custcode = @vCustcode
    END        

	FETCH NEXT FROM cursor_custlonglat INTO 
		@vCustcode ,
		@vLatitude ,
		@vLongtitude ;

END;

CLOSE cursor_custlonglat;

DEALLOCATE cursor_custlonglat;

END
GO

