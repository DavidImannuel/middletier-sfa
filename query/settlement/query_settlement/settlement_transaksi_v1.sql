
use d_sfa;
delete from d_sfa..t_sales_cost;
delete from d_sfa..t_somst;
delete from d_sfa..t_sodtl;
delete from d_sfa..t_domst;
delete from d_sfa..t_dodtl;
delete from d_sfa..t_billmst;
delete from d_sfa..t_billdtl;
delete from d_sfa..t_bankmst;
delete from d_sfa..t_bankdtl;
delete from d_sfa..t_paymentmst;
delete from d_sfa..t_paymentdtl;
delete from d_sfa..t_movebrgmst;
delete from d_sfa..t_movebrgdtl;
delete from d_sfa..t_tagmst;
delete from d_sfa..t_tagdtl;

delete from d_sfa..t_lhpmst;
delete from d_sfa..t_lhpdtl;
DELETE FROM d_sfa..t_lhpmst;
DELETE FROM d_sfa..t_lhpdtl;
DELETE FROM d_sfa..t_lhpbankmst;
DELETE FROM d_sfa..t_lhpbankdtl;
DELETE FROM d_sfa..t_lhppaymentmst;
DELETE FROM d_sfa..t_lhppaymentdtl;

use d_sfa;

insert into d_sfa..t_sales_cost 
select * from [mysql]...[db_sfa.t_sales_cost] where format(fd_date,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd');

insert into d_sfa..t_somst 
select * from [mysql]...[db_sfa.t_somst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd');

insert into d_sfa..t_sodtl 
select * from [mysql]...[db_sfa.t_sodtl] 
where fc_sono in ( select fc_sono from [mysql]...[db_sfa.t_somst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd') );

insert into d_sfa..t_domst 
select * from [mysql]...[db_sfa.t_domst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd');

insert into d_sfa..t_dodtl 
select * from [mysql]...[db_sfa.t_dodtl] 
where fc_dono in ( select fc_dono from [mysql]...[db_sfa.t_domst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd') );

insert into d_sfa..t_billmst 
select * from [mysql]...[db_sfa.t_billmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd');

insert into d_sfa..t_billdtl 
select * from [mysql]...[db_sfa.t_billdtl]
where fc_docno in ( select fc_docno from [mysql]...[db_sfa.t_billmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd') );  

insert into d_sfa..t_bankmst 
select * from [mysql]...[db_sfa.t_bankmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd');
insert into d_sfa..t_bankdtl 
select * from [mysql]...[db_sfa.t_bankdtl] 
where fc_docno in ( select fc_docno from [mysql]...[db_sfa.t_bankmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd') );

insert into d_sfa..t_paymentmst 
select * from [mysql]...[db_sfa.t_paymentmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd');

insert into d_sfa..t_paymentdtl 
select * from [mysql]...[db_sfa.t_paymentdtl] 
where fc_docno in ( select fc_docno from [mysql]...[db_sfa.t_paymentmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd') );

insert into d_sfa..t_movebrgmst 
select * from [mysql]...[db_sfa.t_movebrgmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd');

insert into d_sfa..t_movebrgdtl 
select * from [mysql]...[db_sfa.t_movebrgdtl] 
where fc_docno in  (select fc_docno from [mysql]...[db_sfa.t_movebrgmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd'));

insert into d_sfa..t_tagmst 
select * from [mysql]...[db_sfa.t_tagmst] 
where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd') and fc_loktujuan = '101' AND fc_ket LIKE '%SFA TAG TURUN%';

insert into d_sfa..t_tagdtl 
select * from [mysql]...[db_sfa.t_tagdtl] 
where fc_tagno in ( select fc_tagno from [mysql]...[db_sfa.t_tagmst] where format(fd_docdate,'yyyyMMdd') = format( cast( :date as date) ,'yyyyMMdd') and fc_loktujuan = '101' AND fc_ket LIKE '%SFA TAG TURUN%' )

INSERT INTO d_sfa..t_lhpmst 
SELECT * FROM [MYSQL]...[db_sfa.t_lhpmst] WHERE FORMAT(fd_Inputdate,'yyyyMMdd') = FORMAT(cast( :date as DATE),'yyyyMMdd') ;
INSERT INTO d_sfa..t_lhpdtl
SELECT * FROM [MYSQL]...[db_sfa.t_lhpdtl] WHERE fc_Docno 
IN ( SELECT fc_docno FROM [MYSQL]...[db_sfa.t_lhpmst] WHERE FORMAT(fd_Inputdate,'yyyyMMdd') = FORMAT(cast( :date as DATE),'yyyyMMdd') ) ;

INSERT INTO d_sfa..t_lhpbankmst
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'BI' fc_tipe,
'' fc_docref,format(MAX(a.fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(a.fd_Inputdate) fd_postdate, MAX(a.fd_Inputdate),a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'111101' fc_gltrans,'D' fc_cr,
SUM(a.fm_Pay) fm_value1,SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus,
1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
'0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from d_sfa..t_lhpmst(NOLOCK) a 
where format(a.fd_docdate,'yyyyMMdd') = format(CAST( :date AS DATE),'yyyyMMdd') and a.fm_pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector;

INSERT INTO d_sfa..t_lhpbankdtl
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'01' fc_no,
'' fc_docref,'CUSTOMER AR/CLEARING' cBusTrans, '' as fc_suppcode, '' as fc_custcode, a.fc_collector as fc_receipt, 'R' as fc_bttipe, 
'BI' as fc_tipe, '' as fc_bank, '' as fc_giro, '111101' as fc_gltrans, '219301' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, 
SUM(a.fm_Pay) fm_value1, 
SUM(a.fm_Pay) fm_value2, 
'0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from d_sfa..t_lhpmst(NOLOCK) a 
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode
where format(a.fd_docdate,'yyyyMMdd') = format(CAST( :date AS DATE),'yyyyMMdd') and a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname;


INSERT INTO d_sfa..t_lhpbankmst
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(fd_Inputdate),'yyyyMMdd') ) fc_docno,'AR' fc_tipe,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
format(MAX(fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_Inputdate) fd_postdate,MAX(fd_Inputdate) ,a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'219301' fc_gltrans,'D' fc_cr,
SUM(a.fm_Pay) fm_value1,
SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus, 1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
	'0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from d_sfa..t_lhpmst a
where format(a.fd_docdate,'yyyyMMdd') = format(CAST( :date AS DATE),'yyyyMMdd') and a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode;


INSERT INTO d_sfa..t_lhpbankdtl
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno,'01' fc_no,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
'' cBusTrans, '' as fc_suppcode, a.fc_Custcode as fc_custcode, a.fc_collector as fc_receipt, 'D' as fc_bttipe, 
'AR' as fc_tipe, '' as fc_bank, '' as fc_giro, '219301' as fc_gltrans, '219401' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, 
SUM(a.fm_Pay) fm_value1
, SUM(a.fm_Pay) fm_value2, 
CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
	THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
	ELSE 0
END as fm_sisa1, 
CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
	THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
	ELSE 0
END as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from d_sfa..t_lhpmst a 
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode
where format(a.fd_docdate,'yyyyMMdd') = format(CAST( :date AS DATE),'yyyyMMdd') and a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode;


INSERT INTO d_sfa..t_lhppaymentmst
Select
'0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
'' as fc_docref, 
'D' as fc_tipe, '' fc_suppcode, a.fc_custcode, 
format(MAX(a.fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_Inputdate) fd_postdate,MAX(fd_Inputdate) fd_Inputdate, 
a.fc_collector as fd_inputby, 'IDR' as fc_mucode, '1' as fm_rate, 
SUM(a.fm_Pay) fm_value1, 
SUM(a.fm_Pay) fm_value2, concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, 
'R' as fc_status, '1' as fc_tax, '' as fc_plantbayar
from d_sfa..t_lhpmst a
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode and a.fc_branch = b.cbranch
where format(a.fd_docdate,'yyyyMMdd') = format(CAST( :date AS DATE),'yyyyMMdd') and a.fm_pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode;

INSERT INTO d_sfa..t_lhppaymentdtl
Select 
	a.fc_branch, 
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') )  fc_docref, 
'' as fc_noref,  '01' as fc_no, 
'AR'  as fc_gltrans, 
'' as fc_pk, 'C' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(a.fm_Pay) as fm_netto1, SUM(a.fm_Pay) as fm_netto2, 
SUM(a.fm_Pay) fm_pay1, 
SUM(a.fm_Pay) fm_pay2, 
	'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from d_sfa..t_lhpmst a
where format(a.fd_Docdate,'yyyyMMdd') = format(CAST( :date AS DATE),'yyyyMMdd') AND a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode;

INSERT INTO d_sfa..t_lhppaymentdtl
Select 
a.fc_branch, 
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
	b.fc_Docref fc_docref, 
'' as fc_noref,  '01' fc_no, 
b.fc_Tipe fc_gltrans, 
'' as fc_pk, 'D' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(b.fm_netto) as fm_netto1, SUM(b.fm_netto) as fm_netto2, 
CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
	THEN SUM(b.fm_Netto)
	ELSE SUM(b.fm_pay)
END as fm_pay1, 
CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
	THEN SUM(b.fm_Netto)
	ELSE SUM(b.fm_pay)
END as fm_pay2, 
	'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from 
d_sfa..t_lhpmst a
left join d_sfa..t_lhpdtl b on a.fc_docno = b.fc_docno and a.fc_branch = b.fc_Branch
where format(a.fd_docdate,'yyyyMMdd') = format(CAST( :date AS DATE),'yyyyMMdd') and b.fm_pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode,b.fc_Docref,b.fc_Tipe;


SET XACT_ABORT ON;

BEGIN TRY
   use d_transaksi;
   BEGIN tran transaksi
      
      IF OBJECT_ID('tempdb.dbo.#tempBankOPTCOSTNO', 'U') IS NOT NULL
      DROP TABLE #tempBankOPTCOSTNO; 
      CREATE TABLE #tempBankOPTCOSTNO ( cBankNo varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempSONO', 'U') IS NOT NULL
      DROP TABLE #tempSONO; 
      CREATE TABLE #tempSONO ( cDocno varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempDONO', 'U') IS NOT NULL
      DROP TABLE #tempDONO; 
      CREATE TABLE #tempDONO ( cDocno varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempDONOCAN', 'U') IS NOT NULL
      DROP TABLE #tempDONOCAN; 
      CREATE TABLE #tempDONOCAN ( cDOno varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempGLNDO', 'U') IS NOT NULL
      DROP TABLE #tempGLNDO; 
      CREATE TABLE #tempGLNDO ( cDOno varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempBILLGALON', 'U') IS NOT NULL
      DROP TABLE #tempBILLGALON; 
      CREATE TABLE #tempBILLGALON ( cDocNo varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempBANKBINO', 'U') IS NOT NULL
      DROP TABLE #tempBANKBINO; 
      CREATE TABLE #tempBANKBINO ( cDocno varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempBANKARNO', 'U') IS NOT NULL
      DROP TABLE #tempBANKARNO; 
      CREATE TABLE #tempBANKARNO ( cDocno varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempPAYMENTNO', 'U') IS NOT NULL
      DROP TABLE #tempPAYMENTNO; 
      CREATE TABLE #tempPAYMENTNO ( cDocno varchar(50) );
      
      IF OBJECT_ID('tempdb.dbo.#tempTAGNO', 'U') IS NOT NULL
      DROP TABLE #tempTAGNO; 
      CREATE TABLE #tempTAGNO ( cDocno varchar(50) );


   DECLARE 
      @year varchar(10),@month varchar(10),@cBranch VARCHAR(10), 
      @cDocNo VARCHAR(50),@cDocNoTis VARCHAR(50),
      
      @cArmada VARCHAR(20), @vSalescode VARCHAR(20),@cGL varchar(10),
      
      @cTipe VARCHAR(30),
      @cSalescode VARCHAR(50)
      ;

      
      print 'INSERT OPT COST'
      DECLARE cursorvar_optcost CURSOR LOCAL STATIC
         FOR select fc_branch,fc_userid,fc_armadacode,fc_GL ,year(fd_date),format(fd_date,'MM')
         from d_sfa..t_sales_cost (NOLOCK)
         group by fc_branch,fc_userid,fc_armadacode,fc_GL,year(fd_date),format(fd_date,'MM') ;
      OPEN cursorvar_optcost;

      FETCH NEXT FROM cursorvar_optcost INTO 
         @cBranch , @vSalescode,@cArmada,@cGL,@year,@month ;

      WHILE @@FETCH_STATUS = 0
      BEGIN

         insert into #tempBankOPTCOSTNO exec pr_nomor @cBranch,'BANKA',@year,@month;
         select @cDocNoTis=cBankNo from #tempBankOPTCOSTNO;

         print @cBranch+' '+@vSalescode+' '+@cArmada+' '+@cGL+' '+@year+' '+@month;
         print 'proses OPT COST SALES :  NO TIS : '+@cDocnoTis;   
         update d_sfa..t_sales_cost set fc_costno = @cDocNoTis 
         where fc_branch = @cBranch and fc_userid = @vSalescode and fc_armadacode = @cArmada and fc_Gl = @cGL

         insert into t_bankmst
         SELECT '0002',fc_branch,year(max(fd_date)),format(max(fd_date),'MM'),fc_costno,'GO','',
         format(cast(max(fd_date) as datetime),'yyyy-MM-dd 00:00:00' ),cast(max(fd_date) as datetime),cast(max(fd_date) as datetime)
         ,'MIDDLETIER' ,'IDR','1.00','111201','C',sum(fm_value),sum(fm_value),'','R','1','','','','','',0,0,0,0,0
         from d_sfa..t_sales_cost(NOLOCK)
         where fc_costno = @cDocNoTis
         group by fc_branch,fc_userid,fc_armadacode,fc_costno,fc_gl,year(fd_date),format(fd_date,'MM');

         insert into t_bankdtl
         select 
         '0002',a.fc_branch,YEAR(a.fd_date),format(a.fd_date,'MM'),cast(a.fc_costno as varchar(12)), 
         RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_userid,a.fc_armadacode,fc_gl ORDER BY a.fd_date) ),2 ) cNo  ,'',
         d.cBusTran,'','','',d.cTipe,'GO','','','111201',a.fc_gl,'D',cast('1900-01-01 00:00:00.000' as datetime),
         substring(a.fc_cost_description,1,50),e.cIOCode,e.cCostctr,'IDR',a.fm_value,a.fm_value,'0','0','R','0','0','0',
         '','','','','','','0','','' 
         from  d_sfa..t_sales_cost (NOLOCK) a
         left join t_glacc1 b on a.fc_GL = b.cGL
         left join t_cashjrngl c on b.cGL=c.cGL 
         LEFT join t_cashjrn d on c.cCJCode=d.cCJCode and c.cNo=d.cNo 
         left join t_io e on a.fc_armadacode= e.cDesc and e.cbranch = a.fc_branch
         where a.fc_costno = @cDocNoTis
         order by a.fc_costno;
         EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
         
         update t_nomor set nDocno = nDocno + 1 where cDocument = 'BANKA' and cPeriode =@month and cyear=@year and cBranch = @cBranch;

         FETCH NEXT FROM cursorvar_optcost INTO 
            @cBranch , @vSalescode ,@cArmada,@cGL,@year,@month;

      END;

      CLOSE cursorvar_optcost;

      DEALLOCATE cursorvar_optcost;

      
      print 'INSERT SO';
      DECLARE cursor_so CURSOR LOCAL STATIC 
      FOR select fc_branch,fc_sono,fc_year,fc_periode from d_sfa..t_somst (NOLOCK);
      OPEN cursor_so;
            
      FETCH NEXT FROM cursor_so INTO @cBranch , @cDocNo,@year,@month ;

      WHILE @@FETCH_STATUS = 0
      BEGIN
         
         insert into #tempSONO EXEC pr_nomor @cBranch,'SOA',@year,@month;
         select @cDocNoTis  = cDocno from #tempSONO;  
         print 'proses SO : '+@cDocno+' NO TIS : '+@cDocnoTis;
         update d_sfa..t_somst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;
         update d_sfa..t_sodtl set fc_sono = @cDocnoTis  where  fc_branch = @cBranch and fc_sono = @cDocNo;
         update d_sfa..t_domst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;

         insert into t_somst select * from d_sfa..t_somst(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;     
         insert into t_sodtl 
         select * from d_sfa..t_sodtl(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;

         update t_nomor set nDocno = ndocno + 1 where cDocument = 'SOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
         insert into #tempSONO EXEC pr_nomor @cBranch,'SOA',@year,@month;
         select @cDocNoTis  = cDocno from #tempSONO;

         FETCH NEXT FROM cursor_so INTO @cBranch , @cDocno,@year,@month ;

      END;

      CLOSE cursor_so;
      DEALLOCATE cursor_so;

      
      print 'INSERT DO TO';
      DECLARE cursor_do CURSOR LOCAL STATIC
      FOR select fc_branch,fc_dono,fc_year,fc_periode,left(fc_dono,2) from d_sfa..t_domst(NOLOCK)  where fc_tipe != '631' ;
      OPEN cursor_do;
         FETCH NEXT FROM cursor_do INTO 
            @cBranch , @cDocNo,@year,@month,@cTipe ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
            
            IF (@cTipe = 'TO')
            BEGIN
               insert into #tempDONO EXEC pr_nomor @cBranch,'DOA',@year,@month;
               select @cDocNoTis  = cDocno from #tempDONO;
               print @cBranch+' '+@cDocno+' '+@year+' '+@month;
               print 'proses DO-TO : '+@cDocno+' NO TIS : '+@cDocnoTis;

               update d_sfa..t_domst set fc_dono = @cDocnoTis where  fc_branch = @cBranch and fc_dono = @cDocNo ;
               update d_sfa..t_dodtl set fc_dono = @cDocnoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;

               insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;     
               insert into t_dodtl 
               select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

               update t_domst set cStatus = 'R' where cdono = @cDocNoTis and cBranch = @cBranch;
               update t_domst set cStatus = 'I' where cdono = @cDocNoTis and cBranch = @cBranch;
      
               update t_nomor set nDocno = ndocno + 1 where cDocument = 'DOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
            END
            ELSE
            BEGIN
               insert into #tempDONOCAN EXEC pr_nomor @cBranch,'BILLA',@year,@month;
               select @cDocNoTis  = cDOno from #tempDONOCAN;

               print @cbranch+' '+@cDocno+' '+@year+' '+@month;
               print 'proses DO & BILL CAN : '+@cDocno+' NO TIS : '+@cDocnoTis+' TIPE : '+@cTipe;
               
               update d_sfa..t_domst set fc_dono = @cDocNoTis, fc_sono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo;
               update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
               
               insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;     
               insert into t_dodtl 
               select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

               update d_sfa..t_domst set fc_sono = @cDocNoTis where  fc_branch = @cBranch and fc_sono = @cDocNo;
                  
               update t_domst set cStatus = 'R' where cdono = @cDocNoTis;
               update t_domst set cStatus = 'B' where cdono = @cDocNoTis;
               
               update d_sfa..t_billmst set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
               update d_sfa..t_billdtl set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
               
               insert into t_billmst select * from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;   
               insert into t_billdtl 
               select 
               fc_Branch,fc_Docno,
               RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY fc_branch,fc_docno ORDER BY fc_BrgCode) ),2 ) cNo,
               fc_DocRef,fc_Year,fc_Periode,fc_Brgcode,fc_TipeCode,fc_Grpcode,fc_Valcode,fc_ValTipe,fc_PriceCtr
               ,fc_Prdcode,fc_Mrkcode,fc_Jnscode,fc_Sizcode,fc_MdlCode,fc_GdCode,fc_Batch,fd_Postdate,fc_Suppcode,fc_Custcode,
               fc_CostCTR,fc_Extra,fn_Qty1,fn_Qty2,fc_Satcode,fn_Extra1,fn_Extra2,fn_Harga1,fn_Harga2,fn_Disc1,fn_Disc2,fm_Disc1_1,
               fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,fm_PPn2,fn_SisaGalon,
               fn_SisaQtyCN,fn_SisaValCN  
               from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
               

               EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis;  
      
               update d_sfa..t_billmst set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;
               update d_sfa..t_billdtl set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;

               update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo; 

               update d_sfa..t_movebrgmst set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo; 
               update d_sfa..t_movebrgdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDoCno; 

               update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILLA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
            END

            FETCH NEXT FROM cursor_do INTO 
               @cBranch , @cDocno,@year,@month,@cTipe ;
         END;

      CLOSE cursor_do;
      DEALLOCATE cursor_do;

      
      print 'INSERT DO Galon';
      DECLARE cursor_dogalon CURSOR LOCAL STATIC
      FOR select fc_branch,fc_dono,fc_year,fc_periode from d_sfa..t_domst(NOLOCK) where fc_tipe = '631' ;
      OPEN cursor_dogalon;

         FETCH NEXT FROM cursor_dogalon INTO 
            @cBranch , @cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
   
            insert into #tempGLNDO EXEC pr_nomor @cBranch,'RTNGLNA',@year,@month;
            select @cDocNoTis  = cDOno from #tempGLNDO;

            print @cbranch+' '+@cDocno+' '+@year+' '+@month;
            print 'proses DO Galon : '+@cDocno+' NO TIS : '+@cDocnoTis;
            
            
            update d_sfa..t_domst set fc_dono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo;
         
            update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
            
            insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;     
            insert into t_dodtl 
            select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

            update t_domst set cStatus = 'R' where cdono = @cDocNoTis AND cBranch = @cBranch;

            update t_nomor set nDocno = ndocno + 1 where cDocument = 'RTNGLNA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
            
            FETCH NEXT FROM cursor_dogalon INTO 
               @cBranch , @cDocNo,@year,@month ;

         END;
      CLOSE cursor_dogalon;
      DEALLOCATE cursor_dogalon;

      
      print 'INSERT BILL GALON DAN MOVEBRG';
      DECLARE cursor_billgalon CURSOR LOCAL STATIC
      FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_billmst(NOLOCK) where fc_tipe in ('DG','DI');

      OPEN cursor_billgalon;

         FETCH NEXT FROM cursor_billgalon INTO 
            @cBranch , @cDocNo ,@year,@month;

         WHILE @@FETCH_STATUS = 0
         BEGIN
   
            insert into #tempBILLGALON EXEC pr_nomor @cBranch,'BILGA',@year,@month;
            select @cDocNoTis  = cDocNo from #tempBILLGALON;
            print @cBranch+' '+@cDocno+' '+@year+' '+@month;
            print 'proses BILL GALON + MOVEBRG : '+@cDocno+' NO TIS : '+@cDocnoTis;
            update d_sfa..t_billmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_billdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;

            insert into t_billmst select * from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
            insert into t_billdtl 
            select 
				fc_Branch,fc_Docno,
				RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY fc_branch,fc_docno ORDER BY fc_BrgCode) ),2 ) cNo,
				fc_DocRef,fc_Year,fc_Periode,fc_Brgcode,fc_TipeCode,fc_Grpcode,fc_Valcode,fc_ValTipe,fc_PriceCtr
				,fc_Prdcode,fc_Mrkcode,fc_Jnscode,fc_Sizcode,fc_MdlCode,fc_GdCode,fc_Batch,fd_Postdate,fc_Suppcode,fc_Custcode,
				fc_CostCTR,fc_Extra,fn_Qty1,fn_Qty2,fc_Satcode,fn_Extra1,fn_Extra2,fn_Harga1,fn_Harga2,fn_Disc1,fn_Disc2,fm_Disc1_1,
				fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,fm_PPn2,fn_SisaGalon,
				fn_SisaQtyCN,fn_SisaValCN 
			   from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;

            EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis;

            update d_sfa..t_movebrgmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_movebrgdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;

            insert into t_movebrgmst select * from d_sfa..t_movebrgmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;   
            insert into t_movebrgdtl 
            select * from d_sfa..t_movebrgdtl(NOLOCK) 
            where fc_branch = @cBranch and fc_docno = @cDocNoTis; 

            update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo ;   

            update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILGA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
            insert into #tempBILLGALON EXEC pr_nomor @cBranch,'BILGA',@year,@month;
            select @cDocNoTis  = cDocNo from #tempBILLGALON;

            FETCH NEXT FROM cursor_billgalon INTO 
               @cBranch , @cDocNo ,@year,@month;

         END;

      CLOSE cursor_billgalon;
      DEALLOCATE cursor_billgalon;

      
      print 'INSERT BANK BI';
      DECLARE cursor_bankbi CURSOR LOCAL STATIC
      FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) where fc_tipe = 'BI' ;

      OPEN cursor_bankbi;
         FETCH NEXT FROM cursor_bankbi INTO 
            @cBranch,@cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
            insert into #tempBANKBINO EXEC pr_nomor @cBranch,'BANKA',@year,@month;
            select @cDocNoTis  = cDocno from #tempBANKBINO;

            print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
            print 'proses BANK-BI : '+@cDocno+' NO TIS : '+@cDocnoTis;
            update d_sfa..t_bankmst set fc_docno = @cDocNoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_bankdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;   
            insert into t_bankmst select * from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;   
            insert into t_bankdtl 
            select * from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;

            EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
            update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;
            
            update d_sfa..t_bankmst set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
            update d_sfa..t_bankdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
   
            update t_nomor set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

            FETCH NEXT FROM cursor_bankbi INTO 
               @cBranch , @cDocNo,@year,@month;

         END;
      CLOSE cursor_bankbi;
      DEALLOCATE cursor_bankbi;

      
      print 'INSERT BANK AR';
      DECLARE cursor_bankar CURSOR LOCAL STATIC
      FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) where fc_tipe = 'AR';

      OPEN cursor_bankar;
         FETCH NEXT FROM cursor_bankar INTO 
            @cBranch , @cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
            insert into #tempBANKARNO EXEC pr_nomor @cBranch,'ARDEPA',@year,@month;
            select @cDocNoTis  = cDocno from #tempBANKARNO;
            print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
            print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
            update d_sfa..t_bankmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_bankdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
            print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
            insert into t_bankmst select * from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;   
            insert into t_bankdtl 
            select * from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;
            
            EXEC pr_glhis 'ARDEP','0002',@cBranch,@year,@month,@cDocNoTis;
            
            update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;   
            
            update d_sfa..t_paymentdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;

            update t_nomor set nDocno = ndocno + 1 where cDocument = 'ARDEPA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

            FETCH NEXT FROM cursor_bankar INTO 
               @cBranch , @cDocno,@year,@month ;
               
         END;

      CLOSE cursor_bankar;
      DEALLOCATE cursor_bankar;
      
      print 'INSERT PAYMENT';
      DECLARE cursor_payment CURSOR LOCAL STATIC
      FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_paymentmst(NOLOCK);

      OPEN cursor_payment;
         FETCH NEXT FROM cursor_payment INTO 
            @cBranch , @cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
   
            insert into #tempPAYMENTNO EXEC pr_nomor @cBranch,'RECEIVEA',@year,@month;
            select @cDocNoTis  = cDocno from #tempPAYMENTNO;
            print @cBranch+' '+@cDocno+' '+@year+' '+@month;
            print 'proses PAYMENT : '+@cDocno+' NO TIS : '+@cDocnoTis;
            update d_sfa..t_paymentmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_paymentdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;

            insert into t_paymentmst select * from d_sfa..t_paymentmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;     
            insert into t_paymentdtl 
            select * from d_sfa..t_paymentdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;

            update t_paymentmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;   

            update t_nomor set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

            FETCH NEXT FROM cursor_payment INTO 
               @cBranch , @cDocno,@year,@month ;

         END;

      CLOSE cursor_payment;
      DEALLOCATE cursor_payment;
      
      print 'TAG TURUN';
      DECLARE cursorvar_tagturun CURSOR LOCAL STATIC
      FOR select fc_branch,fc_tagno,fc_year,fc_periode from d_sfa..t_tagmst(NOLOCK) where fc_lokTujuan = '101';

      OPEN cursorvar_tagturun;
         FETCH NEXT FROM cursorvar_tagturun INTO 
            @cBranch , @cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
                        
            insert into #tempTAGNO EXEC pr_nomor @cBranch,'TAG',@year,@month;
            select @cDocNoTis  = cDocno from #tempTAGNO;

            update d_sfa..t_tagmst set fc_tagno = @cDocNoTis where  fc_branch = @cBranch and fc_tagno = @cDocNo ;
            update d_sfa..t_tagdtl set fc_tagno = @cDocnoTis  where  fc_branch = @cBranch and fc_tagno = @cDocNo;

            insert into t_tagdtl1 
            select 
            a.fc_branch,a.fc_Tagno,b.fc_lokAsal,a.fc_Brgcode,a.fc_Batch,
            RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) ),2 ) cNo
            ,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
            from d_sfa..t_tagdtl a
            INNER JOIN d_sfa..t_tagmst b ON a.fc_branch = b.fc_branch AND a.fc_tagno = b.fc_tagno
            where a.fc_Tagno = @cDocNoTis and a.fc_branch = @cBranch;

            insert into t_tagdtl2
            select 
            a.fc_branch,a.fc_Tagno,a.fc_Gdcode,a.fc_Brgcode,a.fc_Batch,
            RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) + b.add_no )  ,2 ) cNo
            ,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
            from d_sfa..t_tagdtl a
            left join ( select fc_branch,fc_tagno,count(fc_tagno) add_no from d_sfa..t_tagdtl group by fc_branch,fc_tagno ) b
            on a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_tagno
            where a.fc_Tagno = @cDocNoTis and a.fc_branch = @cBranch;

            insert into t_tagmst 
            select 
            fc_branch,fc_tagno,fc_tipe,fc_year,fc_periode,fc_costctr,
            fc_docref,fd_docdate,fd_postdate,fd_inputdate,'MDTIER' cINputby,
            fd_editdate,'MDTIER' cEditBy,fc_branchasal,fc_lokasal,fc_branchtujuan,
            fc_loktujuan,fc_ket,'R' cStatus,NULL ordertype
            from d_sfa..t_tagmst 
            where fc_tagno = @cDocnoTis and fc_branch = @cBranch;
   
            update t_nomor set nDocno = ndocno + 1 where cDocument = 'TAG' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
            
            FETCH NEXT FROM cursorvar_tagturun INTO 
               @cBranch , @cDocno,@year,@month ;

         END;

      CLOSE cursorvar_tagturun;
      DEALLOCATE cursorvar_tagturun;

      print 'INSERT  LHP BANK BI';
		DECLARE cursor_lhpbankbi CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode,fc_inputby from d_sfa..t_lhpbankmst(NOLOCK) where fc_tipe = 'BI' ;

		OPEN cursor_lhpbankbi;
			FETCH NEXT FROM cursor_lhpbankbi INTO 
				@cBranch,@cDocNo,@year,@month,@cSalescode ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKBINO EXEC pr_nomor @cBranch,'BANKA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKBINO;

				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses LHP BANK-BI : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_lhpbankmst set fc_docno = @cDocNoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_lhpbankdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;	
				insert into t_bankmst select * from d_sfa..t_lhpbankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
				insert into t_bankdtl select * from d_sfa..t_lhpbankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;

				EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
				update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;
				
				update d_sfa..t_lhpbankmst set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
				update d_sfa..t_lhpbankdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_lhpbankbi INTO 
					@cBranch , @cDocNo,@year,@month,@cSalescode;

			END;
		CLOSE cursor_lhpbankbi;
		DEALLOCATE cursor_lhpbankbi;

		
		print 'INSERT LHP BANK AR';
		DECLARE cursor_lhpbankar CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode,fc_inputby from d_sfa..t_lhpbankmst(NOLOCK) where fc_tipe = 'AR';

		OPEN cursor_lhpbankar;
			FETCH NEXT FROM cursor_lhpbankar INTO 
				@cBranch , @cDocNo,@year,@month,@cSalescode;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKARNO EXEC pr_nomor @cBranch,'ARDEPA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKARNO;
				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_lhpbankmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_lhpbankdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
				insert into t_bankmst select * from d_sfa..t_lhpbankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;  	
				insert into t_bankdtl select * from d_sfa..t_lhpbankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;

				IF NOT EXISTS (SELECT * FROM d_transaksi..t_bankmst WHERE cBranch = @cBranch AND cDocno = @cDocNoTis) 
				BEGIN
					THROW 51000, 'The record does not exist.', 1;  
				END

				EXEC pr_glhis 'ARDEP','0002',@cBranch,@year,@month,@cDocNoTis;
				update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	
				update d_sfa..t_lhppaymentdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'ARDEPA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_lhpbankar INTO 
					@cBranch , @cDocno,@year,@month,@cSalescode;
					
			END;

		CLOSE cursor_lhpbankar;
		DEALLOCATE cursor_lhpbankar;
		
		print 'INSERT LHP PAYMENT';
		DECLARE cursor_lhppayment CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode,fd_inputby from d_sfa..t_lhppaymentmst(NOLOCK);

		OPEN cursor_lhppayment;
			FETCH NEXT FROM cursor_lhppayment INTO 
				@cBranch , @cDocNo,@year,@month,@cSalescode;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempPAYMENTNO EXEC pr_nomor @cBranch,'RECEIVEA',@year,@month;
				select @cDocNoTis  = cDocno from #tempPAYMENTNO;
				print @cBranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses PAYMENT : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_lhppaymentmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_lhppaymentdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;

				insert into t_paymentmst select * from d_sfa..t_lhppaymentmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;  	
				insert into t_paymentdtl select * from d_sfa..t_lhppaymentdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;

				update t_paymentmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_lhppayment INTO 
					@cBranch , @cDocno,@year,@month,@cSalescode;

			END;

		CLOSE cursor_lhppayment;
		DEALLOCATE cursor_lhppayment;

		
		print 'UBAH REKAP LHP ';
		DECLARE cursor_lhprkp CURSOR LOCAL STATIC
		FOR SELECT fc_branch,SUBSTRING(fc_docno,0,CHARINDEX('/',fc_docno)) RkpNo FROM d_sfa..t_lhpmst(NOLOCK)
		GROUP BY fc_branch,SUBSTRING(fc_docno,0,CHARINDEX('/',fc_docno))

		OPEN cursor_lhprkp;
			FETCH NEXT FROM cursor_lhprkp INTO 
				@cBranch , @cDocNo ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				update t_tagihanrkpmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocno;	

				FETCH NEXT FROM cursor_lhprkp INTO 
					@cBranch , @cDocno ;

			END;

		CLOSE cursor_lhprkp;
		DEALLOCATE cursor_lhprkp;


   COMMIT tran transaksi;
END TRY
BEGIN CATCH
   ROLLBACK tran transaksi;
   print 'QUERY ERROR, ROLLBACK TRANSACTION';
   THROW;
END CATCH