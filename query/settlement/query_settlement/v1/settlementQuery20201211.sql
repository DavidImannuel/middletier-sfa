USE [d_sfa]
GO
/****** Object:  StoredProcedure [dbo].[pr_settlementSales]    Script Date: 11/12/2020 14:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[pr_settlementSales] @cBranch char(4),@cSalesCode char(4),@dTransactionDate date 
as 
-- update longlat
exec pr_updateLonglatCustomer @cBranch,@cSalesCode,@dTransactionDate
-- sales cost
print 'INSERT OPT COST'
insert into d_sfa..t_sales_cost 
select fc_costno,fc_costno,fc_userid,fc_armadacode,fc_GL,fc_branch,fc_cost_type,fm_value,fc_cost_description,fd_date,fc_photo1,fc_photo2,fc_photo3
from [mysql]...[db_sfa.t_sales_cost] a
where not exists( 
  select fc_costno from d_sfa..t_sales_cost(nolock) b where a.fc_costno = b.fc_costno and convert(char(8),a.fd_date ,112) = convert(char(8),b.fd_date,112)  
) and  convert(char(8),a.fd_date,112) = convert(char(8),@dTransactionDate,112) and a.fc_userid = @cSalescode and a.fc_branch = @cBranch;
-- so
print 'INSERT SO'
insert into d_sfa..t_somst 
select fc_Branch,fc_SONo,fc_SONo,fc_Year,fc_Periode,fd_Docdate,fd_Postdate,fd_Inputdate,fd_Delvdate,fc_Inputby,
fc_tipe,fc_POSO,fn_TOP,fc_Salescode,fc_CompCode,fc_Areacode,fc_Chanelcode,fc_DivCode,fc_DistCode,
fc_MU,fm_Rate,fc_Custbayar,fc_CustKrm,fc_Gdcode,fc_keterangan,fn_disc1,fn_disc2,fm_Brutto1,
fm_Disc1,fm_Netto1,fm_DPP1,fm_PPn1,fm_Brutto2,fm_Disc2,fm_Netto2,fm_DPP2,fm_PPn2,
fc_AppBy,fd_Appdate,fc_Tax,fc_Status,fc_TipeGM,fc_SJRef,fc_AppBy1,fd_Appdate1,fc_PreSONo
from [mysql]...[db_sfa.t_somst] a 
where not exists( 
  select fc_SfaSOno from d_sfa..t_somst b 
  where a.fc_branch = b.fc_branch and a.fc_sono = b.fc_sfasono and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_salescode = @cSalesCode and a.fc_branch = @cBranch ;

insert into d_sfa..t_sodtl 
select b.fc_Branch,b.fc_SOno,b.fc_SOno,b.fc_BrgCode,b.fc_No,b.fc_Extra,b.fn_qty1,b.fn_qty2,b.fc_Satcode,
b.fn_Extra1,b.fn_Extra2,b.fc_GdCode,b.fn_Harga1,b.fn_Harga2,b.fn_Disc1,b.fn_Disc2,b.fm_Disc1_1,
b.fm_Disc1_2,b.fm_Brutto1,b.fm_Netto1,b.fm_DPP1,b.fm_PPN1,b.fm_Disc2_1,b.fm_Disc2_2,b.fm_Brutto2,
b.fm_Netto2,b.fm_DPP2,b.fm_PPN2,b.fc_Reason,b.fc_BrgRef,b.fc_GrpRef,b.fn_QtyRef,b.fc_PromoRef
from [mysql]...[db_sfa.t_somst] a 
left join [mysql]...[db_sfa.t_sodtl] b on a.fc_branch = b.fc_branch and a.fc_sono = b.fc_sono
where not exists( 
  select fc_SfaSOno from d_sfa..t_sodtl b 
  where a.fc_branch = b.fc_branch and a.fc_sono = b.fc_sfasono
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_salescode = @cSalesCode and a.fc_branch = @cBranch;

-- do
print 'INSERT DO'
insert into d_sfa..t_domst 
select a.fc_Branch,a.fc_DONo,a.fc_DONo,a.fc_SONo,a.fc_Year,a.fc_Periode,a.fd_Docdate,a.fd_Postdate,a.fd_Inputdate,
a.fc_Inputby,a.fc_tipe,a.fn_TOP,a.fc_Salescode,a.fc_CompCode,a.fc_Areacode,a.fc_Chanelcode,
a.fc_DivCode,a.fc_DistCode,a.fc_MU,a.fm_Rate,a.fc_Custbayar,a.fc_CustKrm,a.fc_Gdcode,a.fc_keterangan,
a.fn_disc1,a.fn_disc2,a.fm_Brutto1,a.fm_Disc1,a.fm_Netto1,a.fm_DPP1,a.fm_PPn1,a.fm_Brutto2,
a.fm_Disc2,a.fm_Netto2,a.fm_DPP2,a.fm_PPn2,a.fc_AppBy,a.fd_Appdate,a.fc_PostGoods,a.fc_Tax,a.fc_Status,a.fc_TipeGM,a.fc_SJRef
from [mysql]...[db_sfa.t_domst] a 
where not exists( 
  select fc_SfaDOno from d_sfa..t_domst b 
  where a.fc_branch = b.fc_branch and a.fc_dono = b.fc_sfadono and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_salescode = @cSalesCode and a.fc_branch = @cBranch;

insert into d_sfa..t_dodtl 
select b.fc_Branch,b.fc_DOno,b.fc_DOno,b.fc_BrgCode,b.fc_Batch,b.fc_No,b.fc_Extra,b.fn_qty1,b.fn_qty2,
b.fc_Satcode,b.fn_Extra1,b.fn_Extra2,b.fc_GdCode,b.fn_Harga1,b.fn_Harga2,b.fn_Disc1,
b.fn_Disc2,b.fm_Disc1_1,b.fm_Disc1_2,b.fm_Brutto1,b.fm_Netto1,b.fm_DPP1,b.fm_PPN1,
b.fm_Disc2_1,b.fm_Disc2_2,b.fm_Brutto2,b.fm_Netto2,b.fm_DPP2,b.fm_PPN2,b.fc_ReasonRtr
from [mysql]...[db_sfa.t_domst] a 
inner join [mysql]...[db_sfa.t_dodtl] b on a.fc_branch = b.fc_branch and a.fc_dono = b.fc_dono
where not exists( 
  select fc_SfadOno from d_sfa..t_dodtl b 
  where a.fc_branch = b.fc_branch and a.fc_dono = b.fc_sfadono
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_salescode = @cSalesCode and a.fc_branch = @cBranch;
-- bill
print 'INSERT BILL'
insert into d_sfa..t_billmst 
select a.fc_Branch,a.fc_docno,a.fc_Docno,a.fc_DocRef,a.fc_Year,a.fc_Periode,a.fc_Tipe,a.fc_SalesCode,a.fc_CompCode,
a.fc_Areacode,a.fc_Chanelcode,a.fc_DivCode,a.fc_DistCode,a.fd_Docdate,a.fd_Postdate,a.fd_Inputdate,
a.fc_InputBy,a.fc_PPN,a.fd_DueDate,a.fc_Custcode,a.fc_Suppcode,a.fc_MuCode,a.fm_Rate,a.fn_TOP,
a.fc_Keterangan,a.fn_Disc1,a.fn_Disc2,a.fm_Brutto1,a.fm_Netto1,a.fm_Disc1,a.fm_DPP1,a.fm_PPn1,
a.fm_Brutto2,a.fm_Netto2,a.fm_Disc2,a.fm_DPP2,a.fm_PPn2,a.fc_Tax,a.fc_Return,a.fc_Status,a.fc_Sopircode
from [mysql]...[db_sfa.t_billmst] a 
where not exists( 
  select fc_SfaDocno from d_sfa..t_billmst b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_salescode = @cSalesCode and a.fc_branch = @cBranch;

insert into d_sfa..t_billdtl 
select b.fc_Branch,b.fc_Docno,b.fc_Docno,b.fc_No,b.fc_DocRef,b.fc_Year,b.fc_Periode,b.fc_Brgcode,b.fc_TipeCode,
b.fc_Grpcode,b.fc_Valcode,b.fc_ValTipe,b.fc_PriceCtr,b.fc_Prdcode,b.fc_Mrkcode,b.fc_Jnscode,
b.fc_Sizcode,b.fc_MdlCode,b.fc_GdCode,b.fc_Batch,b.fd_Postdate,b.fc_Suppcode,b.fc_Custcode,
b.fc_CostCTR,b.fc_Extra,b.fn_Qty1,b.fn_Qty2,b.fc_Satcode,b.fn_Extra1,b.fn_Extra2,b.fn_Harga1,
b.fn_Harga2,b.fn_Disc1,b.fn_Disc2,b.fm_Disc1_1,b.fm_Disc1_2,b.fm_Brutto1,b.fm_Netto1,b.fm_DPP1,
b.fm_PPN1,b.fm_Disc2_1,b.fm_Disc2_2,b.fm_Brutto2,b.fm_Netto2,b.fm_DPP2,b.fm_PPn2,b.fn_SisaGalon,b.fn_SisaQtyCN,b.fn_SisaValCN
from [mysql]...[db_sfa.t_billmst] a 
left join [mysql]...[db_sfa.t_billdtl] b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno
where not exists( 
  select fc_Sfadocno from d_sfa..t_billdtl b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_salescode = @cSalesCode and a.fc_branch = @cBranch;
-- bank
print 'INSERT BANK'
insert into d_sfa..t_bankmst 
select a.fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,a.fc_docno,a.fc_docno,a.fc_tipe,a.fc_docref,
a.fd_docdate,a.fd_postdate,a.fd_inputdate,a.fc_inputby,a.fc_mu,a.fm_rate,a.fc_gltrans,
a.fc_cr,a.fm_value1,a.fm_value2,a.fc_note,a.fc_status,a.fc_tax,a.fc_udf1,a.fc_udf2,
a.fc_udf3,a.fc_udf4,a.fc_udf5,a.fm_udf1,a.fm_udf2,a.fm_udf3,a.fm_udf4,a.fm_udf5
from [mysql]...[db_sfa.t_bankmst] a 
where not exists( 
  select fc_SfaDocno from d_sfa..t_bankmst b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_inputby = @cSalescode and a.fc_branch = @cBranch;


insert into d_sfa..t_bankdtl 
select b.fc_compcode,b.fc_branch,b.fc_year,b.fc_periode,b.fc_docno,b.fc_docno,b.fc_no,b.fc_docref,b.fc_bustran,
b.fc_suppcode,b.fc_custcode,b.fc_receipt,b.fc_bttipe,b.fc_tipe,b.fc_bank,b.fc_giro,b.fc_gltrans,
b.fc_glvs,b.fc_cr,b.fd_duedate,b.fc_note,b.fc_iocode,b.fc_costctr,b.fc_mu,b.fm_value1,b.fm_value2,
b.fm_sisa1,b.fm_sisa2,b.fc_status,b.fn_kmawal,b.fn_kmakhir,b.fn_volume,b.fc_bbm,b.fc_tujuan,b.fc_sopircode,
b.fc_bankgirocode,b.fc_bankasal,b.fc_giroasal,b.fm_valueasal,b.fc_docrefbkk,b.fc_noref
from [mysql]...[db_sfa.t_bankmst] a 
left join [mysql]...[db_sfa.t_bankdtl] b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno
where not exists( 
  select fc_Sfadocno from d_sfa..t_bankdtl b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fc_inputby = @cSalescode and a.fc_branch = @cBranch;
--payment
insert into d_sfa..t_paymentmst 
select a.fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,a.fc_docno,a.fc_docno,a.fc_docref,a.fc_tipe,
a.fc_suppcode,a.fc_custcode,a.fd_docdate,a.fd_postdate,a.fd_inputdate,a.fd_inputby,a.fc_mucode,
a.fm_rate,a.fm_value1,a.fm_value2,a.fc_note,a.fc_status,a.fc_tax,a.fc_plantbayar
from [mysql]...[db_sfa.t_paymentmst] a 
where not exists( 
  select fc_SfaDocno from d_sfa..t_paymentmst b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fd_inputby = @cSalescode and a.fc_branch = @cBranch ;
print 'INSERT PAYMENT'
insert into d_sfa..t_paymentdtl 
select b.fc_branch,b.fc_docno,b.fc_docno,b.fc_docref,b.fc_noref,b.fc_no,b.fc_gltrans,b.fc_pk,
b.fc_cr,b.fc_mu,b.fm_rate,b.fm_netto1,b.fm_netto2,b.fm_pay1,b.fm_pay2,
b.fc_status,b.fc_cashdisc,b.fm_cashdisc1,b.fm_cashdisc2
from [mysql]...[db_sfa.t_paymentmst] a 
left join [mysql]...[db_sfa.t_paymentdtl] b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno
where not exists( 
  select fc_Sfadocno from d_sfa..t_paymentdtl b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate,112) and a.fd_inputby = @cSalescode and a.fc_branch = @cBranch;
-- movebrg
insert into d_sfa..t_movebrgmst 
select a.fc_Branch,a.fc_Docno,a.fc_Docno,a.fc_Year,a.fc_Periode,a.fc_Tipe,a.fc_DocRef,a.fd_Docdate,a.fd_Postdate,a.fd_Inputdate,
a.fc_InputBy,a.fc_NoSJ,a.fc_NoExpd,a.fc_NoCont,a.fc_PPN,a.fd_DueDate,a.fc_Custcode,a.fc_Suppcode,a.fc_MuCode,
a.fm_Rate,a.fn_TOP,a.fc_GdFrom,a.fc_GdDest,a.fc_Keterangan,a.fn_Disc1,a.fn_Disc2,a.fm_Brutto1,a.fm_Netto1,
a.fm_Invoice1,a.fm_Disc1,a.fm_DPP1,a.fm_PPn1,a.fm_Brutto2,a.fm_Netto2,a.fm_Invoice2,a.fm_Disc2,a.fm_DPP2,
a.fm_PPn2,a.fc_Status,a.fc_Tcode
from [mysql]...[db_sfa.t_movebrgmst] a 
where not exists( 
  select fc_SfaDocno from d_sfa..t_movebrgmst b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert( char(8),@dTransactionDate,112) and a.fc_inputby = @cSalescode and a.fc_branch = @cBranch;
print 'INSERT MOVEBRG'
insert into d_sfa..t_movebrgdtl 
select b.fc_Branch,b.fc_Docno,b.fc_Docno,b.fc_DocRef,b.fc_Year,b.fc_Periode,b.fc_No,b.fc_Tipe,b.fc_Brgcode,
b.fc_GdCode,b.fc_Batch,b.fc_BrgName,b.fc_NoRef,b.fd_Inputdate,b.fc_Suppcode,b.fc_Custcode,
b.fc_CostCTR,b.fc_CR,b.fc_Satcode,b.fc_Extra,b.fn_Qty1,b.fn_Qty2,b.fn_Extra1,b.fn_Extra2,
b.fn_Harga1,b.fn_Harga2,b.fn_Disc1,b.fn_Disc2,b.fm_Disc1_1,b.fm_Disc1_2,b.fm_Brutto1,
b.fm_Netto1,b.fm_DPP1,b.fm_PPN1,b.fm_Disc2_1,b.fm_Disc2_2,b.fm_Brutto2,b.fm_Netto2,b.fm_DPP2,
b.fm_PPn2,b.fn_NewAvg,b.fm_NewStd,b.fm_NewValue,b.fn_QtySaldo,b.fn_QtySaldo1,b.fn_QtySaldo2,
b.fn_AVGSaldo,b.fn_StdSaldo,b.fn_ValueSaldo,b.fc_InsTipe
from [mysql]...[db_sfa.t_movebrgmst] a 
left join [mysql]...[db_sfa.t_movebrgdtl] b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno
where not exists( 
  select fc_Sfadocno from d_sfa..t_movebrgdtl b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate,112) and a.fc_inputby = @cSalescode and a.fc_branch = @cBranch;
-- tagmst
print 'INSERT TAG'
insert into d_sfa..t_tagmst 
select a.fc_branch,a.fc_tagno,a.fc_Tagno,a.fc_Tipe,a.fc_Year,a.fc_Periode,a.fc_CostCTR,a.fc_Docref,a.fd_Docdate,
a.fd_Postdate,a.fd_Inputdate,a.fc_InputBy,a.fd_Editdate,a.fc_Editby,a.fc_Branchasal,a.fc_LokAsal,
a.fc_Branchtujuan,a.fc_LokTujuan,a.fc_Ket,a.fc_Status,a.fc_OrderType,a.fc_shift,
a.fc_salescode,a.fc_supircode,a.fc_stkno,a.fc_platno
from [mysql]...[db_sfa.t_tagmst] a 
where not exists( 
  select fc_Sfatagno from d_sfa..t_tagmst b 
  where a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_sfatagno and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate,112) 
and a.fc_loktujuan = '101' AND a.fc_ket LIKE '%SFA TAG TURUN%' and a.fc_salescode = @cSalesCode and a.fc_branch = @cBranch;

insert into d_sfa..t_tagdtl 
select b.fc_branch,b.fc_Tagno,b.fc_Tagno,b.fc_Gdcode,b.fc_Brgcode,b.fc_Batch,b.fc_No,b.fn_qty1,b.fn_qty2,b.fc_Satcode,b.fc_GL,b.fm_price
from [mysql]...[db_sfa.t_tagmst] a 
inner join [mysql]...[db_sfa.t_tagdtl] b on a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_tagno
where not exists( 
  select fc_Sfatagno from d_sfa..t_tagdtl b 
  where a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_sfatagno
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate,112) 
and a.fc_loktujuan = '101' AND a.fc_ket LIKE '%SFA TAG TURUN%' and a.fc_salescode = @cSalescode and a.fc_branch = @cBranch;
print 'INSERT LHP'
-- lHP
delete from d_sfa..t_lhpmst where fc_branch = @cBranch ; 
delete from d_sfa..t_lhpdtl where fc_branch = @cBranch ;
INSERT INTO d_sfa..t_lhpmst 
SELECT * FROM [MYSQL]...[db_sfa.t_lhpmst] 
WHERE convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) and fc_collector = @cSalesCode and fc_branch = @cBranch;
INSERT INTO d_sfa..t_lhpdtl
SELECT * FROM [MYSQL]...[db_sfa.t_lhpdtl] WHERE fc_Docno 
IN ( 
SELECT fc_docno FROM [MYSQL]...[db_sfa.t_lhpmst] WHERE convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) 
and fc_collector = @cSalesCode and fc_branch = @cBranch
) ;

-- split lhp
print 'INSERT LHP BANK BI'
-- bank tipe BI MST
DELETE FROM d_sfa..t_lhpbankmst where fc_branch = @cBranch;
DELETE FROM d_sfa..t_lhpbankdtl where fc_branch = @cBranch;
INSERT INTO d_sfa..t_lhpbankmst
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'BI' fc_tipe,
'' fc_docref,format(MAX(a.fd_docdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(a.fd_postdate) fd_postdate, MAX(a.fd_Inputdate),a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'111101' fc_gltrans,'D' fc_cr,
SUM(a.fm_Pay) fm_value1,SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus,
1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
'0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from d_sfa..t_lhpmst(NOLOCK) a 
where convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fm_pay > 0 
and a.fc_collector = @cSalesCode and a.fc_branch = @cBranch
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector;
-- bank tipe BI dtl
INSERT INTO d_sfa..t_lhpbankdtl
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'01' fc_no,
'' fc_docref,'CUSTOMER AR/CLEARING' cBusTrans, '' as fc_suppcode, '' as fc_custcode, a.fc_collector as fc_receipt, 'R' as fc_bttipe, 
'BI' as fc_tipe, '' as fc_bank, '' as fc_giro, '111101' as fc_gltrans, '219301' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, 
SUM(a.fm_Pay) fm_value1, 
SUM(a.fm_Pay) fm_value2, 
'0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from d_sfa..t_lhpmst(NOLOCK) a 
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode 
where convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fm_Pay > 0 and a.fc_collector = @cSalesCode
and a.fc_branch = @cBranch
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname;

--bankmst tipe ad
print 'INSERT LHP BANK AR'
INSERT INTO d_sfa..t_lhpbankmst
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(fd_Inputdate),'yyyyMMdd') ) fc_docno,'AR' fc_tipe,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
format(MAX(fd_docdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_postdate) fd_postdate,MAX(fd_Inputdate) ,a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'219301' fc_gltrans,'D' fc_cr,
SUM(a.fm_Pay) fm_value1,
SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus, 1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
	'0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from d_sfa..t_lhpmst a
where convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fm_Pay > 0 and a.fc_collector = @cSalesCode
and a.fc_branch = @cBranch
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode;

--bankdtl tipe ad
INSERT INTO d_sfa..t_lhpbankdtl
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno,'01' fc_no,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
'' cBusTrans, '' as fc_suppcode, a.fc_Custcode as fc_custcode, a.fc_collector as fc_receipt, 'D' as fc_bttipe, 
'AR' as fc_tipe, '' as fc_bank, '' as fc_giro, '219301' as fc_gltrans, '219401' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, 
SUM(a.fm_Pay) fm_value1
, SUM(a.fm_Pay) fm_value2, 
CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
	THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
	ELSE 0
END as fm_sisa1, 
CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
	THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
	ELSE 0
END as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from d_sfa..t_lhpmst a 
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode
where convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fm_Pay > 0 and a.fc_collector = @cSalesCode
and a.fc_branch = @cBranch
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode;

-- payment lhp
print 'INSERT LHP PAYMENT'

DELETE FROM d_sfa..t_lhppaymentdtl where fc_branch = @cBranch;
DELETE FROM d_sfa..t_lhppaymentmst where fc_branch = @cBranch;
-- DELETE FROM d_sfa..t_lhppaymentdtl where fc_branch = @cBranch and fc_docno in ( select fc_docno from d_sfa..t_lhppaymentmst where fd_inputby = @cSalesCode and fc_branch = @cBranch );
-- DELETE FROM d_sfa..t_lhppaymentmst where fd_inputby = @cSalesCode and fc_branch = @cBranch;
-- payment mst
INSERT INTO d_sfa..t_lhppaymentmst
Select
'0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
'' as fc_docref, 
'D' as fc_tipe, '' fc_suppcode, a.fc_custcode, 
format(MAX(a.fd_docdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_postdate) fd_postdate,MAX(fd_Inputdate) fd_Inputdate, 
a.fc_collector as fd_inputby, 'IDR' as fc_mucode, '1' as fm_rate, 
SUM(a.fm_Pay) fm_value1, 
SUM(a.fm_Pay) fm_value2, concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, 
'R' as fc_status, '1' as fc_tax, '' as fc_plantbayar
from d_sfa..t_lhpmst a
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode and a.fc_branch = b.cbranch
where convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and a.fm_pay > 0 and a.fc_collector = @cSalesCode
and a.fc_branch = @cBranch
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode;
-- paymentdtl ad
INSERT INTO d_sfa..t_lhppaymentdtl
Select 
	a.fc_branch, 
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') )  fc_docref, 
'' as fc_noref,  '01' as fc_no, 
'AR'  as fc_gltrans, 
'' as fc_pk, 'C' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(a.fm_Pay) as fm_netto1, SUM(a.fm_Pay) as fm_netto2, 
SUM(a.fm_Pay) fm_pay1, 
SUM(a.fm_Pay) fm_pay2, 
	'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from d_sfa..t_lhpmst a
where convert(char(8),a.fd_Docdate,112) = convert(char(8),@dTransactionDate,112) AND a.fm_Pay > 0 and a.fc_collector = @cSalesCode
and a.fc_branch = @cBranch
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode;
--payment dtl bill
INSERT INTO d_sfa..t_lhppaymentdtl
Select 
a.fc_branch, 
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
	b.fc_Docref fc_docref, 
'' as fc_noref,  '01' fc_no, 
b.fc_Tipe fc_gltrans, 
'' as fc_pk, 'D' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(b.fm_netto) as fm_netto1, SUM(b.fm_netto) as fm_netto2, 
CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
	THEN SUM(b.fm_Netto)
	ELSE SUM(b.fm_pay)
END as fm_pay1, 
CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
	THEN SUM(b.fm_Netto)
	ELSE SUM(b.fm_pay)
END as fm_pay2, 
	'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from 
d_sfa..t_lhpmst a
left join d_sfa..t_lhpdtl b on a.fc_docno = b.fc_docno and a.fc_branch = b.fc_Branch
where convert(char(8),a.fd_docdate,112) = convert(char(8),@dTransactionDate,112) and b.fm_pay > 0 and a.fc_collector = @cSalesCode
and a.fc_branch = @cBranch
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode,b.fc_Docref,b.fc_Tipe;

-- masukan lhpbank dan payment ke t_bank dan payment 
-- bank
print 'INSERT LHP BANK KE T_BANK'
insert into d_sfa..t_bankmst 
select a.fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,a.fc_docno,a.fc_docno,a.fc_tipe,a.fc_docref,
a.fd_docdate,a.fd_postdate,a.fd_inputdate,a.fc_inputby,a.fc_mu,a.fm_rate,a.fc_gltrans,
a.fc_cr,a.fm_value1,a.fm_value2,a.fc_note,a.fc_status,a.fc_tax,a.fc_udf1,a.fc_udf2,
a.fc_udf3,a.fc_udf4,a.fc_udf5,a.fm_udf1,a.fm_udf2,a.fm_udf3,a.fm_udf4,a.fm_udf5
from d_sfa..t_lhpbankmst a 
where not exists( 
  select fc_SfaDocno from d_sfa..t_bankmst b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate ,112) and a.fc_inputby = @cSalesCode and a.fc_branch = @cBranch;


insert into d_sfa..t_bankdtl 
select b.fc_compcode,b.fc_branch,b.fc_year,b.fc_periode,b.fc_docno,b.fc_docno,b.fc_no,b.fc_docref,b.fc_bustran,
b.fc_suppcode,b.fc_custcode,b.fc_receipt,b.fc_bttipe,b.fc_tipe,b.fc_bank,b.fc_giro,b.fc_gltrans,
b.fc_glvs,b.fc_cr,b.fd_duedate,b.fc_note,b.fc_iocode,b.fc_costctr,b.fc_mu,b.fm_value1,b.fm_value2,
b.fm_sisa1,b.fm_sisa2,b.fc_status,b.fn_kmawal,b.fn_kmakhir,b.fn_volume,b.fc_bbm,b.fc_tujuan,b.fc_sopircode,
b.fc_bankgirocode,b.fc_bankasal,b.fc_giroasal,b.fm_valueasal,b.fc_docrefbkk,b.fc_noref
from d_sfa..t_lhpbankmst a 
left join d_sfa..t_lhpbankdtl b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno
where not exists( 
  select fc_Sfadocno from d_sfa..t_bankdtl b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate,112) and a.fc_inputby = @cSalesCode and a.fc_branch = @cBranch;
--payment
print 'INSERT LHP PAYMENT KE T_PAYMENT'
insert into d_sfa..t_paymentmst 
select a.fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,a.fc_docno,a.fc_docno,a.fc_docref,a.fc_tipe,
a.fc_suppcode,a.fc_custcode,a.fd_docdate,a.fd_postdate,a.fd_inputdate,a.fd_inputby,a.fc_mucode,
a.fm_rate,a.fm_value1,a.fm_value2,a.fc_note,a.fc_status,a.fc_tax,a.fc_plantbayar
from d_sfa..t_lhppaymentmst a 
where not exists( 
  select fc_SfaDocno from d_sfa..t_paymentmst b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno and convert(char(8),a.fd_Docdate,112) =  convert(char(8),b.fd_Docdate,112)
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate,112) and a.fd_inputby = @cSalesCode and a.fc_branch = @cBranch;

insert into d_sfa..t_paymentdtl 
select b.fc_branch,b.fc_docno,b.fc_docno,b.fc_docref,b.fc_noref,b.fc_no,b.fc_gltrans,b.fc_pk,
b.fc_cr,b.fc_mu,b.fm_rate,b.fm_netto1,b.fm_netto2,b.fm_pay1,b.fm_pay2,
b.fc_status,b.fc_cashdisc,b.fm_cashdisc1,b.fm_cashdisc2
from d_sfa..t_lhppaymentmst a 
left join d_sfa..t_lhppaymentdtl b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno
where not exists( 
  select fc_Sfadocno from d_sfa..t_paymentdtl b 
  where a.fc_branch = b.fc_branch and a.fc_docno = b.fc_sfadocno
) and  convert(char(8),a.fd_docdate,112) = convert(char(8), @dTransactionDate,112) and a.fd_inputby = @cSalesCode and a.fc_branch = @cBranch;



SET XACT_ABORT ON;

BEGIN TRY
	BEGIN tran transaksi

	DECLARE 
		@year varchar(10),@month varchar(10),
		@cDocNo VARCHAR(50),-- no temp dari sfa
		@cDocNoTis VARCHAR(50),-- menyimpan no hasil dari insert ant exec pr_nomro ke temp table 
		@cArmada VARCHAR(20),@cGL varchar(10),-- variable untuk opt cost
		@cTipe VARCHAR(30),-- tipe untuk pembeda di do canvaser dan to
		@cDocType VARCHAR(30)-- doctype untuk pembeda ini dokument retur reversal atau dokumen normal
		;

	-- proses opt cost
	IF OBJECT_ID('tempdb.dbo.#tempBankOPTCOSTNO', 'U') IS NOT NULL
	DROP TABLE #tempBankOPTCOSTNO; 
	CREATE TABLE #tempBankOPTCOSTNO ( cDocNo varchar(50) );
	
	print 'INSERT OPT COST'
      
			DECLARE cursorvar_optcost CURSOR LOCAL STATIC
         FOR select fc_armadacode,fc_GL ,year(fd_date),format(fd_date,'MM')
         from d_sfa..t_sales_cost (NOLOCK) 
				 where fc_branch = @cBranch and convert(char(8),@dTransactionDate,112) = convert(char(8),fd_date,112) 
				 and fc_TISDocno = fc_costno and fc_userid = @cSalesCode
         group by fc_branch,fc_userid,fc_armadacode,fc_GL,year(fd_date),format(fd_date,'MM') ;
      OPEN cursorvar_optcost;

      FETCH NEXT FROM cursorvar_optcost INTO 
         @cArmada,@cGL,@year,@month ;

      WHILE @@FETCH_STATUS = 0
      BEGIN

         insert into #tempBankOPTCOSTNO exec d_transaksi..pr_nomor @cBranch,'BANKA',@year,@month;
				 -- update nomor document bank
				 update d_transaksi..t_nomor with(UPDLOCK) set nDocno = nDocno + 1 where cDocument = 'BANKA' and cPeriode =@month and cyear=@year and cBranch = @cBranch;
				 -- simpan nomor ke variable
         select @cDocNoTis = cDocNo from #tempBankOPTCOSTNO;
         print @cBranch+' '+@cSalescode+' '+@cArmada+' '+@cGL+' '+@year+' '+@month;
         print 'proses OPT COST SALES :  NO TIS : '+@cDocnoTis;   
         update d_sfa..t_sales_cost set fc_TISDocno = @cDocNoTis 
         where fc_branch = @cBranch and fc_userid = @cSalesCode and fc_armadacode = @cArmada and fc_Gl = @cGL 
				 and convert(char(8),fd_date,112) = convert(char(8),@dTransactionDate,112);
				 
         insert into d_transaksi..t_bankmst
         SELECT '0002',fc_branch,year(max(fd_date)),format(max(fd_date),'MM'),fc_TISDocno,'GO','',
         format(cast(max(fd_date) as datetime),'yyyy-MM-dd 00:00:00' ),cast(max(fd_date) as datetime),cast(max(fd_date) as datetime)
         ,fc_userid ,'IDR','1.00','111201','C',sum(fm_value),sum(fm_value),'','R','1','','','','','',0,0,0,0,0
         from d_sfa..t_sales_cost(NOLOCK)
         where fc_TISDocno = @cDocNoTis and fc_branch = @cBranch
         group by fc_branch,fc_userid,fc_armadacode,fc_TISDocno,fc_gl,year(fd_date),format(fd_date,'MM');

         insert into d_transaksi..t_bankdtl
         select 
         '0002',a.fc_branch,YEAR(a.fd_date),format(a.fd_date,'MM'),cast(a.fc_TISDocno as varchar(12)), 
         RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_userid,a.fc_armadacode,fc_gl ORDER BY a.fd_date) ),2 ) cNo  ,'',
         d.cBusTran,'','','',d.cTipe,'GO','','','111201',a.fc_gl,'D',cast('1900-01-01 00:00:00.000' as datetime),
         substring(a.fc_cost_description,1,50),e.cIOCode,e.cCostctr,'IDR',a.fm_value,a.fm_value,'0','0','R','0','0','0',
         '','','','','','','0','','' 
         from  d_sfa..t_sales_cost (NOLOCK) a
         left join d_transaksi..t_glacc1 b on a.fc_GL = b.cGL
         left join d_transaksi..t_cashjrngl c on b.cGL=c.cGL 
         LEFT join d_transaksi..t_cashjrn d on c.cCJCode=d.cCJCode and c.cNo=d.cNo 
         left join d_transaksi..t_io e on a.fc_armadacode= e.cDesc and e.cbranch = a.fc_branch and e.cStatus = 1
         where a.fc_TISDocno = @cDocNoTis and a.fc_branch = @cBranch
         order by a.fc_TISDocno;

				 EXEC d_transaksi..pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
				
         FETCH NEXT FROM cursorvar_optcost INTO 
            @cArmada,@cGL,@year,@month;
      END;
      CLOSE cursorvar_optcost;
      DEALLOCATE cursorvar_optcost;

			-- SO NO TEMP
			IF OBJECT_ID('tempdb.dbo.#tempSONO', 'U') IS NOT NULL
			DROP TABLE #tempSONO; 
			CREATE TABLE #tempSONO ( cDocNo varchar(50) );
			print 'INSERT SO';
      DECLARE cursor_so CURSOR LOCAL STATIC 
				FOR select fc_sono,fc_year,fc_periode from d_sfa..t_somst(NOLOCK) 
				where fc_branch = @cBranch and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) 
				and fc_sfasono = fc_sono and fc_salescode = @cSalesCode;
			OPEN cursor_so;      
      FETCH NEXT FROM cursor_so INTO @cDocNo,@year,@month ;

      WHILE @@FETCH_STATUS = 0
      BEGIN
         
        insert into #tempSONO EXEC d_transaksi..pr_nomor @cBranch,'SOA',@year,@month;
				-- update nomor dokumen SO
        update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'SOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				-- simpan nomor ke variable
        select @cDocNoTis  = cDocNo from #tempSONO;  
        print 'proses SO : '+@cDocNo+' NO TIS : '+@cDocnoTis;
        update d_sfa..t_somst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;
        update d_sfa..t_sodtl set fc_sono = @cDocnoTis  where  fc_branch = @cBranch and fc_sono = @cDocNo;
				-- update do yang mereferensi so
        update d_sfa..t_domst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;
				
        insert into d_transaksi..t_somst 
				select fc_Branch,fc_SOno,fc_Year,fc_Periode,fd_Docdate,fd_Postdate,fd_Inputdate,fd_Delvdate,fc_Inputby,
				fc_tipe,fc_POSO,fn_TOP,fc_Salescode,fc_CompCode,fc_Areacode,fc_Chanelcode,fc_DivCode,fc_DistCode,
				fc_MU,fm_Rate,fc_Custbayar,fc_CustKrm,fc_Gdcode,fc_keterangan,fn_disc1,fn_disc2,fm_Brutto1,
				fm_Disc1,fm_Netto1,fm_DPP1,fm_PPn1,fm_Brutto2,fm_Disc2,fm_Netto2,fm_DPP2,fm_PPn2,
				fc_AppBy,fd_Appdate,fc_Tax,fc_Status,fc_TipeGM,fc_SJRef,fc_AppBy1,fd_Appdate1,fc_PreSONo 
				from d_sfa..t_somst(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;     
         
				insert into d_transaksi..t_sodtl 
        select fc_Branch,fc_SOno,fc_BrgCode,fc_No,fc_Extra,fn_qty1,fn_qty2,fc_Satcode,
				fn_Extra1,fn_Extra2,fc_GdCode,fn_Harga1,fn_Harga2,fn_Disc1,fn_Disc2,fm_Disc1_1,
				fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,fm_Disc2_1,fm_Disc2_2,fm_Brutto2,
				fm_Netto2,fm_DPP2,fm_PPN2,fc_Reason,fc_BrgRef,fc_GrpRef,fn_QtyRef,fc_PromoRef 
				from d_sfa..t_sodtl(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;
				
        FETCH NEXT FROM cursor_so INTO @cDocNo,@year,@month ;
      END;
      CLOSE cursor_so;
      DEALLOCATE cursor_so;

			-- DO NO TO TEMP
			IF OBJECT_ID('tempdb.dbo.#tempDONO', 'U') IS NOT NULL
			DROP TABLE #tempDONO; 
			CREATE TABLE #tempDONO ( cDocNo varchar(50) );
			-- DO NO CAN TEMP
			IF OBJECT_ID('tempdb.dbo.#tempDONOCAN', 'U') IS NOT NULL
			DROP TABLE #tempDONOCAN; 
			CREATE TABLE #tempDONOCAN ( cDocNo varchar(50) );
		
			print 'INSERT DO';
      DECLARE cursor_do CURSOR LOCAL STATIC
      FOR select fc_dono,fc_year,fc_periode,left(fc_dono,2) from d_sfa..t_domst(NOLOCK)  
			where fc_tipe not in ('631','DCR','DRC') and fc_branch = @cBranch 
			and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) and fc_sfadono = fc_dono and fc_salescode = @cSalesCode;
      
			OPEN cursor_do;
         FETCH NEXT FROM cursor_do INTO 
          @cDocNo,@year,@month,@cTipe ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
            
          IF (@cTipe = 'TO')
          BEGIN
							
              insert into #tempDONO EXEC d_transaksi..pr_nomor @cBranch,'DOA',@year,@month;
              -- update nomor document DO
              update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'DOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
							-- simpan kedalam variable
							select @cDocNoTis  = cDocNo from #tempDONO;
              print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
              print 'proses DO-TO : '+@cDocNo+' NO TIS : '+@cDocnoTis;
							-- update do
              update d_sfa..t_domst set fc_dono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo ;
              update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
							-- insert do
              insert into d_transaksi..t_domst 
							select fc_Branch,fc_DONo,fc_SONo,fc_Year,fc_Periode,fd_Docdate,fd_Postdate,fd_Inputdate,
							fc_Inputby,fc_tipe,fn_TOP,fc_Salescode,fc_CompCode,fc_Areacode,fc_Chanelcode,
							fc_DivCode,fc_DistCode,fc_MU,fm_Rate,fc_Custbayar,fc_CustKrm,fc_Gdcode,fc_keterangan,
							fn_disc1,fn_disc2,fm_Brutto1,fm_Disc1,fm_Netto1,fm_DPP1,fm_PPn1,fm_Brutto2,
							fm_Disc2,fm_Netto2,fm_DPP2,fm_PPn2,fc_AppBy,fd_Appdate,fc_PostGoods,fc_Tax,fc_Status,fc_TipeGM,fc_SJRef  
							from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;     
               
							insert into d_transaksi..t_dodtl 
              select fc_Branch,fc_DOno,fc_BrgCode,fc_Batch,fc_No,fc_Extra,fn_qty1,fn_qty2,
							fc_Satcode,fn_Extra1,fn_Extra2,fc_GdCode,fn_Harga1,fn_Harga2,fn_Disc1,
							fn_Disc2,fm_Disc1_1,fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,
							fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,fm_PPN2,fc_ReasonRtr 
							from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;
							-- trigger update
              update d_transaksi..t_domst set cStatus = 'R' where cdono = @cDocNoTis and cBranch = @cBranch;
              update d_transaksi..t_domst set cStatus = 'I' where cdono = @cDocNoTis and cBranch = @cBranch;
							
            END
            ELSE IF (@cTipe = 'CA')
            BEGIN
              insert into #tempDONOCAN EXEC d_transaksi..pr_nomor @cBranch,'BILLA',@year,@month;
              -- update nomor BILL canvaser untuk DO 
              update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'BILLA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
							-- simpan nomor kedalam variable
							select @cDocNoTis  = cDocNo from #tempDONOCAN;
              print @cBranch+' '+@cDocno+' '+@year+' '+@month;
              print 'proses DO & BILL CAN : '+@cDocNo+' NO TIS : '+@cDocNoTis+' TIPE : '+@cTipe;
              -- update no do canvaser
              update d_sfa..t_domst set fc_dono = @cDocNoTis, fc_sono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo;
              update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
							-- insert no do canvaser
              insert into d_transaksi..t_domst 
							select fc_Branch,fc_DONo,fc_SONo,fc_Year,fc_Periode,fd_Docdate,fd_Postdate,fd_Inputdate,
							fc_Inputby,fc_tipe,fn_TOP,fc_Salescode,fc_CompCode,fc_Areacode,fc_Chanelcode,
							fc_DivCode,fc_DistCode,fc_MU,fm_Rate,fc_Custbayar,fc_CustKrm,fc_Gdcode,fc_keterangan,
							fn_disc1,fn_disc2,fm_Brutto1,fm_Disc1,fm_Netto1,fm_DPP1,fm_PPn1,fm_Brutto2,
							fm_Disc2,fm_Netto2,fm_DPP2,fm_PPn2,fc_AppBy,fd_Appdate,fc_PostGoods,fc_Tax,fc_Status,fc_TipeGM,fc_SJRef 
							from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;     
               
							insert into d_transaksi..t_dodtl 
              select fc_Branch,fc_DOno,fc_BrgCode,fc_Batch,fc_No,fc_Extra,fn_qty1,fn_qty2,
							fc_Satcode,fn_Extra1,fn_Extra2,fc_GdCode,fn_Harga1,fn_Harga2,fn_Disc1,
							fn_Disc2,fm_Disc1_1,fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,
							fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,fm_PPN2,fc_ReasonRtr 
							from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;
							-- update referesnsi do retur galon
              update d_sfa..t_domst set fc_sono = @cDocNoTis where  fc_branch = @cBranch and fc_sono = @cDocNo;                  
              -- trigger do
							update d_transaksi..t_domst set cStatus = 'R' where cdono = @cDocNoTis and cBranch = @cBranch;
              update d_transaksi..t_domst set cStatus = 'B' where cdono = @cDocNoTis and cBranch = @cBranch;
              --update bill canvaser
              update d_sfa..t_billmst set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
              update d_sfa..t_billdtl set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
              -- insert bill canvaser 
              insert into d_transaksi..t_billmst 
							select fc_Branch,fc_Docno,fc_DocRef,fc_Year,fc_Periode,fc_Tipe,fc_SalesCode,fc_CompCode,
							fc_Areacode,fc_Chanelcode,fc_DivCode,fc_DistCode,fd_Docdate,fd_Postdate,fd_Inputdate,
							fc_InputBy,fc_PPN,fd_DueDate,fc_Custcode,fc_Suppcode,fc_MuCode,fm_Rate,fn_TOP,
							fc_Keterangan,fn_Disc1,fn_Disc2,fm_Brutto1,fm_Netto1,fm_Disc1,fm_DPP1,fm_PPn1,
							fm_Brutto2,fm_Netto2,fm_Disc2,fm_DPP2,fm_PPn2,fc_Tax,fc_Return,fc_Status,fc_Sopircode 
						 	from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;   
              
							insert into d_transaksi..t_billdtl 
              select fc_Branch,fc_Docno,RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY fc_branch,fc_docno ORDER BY fc_BrgCode) ),2 ) cNo,
              fc_DocRef,fc_Year,fc_Periode,fc_Brgcode,fc_TipeCode,fc_Grpcode,fc_Valcode,fc_ValTipe,fc_PriceCtr
              ,fc_Prdcode,fc_Mrkcode,fc_Jnscode,fc_Sizcode,fc_MdlCode,fc_GdCode,fc_Batch,fd_Postdate,fc_Suppcode,fc_Custcode,
              fc_CostCTR,fc_Extra,fn_Qty1,fn_Qty2,fc_Satcode,fn_Extra1,fn_Extra2,fn_Harga1,fn_Harga2,fn_Disc1,fn_Disc2,fm_Disc1_1,
              fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,fm_PPn2,fn_SisaGalon,
              fn_SisaQtyCN,fn_SisaValCN  
              from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
              -- sp bill
              EXEC d_transaksi..pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis;  
							-- update referensi bill galon (DG,DI)
              update d_sfa..t_billmst set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;
              update d_sfa..t_billdtl set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;
							-- update referensi payment
              update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo; 
							-- update movebrg(galon)
              update d_sfa..t_movebrgmst set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo; 
              update d_sfa..t_movebrgdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo; 
							
            END

            FETCH NEXT FROM cursor_do INTO 
              @cDocNo,@year,@month,@cTipe ;
         END;
      CLOSE cursor_do;
      DEALLOCATE cursor_do;

			-- RETUR GALON DO NO
			IF OBJECT_ID('tempdb.dbo.#tempRTRGLNDO', 'U') IS NOT NULL
			DROP TABLE #tempRTRGLNDO; 
			CREATE TABLE #tempRTRGLNDO ( cDocNo varchar(50) );
      print 'INSERT RETUR Galon';
      DECLARE cursor_rtrgalon CURSOR LOCAL STATIC
      FOR select fc_dono,fc_year,fc_periode from d_sfa..t_domst(NOLOCK) 
			where fc_tipe = '631' and fc_branch = @cBranch 
			and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) and fc_sfadono = fc_dono and fc_salescode = @cSalesCode;
      OPEN cursor_rtrgalon;

         FETCH NEXT FROM cursor_rtrgalon INTO 
					@cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
   
            insert into #tempRTRGLNDO EXEC d_transaksi..pr_nomor @cBranch,'RTNGLNA',@year,@month;
						-- update nomor do galon
            update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'RTNGLNA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
						-- simpan nomor kedalam variable
            select @cDocNoTis  = cDocNo from #tempRTRGLNDO;
            print @cbranch+' '+@cDocno+' '+@year+' '+@month;
            print 'proses DO Galon : '+@cDocno+' NO TIS : '+@cDocnoTis;
            -- update nomor retur galon
            update d_sfa..t_domst set fc_dono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo;        
            update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
            -- insert retur galon
            insert into d_transaksi..t_domst 
						select fc_Branch,fc_DONo,fc_SONo,fc_Year,fc_Periode,fd_Docdate,fd_Postdate,fd_Inputdate,
						fc_Inputby,fc_tipe,fn_TOP,fc_Salescode,fc_CompCode,fc_Areacode,fc_Chanelcode,
						fc_DivCode,fc_DistCode,fc_MU,fm_Rate,fc_Custbayar,fc_CustKrm,fc_Gdcode,fc_keterangan,
						fn_disc1,fn_disc2,fm_Brutto1,fm_Disc1,fm_Netto1,fm_DPP1,fm_PPn1,fm_Brutto2,
						fm_Disc2,fm_Netto2,fm_DPP2,fm_PPn2,fc_AppBy,fd_Appdate,fc_PostGoods,fc_Tax,fc_Status,fc_TipeGM,fc_SJRef   
						from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;     

            insert into d_transaksi..t_dodtl 
            select fc_Branch,fc_DOno,fc_BrgCode,fc_Batch,fc_No,fc_Extra,fn_qty1,fn_qty2,
						fc_Satcode,fn_Extra1,fn_Extra2,fc_GdCode,fn_Harga1,fn_Harga2,fn_Disc1,
						fn_Disc2,fm_Disc1_1,fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,
						fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,fm_PPN2,fc_ReasonRtr 
						from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;
						-- triger do
            update d_transaksi..t_domst set cStatus = 'R' where cdono = @cDocNoTis AND cBranch = @cBranch;
						
            FETCH NEXT FROM cursor_rtrgalon INTO 
              @cDocNo,@year,@month ;
         END;
      CLOSE cursor_rtrgalon;
      DEALLOCATE cursor_rtrgalon;

			-- BILL GALON NO TEMP
			IF OBJECT_ID('tempdb.dbo.#tempBILLGALON', 'U') IS NOT NULL
			DROP TABLE #tempBILLGALON; 
			CREATE TABLE #tempBILLGALON ( cDocNo varchar(50) );

			print 'INSERT BILL GALON DAN MOVEBRG';
      DECLARE cursor_billgalon CURSOR LOCAL STATIC
      FOR select fc_docno,fc_year,fc_periode from d_sfa..t_billmst(NOLOCK) 
			where fc_tipe in ('DG','DI') and fc_branch = @cBranch 
			and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) and fc_sfadocno = fc_docno and fc_salescode = @cSalesCode;
      
			OPEN cursor_billgalon;
         FETCH NEXT FROM cursor_billgalon INTO 
            @cDocNo ,@year,@month;

         WHILE @@FETCH_STATUS = 0
         BEGIN
   
            insert into #tempBILLGALON EXEC d_transaksi..pr_nomor @cBranch,'BILGA',@year,@month;
						-- update nomor bill galon
            update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'BILGA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
						-- simpan nomor kedalam variable
            select @cDocNoTis  = cDocNo from #tempBILLGALON;
            print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
            print 'proses BILL GALON + MOVEBRG : '+@cDocno+' NO TIS : '+@cDocnoTis;
						-- update nomor bill galon
            update d_sfa..t_billmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_billdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
						-- insert bill galon
            insert into d_transaksi..t_billmst 
						select fc_Branch,fc_Docno,fc_DocRef,fc_Year,fc_Periode,fc_Tipe,fc_SalesCode,fc_CompCode,
						fc_Areacode,fc_Chanelcode,fc_DivCode,fc_DistCode,fd_Docdate,fd_Postdate,fd_Inputdate,
						fc_InputBy,fc_PPN,fd_DueDate,fc_Custcode,fc_Suppcode,fc_MuCode,fm_Rate,fn_TOP,
						fc_Keterangan,fn_Disc1,fn_Disc2,fm_Brutto1,fm_Netto1,fm_Disc1,fm_DPP1,fm_PPn1,
						fm_Brutto2,fm_Netto2,fm_Disc2,fm_DPP2,fm_PPn2,fc_Tax,fc_Return,fc_Status,fc_Sopircode  
						from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
            
						insert into d_transaksi..t_billdtl 
            select 
						fc_Branch,fc_Docno,
						RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY fc_branch,fc_docno ORDER BY fc_BrgCode) ),2 ) cNo,
						fc_DocRef,fc_Year,fc_Periode,fc_Brgcode,fc_TipeCode,fc_Grpcode,fc_Valcode,fc_ValTipe,fc_PriceCtr
						,fc_Prdcode,fc_Mrkcode,fc_Jnscode,fc_Sizcode,fc_MdlCode,fc_GdCode,fc_Batch,fd_Postdate,fc_Suppcode,fc_Custcode,
						fc_CostCTR,fc_Extra,fn_Qty1,fn_Qty2,fc_Satcode,fn_Extra1,fn_Extra2,fn_Harga1,fn_Harga2,fn_Disc1,fn_Disc2,fm_Disc1_1,
						fm_Disc1_2,fm_Brutto1,fm_Netto1,fm_DPP1,fm_PPN1,fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,fm_PPn2,fn_SisaGalon,
						fn_SisaQtyCN,fn_SisaValCN 
						from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
						-- sp bill
            EXEC d_transaksi..pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis;
						-- update no movebrg
            update d_sfa..t_movebrgmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_movebrgdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
						-- insert movebrg
            insert into d_transaksi..t_movebrgmst 
						select fc_Branch,fc_Docno,fc_Year,fc_Periode,fc_Tipe,fc_DocRef,fd_Docdate,fd_Postdate,fd_Inputdate,
						fc_InputBy,fc_NoSJ,fc_NoExpd,fc_NoCont,fc_PPN,fd_DueDate,fc_Custcode,fc_Suppcode,fc_MuCode,
						fm_Rate,fn_TOP,fc_GdFrom,fc_GdDest,fc_Keterangan,fn_Disc1,fn_Disc2,fm_Brutto1,fm_Netto1,
						fm_Invoice1,fm_Disc1,fm_DPP1,fm_PPn1,fm_Brutto2,fm_Netto2,fm_Invoice2,fm_Disc2,fm_DPP2,
						fm_PPn2,fc_Status,fc_Tcode  
						from d_sfa..t_movebrgmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;   
            
						insert into d_transaksi..t_movebrgdtl 
            select fc_Branch,fc_Docno,fc_DocRef,fc_Year,fc_Periode,fc_No,fc_Tipe,fc_Brgcode,
						fc_GdCode,fc_Batch,fc_BrgName,fc_NoRef,fd_Inputdate,fc_Suppcode,fc_Custcode,
						fc_CostCTR,fc_CR,fc_Satcode,fc_Extra,fn_Qty1,fn_Qty2,fn_Extra1,fn_Extra2,
						fn_Harga1,fn_Harga2,fn_Disc1,fn_Disc2,fm_Disc1_1,fm_Disc1_2,fm_Brutto1,
						fm_Netto1,fm_DPP1,fm_PPN1,fm_Disc2_1,fm_Disc2_2,fm_Brutto2,fm_Netto2,fm_DPP2,
						fm_PPn2,fn_NewAvg,fm_NewStd,fm_NewValue,fn_QtySaldo,fn_QtySaldo1,fn_QtySaldo2,
						fn_AVGSaldo,fn_StdSaldo,fn_ValueSaldo,fc_InsTipe  
						from d_sfa..t_movebrgdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis; 
						-- update referensi payment
            update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo ;   
						
            FETCH NEXT FROM cursor_billgalon INTO 
               @cDocNo ,@year,@month;

         END;

      CLOSE cursor_billgalon;
      DEALLOCATE cursor_billgalon;

			-- BANK BI NO TEMP
			IF OBJECT_ID('tempdb.dbo.#tempBANKBINO', 'U') IS NOT NULL
			DROP TABLE #tempBANKBINO; 
			CREATE TABLE #tempBANKBINO ( cDocNo varchar(50) );

			print 'INSERT BANK BI';
      DECLARE cursor_bankbi CURSOR LOCAL STATIC
      FOR select fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) 
			where fc_tipe = 'BI' and fc_branch = @cBranch 
			and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) and fc_sfadocno = fc_docno and fc_inputby = @cSalesCode;

      OPEN cursor_bankbi;
         FETCH NEXT FROM cursor_bankbi INTO 
					@cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
            insert into #tempBANKBINO EXEC d_transaksi..pr_nomor @cBranch,'BANKA',@year,@month;
            -- update nomor banka
            update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
						-- simpan nomer bank kedalam variable
						select @cDocNoTis  = cDocNo from #tempBANKBINO;
            print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
            print 'proses BANK-BI : '+@cDocno+' NO TIS : '+@cDocnoTis;
						-- update nomor
            update d_sfa..t_bankmst set fc_docno = @cDocNoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_bankdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
            -- insert bank BI
						insert into d_transaksi..t_bankmst 
						select fc_compcode,fc_branch,fc_year,fc_periode,fc_docno,fc_tipe,fc_docref,
						fd_docdate,fd_postdate,fd_inputdate,fc_inputby,fc_mu,fm_rate,fc_gltrans,
						fc_cr,fm_value1,fm_value2,fc_note,fc_status,fc_tax,fc_udf1,fc_udf2,
						fc_udf3,fc_udf4,fc_udf5,fm_udf1,fm_udf2,fm_udf3,fm_udf4,fm_udf5  
						from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;   
            
						insert into d_transaksi..t_bankdtl 
            select fc_compcode,fc_branch,fc_year,fc_periode,fc_docno,fc_no,fc_docref,fc_bustran,
						fc_suppcode,fc_custcode,fc_receipt,fc_bttipe,fc_tipe,fc_bank,fc_giro,fc_gltrans,
						fc_glvs,fc_cr,fd_duedate,fc_note,fc_iocode,fc_costctr,fc_mu,fm_value1,fm_value2,
						fm_sisa1,fm_sisa2,fc_status,fn_kmawal,fn_kmakhir,fn_volume,fc_bbm,fc_tujuan,fc_sopircode,
						fc_bankgirocode,fc_bankasal,fc_giroasal,fm_valueasal,fc_docrefbkk,fc_noref  
						from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
						-- sp bank
            EXEC d_transaksi..pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
						-- tr bank
            update d_transaksi..t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;
            -- update referensi bank bi (AR)
            update d_sfa..t_bankmst set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
            update d_sfa..t_bankdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
						
            FETCH NEXT FROM cursor_bankbi INTO 
               @cDocNo,@year,@month;
         END;
      CLOSE cursor_bankbi;
      DEALLOCATE cursor_bankbi;

			-- BANK AR NO TEMP
			IF OBJECT_ID('tempdb.dbo.#tempBANKARNO', 'U') IS NOT NULL
			DROP TABLE #tempBANKARNO; 
			CREATE TABLE #tempBANKARNO ( cDocNo varchar(50) );

			print 'INSERT BANK AR';
      DECLARE cursor_bankar CURSOR LOCAL STATIC
      FOR select fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) 
			where fc_tipe = 'AR' and fc_branch = @cBranch 
			and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) and fc_sfadocno = fc_docno and fc_inputby = @cSalesCode;

      OPEN cursor_bankar;
         FETCH NEXT FROM cursor_bankar INTO 
            @cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
            insert into #tempBANKARNO EXEC d_transaksi..pr_nomor @cBranch,'ARDEPA',@year,@month;
						-- update nomor bank - ar
            update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'ARDEPA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
						-- simpan nomor kedalam variable
            select @cDocNoTis  = cDocNo from #tempBANKARNO;
            print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
            print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
						-- update bank ar
            update d_sfa..t_bankmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_bankdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
            -- insert bank ar
            insert into d_transaksi..t_bankmst 
						select fc_compcode,fc_branch,fc_year,fc_periode,fc_docno,fc_tipe,fc_docref,
						fd_docdate,fd_postdate,fd_inputdate,fc_inputby,fc_mu,fm_rate,fc_gltrans,
						fc_cr,fm_value1,fm_value2,fc_note,fc_status,fc_tax,fc_udf1,fc_udf2,
						fc_udf3,fc_udf4,fc_udf5,fm_udf1,fm_udf2,fm_udf3,fm_udf4,fm_udf5  
						from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;   

            insert into d_transaksi..t_bankdtl 
            select fc_compcode,fc_branch,fc_year,fc_periode,fc_docno,fc_no,fc_docref,fc_bustran,
						fc_suppcode,fc_custcode,fc_receipt,fc_bttipe,fc_tipe,fc_bank,fc_giro,fc_gltrans,
						fc_glvs,fc_cr,fd_duedate,fc_note,fc_iocode,fc_costctr,fc_mu,fm_value1,fm_value2,
						fm_sisa1,fm_sisa2,fc_status,fn_kmawal,fn_kmakhir,fn_volume,fc_bbm,fc_tujuan,fc_sopircode,
						fc_bankgirocode,fc_bankasal,fc_giroasal,fm_valueasal,fc_docrefbkk,fc_noref 
						from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
            -- sp bank
            EXEC d_transaksi..pr_glhis 'ARDEP','0002',@cBranch,@year,@month,@cDocNoTis;
            -- tr bank
            update d_transaksi..t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;   
            -- update referensi bank di payment
            update d_sfa..t_paymentdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
						
            FETCH NEXT FROM cursor_bankar INTO 
               @cDocno,@year,@month ;  
         END;

      CLOSE cursor_bankar;
      DEALLOCATE cursor_bankar;


			-- PAYMENT NO TEMP
			IF OBJECT_ID('tempdb.dbo.#tempPAYMENTNO', 'U') IS NOT NULL
			DROP TABLE #tempPAYMENTNO; 
			CREATE TABLE #tempPAYMENTNO ( cDocNo varchar(50) );

			print 'INSERT PAYMENT';
      DECLARE cursor_payment CURSOR LOCAL STATIC
      FOR select fc_docno,fc_year,fc_periode from d_sfa..t_paymentmst(NOLOCK) 
			where fc_branch = @cBranch and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) 
			and fc_sfadocno = fc_docno and fd_inputby = @cSalesCode;

      OPEN cursor_payment;
         FETCH NEXT FROM cursor_payment INTO 
            @cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
   
            insert into #tempPAYMENTNO EXEC d_transaksi..pr_nomor @cBranch,'RECEIVEA',@year,@month;
						-- update nomor payment
            update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
						-- simpan kedalam variable	
            select @cDocNoTis  = cDocno from #tempPAYMENTNO;
            print @cBranch+' '+@cDocno+' '+@year+' '+@month;
            print 'proses PAYMENT : '+@cDocno+' NO TIS : '+@cDocnoTis;
						-- update nomor
            update d_sfa..t_paymentmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
            update d_sfa..t_paymentdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
						-- insert payment
            insert into d_transaksi..t_paymentmst 
						select fc_compcode,fc_branch,fc_year,fc_periode,fc_docno,fc_docref,fc_tipe,
						fc_suppcode,fc_custcode,fd_docdate,fd_postdate,fd_inputdate,fd_inputby,fc_mucode,
						fm_rate,fm_value1,fm_value2,fc_note,fc_status,fc_tax,fc_plantbayar  
						from d_sfa..t_paymentmst(NOLOCK) 
						where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;     
            
						insert into d_transaksi..t_paymentdtl 
            select fc_branch,fc_docno,fc_docref,fc_noref,fc_no,fc_gltrans,fc_pk,
						fc_cr,fc_mu,fm_rate,fm_netto1,fm_netto2,fm_pay1,fm_pay2,
						fc_status,fc_cashdisc,fm_cashdisc1,fm_cashdisc2  
						from d_sfa..t_paymentdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;
						-- tr payment
            update d_transaksi..t_paymentmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;   
						
            FETCH NEXT FROM cursor_payment INTO 
               @cDocno,@year,@month ;

         END;

      CLOSE cursor_payment;
      DEALLOCATE cursor_payment;

			-- TAG TURUN
			IF OBJECT_ID('tempdb.dbo.#tempTAGNO', 'U') IS NOT NULL
			DROP TABLE #tempTAGNO; 
			CREATE TABLE #tempTAGNO ( cDocNo varchar(50) );
			print 'PROSES TAG TURUN';
      DECLARE cursorvar_tagturun CURSOR LOCAL STATIC
      FOR select fc_tagno,fc_year,fc_periode from d_sfa..t_tagmst(NOLOCK) 
			where fc_lokTujuan = '101' and fc_branch = @cBranch 
			and convert(char(8),fd_docdate,112) = convert(char(8),@dTransactionDate,112) and fc_sfatagno = fc_tagno and fc_salescode = @cSalesCode;

      OPEN cursorvar_tagturun;
         FETCH NEXT FROM cursorvar_tagturun INTO 
            @cDocNo,@year,@month ;

         WHILE @@FETCH_STATUS = 0
         BEGIN
                        
            insert into #tempTAGNO EXEC d_transaksi..pr_nomor @cBranch,'TAG',@year,@month;
						--update nomor tag
						update d_transaksi..t_nomor with(UPDLOCK) set nDocno = ndocno + 1 where cDocument = 'TAG' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
            -- simpan nomor kedalam variable
            select @cDocNoTis  = cDocno from #tempTAGNO;
						-- update nomor
            update d_sfa..t_tagmst set fc_tagno = @cDocNoTis where  fc_branch = @cBranch and fc_tagno = @cDocNo ;
            update d_sfa..t_tagdtl set fc_tagno = @cDocnoTis  where  fc_branch = @cBranch and fc_tagno = @cDocNo;
						-- insert tag
            insert into d_transaksi..t_tagdtl1 
            select 
            a.fc_branch,a.fc_Tagno,b.fc_lokAsal,a.fc_Brgcode,a.fc_Batch,
            RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) ),2 ) cNo
            ,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
            from d_sfa..t_tagdtl a
            INNER JOIN d_sfa..t_tagmst b ON a.fc_branch = b.fc_branch AND a.fc_tagno = b.fc_tagno
            where a.fc_Tagno = @cDocNoTis and a.fc_branch = @cBranch;

            insert into d_transaksi..t_tagdtl2
            select 
            a.fc_branch,a.fc_Tagno,a.fc_Gdcode,a.fc_Brgcode,a.fc_Batch,
            RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) + b.add_no )  ,2 ) cNo
            ,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
            from d_sfa..t_tagdtl a
            left join ( select fc_branch,fc_tagno,count(fc_tagno) add_no from d_sfa..t_tagdtl group by fc_branch,fc_tagno ) b
            on a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_tagno
            where a.fc_Tagno = @cDocNoTis and a.fc_branch = @cBranch;

            insert into d_transaksi..t_tagmst 
            select 
            fc_branch,fc_tagno,fc_tipe,fc_year,fc_periode,fc_costctr,
            fc_docref,fd_docdate,fd_postdate,fd_inputdate,'MDTIER' cINputby,
            fd_editdate,'MDTIER' cEditBy,fc_branchasal,fc_lokasal,fc_branchtujuan,
            fc_loktujuan,fc_ket,'R' cStatus,NULL ordertype
            from d_sfa..t_tagmst 
            where fc_tagno = @cDocnoTis and fc_branch = @cBranch;
   
            
            FETCH NEXT FROM cursorvar_tagturun INTO 
               @cDocno,@year,@month ;

         END;

      CLOSE cursorvar_tagturun;
      DEALLOCATE cursorvar_tagturun;

			print 'UBAH REKAP LHP ';
			DECLARE cursor_lhprkp CURSOR LOCAL STATIC
			FOR SELECT SUBSTRING(fc_docno,0,CHARINDEX('/',fc_docno)) RkpNo 
			FROM d_sfa..t_lhpmst(NOLOCK) where fc_branch = @cBranch and fc_collector = @cSalesCode
			GROUP BY fc_branch,SUBSTRING(fc_docno,0,CHARINDEX('/',fc_docno))

			OPEN cursor_lhprkp;
				FETCH NEXT FROM cursor_lhprkp INTO 
					@cDocNo ;

				WHILE @@FETCH_STATUS = 0
				BEGIN
					-- update tagihanrkpmst jadi R
					print 'LHP : '+@cBranch+' '+@cDocNo
					update d_transaksi..t_tagihanrkpmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocno;	
					FETCH NEXT FROM cursor_lhprkp INTO 
						@cDocno ;
				END;

			CLOSE cursor_lhprkp;
			DEALLOCATE cursor_lhprkp;

	COMMIT tran transaksi;
END TRY
BEGIN CATCH
	ROLLBACK tran transaksi;
	print 'QUERY ERROR, ROLLBACK TRANSACTION';
	THROW;
END CATCH