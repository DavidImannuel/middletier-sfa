select * from t_sales_cost

exec pr_settlementSales '2001','B008','2020-11-02' 
select * from d_transaksi..t_bankmst where cDOcno = 'BN2011-01378'
select * from d_transaksi..t_bankdtl where cDOcno = 'BN2011-01378'

select * from t_somst
select * from t_sodtl

use d_sfa;
truncate table t_somst;
truncate table t_sodtl;
truncate table t_domst;
truncate table t_dodtl;
truncate table t_billmst;
truncate table t_billdtl;
truncate table t_movebrgmst;
truncate table t_movebrgdtl;
truncate table t_bankmst;
truncate table t_bankdtl;
truncate table t_paymentmst;
truncate table t_paymentdtl;
truncate table t_tagmst;
truncate table t_tagdtl;

exec d_sfa..pr_settlementSales '2001','B040','2020-11-12'; 
-- check do
select * from d_sfa..t_domst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105'
select * from d_sfa..t_dodtl 
where fc_dono in ( select fc_dono from d_sfa..t_domst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105' )
select * from d_transaksi..t_domst 
where cDoNo in (select fc_dono from d_sfa..t_domst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
select * from d_transaksi..t_dodtl
where cDoNo in (select fc_dono from d_sfa..t_domst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
-- check bill
select * from d_sfa..t_billmst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105'
select * from d_sfa..t_billdtl 
where fc_docno in ( select fc_docno from d_sfa..t_billmst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105' )
select * from d_transaksi..t_billmst 
where cDocNo in (select fc_docno from d_sfa..t_billmst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
select * from d_transaksi..t_billdtl
where cDocNo in (select fc_docno from d_sfa..t_billmst where fc_salescode = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
-- check bank
select * from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105'
select * from d_sfa..t_bankdtl 
where fc_docno in ( select fc_docno from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105' )
select * from d_transaksi..t_bankmst 
where cDocNo in (select fc_docno from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
select * from d_transaksi..t_bankdtl
where cDocNo in (select fc_docno from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
-- check payment
select * from d_sfa..t_paymentmst where fd_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105'
select * from d_sfa..t_paymentdtl 
where fc_docno in ( select fc_docno from d_sfa..t_paymentmst where fd_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105' )
select * from d_transaksi..t_paymentmst 
where cDocNo in (select fc_docno from d_sfa..t_paymentmst where fd_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
select * from d_transaksi..t_paymentdtl
where cDocNo in (select fc_docno from d_sfa..t_paymentmst where fd_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
-- lhp
-- check bank
select * from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105'
select * from d_sfa..t_bankdtl 
where fc_docno in ( select fc_docno from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105' )
select * from d_transaksi..t_bankmst 
where cDocNo in (select fc_docno from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
select * from d_transaksi..t_bankdtl
where cDocNo in (select fc_docno from d_sfa..t_bankmst where fc_inputby = 'B002' and format(fd_docdate,'yyyyMMdd') = '20201105')
and cBranch = '2001'
-- check payment
select * from d_sfa..t_paymentmst where fd_inputby = 'B040' and format(fd_docdate,'yyyyMMdd') = '20201112'
select * from d_sfa..t_paymentdtl 
where fc_docno in ( select fc_docno from d_sfa..t_paymentmst where fd_inputby = 'B040' and format(fd_docdate,'yyyyMMdd') = '20201112' )
select * from d_transaksi..t_paymentmst 
where cDocNo in (select fc_docno from d_sfa..t_paymentmst where fd_inputby = 'B040' and format(fd_docdate,'yyyyMMdd') = '20201112')
and cBranch = '2001'
select * from d_transaksi..t_paymentdtl
where cDocNo in (select fc_docno from d_sfa..t_paymentmst where fd_inputby = 'B040' and format(fd_docdate,'yyyyMMdd') = '20201112')
and cBranch = '2001'