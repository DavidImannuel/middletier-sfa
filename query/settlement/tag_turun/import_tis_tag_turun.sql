--  temporary table paymentno
IF OBJECT_ID('tempdb.dbo.#tempTAGNO', 'U') IS NOT NULL
DROP TABLE #tempTAGNO; 
CREATE TABLE #tempTAGNO ( cDocno varchar(50) );

use d_transaksi;

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30);

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');

insert into #tempTAGNO EXEC pr_nomor '2001','TAG',@year,@month;
select @cDocNoTis  = cDocno from #tempTAGNO;

DECLARE cursorvar_tagturun CURSOR
FOR select fc_branch,fc_tagno from [MYSQL]...[db_sfa.t_tagmst] 
where fc_lokTujuan = '101' and format(fd_docdate,'yyyyMMdd') = format(getdate(),'yyyyMMdd');

OPEN cursorvar_tagturun;
FETCH NEXT FROM cursorvar_tagturun INTO 
    @cBranch , @cDocNo ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	update [MYSQL]...[db_sfa.t_tagmst] set fc_tagno = @cDocnoTis where  fc_branch = @cBranch and fc_tagno = @cDocNo ;
	update [MYSQL]...[db_sfa.t_tagdtl] set fc_tagno = @cDocnoTis  where  fc_branch = @cBranch and fc_tagno = @cDocNo;

	insert into t_tagmst 
	select 
	fc_branch,fc_tagno,fc_tipe,fc_year,fc_periode,fc_costctr,
	fc_docref,fd_docdate,fd_postdate,fd_inputdate,'MDTIER' cINputby,
	fd_editdate,'MDTIER' cEditBy,fc_branchasal,fc_lokasal,fc_branchtujuan,
	fc_loktujuan,fc_ket,'R' cStatus,NULL ordertype
	from [mysql]...[db_sfa.t_tagmst] 
	where fc_tagno = @cDocnoTis

	insert into t_tagdtl1 
	select 
	fc_branch,fc_Tagno,fc_Gdcode,fc_Brgcode,fc_Batch,
	RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) ),2 ) cNo
	,fn_qty1,fn_qty2,fc_Satcode,fc_GL,fm_price
	from [mysql]...[db_sfa.t_tagdtl] a
	where a.fc_Tagno = @cDocnoTis

	insert into t_tagdtl2
	select 
	a.fc_branch,a.fc_Tagno,a.fc_Gdcode,a.fc_Brgcode,a.fc_Batch,
	RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) + b.add_no )  ,2 ) cNo
	,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
	from [mysql]...[db_sfa.t_tagdtl] a
	left join ( select fc_branch,fc_tagno,count(fc_tagno) add_no from [mysql]...[db_sfa.t_tagdtl] group by fc_branch,fc_tagno ) b
	on a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_tagno
	where a.fc_Tagno = @cDocnoTis
	
	update t_nomor set nDocno = ndocno + 1 where cDocument = 'TAG' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempTAGNO EXEC pr_nomor '2001','TAG',@year,@month;
	select @cDocNoTis  = cDocno from #tempTAGNO;

	FETCH NEXT FROM cursorvar_tagturun INTO 
		@cBranch , @cDocno ;

END;

CLOSE cursorvar_tagturun;
DEALLOCATE cursorvar_tagturun;
