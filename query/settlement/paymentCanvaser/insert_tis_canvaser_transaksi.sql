-- 6
insert into t_domst select a.* from [mysql]...[db_sfa.t_domst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN';

insert into t_dodtl select t1.* from [mysql]...[db_sfa.t_dodtl] t1 where exists ( select * from [mysql]...[db_sfa.t_domst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN' where t1.fc_dono = a.fc_dono );

insert into t_billmst select a.* from [mysql]...[db_sfa.t_billmst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN';

insert into t_billdtl select t1.* from [mysql]...[db_sfa.t_billdtl] t1 where exists (select * from [mysql]...[db_sfa.t_billmst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN' where t1.fc_docno = a.fc_docno) ;

insert into t_bankmst select * from [mysql]...[db_sfa.t_bankmst];
insert into t_bankdtl select * from [mysql]...[db_sfa.t_bankdtl];

insert into t_paymentmst select * from [mysql]...[db_sfa.t_paymentmst];
insert into t_paymentdtl select * from [mysql]...[db_sfa.t_paymentdtl];

insert into t_movebrgmst select * from [mysql]...[db_sfa.t_movebrgmst];
insert into t_movebrgdtl select * from [mysql]...[db_sfa.t_movebrgdtl];