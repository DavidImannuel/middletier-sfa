-- 5
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempPAYMENTNO', 'U') IS NOT NULL
DROP TABLE #tempPAYMENTNO; 
CREATE TABLE #tempPAYMENTNO ( cDocno varchar(50) );

GO

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30);

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');

insert into #tempPAYMENTNO EXEC pr_nomor '2001','RECEIVEA',@year,@month;
select @cDocNoTis  = cDocno from #tempPAYMENTNO;

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_docno from [MYSQL]...[db_sfa.t_paymentmst] ;

OPEN cursorvar;
FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDocNo ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	update [MYSQL]...[db_sfa.t_paymentmst] set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
	update [MYSQL]...[db_sfa.t_paymentdtl] set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
	
	update t_nomor set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempPAYMENTNO EXEC pr_nomor '2001','RECEIVEA',@year,@month;
	select @cDocNoTis  = cDocno from #tempPAYMENTNO;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDocno ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;
