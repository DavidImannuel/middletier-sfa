-- 3
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempBANKNO', 'U') IS NOT NULL
DROP TABLE #tempBANKNO; 
CREATE TABLE #tempBANKNO ( cDocno varchar(50) );

GO

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30);

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');

insert into #tempBANKNO EXEC pr_nomor '2001','BANKA',@year,@month;
select @cDocNoTis  = cDocno from #tempBANKNO;

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_docno from [MYSQL]...[db_sfa.t_bankmst] where fc_tipe = 'BI' ;

OPEN cursorvar;
FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDocNo ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	update [MYSQL]...[db_sfa.t_bankmst] set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
	update [MYSQL]...[db_sfa.t_bankdtl] set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
	update [MYSQL]...[db_sfa.t_bankmst] set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
	update [MYSQL]...[db_sfa.t_bankdtl] set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
	
	update t_nomor set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempBANKNO EXEC pr_nomor '2001','BANKA',@year,@month;
	select @cDocNoTis  = cDocno from #tempBANKNO;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDocno ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;