-- 2
use d_transaksi;

IF OBJECT_ID('tempdb.dbo.#tempDONO_CAN', 'U') IS NOT NULL
DROP TABLE #tempDONO_CAN; 
-- create table temp
CREATE TABLE #tempDONO_CAN ( cDocNo varchar(50) );

GO

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cNoSFA VARCHAR(30),@cNoTis VARCHAR(30);

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');

insert into #tempDONO_CAN EXEC pr_nomor '2001','BILGA',@year,@month;
select @cNoTis  = cDocNo from #tempDONO_CAN;

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_docno from [MYSQL]...[db_sfa.t_billmst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN'
	where a.fc_tipe in ('DG','DI');

OPEN cursorvar;

FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cNoSFA ;

WHILE @@FETCH_STATUS = 0
BEGIN
	

	update [MYSQL]...[db_sfa.t_billmst] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;
	update [MYSQL]...[db_sfa.t_billdtl] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;

	update [MYSQL]...[db_sfa.t_movebrgmst] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;
	update [MYSQL]...[db_sfa.t_movebrgdtl] set fc_docno = @cNoTis  where  fc_branch = @cBranch and fc_docno = @cNoSFA ;

	update [MYSQL]...[db_sfa.t_paymentdtl] set fc_docref = @cNoTis  where  fc_branch = @cBranch and fc_docref = @cNoSFA ;	
	

	update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILGA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempDONO_CAN EXEC pr_nomor '2001','BILGA',@year,@month;
	select @cNoTis  = cDocNo from #tempDONO_CAN;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cNoSFA ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;

