use d_transaksi
DECLARE 
    @cBranch VARCHAR(10), 
	@vCustcode VARCHAR(30),
	@vLongtitude varchar(75),
	@vLatitude varchar(75);

DECLARE cursor_custlonglat CURSOR
FOR select fc_branch,fc_custcode,fc_latitude,fc_longtitude from [MYSQL]...[db_sfa.t_customer_edit];

OPEN cursor_custlonglat;

FETCH NEXT FROM cursor_custlonglat INTO 
    @cBranch , 
	@vCustcode ,
	@vLongtitude ,
	@vLatitude ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	IF EXISTS (
            select * from t_customerCoordinate where cBranch = @cBranch and vCustcode = @vCustcode
            )
    BEGIN
        UPDATE t_customerCoordinate
        SET vLatitude = @vLatitude,
			vLongitude = @vLongtitude
        WHERE cBranch = @cBranch and vCustcode = @vCustcode
    END
    ELSE
    BEGIN
        INSERT INTO t_customerCoordinate
        select fc_branch,fc_custcode,fc_latitude,fc_longtitude from [MYSQL]...[db_sfa.t_customer_edit]
		where fc_branch = @cBranch and fc_custcode = @vCustcode
    END        

	FETCH NEXT FROM cursor_custlonglat INTO 
		@cBranch , 
		@vCustcode ,
		@vLongtitude ,
		@vLatitude ;

END;

CLOSE cursor_custlonglat;

DEALLOCATE cursor_custlonglat;