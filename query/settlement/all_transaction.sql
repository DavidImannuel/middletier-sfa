-- insert db_sfa mysql to sql server
use d_sfa;
delete from d_sfa..t_noo;
delete from d_sfa..t_sales_cost;
delete from d_sfa..t_somst;
delete from d_sfa..t_sodtl;
delete from d_sfa..t_domst;
delete from d_sfa..t_dodtl;
delete from d_sfa..t_billmst;
delete from d_sfa..t_billdtl;
delete from d_sfa..t_bankmst;
delete from d_sfa..t_bankdtl;
delete from d_sfa..t_paymentmst;
delete from d_sfa..t_paymentdtl;
delete from d_sfa..t_movebrgmst;
delete from d_sfa..t_movebrgdtl;
delete from d_sfa..t_tagmst;
delete from d_sfa..t_tagdtl;

-- noo
insert into d_sfa..t_noo select * from [mysql]...[db_sfa.t_noo] where format(fd_dateconfirm,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd') and fc_statusconfirm = 'Y';
-- sales cost
insert into d_sfa..t_sales_cost select * from [mysql]...[db_sfa.t_sales_cost] where format(fd_date,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
-- so
insert into d_sfa..t_somst select * from [mysql]...[db_sfa.t_somst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
insert into d_sfa..t_sodtl 
select b.* from [mysql]...[db_sfa.t_somst] a
inner join [mysql]...[db_sfa.t_sodtl] b on a.fc_sono = b.fc_sono
where format(a.fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
-- do
insert into d_sfa..t_domst select * from [mysql]...[db_sfa.t_domst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
insert into d_sfa..t_dodtl 
select b.* from [mysql]...[db_sfa.t_domst] a
inner join [mysql]...[db_sfa.t_dodtl] b on a.fc_dono = b.fc_dono
where format(a.fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
-- bill
insert into d_sfa..t_billmst select * from [mysql]...[db_sfa.t_billmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
insert into d_sfa..t_billdtl 
select b.* from [mysql]...[db_sfa.t_billmst] a 
inner join [mysql]...[db_sfa.t_billdtl] b on a.fc_docno = b.fc_docno
where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
-- bank
insert into d_sfa..t_bankmst select * from [mysql]...[db_sfa.t_bankmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
insert into d_sfa..t_bankdtl 
select b.* from [mysql]...[db_sfa.t_bankmst] a
inner join [mysql]...[db_sfa.t_bankdtl] b on a.fc_docno = b.fc_docno
where format(a.fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
--payment
insert into d_sfa..t_paymentmst select * from [mysql]...[db_sfa.t_paymentmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
insert into d_sfa..t_paymentdtl 
select b.* from [mysql]...[db_sfa.t_paymentmst] a
inner join [mysql]...[db_sfa.t_paymentdtl] b on a.fc_docno = b.fc_docno
where format(a.fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
-- movebrg
insert into d_sfa..t_movebrgmst select * from [mysql]...[db_sfa.t_movebrgmst] where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
insert into d_sfa..t_movebrgdtl 
select b.* from [mysql]...[db_sfa.t_movebrgmst] a
inner join [mysql]...[db_sfa.t_movebrgdtl] b on a.fc_docno = b.fc_docno
where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd');
-- tagmst
insert into d_sfa..t_tagmst select * from [mysql]...[db_sfa.t_tagmst] 
where format(fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd') and fc_loktujuan = '101' AND fc_ket LIKE '%SFA TAG TURUN%';
insert into d_sfa..t_tagdtl 
select b.* from [mysql]...[db_sfa.t_tagmst] a
inner join [mysql]...[db_sfa.t_tagdtl] b on a.fc_tagno = b.fc_tagno
where format(a.fd_docdate,'yyyyMMdd') = format( cast( '2020-10-07' as date) ,'yyyyMMdd') and fc_loktujuan = '101' AND fc_ket LIKE '%SFA TAG TURUN%';


SET XACT_ABORT ON;

BEGIN TRY
	use d_transaksi
	BEGIN tran transaksi
		-- NOO NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempCustNO', 'U') IS NOT NULL
		DROP TABLE #tempCustNO ;
		CREATE TABLE #tempCustNO ( cCustno varchar(50) );
		-- OPT COST NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBankOPTCOSTNO', 'U') IS NOT NULL
		DROP TABLE #tempBankOPTCOSTNO; 
		CREATE TABLE #tempBankOPTCOSTNO ( cBankNo varchar(50) );
		-- SO NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempSONO', 'U') IS NOT NULL
		DROP TABLE #tempSONO; 
		CREATE TABLE #tempSONO ( cDocno varchar(50) );
		-- DO NO TO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempDONOTO', 'U') IS NOT NULL
		DROP TABLE #tempDONOTO; 
		CREATE TABLE #tempDONOTO ( cDocno varchar(50) );
		-- DO NO CAN TEMP
		IF OBJECT_ID('tempdb.dbo.#tempDONOCAN', 'U') IS NOT NULL
		DROP TABLE #tempDONOCAN; 
		CREATE TABLE #tempDONOCAN ( cDOno varchar(50) );
		-- BILL GALON NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBILLGALON', 'U') IS NOT NULL
		DROP TABLE #tempBILLGALON; 
		CREATE TABLE #tempBILLGALON ( cDocNo varchar(50) );
		-- BANK BI NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBANKBINO', 'U') IS NOT NULL
		DROP TABLE #tempBANKBINO; 
		CREATE TABLE #tempBANKBINO ( cDocno varchar(50) );
		-- BANK AR NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBANKARNO', 'U') IS NOT NULL
		DROP TABLE #tempBANKARNO; 
		CREATE TABLE #tempBANKARNO ( cDocno varchar(50) );
		-- PAYMENT NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempPAYMENTNO', 'U') IS NOT NULL
		DROP TABLE #tempPAYMENTNO; 
		CREATE TABLE #tempPAYMENTNO ( cDocno varchar(50) );
		-- TAG TURUN
		IF OBJECT_ID('tempdb.dbo.#tempTAGNO', 'U') IS NOT NULL
		DROP TABLE #tempTAGNO; 
		CREATE TABLE #tempTAGNO ( cDocno varchar(50) );


	DECLARE 
		@year varchar(10),@month varchar(10),@cBranch VARCHAR(10), 
		@cDocNo VARCHAR(30),@cDocNoTis VARCHAR(30),
		-- opt cost
		@cArmada VARCHAR(20), @vSalescode VARCHAR(20),@cGL varchar(10);

		print 'INSERT NOO';
		DECLARE cursor_noo CURSOR LOCAL STATIC
		FOR 
		select fc_branch,fc_custcode from d_sfa..t_noo (NOLOCK);

		OPEN cursor_noo;

			FETCH NEXT FROM cursor_noo INTO 
			@cBranch,@cDocNo ;

			WHILE @@FETCH_STATUS = 0
			BEGIN

				insert into #tempCustNO EXEC pr_nomor_CustCode @cBranch,'CUST',@year,@month;
				select @cDocNoTis=cCustno from #tempCustNO;
				print @cbranch+' '+@cDocNo;
				print 'proses NOO : '+@cDocno+' NO TIS : '+@cDocnoTis;				
				update d_sfa..t_noo set fc_custcode = @cDocNoTis where fc_custcode = @cDocNo;
	  			update d_sfa..t_somst set fc_custbayar =  @cDocNoTis,  fc_custkrm =  @cDocNoTis where fc_custbayar = @cDocNo;
				update d_sfa..t_domst set fc_custbayar =  @cDocNoTis,  fc_custkrm =  @cDocNoTis where fc_custbayar = @cDocNo;
				update d_sfa..t_billmst set fc_custcode =  @cDocNoTis where fc_custcode = @cDocNo;	
				update d_sfa..t_billdtl set fc_custcode =  @cDocNoTis where fc_custcode = @cDocNo;	
				update d_sfa..t_bankdtl set fc_custcode =  @cDocNoTis where fc_custcode = @cDocNo;	
				update d_sfa..t_paymentmst set fc_custcode =  @cDocNoTis where fc_custcode = @cDocNo;	
				update d_sfa..t_movebrgmst set fc_custcode =  @cDocNoTis where fc_custcode = @cDocNo;
				update d_sfa..t_movebrgdtl set fc_custcode =  @cDocNoTis where fc_custcode = @cDocNo;

				insert into t_customer 
				(ccustcode,cnama,calamat1,calamat2,cpropinsi,ckota,ckecamatan,cdesa,cnegara,
				ckodepos,cphone,cfax,ccontact,cemail,cpkp,cketerangan,nlimitnota,
				nlimittotal,nso,ndo,
				nar,npdc,cinputby,dinputdate,ceditby,deditdate,ndisc,ntop,
				csalescode,ccompcode,careacode,cchanelcode,cdivcode,cdistcode,
				cstatus,cgl,ccol,cgrpcode,chp,car_delv,cdt,crdt,nqtydokunblock,ckunjungan,dstartjdwl,
				ccusttagih,ccashdisc,ccodetoko,ntop_toleransi,cnik,cpaspor,ctunaitransfer,ctaxreq)
				select  
				fc_custcode,fc_custnama ,fc_alamat1 , '' as fc_alamat2 , fc_propinsi ,fc_kota ,fc_kecamatan, 
				fc_desa ,fc_negara ,fc_kodepos ,fc_phone , '' as fc_fax , fc_contact ,fc_email ,'0' as fc_pkp , 'NOO SFA' , 0 as nlimitnota ,
				0 as fn_limittotal, 0 as fn_so, 0 as fn_do,
				0 as fn_ar, 0 as fn_pdc, fc_salescode as fc_inputby , GETDATE() , 'CHRIS' as fc_editby, GETDATE() as fd_editdate , 0 as fn_disc , 0 as fn_top ,
				fc_userid, '0002' as fc_compcode ,'25' as fc_areacode, fc_chanelcode , '60' as fc_divcode ,'24' as fc_distcode , 
				'1' as fc_status, '1200000000' as fc_gl, 'KASIR1' as fc_col,'1001' as fc_grpcode, '' as fc_hp, '1' as fc_ar_delv, '0' as fc_dt, 
				'0' as fc_rdt, '0' as fn_qtydokunblock, '0' as fc_kunjungan, '1900-01-01 00:00:00' as fd_startjdwl,
				'' as fc_custtagih, '0' as fc_cashdisc, '' as fc_codetoko, '0' as fn_top_toleransi, '' as fc_nik, '' as fc_paspor, '' as fc_tunaitransfer, '0' fc_taxreq
				from d_sfa..t_noo (NOLOCK)
				where fc_custcode = @cDocNoTis and fc_branch = @cBranch;

				IF EXISTS ( select cCustcode from t_customer where cCustcode = @cDocNoTis )
				begin
					insert into t_customercomp 
						select fc_custcode,fc_compcode,fc_branch from d_sfa..t_noo(NOLOCK)
						where fc_custcode = @cDocNoTis and fc_branch = @cBranch  ;

					insert into t_custtax 
						select fc_custcode,fc_custnama,fc_alamat1,'00.000.000.0.000.000','00.000.000.0.000.000',getdate(),NULL 
						from d_sfa..t_noo (NOLOCK)
						where fc_custcode = @cDocNoTis and fc_branch = @cBranch  ;

					insert into t_customercoordinate 
						select fc_branch,fc_custcode,fc_longtitude,fc_latitude from d_sfa..t_noo (NOLOCK)
						where fc_custcode = @cDocNoTis and fc_branch = @cBranch  ;
				end

				update t_nomor set nDocno = nDocno + 1 where cDocument = 'CUST' and cPeriode =@month and cyear=@year and cBranch = @cBranch;

			FETCH NEXT FROM cursor_noo INTO 
			@cBranch,@cDocNo ;
			END;

		CLOSE cursor_noo;

		DEALLOCATE cursor_noo;

		-- insert optCost
		print 'INSERT OPT COST'
		DECLARE cursorvar_optcost CURSOR LOCAL STATIC
			FOR select fc_branch,fc_userid,fc_armadacode,fc_GL ,year(fd_date),format(fd_date,'MM')
			from d_sfa..t_sales_cost (NOLOCK)
			group by fc_branch,fc_userid,fc_armadacode,fc_GL,year(fd_date),format(fd_date,'MM') ;
		OPEN cursorvar_optcost;

		FETCH NEXT FROM cursorvar_optcost INTO 
			@cBranch , @vSalescode,@cArmada,@cGL,@year,@month ;

		WHILE @@FETCH_STATUS = 0
		BEGIN

			insert into #tempBankOPTCOSTNO exec pr_nomor @cBranch,'BANKA',@year,@month;
			select @cDocNoTis=cBankNo from #tempBankOPTCOSTNO;

			print @cBranch+' '+@vSalescode+' '+@cArmada+' '+@cGL+' '+@year+' '+@month;
			print 'proses OPT COST SALES :  NO TIS : '+@cDocnoTis; 	
			update d_sfa..t_sales_cost set fc_costno = @cDocNoTis 
			where fc_branch = @cBranch and fc_userid = @vSalescode and fc_armadacode = @cArmada and fc_Gl = @cGL

			insert into t_bankmst
			SELECT '0002',fc_branch,year(fd_date),format(fd_date,'MM'),fc_costno,'GO','',cast(fd_date as datetime),cast(fd_date as datetime),getdate()
			,'MIDDLETIER' ,'IDR','1.00','111201','C',sum(fm_value),sum(fm_value),'','R','1','','','','','',0,0,0,0,0
			from d_sfa..t_sales_cost(NOLOCK)
			where fc_costno = @cDocNoTis
			group by fc_branch,fc_userid,fc_armadacode,fc_costno,fc_gl,year(fd_date),format(fd_date,'MM'),cast(fd_date as datetime);

			insert into t_bankdtl
			select 
			'0002',a.fc_branch,YEAR(a.fd_date),format(a.fd_date,'MM'),cast(a.fc_costno as varchar(12)), 
			RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_userid,a.fc_armadacode,fc_gl ORDER BY a.fd_date) ),2 ) cNo  ,'',
			d.cBusTran,'','','',d.cTipe,'GO','','','111201',a.fc_gl,'D',cast('1900-01-01 00:00:00.000' as datetime),
			substring(a.fc_cost_description,1,50),e.cIOCode,e.cCostctr,'IDR',a.fm_value,a.fm_value,'0','0','R','0','0','0',
			'','','','','','','0','','' 
			from  d_sfa..t_sales_cost (NOLOCK) a
			left join t_glacc1 b on a.fc_GL = b.cGL
			left join t_cashjrngl c on b.cGL=c.cGL 
			LEFT join t_cashjrn d on c.cCJCode=d.cCJCode and c.cNo=d.cNo 
			left join t_io e on a.fc_armadacode= e.cDesc and e.cbranch = a.fc_branch
			where a.fc_costno = @cDocNoTis
			order by a.fc_costno;
			EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
			
			update t_nomor set nDocno = nDocno + 1 where cDocument = 'BANKA' and cPeriode =@month and cyear=@year and cBranch = @cBranch;

			FETCH NEXT FROM cursorvar_optcost INTO 
				@cBranch , @vSalescode ,@cArmada,@cGL,@year,@month;

		END;

		CLOSE cursorvar_optcost;

		DEALLOCATE cursorvar_optcost;

		-- insert SO
		print 'INSERT SO';
		DECLARE cursor_so CURSOR LOCAL STATIC 
		FOR select fc_branch,fc_sono,fc_year,fc_periode from d_sfa..t_somst (NOLOCK);
		OPEN cursor_so;
				
		FETCH NEXT FROM cursor_so INTO @cBranch , @cDocNo,@year,@month ;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			insert into #tempSONO EXEC pr_nomor @cBranch,'SOA',@year,@month;
			select @cDocNoTis  = cDocno from #tempSONO;	
			print 'proses SO : '+@cDocno+' NO TIS : '+@cDocnoTis;
			update d_sfa..t_somst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;
			update d_sfa..t_sodtl set fc_sono = @cDocnoTis  where  fc_branch = @cBranch and fc_sono = @cDocNo;
			update d_sfa..t_domst set fc_sono = @cDocnoTis where  fc_branch = @cBranch and fc_sono = @cDocNo ;

			insert into t_somst select * from d_sfa..t_somst(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;  	
			insert into t_sodtl select * from d_sfa..t_sodtl(NOLOCK) where fc_branch = @cBranch and fc_sono = @cDocNoTis;

			update t_nomor set nDocno = ndocno + 1 where cDocument = 'SOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
			insert into #tempSONO EXEC pr_nomor @cBranch,'SOA',@year,@month;
			select @cDocNoTis  = cDocno from #tempSONO;

			FETCH NEXT FROM cursorvar_optcost INTO @cBranch , @cDocno,@year,@month ;

			END;

		CLOSE cursor_so;
		DEALLOCATE cursor_so;

		-- insert do taking order
		print 'INSERT DO TO';
		DECLARE cursor_doto CURSOR LOCAL STATIC
		FOR select fc_branch,fc_dono,fc_year,fc_periode from d_sfa..t_domst(NOLOCK) where left(fc_dono,2) = 'TO' ;
		OPEN cursor_doto;
			FETCH NEXT FROM cursor_doto INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				insert into #tempDONOTO EXEC pr_nomor @cBranch,'DOA',@year,@month;
				select @cDocNoTis  = cDocno from #tempDONOTO;
				print @cBranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses DO-TO : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_domst set fc_dono = @cDocnoTis where  fc_branch = @cBranch and fc_dono = @cDocNo ;
				update d_sfa..t_dodtl set fc_dono = @cDocnoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;

				insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;  	
				insert into t_dodtl select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

				update t_domst set cStatus = 'R' where cdono = @cDocNoTis and cBranch = @cBranch;
				update t_domst set cStatus = 'P' where cdono = @cDocNoTis and cBranch = @cBranch;
	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'DOA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				FETCH NEXT FROM cursor_doto INTO 
					@cBranch , @cDocno,@year,@month ;
			END;

		CLOSE cursor_doto;
		DEALLOCATE cursor_doto;

		--insert DO & BILL Canvaser
		print 'INSERT DO BILL CAN';
		DECLARE cursor_dobillcan CURSOR LOCAL STATIC
		FOR select fc_branch,fc_dono,fc_year,fc_periode from d_sfa..t_domst(NOLOCK) where left(fc_dono,2) != 'TO' ;
		OPEN cursor_dobillcan;

			FETCH NEXT FROM cursor_dobillcan INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempDONOCAN EXEC pr_nomor @cBranch,'BILLA',@year,@month;
				select @cDocNoTis  = cDOno from #tempDONOCAN;

				print @cbranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses DO & BILL CAN : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_domst set fc_dono = @cDocNoTis,fc_sono = @cDocNoTis where  fc_branch = @cBranch and fc_dono = @cDocNo;
				update d_sfa..t_dodtl set fc_dono = @cDocNoTis  where  fc_branch = @cBranch and fc_dono = @cDocNo;
				insert into t_domst select * from d_sfa..t_domst(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;  	
				insert into t_dodtl select * from d_sfa..t_dodtl(NOLOCK) where fc_branch = @cBranch and fc_dono = @cDocNoTis;

				update t_domst set cStatus = 'R' where cdono = @cDocNoTis;
				update t_domst set cStatus = 'B' where cdono = @cDocNoTis;
				
				update d_sfa..t_billmst set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
				update d_sfa..t_billdtl set fc_docno = @cDocNoTis,fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
				
				insert into t_billmst select * from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
				insert into t_billdtl select * from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
				--EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis	
	
				update d_sfa..t_billmst set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;
				update d_sfa..t_billdtl set fc_docRef = @cDocNoTis  where  fc_branch = @cBranch and fc_docRef = @cDocNo ;

				update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;	

				update d_sfa..t_movebrgmst set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;	
				update d_sfa..t_movebrgdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDoCno;	

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILLA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				
				FETCH NEXT FROM cursor_dobillcan INTO 
					@cBranch , @cDocNo,@year,@month  ;

			END;
		CLOSE cursor_dobillcan;
		DEALLOCATE cursor_dobillcan;

		--insert BILL GALON & MOVE BRG CANVASER
		print 'INSERT BILL GALON DAN MOVEBRG';
		DECLARE cursor_billgalon CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_billmst(NOLOCK) where fc_tipe in ('DG','DI');

		OPEN cursor_billgalon;

			FETCH NEXT FROM cursor_billgalon INTO 
				@cBranch , @cDocNo ,@year,@month;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempBILLGALON EXEC pr_nomor @cBranch,'BILGA',@year,@month;
				select @cDocNoTis  = cDocNo from #tempBILLGALON;
				print @cBranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses BILL GALON + MOVEBRG : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_billmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_billdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;

				insert into t_billmst select * from d_sfa..t_billmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
				insert into t_billdtl select * from d_sfa..t_billdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;
				EXEC pr_posting 'DP','0002',@cBranch,@year,@month,@cDocNoTis;

				update d_sfa..t_movebrgmst set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_movebrgdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo ;

				insert into t_movebrgmst select * from d_sfa..t_movebrgmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
				insert into t_movebrgdtl select * from d_sfa..t_movebrgdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;	

				update d_sfa..t_paymentdtl set fc_docref = @cDocNoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo ;	

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILGA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;
				insert into #tempBILLGALON EXEC pr_nomor '2001','BILGA',@year,@month;
				select @cDocNoTis  = cDocNo from #tempBILLGALON;

				FETCH NEXT FROM cursor_billgalon INTO 
					@cBranch , @cDocNo ,@year,@month;

			END;

		CLOSE cursor_billgalon;
		DEALLOCATE cursor_billgalon;

		-- BANK TIPE BI
		print 'INSERT BANK BI';
		DECLARE cursor_bankbi CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) where fc_tipe = 'BI' ;

		OPEN cursor_bankbi;
			FETCH NEXT FROM cursor_bankbi INTO 
				@cBranch,@cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKBINO EXEC pr_nomor '2001','BANKA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKBINO;

				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses BANK-BI : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_bankmst set fc_docno = @cDocNoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_bankdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;	
				insert into t_bankmst select * from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
				insert into t_bankdtl select * from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;

				update d_sfa..t_bankmst set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
				update d_sfa..t_bankdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_bankbi INTO 
					@cBranch , @cDocNo,@year,@month;

			END;
		CLOSE cursor_bankbi;
		DEALLOCATE cursor_bankbi;

		-- BANK TIPE AR
		print 'INSERT BANK AR';
		DECLARE cursor_bankar CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_bankmst(NOLOCK) where fc_tipe = 'AR';

		OPEN cursor_bankar;
			FETCH NEXT FROM cursor_bankar INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKARNO EXEC pr_nomor @cBranch,'ARDEPA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKARNO;
				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_bankmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_bankdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
				print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
				insert into t_bankmst select * from d_sfa..t_bankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;  	
				insert into t_bankdtl select * from d_sfa..t_bankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;
				EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
				update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	
				
				update d_sfa..t_paymentdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'ARDEPA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_bankar INTO 
					@cBranch , @cDocno,@year,@month ;
					
			END;

		CLOSE cursor_bankar;
		DEALLOCATE cursor_bankar;
		-- PAYMENT
		print 'INSERT PAYMENT';
		DECLARE cursor_payment CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode from d_sfa..t_paymentmst(NOLOCK);

		OPEN cursor_payment;
			FETCH NEXT FROM cursor_payment INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempPAYMENTNO EXEC pr_nomor @cBranch,'RECEIVEA',@year,@month;
				select @cDocNoTis  = cDocno from #tempPAYMENTNO;
				print @cBranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses PAYMENT : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_paymentmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_paymentdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;

				insert into t_paymentmst select * from d_sfa..t_paymentmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;  	
				insert into t_paymentdtl select * from d_sfa..t_paymentdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;
				update t_paymentmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_payment INTO 
					@cBranch , @cDocno,@year,@month ;

			END;

		CLOSE cursor_payment;
		DEALLOCATE cursor_payment;
		-- TAG TURUN
		print 'TAG TURUN';
		DECLARE cursorvar_tagturun CURSOR LOCAL STATIC
		FOR select fc_branch,fc_tagno,fc_year,fc_periode from d_sfa..t_tagmst(NOLOCK) where fc_lokTujuan = '101';

		OPEN cursorvar_tagturun;
			FETCH NEXT FROM cursorvar_tagturun INTO 
				@cBranch , @cDocNo,@year,@month ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
								
				insert into #tempTAGNO EXEC pr_nomor @cBranch,'TAG',@year,@month;
				select @cDocNoTis  = cDocno from #tempTAGNO;

				update d_sfa..t_tagmst set fc_tagno = @cDocNoTis where  fc_branch = @cBranch and fc_tagno = @cDocNo ;
				update d_sfa..t_tagdtl set fc_tagno = @cDocnoTis  where  fc_branch = @cBranch and fc_tagno = @cDocNo;

				insert into t_tagdtl1 
				select 
				a.fc_branch,a.fc_Tagno,b.fc_lokAsal,a.fc_Brgcode,a.fc_Batch,
				RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) ),2 ) cNo
				,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
				from d_sfa..t_tagdtl a
				INNER JOIN d_sfa..t_tagmst b ON a.fc_branch = b.fc_branch AND a.fc_tagno = b.fc_tagno
				where a.fc_Tagno = @cDocNoTis and fc_branch = @cBranch;

				insert into t_tagdtl2
				select 
				a.fc_branch,a.fc_Tagno,a.fc_Gdcode,a.fc_Brgcode,a.fc_Batch,
				RIGHT('0'+CONVERT(VARCHAR,ROW_NUMBER() OVER ( PARTITION BY a.fc_branch,a.fc_tagno ORDER BY a.fc_tagno) + b.add_no )  ,2 ) cNo
				,a.fn_qty1,a.fn_qty2,a.fc_Satcode,a.fc_GL,a.fm_price
				from d_sfa..t_tagdtl a
				left join ( select fc_branch,fc_tagno,count(fc_tagno) add_no from d_sfa..t_tagdtl group by fc_branch,fc_tagno ) b
				on a.fc_branch = b.fc_branch and a.fc_tagno = b.fc_tagno
				where a.fc_Tagno = @cDocNoTis and a.fc_branch = @cBranch;

				insert into t_tagmst 
				select 
				fc_branch,fc_tagno,fc_tipe,fc_year,fc_periode,fc_costctr,
				fc_docref,fd_docdate,fd_postdate,fd_inputdate,'MDTIER' cINputby,
				fd_editdate,'MDTIER' cEditBy,fc_branchasal,fc_lokasal,fc_branchtujuan,
				fc_loktujuan,fc_ket,'R' cStatus,NULL ordertype
				from d_sfa..t_tagmst 
				where fc_tagno = @cDocnoTis and fc_branch = @cBranch;
	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'TAG' and cPeriode = @month and cyear = @year and cBranch = '2001';
				
				FETCH NEXT FROM cursorvar_tagturun INTO 
					@cBranch , @cDocno,@year,@month ;

			END;

		CLOSE cursorvar_tagturun;
		DEALLOCATE cursorvar_tagturun;


	COMMIT tran transaksi;
END TRY
BEGIN CATCH
	ROLLBACK tran transaksi;
	print 'QUERY ERROR, ROLLBACK TRANSACTION';
	THROW;
END CATCH