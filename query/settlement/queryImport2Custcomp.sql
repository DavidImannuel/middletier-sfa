use d_transaksi
DECLARE 
	@nNo INT,@cBranch VARCHAR(10), @vCustcode VARCHAR(30);
DECLARE cursorvar CURSOR
FOR select fc_branch,fc_custcode from [MYSQL]...[db_sfa.t_noo_user];

OPEN cursorvar;

SET @nNo = 0;
FETCH NEXT FROM cursorvar INTO 
    @cBranch , @vCustcode ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	--print  @cBranch +' '+  @vCustcode+' '+cast(@nNO as varchar(100));
	--select top 10 * from t_customercomp
	IF EXISTS ( select * from t_customer where cCustcode = @vCustcode and cBranch = @cBranch )
	begin
		insert into t_customercomp select fc_custcode,fc_compcode,fc_branch from [MYSQL]...[db_sfa.t_noo_user] 
			where fc_code = @vCustcode and fc_branch = @cBranch;
	end

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , 
		@vCustcode ;

END;

CLOSE cursorvar;

DEALLOCATE cursorvar;
