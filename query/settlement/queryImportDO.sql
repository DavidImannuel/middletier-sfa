IF OBJECT_ID('tempdb.dbo.#tempDONO_CAN', 'U') IS NOT NULL
DROP TABLE #tempDONO_CAN; 
-- create table temp
CREATE TABLE #tempDONO_CAN ( cDOno varchar(50) );

use d_transaksi;

DECLARE 
	@year varchar(10),@month varchar(10),
	@cBranch VARCHAR(10), @cDOno VARCHAR(30),@cDOnoTis VARCHAR(30);

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');

insert into #tempDONO_CAN EXEC pr_nomor '2001','BILLA',@year,@month;
select @cDOnoTis  = cDOno from #tempDONO_CAN;

DECLARE cursorvar CURSOR
FOR select fc_branch,fc_dono from [MYSQL]...[db_sfa.t_domst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN';

OPEN cursorvar;

FETCH NEXT FROM cursorvar INTO 
    @cBranch , @cDOno ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	update [MYSQL]...[db_sfa.t_domst] set fc_dono = @cDOnoTis,fc_sono = @cDOnoTis where  fc_branch = @cBranch and fc_dono = @cDOno;
	update [MYSQL]...[db_sfa.t_dodtl] set fc_dono = @cDOnoTis  where  fc_branch = @cBranch and fc_dono = @cDOno;
	update [MYSQL]...[db_sfa.t_billmst] set fc_docno = @cDOnoTis,fc_docRef = @cDOnoTis  where  fc_branch = @cBranch and fc_docno = @cDOno;
	update [MYSQL]...[db_sfa.t_billdtl] set fc_docno = @cDOnoTis,fc_docRef = @cDOnoTis  where  fc_branch = @cBranch and fc_docno = @cDOno;
	update [MYSQL]...[db_sfa.t_paymentdtl] set fc_docref = @cDOnoTis  where  fc_branch = @cBranch and fc_docrefno = @cDOno;	

	update t_nomor set nDocno = ndocno + 1 where cDocument = 'BILLA' and cPeriode = @month and cyear = @year and cBranch = '2001';
	insert into #tempDONO_CAN EXEC pr_nomor '2001','BILLA',@year,@month;
	select @cDOnoTis  = cDOno from #tempDONO_CAN;

	FETCH NEXT FROM cursorvar INTO 
		@cBranch , @cDOno ;

END;

CLOSE cursorvar;
DEALLOCATE cursorvar;



insert into t_domst select a.* from [mysql]...[db_sfa.t_domst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN';

insert into t_dodtl select t1.* from [mysql]...[db_sfa.t_dodtl] t1 where exists ( select * from [mysql]...[db_sfa.t_domst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN' where t1.fc_dono = a.fc_dono );

insert into t_billmst select a.* from [mysql]...[db_sfa.t_billmst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN';

insert into t_billdtl select t1.* from [mysql]...[db_sfa.t_billdtl] t1 where exists (select * from [mysql]...[db_sfa.t_billmst] a
	inner join t_salesoperasionaltype b on a.fc_salescode = b.vSalescode
	inner join t_SalesOperational c on c.vTipeOperasional = b.vTipeOperasional and vGroupOps = 'CAN' where t1.fc_docno = a.fc_docno) ;
