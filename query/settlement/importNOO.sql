use d_transaksi

-- pindahkan t_noo_user ke t_noo (mysql)
insert into [MYSQL]...[db_sfa.t_noo] select * from [MYSQL]...[db_sfa.t_noo_user];
-- hapus temp table jika ada
IF OBJECT_ID('tempdb.dbo.#tempCustNO', 'U') IS NOT NULL
DROP TABLE #tempCustNO; 
-- create table temp
CREATE TABLE #tempCustNO ( cCustno varchar(50) )
DECLARE 	
@custcodetis varchar(20),@fc_custcode varchar(20) ,@fc_custnama varchar(50),@year varchar(4),@month varchar(2)  ;

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');
insert into #tempCustNO EXEC pr_nomor_CustCode '2001','CUST',@year,@month;
-- select * from #tempCustNO
select @custcodetis=cCustno from #tempCustNO;
DECLARE cursor_noo CURSOR
FOR 
select  
fc_custcode ,fc_custnama 
from [MYSQL]...[db_sfa.t_noo_user];
OPEN cursor_noo;

FETCH NEXT FROM cursor_noo INTO 
@fc_custcode ,@fc_custnama ;

WHILE @@FETCH_STATUS = 0
BEGIN

	--print @fc_custcode+' '+@fc_custnama+' '+@custcodetis;
	update [MYSQL]...[db_sfa.t_noo_user] set fc_custcode = @custcodetis where fc_custcode = @fc_custcode;
	update t_nomor set nDocno = nDocno + 1 where cDocument = 'CUST' and cPeriode ='08' and cyear='2020' and cBranch = '2001';

	delete from #tempCustNO;
	insert into #tempCustNO exec pr_nomor_CustCode '2001','CUST',@year,@month;
	select @custcodetis=cCustno from #tempCustNO;

FETCH NEXT FROM cursor_noo INTO 
@fc_custcode ,@fc_custnama ;

END;

CLOSE cursor_noo;

DEALLOCATE cursor_noo;

-- record yg sudah diupdate bisa masuk di t_customer bisa masuk di table " yg berkaitan dengan t_customer

insert into t_customer 
(cCustCode,cNama,cAlamat1,cPropinsi,Ckota,cKecamatan,cDesa, cNegara,cKodepos,
cPhone,cContact,cEmail,cPKP, cInputBy,dInputdate,cSalesCode,cCompcode,cAreacode,
cChanelcode,cDivcode,cDistcode,cCusttagih )
select  
fc_custcode,fc_custnama ,fc_alamat1 ,fc_propinsi ,fc_kota ,fc_kecamatan, 
fc_desa ,fc_negara ,fc_kodepos ,fc_phone ,fc_contact ,fc_email ,fc_pkp ,
'MIDDLETIER',GETDATE() ,fc_userid,fc_compcode ,fc_areacode, 
fc_chanelcode ,fc_divcode ,fc_distcode ,fc_custtagih 
from [MYSQL]...[db_sfa.t_noo_user];

--insert into t_customercomp
--select fc_custcode,fc_compcode,fc_branch
--from [MYSQL]...[db_sfa.t_noo_user];
--insert into t_custtax
--select fc_custcode,fc_custnama,fc_alamat1,fc_npwp,fc_nppkp,getdate(),NULL
--from [MYSQL]...[db_sfa.t_noo_user];
--insert into t_customercoordinate
--select fc_branch,fc_custcode,fc_longtitude,fc_latitude
--from [MYSQL]...[db_sfa.t_noo_user];


