USE d_sfa
-- structure table
CREATE TABLE t_lhpmst (
   [fc_Branch] char(4) NOT NULL,
   [fc_Docno] varchar(40) NOT NULL,
   [fc_Custcode] varchar(15) NOT NULL,
   [fc_Year] char(4) NOT NULL,
   [fc_Periode] char(2) NOT NULL,
   [fd_Docdate] datetime2(0) NOT NULL,
   [fd_Postdate] datetime2(0) NOT NULL,
   [fd_Inputdate] datetime2(0) NOT NULL,
   [fc_Collector] varchar(10) NOT NULL,
   [fm_Netto] float NOT NULL,
   [fm_Pay] float NOT NULL,
   [fc_Status] char(1) NOT NULL DEFAULT '',
   [fc_PayType] varchar(10) NOT NULL,
   [fc_Bank] varchar(15) DEFAULT '',
   [fc_NoRek] varchar(50) DEFAULT '',
   PRIMARY KEY ([fc_Branch],[fc_Docno],[fc_Custcode],[fc_Year],[fc_Periode])
 ) ;
 CREATE TABLE t_lhpdtl (
   [fc_Branch] char(4) NOT NULL,
   [fc_Docno] varchar(40) NOT NULL,
   [fc_Docref] varchar(40) NOT NULL,
   [fc_Tipe] char(20) NOT NULL,
   [fd_Docdate] datetime2(0) NOT NULL,
   [fd_Postdate] datetime2(0) NOT NULL,
   [fd_Inputdate] datetime2(0) NOT NULL,
   [fd_Duedate] datetime2(0) NOT NULL,
   [fm_Netto] float NOT NULL,
   [fm_Pay] float NOT NULL,
   [fc_Status] char(1) NOT NULL DEFAULT '',
   PRIMARY KEY ([fc_Branch],[fc_Docno],[fc_Docref],[fc_Tipe])
 ) ;
CREATE TABLE t_lhpbankmst (
   [fc_compcode] char(4) NOT NULL DEFAULT '',
   [fc_branch] char(4) NOT NULL DEFAULT '',
   [fc_year] char(4) NOT NULL DEFAULT '',
   [fc_periode] char(2) NOT NULL DEFAULT '',
   [fc_docno] char(40) NOT NULL DEFAULT '',
   [fc_tipe] char(2) NOT NULL DEFAULT '',
   [fc_docref] char(40) NOT NULL DEFAULT '',
   [fd_docdate] datetime2(0) NOT NULL DEFAULT '0001-01-01 01:01:01',
   [fd_postdate] datetime2(0) NOT NULL DEFAULT '0001-01-01 01:01:01',
   [fd_inputdate] datetime2(0) NOT NULL DEFAULT '0001-01-01 01:01:01',
   [fc_inputby] varchar(10) NOT NULL DEFAULT '',
   [fc_mu] char(3) NOT NULL DEFAULT '',
   [fm_rate] float NOT NULL DEFAULT '0.000',
   [fc_gltrans] varchar(15) NOT NULL DEFAULT '',
   [fc_cr] char(1) NOT NULL DEFAULT '',
   [fm_value1] float NOT NULL DEFAULT '0.000',
   [fm_value2] float NOT NULL DEFAULT '0.000',
   [fc_note] varchar(50) NOT NULL DEFAULT '',
   [fc_status] char(1) NOT NULL DEFAULT '',
   [fc_tax] char(1) NOT NULL DEFAULT '',
   [fc_udf1] varchar(1) NOT NULL DEFAULT '',
   [fc_udf2] varchar(1) NOT NULL DEFAULT '',
   [fc_udf3] varchar(1) NOT NULL DEFAULT '',
   [fc_udf4] varchar(1) NOT NULL DEFAULT '',
   [fc_udf5] varchar(1) NOT NULL DEFAULT '',
   [fm_udf1] float NOT NULL DEFAULT '0.000',
   [fm_udf2] float NOT NULL DEFAULT '0.000',
   [fm_udf3] float NOT NULL DEFAULT '0.000',
   [fm_udf4] float NOT NULL DEFAULT '0.000',
   [fm_udf5] float NOT NULL DEFAULT '0.000',
   PRIMARY KEY ([fc_compcode],[fc_branch],[fc_year],[fc_periode],[fc_docno])
 ) ;
 CREATE TABLE t_lhpbankdtl (
   [fc_compcode] char(4) NOT NULL DEFAULT '',
   [fc_branch] char(4) NOT NULL DEFAULT '',
   [fc_year] char(4) NOT NULL DEFAULT '',
   [fc_periode] char(2) NOT NULL DEFAULT '',
   [fc_docno] char(40) NOT NULL DEFAULT '',
   [fc_no] char(10) NOT NULL DEFAULT '',
   [fc_docref] char(40) NOT NULL DEFAULT '',
   [fc_bustran] varchar(30) NOT NULL DEFAULT '',
   [fc_suppcode] varchar(10) NOT NULL DEFAULT '',
   [fc_custcode] varchar(15) NOT NULL DEFAULT '',
   [fc_receipt] varchar(10) NOT NULL DEFAULT '',
   [fc_bttipe] char(1) NOT NULL DEFAULT '',
   [fc_tipe] char(2) NOT NULL DEFAULT '',
   [fc_bank] varchar(10) NOT NULL DEFAULT '',
   [fc_giro] varchar(15) NOT NULL DEFAULT '',
   [fc_gltrans] varchar(15) NOT NULL DEFAULT '',
   [fc_glvs] varchar(15) NOT NULL DEFAULT '',
   [fc_cr] char(1) NOT NULL DEFAULT '',
   [fd_duedate] datetime2(0) NOT NULL DEFAULT '0001-01-01 01:01:01',
   [fc_note] varchar(50) NOT NULL DEFAULT '',
   [fc_iocode] char(5) NOT NULL DEFAULT '',
   [fc_costctr] varchar(10) NOT NULL DEFAULT '',
   [fc_mu] char(3) NOT NULL DEFAULT '',
   [fm_value1] float NOT NULL DEFAULT '0.000',
   [fm_value2] float NOT NULL DEFAULT '0.000',
   [fm_sisa1] float NOT NULL DEFAULT '0.000',
   [fm_sisa2] float NOT NULL DEFAULT '0.000',
   [fc_status] char(1) NOT NULL DEFAULT '',
   [fn_kmawal] decimal(20,3) NOT NULL DEFAULT '0.000',
   [fn_kmakhir] decimal(20,3) NOT NULL DEFAULT '0.000',
   [fn_volume] decimal(20,3) NOT NULL DEFAULT '0.000',
   [fc_bbm] varchar(10) NOT NULL DEFAULT '',
   [fc_tujuan] varchar(50) NOT NULL DEFAULT '',
   [fc_sopircode] char(4) NOT NULL DEFAULT '',
   [fc_bankgirocode] varchar(20) NOT NULL DEFAULT '',
   [fc_bankasal] varchar(20) NOT NULL DEFAULT '',
   [fc_giroasal] varchar(15) NOT NULL DEFAULT '',
   [fm_valueasal] float NOT NULL DEFAULT '0.000',
   [fc_docrefbkk] char(12) NOT NULL DEFAULT '',
   [fc_noref] char(2) NOT NULL DEFAULT '',
   PRIMARY KEY ([fc_compcode],[fc_branch],[fc_year],[fc_periode],[fc_docno],[fc_no])
 ) ;
 CREATE TABLE t_lhppaymentmst (
   [fc_compcode] char(4) NOT NULL DEFAULT '',
   [fc_branch] char(4) NOT NULL DEFAULT '',
   [fc_year] char(4) NOT NULL DEFAULT '',
   [fc_periode] char(2) NOT NULL DEFAULT '',
   [fc_docno] char(40) NOT NULL DEFAULT '',
   [fc_docref] char(40) NOT NULL DEFAULT '',
   [fc_tipe] char(1) NOT NULL DEFAULT '',
   [fc_suppcode] varchar(10) NOT NULL DEFAULT '',
   [fc_custcode] varchar(15) NOT NULL DEFAULT '',
   [fd_docdate] datetime2(0) NOT NULL DEFAULT '0001-01-01 01:01:01',
   [fd_postdate] datetime2(0) NOT NULL DEFAULT '0001-01-01 01:01:01',
   [fd_inputdate] datetime2(0) NOT NULL DEFAULT '0001-01-01 01:01:01',
   [fd_inputby] varchar(10) NOT NULL DEFAULT '',
   [fc_mucode] char(3) NOT NULL DEFAULT '',
   [fm_rate] float NOT NULL DEFAULT '0.000',
   [fm_value1] float NOT NULL DEFAULT '0.000',
   [fm_value2] float NOT NULL DEFAULT '0.000',
   [fc_note] varchar(50) NOT NULL DEFAULT '',
   [fc_status] char(1) NOT NULL DEFAULT '',
   [fc_tax] char(1) NOT NULL DEFAULT '',
   [fc_plantbayar] char(4) NOT NULL DEFAULT '',
   PRIMARY KEY ([fc_compcode],[fc_branch],[fc_year],[fc_periode],[fc_docno])
 ) ;
 CREATE TABLE t_lhppaymentdtl (
   [fc_branch] char(4) NOT NULL DEFAULT '',
   [fc_docno] varchar(40) NOT NULL DEFAULT '',
   [fc_docref] varchar(40) NOT NULL DEFAULT '',
   [fc_noref] char(2) NOT NULL DEFAULT '',
   [fc_no] char(3) NOT NULL DEFAULT '',
   [fc_gltrans] varchar(15) NOT NULL DEFAULT '',
   [fc_pk] char(2) NOT NULL DEFAULT '',
   [fc_cr] char(1) NOT NULL DEFAULT '',
   [fc_mu] char(3) NOT NULL DEFAULT '',
   [fm_rate] float NOT NULL DEFAULT '0.000',
   [fm_netto1] float NOT NULL DEFAULT '0.000',
   [fm_netto2] float NOT NULL DEFAULT '0.000',
   [fm_pay1] float NOT NULL DEFAULT '0.000',
   [fm_pay2] float NOT NULL DEFAULT '0.000',
   [fc_status] char(1) NOT NULL DEFAULT '',
   [fc_cashdisc] char(1) NOT NULL DEFAULT '',
   [fm_cashdisc1] float NOT NULL DEFAULT '0.000',
   [fm_cashdisc2] float NOT NULL DEFAULT '0.000',
   PRIMARY KEY ([fc_branch],[fc_docno],[fc_docref],[fc_noref],[fc_no])
 ) ;

 -- insert from mysql to sql server
USE d_sfa;

SELECT * FROM d_tansaksi..t_tagihanrkpmst
SELECT * FROM d_tansaksi..t_tagihanrkpdtl
SELECT * FROM d_tansaksi..t_tagihanmst
SELECT * FROM d_tansaksi..t_tagihandtl



INSERT INTO d_sfa..t_lhpmst 
SELECT * FROM [MYSQL]...[db_sfa.t_lhpmst] WHERE FORMAT(fd_Inputdate,'yyyyMMdd') = FORMAT(CAST('2020-10-10' AS DATE),'yyyyMMdd') AND fm_Pay > 0
INSERT INTO d_sfa..t_lhpdtl
SELECT * FROM [MYSQL]...[db_sfa.t_lhpdtl] WHERE fc_Docno 
IN ( SELECT fc_docno FROM [MYSQL]...[db_sfa.t_lhpmst] WHERE FORMAT(fd_Inputdate,'yyyyMMdd') = FORMAT(CAST('2020-10-10' AS DATE),'yyyyMMdd') AND fm_Pay > 0 ) AND fm_Pay > 0

SELECT * FROM d_sfa..t_lhpmst
SELECT * FROM d_sfa..t_lhpdtl

BEGIN TRAN

SET XACT_ABORT ON;
BEGIN TRY
	
	DELETE FROM d_sfa..t_lhpbankmst;
	DELETE FROM d_sfa..t_lhpbankdtl;
	DELETE FROM d_sfa..t_lhppaymentmst;
	DELETE FROM d_sfa..t_lhppaymentdtl;

	-- bank tipe BI MST
	INSERT INTO d_sfa..t_lhpbankmst
	select 
	'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
	concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'BI' fc_tipe,
	'' fc_docref,format(MAX(a.fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(a.fd_Inputdate) fd_postdate, MAX(a.fd_Inputdate),a.fc_Collector,
	'IDR' fc_MU,'1' fm_Rate,'111101' fc_gltrans,'D' fc_cr,
	SUM(a.fm_Pay) fm_value1,SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus,
	1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
	'0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
	from d_sfa..t_lhpmst(NOLOCK) a 
	where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-10' AS DATE),'yyyyMMdd')
	group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector
	-- bank tipe BI dtl
	INSERT INTO d_sfa..t_lhpbankdtl
	select 
	'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
	concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'01' fc_no,
	'' fc_docref,'CUSTOMER AR/CLEARING' cBusTrans, '' as fc_suppcode, '' as fc_custcode, a.fc_collector as fc_receipt, 'R' as fc_bttipe, 
	'BI' as fc_tipe, '' as fc_bank, '' as fc_giro, '111101' as fc_gltrans, '219301' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
	concat('PENAGIHAN SALES ',b.csalesname) as fc_note, '' as fc_iocode, 
	'' fc_costctr, 'IDR' as fc_mu, 
	SUM(a.fm_Pay) fm_value1, 
	SUM(a.fm_Pay) fm_value2, 
	'0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
	'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
	'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
	from d_sfa..t_lhpmst(NOLOCK) a 
	left join d_transaksi..t_sales b on a.fc_collector = b.csalescode
	where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-10' AS DATE),'yyyyMMdd')
	group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname

	--bankmst tipe ad
	INSERT INTO d_sfa..t_lhpbankmst
	select 
	'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
	concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(fd_Inputdate),'yyyyMMdd') ) fc_docno,'AR' fc_tipe,
	concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
	format(MAX(fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_Inputdate) fd_postdate,MAX(fd_Inputdate) ,a.fc_Collector,
	'IDR' fc_MU,'1' fm_Rate,'219301' fc_gltrans,'D' fc_cr,
	SUM(a.fm_Pay) fm_value1,
	SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus, 1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
	   '0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
	from d_sfa..t_lhpmst a
	where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-10' AS DATE),'yyyyMMdd')
	group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode

	--bankdtl tipe ad
	INSERT INTO d_sfa..t_lhpbankdtl
	select 
	'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
	concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno,'01' fc_no,
	concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
	'' cBusTrans, '' as fc_suppcode, a.fc_Custcode as fc_custcode, a.fc_collector as fc_receipt, 'D' as fc_bttipe, 
	'AR' as fc_tipe, '' as fc_bank, '' as fc_giro, '219301' as fc_gltrans, '219401' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
	concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, '' as fc_iocode, 
	'' fc_costctr, 'IDR' as fc_mu, 
	SUM(a.fm_Pay) fm_value1
	, SUM(a.fm_Pay) fm_value2, 
	CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
		THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
		ELSE 0
	END as fm_sisa1, 
	CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
		THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
		ELSE 0
	END as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
	'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
	'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
	from d_sfa..t_lhpmst a 
	left join d_transaksi..t_sales b on a.fc_collector = b.csalescode
	where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-10' AS DATE),'yyyyMMdd')
	group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode

	-- payment mst
	INSERT INTO d_sfa..t_lhppaymentmst
	Select
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  
   concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
    '' as fc_docref, 
   'D' as fc_tipe, '' fc_suppcode, a.fc_custcode, 
   format(MAX(a.fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_Inputdate) fd_postdate,MAX(fd_Inputdate) fd_Inputdate, 
   a.fc_collector as fd_inputby, 'IDR' as fc_mucode, '1' as fm_rate, 
   SUM(a.fm_Pay) fm_value1, 
   SUM(a.fm_Pay) fm_value2, concat('PENAGIHAN collector ',b.csalesname) as fc_note, 
   'R' as fc_status, '1' as fc_tax, '' as fc_plantbayar
	from d_sfa..t_lhpmst a
	left join d_transaksi..t_sales b on a.fc_collector = b.csalescode and a.fc_branch = b.cbranch
	where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-10' AS DATE),'yyyyMMdd')
	group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode;
	-- paymentdtl ad
	INSERT INTO d_sfa..t_lhppaymentdtl
	Select 
	 a.fc_branch, 
	concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
	concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') )  fc_docref, 
	'' as fc_noref,  '01' as fc_no, 
	'AR'  as fc_gltrans, 
	'' as fc_pk, 'C' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(a.fm_Netto) as fm_netto1, SUM(a.fm_Netto) as fm_netto2, 
	SUM(a.fm_Pay) fm_pay1, 
	SUM(a.fm_Pay) fm_pay2, 
	   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
	from d_sfa..t_lhpmst a
	where format(a.fd_Docdate,'yyyyMMdd') = format(CAST('2020-10-10' AS DATE),'yyyyMMdd')
	group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode;
	--payment dtl bill
	INSERT INTO d_sfa..t_lhppaymentdtl
	Select 
	a.fc_branch, 
	concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
		b.fc_Docref fc_docref, 
	'' as fc_noref,  '01' as fc_no,--c.cNo as fc_no, 
	b.fc_Tipe fc_gltrans, 
	'' as fc_pk, 'D' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(b.fm_netto) as fm_netto1, SUM(b.fm_netto) as fm_netto2, 
   CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
		THEN SUM(b.fm_Netto)
		ELSE SUM(b.fm_pay)
	END as fm_pay1, 
	CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
		THEN SUM(b.fm_Netto)
		ELSE SUM(b.fm_pay)
	END as fm_pay2, 
	   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
	from 
	d_sfa..t_lhpmst a
	left join d_sfa..t_lhpdtl b on a.fc_docno = b.fc_docno and a.fc_branch = b.fc_Branch
	LEFT JOIN d_transaksi..t_billdtl c ON b.fc_Docref = c.cDocno AND b.fc_Branch = c.cBranch
	where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-10' AS DATE),'yyyyMMdd')
	group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode,b.fc_Docref,b.fc_Tipe,c.cNo



END TRY
BEGIN CATCH
	
END CATCH

 
 
 
