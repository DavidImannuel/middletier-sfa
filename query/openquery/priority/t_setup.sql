use d_transaksi;
DECLARE @TSQL varchar(8000), @cBranch char(4);
select @cBranch =CAST( '2001' AS CHAR(4) );
SELECT @TSQL = 'DELETE FROM OPENQUERY(MYSQL,''select * from db_sfa.t_setup where fc_branch = ''''' + @cBranch + ''''''')'
EXEC (@TSQL)
insert into OPENQUERY(MYSQL,'select * from db_sfa.t_setup')
SELECT cCompCode,cBranch,cDesc,cAlamat,cKota,cGLCust,cGLSupp,cGLTitipAR,cGLBarang,cGLInvoice,cAutobill,cAutoGL,cTax,cTelp,cFax,cNoISO,
dTglISO,cNoISOPurc,dTglISOPurc,cFormatPGI,cFormatBill,cFormatCN,cFormatPO,cFormatGR,cFormatBank,cEQTY,cKabag,cRepeatPGI,cHold,cCabang,
cPusat,cRefPO,cTipeSupp,cKHutang,cOnline,cAP,cPPnIn,cPPnOut,cFixPrice,nLimitCust,cFIFO,cGLKurs,cPKKursLebih,cPKKursKurang,cOnlineData,
cPath_Report,cHarga,cOverdue,cNoBillManual,cPrefixBill,nLenBill,cPrefixGalon,nLenGalon,cPrefixJam,nLenJam,cFormatTax,cRepeatBill,ntop,cFormatBillGLN,
cpr,cprdibuat,cprpm,cprdir,cprpurch,cprfax,cDescbarang,cAddcost,cBonus,cBonusTambahan,cDisc,cDeliveryto,cPaymentTagih,nUpdPasswPeriodic,cMultiCOL,
nBlockCustomerEOM,cFormatTTI,cJadwal,mValueMAxCNDN,cMultiLevelDC,cSJGalonFilled,cUserRole,cAuto_ExtraSJ,cAPUnverify,cAPVerify,dLoadBillGlnAutoStart,
cMtomProtect,nCreateDO_minDays,cRefReturLPB,nValueGalon,cLimitGalon,cSet_Harga_BillGalon,cHarga_BillGalon_BE,cHarga_BillGalon_JA,cKasBon,cISO,
cPlantToPlant_Out_Ref,cTransferNoSeri,cIP,cPenjualanTunai,cKasPenjualanTunai,cRefSJ,cM2M_RejectGalon_ref,cAutoSupplier,cHarga_BillGalon,cIncludePPN_PO,cEQTY1,
cRepeatPGI1,cHold1,cCabang1,cPusat1,cRefPO1,cTipeSupp1,cAP1,cOnline1,cPPnIn1,cPPnOut1,cFixPrice1,cFIFO1,cGLKurs1,cPKKursLebih1,cPKKursKurang1,nLimitCust1,cOnlineData1,
cPath_Report1,cHarga1,cOverdue1,cpr1,cprdibuat1,cprpm1,cprdir1,cprpurch1,cprfax1,cRepeatBill1,cAPUnverify1,cAPVerify1,cBonus1,cDisc1,cAddCost1,cDeliveryTo1,cDescBarang1,
cNoBillManual1,cPrefixBill1,nLenBill1,cPrefixGalon1,nLenGalon1,cPrefixJam1,nLenJam1,cFormatTax1,cBonusTambahan1,nTop1,cPaymentTagih1,nUpdPasswPeriodic1,cMultiCOL1,
dLoadBillGlnAutoStart1,cSJGalonFilled1,nBlockCustomerEOM1,cJadwal1,cMultiLevelDC1,mValueMAxCNDN1,cAuto_ExtraSJ1,cUserRole1,cMtomProtect1,nCreateDO_minDays1,cRefReturLPB1,
nValueGalon1,cLimitGalon1,nMasaKPI_DO,cISO1,cKasBon1,cPP_CekStock_auto,dIOKasBonStart,cGdcodeMB,nEdit_KalenderKerja_Allowed,dSerahTerimaSJ_Start,cMaintBan,cGudangCountProduksi,
cRefSJ1,cSalescode_Fix,cAutoReversalSJ,cPeriode_Load_StartPeriode,dRefBkkStart,nBackDate_Retur,cPlantMaintenance_Modul,cProjectManagement_Modul,cPM,mMinCostAsset,nPPC,nKPIAP,
cPointReward,cCekVersion,cRefRSJ,cCustCodeAuto,cTAGApp,cFPS_DG_090,cPaymentAPCab,cBonusValue,cDiscMix,cDiffLongLat,cKasBonNonEkspedisi	 
from t_setup(nolock) where cbranch = @cBranch ;