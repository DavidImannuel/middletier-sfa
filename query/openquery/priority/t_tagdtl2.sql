insert into OPENQUERY(MYSQL,'select * from db_dltisnow.t_tagdtl2')
select a.* from t_tagdtl2 a
inner join 
( select * from t_tagmst (nolock)
where format(dInputdate,'yyyyMMdd') = format(cast(:date as date),'yyyyMMdd') 
and cStatus = 'R' and cbranch = :branch and cLoktujuan != '101'
) b
on a.cTagno = b.cTagno and a.cbranch = b.cbranch
left join OPENQUERY(MYSQL,'select * from db_dltisnow.t_tagdtl2') c
on 
a.cbranch = c.fc_branch and a.cTagno = c.fc_tagno and
a.cGdcode = c.fc_gdcode and a.cBrgcode = c.fc_brgcode and
a.cBatch = c.fc_batch and a.cNo = c.fc_no 
where c.fc_branch is null and c.fc_tagno is null and 
c.fc_gdcode is null and c.fc_brgcode is null and c.fc_batch is null
and c.fc_no is null