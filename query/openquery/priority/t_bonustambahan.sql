use d_transaksi;
insert into OPENQUERY(MYSQL,'select * from db_sfa.t_bonuscustambahan')
select * from t_bonuscustambahan a
where not exists (
   select * from OPENQUERY(MYSQL,'select * from db_sfa.t_bonuscustambahan') b
   where fc_custcode = ccustcode and fc_brgcode = cBrgcode2 and 
         fc_status = 0 and CONVERT(char(8),a.dValidUntil,112)>=CONVERT(char(8),GETDATE(),112)
);