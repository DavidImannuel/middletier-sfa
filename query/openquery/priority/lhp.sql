use d_transaksi
DECLARE 
	@cBranch VARCHAR(10), @cDocno VARCHAR(30);
DECLARE cursorvar_lhp CURSOR
FOR select cBranch,cDocno from t_tagihanrkpmst WHERE CONVERT(CHAR(8),dDocdate,112) = CONVERT(CHAR(8),CAST( :date AS DATE),112) AND cStatus != 'X' and cBranch = :branch ;

OPEN cursorvar_lhp;

FETCH NEXT FROM cursorvar_lhp INTO 
    @cBranch , @cDocno ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	IF EXISTS( select 
		a.cBranch,concat(a.cDocno,'/',c.cDocno) cDocno,c.ccustcode,a.cYear,a.cPeriode,a.dDocdate,
		c.dPOstdate,c.dinputdate,a.cCollector,c.msisacn,'0',
		'F','',c.cBank,c.cNorek
		from t_tagihanrkpmst a
		left join t_tagihanrkpdtl b on a.cDocno = b.cDocno and a.cbranch = b.cbranch
		left join t_tagihanmst c on c.cDocno = b.cDocref and a.cbranch = c.cbranch
		left join OPENQUERY(MYSQL,'select * from db_sfa.t_lhpmst') d on concat(a.cDocno,'/',c.cDocno) = d.fc_docno and a.cbranch = d.fc_branch
		where  d.fc_docno is null and a.cBranch = @cBranch AND c.mSisaCN != 0 AND a.cDocno = @cDocno
	)
	BEGIN
		insert into OPENQUERY(MYSQL,'select * from db_sfa.t_lhpmst')
		select 
		a.cBranch,concat(a.cDocno,'/',c.cDocno) cDocno,c.ccustcode,a.cYear,a.cPeriode,a.dDocdate,
		a.dPostdate,a.dinputdate,a.cCollector,c.msisacn,'0',
		'F','',c.cBank,c.cNorek
		from t_tagihanrkpmst a
		left join t_tagihanrkpdtl b on a.cDocno = b.cDocno and a.cbranch = b.cbranch
		left join t_tagihanmst c on c.cDocno = b.cDocref and a.cbranch = c.cbranch
		left join OPENQUERY(MYSQL,'select * from db_sfa.t_lhpmst') d on concat(a.cDocno,'/',c.cDocno) = d.fc_docno and a.cbranch = d.fc_branch
		where  d.fc_docno is null and a.cBranch = @cBranch AND c.mSisaCN != 0 AND a.cDocno = @cDocno;

		insert into OPENQUERY(MYSQL,'select * from db_sfa.t_lhpdtl') 
		select 
		a.cBranch,concat(a.cDocno,'/',c.cDocno) cDOcno,d.cDocref,d.cTipe,d.dDocdate,d.dPostdate,
		d.dInputdate,d.dDuedate,d.mSisaCN,'0','F'
		from t_tagihanrkpmst a
		left join t_tagihanrkpdtl b on a.cDocno = b.cDocno and a.cbranch = b.cbranch
		left join t_tagihanmst c on c.cDocno = b.cDocref and c.cBranch = a.cbranch
		left join t_tagihandtl d on c.cDocno = d.cDocno and d.cbranch = c.cbranch
		left join OPENQUERY(MYSQL,'select * from db_sfa.t_lhpdtl') e on concat(a.cDocno,'/',c.cDocno) = e.fc_docno and a.cbranch = e.fc_branch
		where e.fc_docno is null and a.cBranch = @cBranch AND c.mSisaCN != 0 AND a.cDocno = @cDocno;

		UPDATE t_tagihanrkpmst SET cStatus = 'X' WHERE cDocno = @cDocno AND cBranch = @cBranch;
	END

	FETCH NEXT FROM cursorvar_lhp INTO 
		@cBranch , @cDocno ;
END;

CLOSE cursorvar_lhp;

DEALLOCATE cursorvar_lhp;

