DECLARE @OutputFile NVARCHAR(100) ,

    @FilePath NVARCHAR(100) ,

    @bcpCommand NVARCHAR(1000)

SET @bcpCommand = 'bcp "SELECT * FROM sys.objects" queryout '

SET @FilePath = '"C:\'

SET @OutputFile = 'FileName_1.txt"'

SET @bcpCommand = @bcpCommand + @FilePath + @OutputFile + ' -T -t"," -S "SERVER NAME" -c'

print @bcpCommand

EXEC master..xp_cmdshell @bcpCommand



-- Make sure xp_cmdshell is enabled otherwise run this:


exec sp_configure 'show advanced option',1
reconfigure

exec sp_configure  xp_cmdshell,1
reconfigure