use d_transaksi
DECLARE 
    @group VARCHAR(10), 
	@tipe VARCHAR(10),
	@desc varchar(75);

DECLARE cursor_salesopr CURSOR
FOR SELECT 
        *
    FROM 
        t_SalesOperational;

OPEN cursor_salesopr;

FETCH NEXT FROM cursor_salesopr INTO 
    @group, 
    @tipe,
	@desc;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	IF EXISTS (
            SELECT *
            FROM [MYSQL]...[db_dltisnow.t_salesoperational]
            WHERE fc_tipeoperasional = @tipe
            )
    BEGIN
        UPDATE [MYSQL]...[db_dltisnow.t_salesoperational]
        SET fc_groupops = @group,
			fc_desc = @desc
        WHERE fc_tipeoperasional = @tipe
    END
    ELSE
    BEGIN
        INSERT INTO [MYSQL]...[db_dltisnow.t_salesoperational]
        select vGroupOps,vTipeOperasional,cDesc from t_salesoperational
		WHERE vTipeOperasional = @tipe
    END        

	FETCH NEXT FROM cursor_salesopr INTO 
		@group, 
		@tipe,
		@desc;

END;

CLOSE cursor_salesopr;

DEALLOCATE cursor_salesopr;