-- insert db_sfa mysql to sql server
DECLARE 
	@branch CHAR(4),
	@processDate DATE ;

SELECT @processdate = cast( '2020-10-23' as date);
SELECT @branch = CAST( '2001' AS CHAR )

use d_sfa;
delete from d_sfa..t_lhpmst;
delete from d_sfa..t_lhpdtl;

-- lHP
DELETE FROM d_sfa..t_lhpmst;
DELETE FROM d_sfa..t_lhpdtl;
INSERT INTO d_sfa..t_lhpmst 
SELECT * FROM [MYSQL]...[db_sfa.t_lhpmst] WHERE FORMAT(fd_Inputdate,'yyyyMMdd') = FORMAT(cast('2020-10-23' as DATE),'yyyyMMdd') ;
INSERT INTO d_sfa..t_lhpdtl
SELECT * FROM [MYSQL]...[db_sfa.t_lhpdtl] WHERE fc_Docno 
IN ( SELECT fc_docno FROM [MYSQL]...[db_sfa.t_lhpmst] WHERE FORMAT(fd_Inputdate,'yyyyMMdd') = FORMAT(cast('2020-10-23' as DATE),'yyyyMMdd') ) ;

-- split lhp
DELETE FROM d_sfa..t_lhpbankmst;
DELETE FROM d_sfa..t_lhpbankdtl;
DELETE FROM d_sfa..t_lhppaymentmst;
DELETE FROM d_sfa..t_lhppaymentdtl;

-- bank tipe BI MST
INSERT INTO d_sfa..t_lhpbankmst
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'BI' fc_tipe,
'' fc_docref,format(MAX(a.fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(a.fd_Inputdate) fd_postdate, MAX(a.fd_Inputdate),a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'111101' fc_gltrans,'D' fc_cr,
SUM(a.fm_Pay) fm_value1,SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus,
1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
'0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from d_sfa..t_lhpmst(NOLOCK) a 
where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-23' AS DATE),'yyyyMMdd') and a.fm_pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector
-- bank tipe BI dtl
INSERT INTO d_sfa..t_lhpbankdtl
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd')) fc_docno,'01' fc_no,
'' fc_docref,'CUSTOMER AR/CLEARING' cBusTrans, '' as fc_suppcode, '' as fc_custcode, a.fc_collector as fc_receipt, 'R' as fc_bttipe, 
'BI' as fc_tipe, '' as fc_bank, '' as fc_giro, '111101' as fc_gltrans, '219301' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN SALES ',b.csalesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, 
SUM(a.fm_Pay) fm_value1, 
SUM(a.fm_Pay) fm_value2, 
'0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from d_sfa..t_lhpmst(NOLOCK) a 
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode
where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-23' AS DATE),'yyyyMMdd') and a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname

--bankmst tipe ad
INSERT INTO d_sfa..t_lhpbankmst
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(fd_Inputdate),'yyyyMMdd') ) fc_docno,'AR' fc_tipe,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
format(MAX(fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_Inputdate) fd_postdate,MAX(fd_Inputdate) ,a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'219301' fc_gltrans,'D' fc_cr,
SUM(a.fm_Pay) fm_value1,
SUM(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus, 1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
	'0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from d_sfa..t_lhpmst a
where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-23' AS DATE),'yyyyMMdd') and a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode

--bankdtl tipe ad
INSERT INTO d_sfa..t_lhpbankdtl
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno,'01' fc_no,
concat('BN-LHP',a.fc_collector,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docref,
'' cBusTrans, '' as fc_suppcode, a.fc_Custcode as fc_custcode, a.fc_collector as fc_receipt, 'D' as fc_bttipe, 
'AR' as fc_tipe, '' as fc_bank, '' as fc_giro, '219301' as fc_gltrans, '219401' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN COLLECTOR ',b.csalesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, 
SUM(a.fm_Pay) fm_value1
, SUM(a.fm_Pay) fm_value2, 
CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
	THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
	ELSE 0
END as fm_sisa1, 
CASE WHEN SUM(a.fm_pay) > SUM(a.fm_Netto) 
	THEN SUM(a.fm_Netto) - SUM(a.fm_pay)
	ELSE 0
END as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from d_sfa..t_lhpmst a 
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode
where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-23' AS DATE),'yyyyMMdd') and a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode

-- payment mst
INSERT INTO d_sfa..t_lhppaymentmst
Select
'0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
'' as fc_docref, 
'D' as fc_tipe, '' fc_suppcode, a.fc_custcode, 
format(MAX(a.fd_Inputdate),'yyyy-MM-dd 00:00:00') fd_docdate,MAX(fd_Inputdate) fd_postdate,MAX(fd_Inputdate) fd_Inputdate, 
a.fc_collector as fd_inputby, 'IDR' as fc_mucode, '1' as fm_rate, 
SUM(a.fm_Pay) fm_value1, 
SUM(a.fm_Pay) fm_value2, concat('PENAGIHAN collector ',b.csalesname) as fc_note, 
'R' as fc_status, '1' as fc_tax, '' as fc_plantbayar
from d_sfa..t_lhpmst a
left join d_transaksi..t_sales b on a.fc_collector = b.csalescode and a.fc_branch = b.cbranch
where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-23' AS DATE),'yyyyMMdd') and a.fm_pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,b.cSalesname,a.fc_Custcode;
-- paymentdtl ad
INSERT INTO d_sfa..t_lhppaymentdtl
Select 
	a.fc_branch, 
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
concat('AD-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') )  fc_docref, 
'' as fc_noref,  '01' as fc_no, 
'AR'  as fc_gltrans, 
'' as fc_pk, 'C' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(a.fm_Pay) as fm_netto1, SUM(a.fm_Pay) as fm_netto2, 
SUM(a.fm_Pay) fm_pay1, 
SUM(a.fm_Pay) fm_pay2, 
	'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from d_sfa..t_lhpmst a
where format(a.fd_Docdate,'yyyyMMdd') = format(CAST('2020-10-23' AS DATE),'yyyyMMdd') AND a.fm_Pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode;
--payment dtl bill
INSERT INTO d_sfa..t_lhppaymentdtl
Select 
a.fc_branch, 
concat('REC-LHP',a.fc_Collector,a.fc_Custcode,format(MAX(a.fd_Inputdate),'yyyyMMdd') ) fc_docno, 
	b.fc_Docref fc_docref, 
'' as fc_noref,  '01' fc_no, 
b.fc_Tipe fc_gltrans, 
'' as fc_pk, 'D' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(b.fm_netto) as fm_netto1, SUM(b.fm_netto) as fm_netto2, 
CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
	THEN SUM(b.fm_Netto)
	ELSE SUM(b.fm_pay)
END as fm_pay1, 
CASE WHEN SUM(b.fm_pay) > SUM(b.fm_Netto) 
	THEN SUM(b.fm_Netto)
	ELSE SUM(b.fm_pay)
END as fm_pay2, 
	'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from 
d_sfa..t_lhpmst a
left join d_sfa..t_lhpdtl b on a.fc_docno = b.fc_docno and a.fc_branch = b.fc_Branch
where format(a.fd_docdate,'yyyyMMdd') = format(CAST('2020-10-23' AS DATE),'yyyyMMdd') and b.fm_pay > 0
group by a.fc_Branch,a.fc_Year,a.fc_Periode,a.fc_Collector,a.fc_Custcode,b.fc_Docref,b.fc_Tipe


--SELECT * FROM d_sfa..t_lhpbankmst
--SELECT * FROM d_sfa..t_lhpbankdtl
--SELECT * FROM d_sfa..t_lhppaymentmst
--SELECT * FROM d_sfa..t_lhppaymentdtl



SET XACT_ABORT ON;

BEGIN TRY
	use d_transaksi;
	BEGIN tran transaksi
		-- BANK BI NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBANKBINO', 'U') IS NOT NULL
		DROP TABLE #tempBANKBINO; 
		CREATE TABLE #tempBANKBINO ( cDocno varchar(50) );
		-- BANK AR NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempBANKARNO', 'U') IS NOT NULL
		DROP TABLE #tempBANKARNO; 
		CREATE TABLE #tempBANKARNO ( cDocno varchar(50) );
		-- PAYMENT NO TEMP
		IF OBJECT_ID('tempdb.dbo.#tempPAYMENTNO', 'U') IS NOT NULL
		DROP TABLE #tempPAYMENTNO; 
		CREATE TABLE #tempPAYMENTNO ( cDocno varchar(50) );

		SELECT * FROM #tempBANKBINO;
		SELECT * FROM #tempBANKARNO;
		SELECT * FROM #tempPAYMENTNO; 

	DECLARE 
		@year varchar(10),@month varchar(10),@cBranch VARCHAR(10), 
		@cDocNo VARCHAR(50),-- no temp dari sfa
		@cDocNoTis VARCHAR(50),-- menyimpan no hasil dari insert ant exec pr_nomro ke temp table 
		@cSalescode VARCHAR(50)
		;

		-- LHP BANK TIPE BI
		print 'INSERT BANK BI';
		DECLARE cursor_lhpbankbi CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode,fc_inputby from d_sfa..t_lhpbankmst(NOLOCK) where fc_tipe = 'BI' ;

		OPEN cursor_lhpbankbi;
			FETCH NEXT FROM cursor_lhpbankbi INTO 
				@cBranch,@cDocNo,@year,@month,@cSalescode ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKBINO EXEC pr_nomor @cBranch,'BANKA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKBINO;

				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses LHP BANK-BI : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_lhpbankmst set fc_docno = @cDocNoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_lhpbankdtl set fc_docno = @cDocNoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;	
				insert into t_bankmst select * from d_sfa..t_lhpbankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;  	
				insert into t_bankdtl select * from d_sfa..t_lhpbankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis;

				EXEC pr_glhis 'BANK','0002',@cBranch,@year,@month,@cDocNoTis;
				update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;
				
				update d_sfa..t_lhpbankmst set fc_docref = @cDocnoTis where  fc_branch = @cBranch and fc_docref = @cDocNo ;
				update d_sfa..t_lhpbankdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
	
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'BANKA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_lhpbankbi INTO 
					@cBranch , @cDocNo,@year,@month,@cSalescode;

			END;
		CLOSE cursor_lhpbankbi;
		DEALLOCATE cursor_lhpbankbi;

		-- LHP BANK TIPE AR
		print 'INSERT LHP BANK AR';
		DECLARE cursor_lhpbankar CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode,fc_inputby from d_sfa..t_lhpbankmst(NOLOCK) where fc_tipe = 'AR';

		OPEN cursor_lhpbankar;
			FETCH NEXT FROM cursor_lhpbankar INTO 
				@cBranch , @cDocNo,@year,@month,@cSalescode;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				insert into #tempBANKARNO EXEC pr_nomor @cBranch,'ARDEPA',@year,@month;
				select @cDocNoTis  = cDocno from #tempBANKARNO;
				print @cBranch+' '+@cDocNo+' '+@year+' '+@month;
				print 'proses BANK-AR : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_lhpbankmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_lhpbankdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;
				insert into t_bankmst select * from d_sfa..t_lhpbankmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;  	
				insert into t_bankdtl select * from d_sfa..t_lhpbankdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocnoTis;

				IF NOT EXISTS (SELECT * FROM d_transaksi..t_bankmst WHERE cBranch = @cBranch AND cDocno = @cDocNoTis) 
				BEGIN
					THROW 51000, 'The record does not exist.', 1;  
				END

				EXEC pr_glhis 'ARDEP','0002',@cBranch,@year,@month,@cDocNoTis;
				update t_bankmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	
				update d_sfa..t_lhppaymentdtl set fc_docref = @cDocnoTis  where  fc_branch = @cBranch and fc_docref = @cDocNo;
				update t_nomor set nDocno = ndocno + 1 where cDocument = 'ARDEPA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_lhpbankar INTO 
					@cBranch , @cDocno,@year,@month,@cSalescode;
					
			END;

		CLOSE cursor_lhpbankar;
		DEALLOCATE cursor_lhpbankar;
		-- LHP PAYMENT
		print 'INSERT LHP PAYMENT';
		DECLARE cursor_lhppayment CURSOR LOCAL STATIC
		FOR select fc_branch,fc_docno,fc_year,fc_periode,fd_inputby from d_sfa..t_lhppaymentmst(NOLOCK);

		OPEN cursor_lhppayment;
			FETCH NEXT FROM cursor_lhppayment INTO 
				@cBranch , @cDocNo,@year,@month,@cSalescode;

			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				insert into #tempPAYMENTNO EXEC pr_nomor @cBranch,'RECEIVEA',@year,@month;
				select @cDocNoTis  = cDocno from #tempPAYMENTNO;
				print @cBranch+' '+@cDocno+' '+@year+' '+@month;
				print 'proses PAYMENT : '+@cDocno+' NO TIS : '+@cDocnoTis;
				update d_sfa..t_lhppaymentmst set fc_docno = @cDocnoTis where  fc_branch = @cBranch and fc_docno = @cDocNo ;
				update d_sfa..t_lhppaymentdtl set fc_docno = @cDocnoTis  where  fc_branch = @cBranch and fc_docno = @cDocNo;

				insert into t_paymentmst select * from d_sfa..t_lhppaymentmst(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;  	
				insert into t_paymentdtl select * from d_sfa..t_lhppaymentdtl(NOLOCK) where fc_branch = @cBranch and fc_docno = @cDocNoTis and fc_branch = @cBranch;

				update t_paymentmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocnoTis;	

				update t_nomor set nDocno = ndocno + 1 where cDocument = 'RECEIVEA' and cPeriode = @month and cyear = @year and cBranch = @cBranch;

				FETCH NEXT FROM cursor_lhppayment INTO 
					@cBranch , @cDocno,@year,@month,@cSalescode;

			END;

		CLOSE cursor_lhppayment;
		DEALLOCATE cursor_lhppayment;

		-- update tagihan rekap jadi R
		print 'UBAH REKAP LHP ';
		DECLARE cursor_lhprkp CURSOR LOCAL STATIC
		FOR SELECT fc_branch,SUBSTRING(fc_docno,0,CHARINDEX('/',fc_docno)) RkpNo FROM d_sfa..t_lhpmst(NOLOCK)
		GROUP BY fc_branch,SUBSTRING(fc_docno,0,CHARINDEX('/',fc_docno))

		OPEN cursor_lhprkp;
			FETCH NEXT FROM cursor_lhprkp INTO 
				@cBranch , @cDocNo ;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				update t_tagihanrkpmst set cStatus = 'R' where cBranch = @cBranch and cDocNo = @cDocno;	

				FETCH NEXT FROM cursor_lhprkp INTO 
					@cBranch , @cDocno ;

			END;

		CLOSE cursor_lhprkp;
		DEALLOCATE cursor_lhprkp;


	COMMIT tran transaksi;
END TRY
BEGIN CATCH
	ROLLBACK tran transaksi;
	print 'QUERY ERROR, ROLLBACK TRANSACTION';
	THROW;
END CATCH