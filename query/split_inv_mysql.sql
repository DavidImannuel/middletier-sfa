delete from t_sodtl where fc_sono in (
select fc_sono from t_somst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d'));
delete from t_somst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d');


delete from t_dodtl where fc_dono in (
select fc_dono from t_domst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d'));
delete from t_domst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d');

delete from t_billdtl where fc_docno in (
select fc_docno from t_billmst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d'));
delete from t_billmst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d');

delete from t_movebrgdtl where fc_docno in (
select fc_docno from t_movebrgmst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d'));
delete from t_movebrgmst where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d');

#SO Jika Ada
INSERT INTO t_somst
  (fc_Branch, fc_SONo, fc_Year, fc_Periode, fd_Docdate, fd_Postdate, fd_Inputdate, 
   fd_Delvdate, fc_Inputby, fc_tipe, fc_POSO, fn_TOP, fc_Salescode, fc_CompCode, 
   fc_Areacode, fc_Chanelcode, fc_DivCode, fc_DistCode, fc_MU, fm_Rate, 
   fc_Custbayar, fc_CustKrm, fc_Gdcode, fc_keterangan, fn_disc1, fn_disc2, 
   fm_Brutto1, fm_Disc1, fm_Netto1, fm_DPP1, fm_PPn1, fm_Brutto2, fm_Disc2, 
   fm_Netto2, fm_DPP2, fm_PPn2, fc_AppBy, fd_Appdate, fc_Tax, fc_Status, 
   fc_TipeGM, fc_SJRef, fc_AppBy1, fd_Appdate1, fc_PreSONo)
Select 
a.fc_branch , concat('SO',a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) ,   
a.fc_year , date_format(a.fd_inputdate,'%m') , a.fd_docdate , a.fd_postdate , a.fd_inputdate , a.fd_inputdate as fd_delvdate, 
a.fc_Inputby , 
case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end , '' as fc_POSO , 
'1' as fn_top , a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , a.fc_mu , a.fm_rate , 
a.fc_custbayar , a.fc_CustKrm , a.fc_Gdcode , 'SFA SAlES ORDER' as fc_keterangan , 
SUM(b.fn_disc1) , SUM(b.fn_disc2) , SUM(b.fm_brutto1) , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) ,
 SUM(b.fm_dpp1) , SUM(b.fm_ppn1) ,
SUM(b.fm_brutto1) as fm_brutto2 , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'' as fc_appby , sysdate() as fd_appdate ,  '1' as fc_tax , 'D' as fc_status , '601' as fc_tipeGM , '' as fc_sjref , 
'' as fc_appby1 , sysdate() as fd_appdate1 , '' as fc_presono 
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and b.fc_type = 'Normal' 
and a.fc_tipe = 'TO'
group by a.fc_branch , a.fc_invno , b.fc_type;

insert into t_domst 
(fc_branch , fc_dono , fc_sono , fc_year , fc_periode , fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_tipe , 
fn_top , fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode , fc_mu , fm_rate , 
fc_custbayar , fc_CustKrm , fc_Gdcode , fc_keterangan , fn_disc1 , fn_disc2 , fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_appby , fd_appdate , fc_postgoods , fc_tax , fc_status , fc_tipeGM , fc_sjref)
Select 
a.fc_branch , concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) , 
concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
		 else 'DR'
end ) ,  
a.fc_year , date_format(a.fd_inputdate,'%m') , a.fd_docdate , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end , 
'1' as fn_top , a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , a.fc_mu , a.fm_rate , 
a.fc_custbayar , a.fc_CustKrm , a.fc_Gdcode , a.fc_keterangan , 
SUM(b.fn_disc1) , SUM(b.fn_disc2) , SUM(b.fm_brutto1) , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) ,
 SUM(b.fm_dpp1) , SUM(b.fm_ppn1) ,
SUM(b.fm_brutto1) as fm_brutto2 , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'YUDHA' as fc_appby , sysdate() as fd_appdate , a.fc_postgoods , '1' as fc_tax , 'P' as fc_status , '' as fc_tipeGM , '' as fc_sjref 
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and b.fc_type = 'Normal' and a.fc_tipe = 'TO'
group by a.fc_branch , a.fc_invno , b.fc_type;

#SJ
insert into t_domst 
(fc_branch , fc_dono , fc_sono , fc_year , fc_periode , fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_tipe , 
fn_top , fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode , fc_mu , fm_rate , 
fc_custbayar , fc_CustKrm , fc_Gdcode , fc_keterangan , fn_disc1 , fn_disc2 , fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_appby , fd_appdate , fc_postgoods , fc_tax , fc_status , fc_tipeGM , fc_sjref)
Select 
a.fc_branch , concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) , 
concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
		 else 'DR'
end ) ,  
a.fc_year , date_format(a.fd_inputdate,'%m') , a.fd_docdate , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end , 
'1' as fn_top , a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , a.fc_mu , a.fm_rate , 
a.fc_custbayar , a.fc_CustKrm , a.fc_Gdcode , a.fc_keterangan , 
SUM(b.fn_disc1) , SUM(b.fn_disc2) , SUM(b.fm_brutto1) , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) ,
 SUM(b.fm_dpp1) , SUM(b.fm_ppn1) ,
SUM(b.fm_brutto1) as fm_brutto2 , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'YUDHA' as fc_appby , sysdate() as fd_appdate , a.fc_postgoods , '1' as fc_tax , 'B' as fc_status , '' as fc_tipeGM , '' as fc_sjref 
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and b.fc_type = 'Normal' and a.fc_tipe = 'Normal'
group by a.fc_branch , a.fc_invno , b.fc_type;

insert into t_billmst 
(fc_branch , fc_docno , fc_docref , fc_year , fc_periode , 
fc_tipe , 
fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode ,  
fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_ppn , fd_duedate , 
fc_custcode , fc_suppcode , fc_MUcode , fm_rate , fn_top , fc_keterangan , 
fn_disc1 , fn_disc2 , 
fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_tax , fc_return , fc_status , fc_sopircode)
Select 
a.fc_branch , concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) , 
concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
		 else 'DR'
end ) as docref , 
a.fc_year , date_format(a.fd_inputdate,'%m') , 
case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end as fc_docref, 
a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , 
a.fd_docdate , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
'1' as fc_ppn , sysdate() as fd_duedate , 
a.fc_custcode as fc_custcode , '' as fc_suppcode , 'IDR' as fc_MU , '1' as fm_rate , '1' as fn_top , a.fc_keterangan , 
SUM(b.fn_disc1) , SUM(b.fn_disc2) , SUM(b.fm_brutto1) , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) ,
SUM(b.fm_dpp1) , SUM(b.fm_ppn1) ,
SUM(b.fm_brutto1) as fm_brutto2 , SUM(b.fm_disc1_1 + b.fm_disc1_2) , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'1' as fc_tax , '' as fc_return ,  'R' as fc_status , '' as fc_sopircode
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and b.fc_type in ('Normal','Galon Jamin','Galon Beli') and a.fc_tipe in ('Normal','Galon Jamin','Galon Beli')
group by a.fc_branch , a.fc_invno , b.fc_type;


INSERT INTO t_movebrgmst
  (fc_Branch, fc_Docno, fc_Year, fc_Periode, fc_Tipe, fc_DocRef, fd_Docdate, 
   fd_Postdate, fd_Inputdate, fc_InputBy, fc_NoSJ, fc_NoExpd, fc_NoCont, 
   fc_PPN, fd_DueDate, fc_Custcode, fc_Suppcode, fc_MuCode, fm_Rate, fn_TOP, 
   fc_GdFrom, fc_GdDest, fc_Keterangan, fn_Disc1, fn_Disc2, fm_Brutto1, 
   fm_Netto1, fm_Invoice1, fm_Disc1, fm_DPP1, fm_PPn1, fm_Brutto2, fm_Netto2, 
   fm_Invoice2, fm_Disc2, fm_DPP2, fm_PPn2, fc_Status, fc_Tcode)
Select
   '2001' as fc_Branch, 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
  end )as fc_Docno, a.fc_Year, a.fc_Periode,
  case when b.fc_type = 'Galon Beli' then 'DG'  
       when b.fc_type = 'Galon Jamin' then 'DI'
       else 'DR'
  end as fc_Tipe, 
  concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
		 else 'DR'
end )as fc_DocRef , a.fd_Docdate, a.fd_Postdate, a.fd_Inputdate, fc_salescode fc_InputBy, 
  '' as fc_NoSJ, '' as fc_NoExpd, '' as fc_NoCont, '1' as fc_PPN, a.fd_inputdate as fd_DueDate, a.fc_Custcode, 
  '' as fc_Suppcode, 'IDR' as fc_MuCode, 
   '1' as fm_Rate, '1' as fn_TOP, '' as fc_GdFrom, '' as fc_GdDest, 'SFA' as fc_Keterangan, '0' as fn_Disc1, 
   '0' as fn_Disc2, SUM(b.fm_Brutto1), SUM(b.fm_Netto1), '0' as fm_Invoice1, SUM(b.fm_Disc1_1 + b.fm_disc1_2), SUM(b.fm_DPP1), 
   SUM(a.fm_PPn1), SUM(b.fm_Brutto1) as fm_brutto2, SUM(b.fm_Netto1) as fm_netto2, 
   '0' as fm_invoice2 , SUM(b.fm_Disc1_1 + b.fm_disc1_2) as fm_disc2 , SUM(b.fm_DPP1) as fm_dpp2, SUM(a.fm_PPn1) as fm_ppn2, 
    'R' as fc_Status, '' as fc_Tcode
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
where date_format(fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and b.fc_type in ('Galon Jamin','Galon Beli') and a.fc_tipe in ('Normal' , 'DO')
group by a.fc_branch , a.fc_invno , b.fc_type;

INSERT INTO t_dodtl
  (fc_Branch, fc_DOno, fc_BrgCode, fc_Batch, fc_No, fc_Extra, fn_qty1, 
   fn_qty2, fc_Satcode, fn_Extra1, fn_Extra2, fc_GdCode, fn_Harga1, fn_Harga2, 
   fn_Disc1, fn_Disc2, fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, 
   fm_PPN1, fm_Disc2_1, fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPN2, 
   fc_ReasonRtr)

Select
   b.fc_Branch, concat(b.fc_invno,'-',
   case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) , b.fc_BrgCode, '1' as fc_Batch, b.fc_No, '1' as fc_Extra, fn_qty - fn_qty_return as fn_qty1, 
   (fn_qty - fn_qty_return) * coalesce(c.fn_qty2,0) as fn_qty2, b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2, b.fc_GdCode, b.fn_Harga, 
   coalesce(b.fn_harga / c.fn_qty2 ,0) as fn_Harga2, b.fn_Disc1, b.fn_Disc2, b.fm_Disc1_1, b.fm_Disc1_2, b.fm_Brutto1, 
   b.fm_Netto1, b.fm_DPP1, b.fm_PPN1,  b.fm_Disc1_1 as fm_Disc2_1,  b.fm_Disc1_2 as fm_Disc2_2, b.fm_Brutto1 as fm_Brutto2,
   b.fm_Netto1 as fm_Netto2, b.fm_DPP1 as fm_DPP2, b.fm_PPN1 as fm_PPN2, '' as fc_ReasonRtr
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_mdlsatuan c on b.fc_mdlcode = c.fc_mdlcode
left outer join t_barang d on b.fc_brgcode = d.fc_brgcode
where date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and b.fc_type = 'Normal' and a.fc_tipe = 'Normal';

INSERT INTO t_billdtl
  (fc_Branch, fc_Docno, fc_No, fc_DocRef, fc_Year, fc_Periode, fc_Brgcode, 
   fc_TipeCode, fc_Grpcode, fc_Valcode, fc_ValTipe, fc_PriceCtr, fc_Prdcode, 
   fc_Mrkcode, fc_Jnscode, fc_Sizcode, fc_MdlCode, fc_GdCode, fc_Batch, 
   fd_Postdate, fc_Suppcode, fc_Custcode, fc_CostCTR, fc_Extra, fn_Qty1, 
   fn_Qty2, fc_Satcode, fn_Extra1, fn_Extra2, fn_Harga1, fn_Harga2, fn_Disc1, 
   fn_Disc2, fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, fm_PPN1, 
   fm_Disc2_1, fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPn2, fn_SisaGalon, 
   fn_SisaQtyCN, fn_SisaValCN)
Select 
   b.fc_Branch, concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) as fc_docno , b.fc_No, concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
		 else 'DR'
end ) as fc_docref , date_format(sysdate(),'%Y') as fc_Year, date_format(sysdate(),'%m') as fc_Periode, b.fc_Brgcode, 
   b.fc_TipeCode, b.fc_Grpcode, b.fc_Valcode, b.fc_ValTipe, b.fc_PriceCtr, b.fc_Prdcode, 
   b.fc_Mrkcode, b.fc_Jnscode, b.fc_Sizcode, b.fc_MdlCode, b.fc_GdCode, '1' as fc_Batch, 
   a.fd_Postdate , '' as fc_Suppcode, a.fc_Custcode, d.fc_costctr as fc_CostCTR, '1' as fc_Extra, fn_qty - fn_qty_return as fn_qty1, 
   (fn_qty - fn_qty_return) * coalesce(c.fn_qty2,0) as fn_qty2,  b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2 , b.fn_Harga, coalesce(b.fn_harga / c.fn_qty2 ,0) as fn_Harga2, 
   b.fn_Disc1, b.fn_Disc2, 
   b.fm_Disc1_1, b.fm_Disc1_2, b.fm_Brutto1, b.fm_Netto1, b.fm_DPP1, b.fm_PPN1, 
   b.fm_Disc1_1 as fm_Disc2_1, b.fm_Disc1_2 as fm_Disc2_2, b.fm_Brutto1 as fm_Brutto2, 0 as fm_Netto2, b.fm_DPP1 as fm_DPP2, b.fm_PPN1 as fm_PPn2, 
   0 as fn_SisaGalon, 0 as fn_SisaQtyCN, 0 as fn_SisaValCN
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join db_dltisnow.t_mdlsatuan c on c.fc_mdlcode = b.fc_mdlcode 
left outer join t_barang d on b.fc_brgcode = d.fc_brgcode
where date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and a.fc_tipe in ('Normal' , 'DO');

INSERT INTO t_movebrgdtl
  (fc_Branch, fc_Docno, fc_DocRef, fc_Year, fc_Periode, fc_No, fc_Tipe, 
   fc_Brgcode, fc_GdCode, fc_Batch, fc_BrgName, fc_NoRef, fd_Inputdate, 
   fc_Suppcode, fc_Custcode, fc_CostCTR, fc_CR, fc_Satcode, fc_Extra, fn_Qty1, 
   fn_Qty2, fn_Extra1, fn_Extra2, fn_Harga1, fn_Harga2, fn_Disc1, fn_Disc2, 
   fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, fm_PPN1, fm_Disc2_1, 
   fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPn2, fn_NewAvg, fm_NewStd, 
   fm_NewValue, fn_QtySaldo, fn_QtySaldo1, fn_QtySaldo2, fn_AVGSaldo, fn_StdSaldo, 
   fn_ValueSaldo, fc_InsTipe)
Select
   b.fc_Branch, 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
         when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
   end ) as fc_docno , 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
         when b.fc_type = 'Galon Jamin' then 'DR'
		 else 'DR'
   end ) as fc_docref , a.fc_Year, a.fc_Periode, b.fc_No, 
   case when b.fc_type = 'Galon Beli' then 'DG'  
       when b.fc_type = 'Galon Jamin' then 'DI'
       else 'DR'
   end as fc_Tipe, 
   b.fc_Brgcode, a.fc_GdCode, 
  case when b.fc_type = 'Galon Beli' then 'BE'  
       when b.fc_type = 'Galon Jamin' then 'JA'
       else ''
   end as fc_Batch, '' as  fc_BrgName,  '' as fc_NoRef, a.fd_Inputdate, 
   '' as fc_Suppcode, a.fc_Custcode, '' as fc_CostCTR, 'C' as fc_CR, b.fc_Satcode, '1' as fc_Extra, 
   fn_qty - fn_qty_return as fn_qty1, 
   (fn_qty - fn_qty_return) * coalesce(c.fn_qty2,0) as fn_qty2 , '0' as fn_Extra1, '0' as fn_Extra2 , b.fn_Harga, coalesce(b.fn_harga / c.fn_qty2 ,0) as fn_Harga2, 
   b.fn_Disc1, b.fn_Disc2, b.fm_Disc1_1, b.fm_Disc1_2, b.fm_Brutto1, b.fm_Netto1, 
   b.fm_DPP1, b.fm_PPN1, 0 as fm_Disc2_1, 0 as fm_Disc2_2, 0 as fm_Brutto2, 0 as fm_Netto2, 
   0 as fm_DPP2, 0 as fm_PPn2, '0' as fn_NewAvg, '0' as fm_NewStd, '0' as fm_NewValue, '0' as fn_QtySaldo, 
   '0' as fn_QtySaldo1, '0' as fn_QtySaldo2, '0' as fn_AVGSaldo, '0' as fn_StdSaldo, '0' as fn_ValueSaldo, 
   'AV' as fc_InsTipe
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join db_dltisnow.t_mdlsatuan c on c.fc_mdlcode = b.fc_mdlcode 
left outer join t_barang d on b.fc_brgcode = d.fc_brgcode
where date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') and 
b.fc_type in ('Galon Jamin','Galon Beli') and a.fc_tipe in ('Normal' , 'DO');
   
delete from t_paymentdtl where fc_branch = '2001' and fc_docno in (
select fc_docno from t_paymentmst where fc_branch = '2001' and date_format(fd_docdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d'));
delete from t_paymentmst where fc_branch = '2001' and date_format(fd_docdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d');
delete from t_bankdtl where fc_branch = '2001' and fc_docno in (
select fc_docno from t_bankmst where fc_branch = '2001' and date_format(fd_docdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d'));
delete from t_bankmst where fc_branch = '2001' and date_format(fd_docdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d');

INSERT INTO t_paymentdtl
  (fc_branch, fc_docno, fc_docref, fc_noref, fc_no, fc_gltrans, fc_pk, 
   fc_cr, fc_mu, fm_rate, fm_netto1, fm_netto2, fm_pay1, fm_pay2, fc_status, 
   fc_cashdisc, fm_cashdisc1, fm_cashdisc2)
Select 
   a.fc_branch, 
   concat('REC-',a.fc_invno) as fc_docno, 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
       when b.fc_type = 'Galon Jamin' then 'DI' else 'DR'
   end ) as fc_docref, 
   '' as fc_noref,  '01' as fc_no, 
   case when b.fc_type = 'Galon Beli' then 'DG'  
      when b.fc_type = 'Galon Jamin' then 'DI' else 'DR'
   end  as fc_gltrans, 
   '' as fc_pk, 'D' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(b.fm_netto1) as fm_netto1, SUM(b.fm_netto1) as fm_netto2, 
   SUM(b.fm_netto1) as fm_pay1, SUM(b.fm_netto1) as fm_pay2, 
   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from t_invpay a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join t_invmst c on a.fc_branch = c.fc_branch and a.fc_invno = c.fc_invno
where a.fc_branch = '2001' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') 
and a.fc_status_payment = 'CASH'
and b.fc_type in ('Normal','Galon Jamin','Galon Beli') 
 and c.fc_tipe in ('Normal','DO')
group by a.fc_branch , a.fc_invno , b.fc_type;

INSERT INTO t_paymentdtl
  (fc_branch, fc_docno, fc_docref, fc_noref, fc_no, fc_gltrans, fc_pk, 
   fc_cr, fc_mu, fm_rate, fm_netto1, fm_netto2, fm_pay1, fm_pay2, fc_status, 
   fc_cashdisc, fm_cashdisc1, fm_cashdisc2)
Select 
   a.fc_branch, 
    concat('REC-',a.fc_invno) as fc_docno, 
   concat('AD-',a.fc_invno) as fc_docref, 
   '' as fc_noref,  '01' as fc_no, 
   'AR'  as fc_gltrans, 
   '' as fc_pk, 'C' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, fm_netto as fm_netto1, fm_netto as fm_netto2, 
   fm_pay_actual as fm_pay1, fm_pay_actual as fm_pay2, 
   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from t_invpay a
left outer join t_invmst c on a.fc_branch = c.fc_branch and a.fc_invno = c.fc_invno
where a.fc_branch = '2001' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') 
and a.fc_status_payment = 'CASH' and c.fc_tipe in ('Normal','DO')
Group by a.fc_branch , a.fc_salescode ,  a.fc_custcode;

INSERT INTO t_paymentmst
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_docref, fc_tipe, 
   fc_suppcode, fc_custcode, fd_docdate, fd_postdate, fd_inputdate, fd_inputby, 
   fc_mucode, fm_rate, fm_value1, fm_value2, fc_note, fc_status, fc_tax, 
   fc_plantbayar)
Select
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  
   concat('REC-',a.fc_invno) as fc_docno, 
    '' as fc_docref, 
   'D' as fc_tipe, a.fc_suppcode, a.fc_custcode, a.fd_docdate, a.fd_postdate, a.fd_inputdate, 
   a.fc_salescode as fd_inputby, 'IDR' as fc_mucode, '1' as fm_rate, fm_pay_actual as fm_value1, fm_pay_actual as fm_value2, concat('JUAL TUNAI SALES',fc_salesname) as fc_note, 
   'R' as fc_status, '1' as fc_tax, '' as fc_plantbayar
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join t_sales c on a.fc_salescode = c.fc_salescode and a.fc_branch = c.fc_branch
where a.fc_branch = '2001' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') 
and b.fc_status_payment = 'CASH' ;
#AD

INSERT INTO t_bankdtl
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_no, fc_docref, 
   fc_bustran, fc_suppcode, fc_custcode, fc_receipt, fc_bttipe, fc_tipe, 
   fc_bank, fc_giro, fc_gltrans, fc_glvs, fc_cr, fd_duedate, fc_note, fc_iocode, 
   fc_costctr, fc_mu, fm_value1, fm_value2, fm_sisa1, fm_sisa2, fc_status, 
   fn_kmawal, fn_kmakhir, fn_volume, fc_bbm, fc_tujuan, fc_sopircode, fc_bankgirocode, 
   fc_bankasal, fc_giroasal, fm_valueasal, fc_docrefbkk, fc_noref)
Select 
   '0002' as fc_compcode, a.fc_branch, fc_year, fc_periode,  concat('AD-',a.fc_invno) as fc_docno, '01' as fc_no, 
   concat('BN-',a.fc_salescode,date_format(date_add(sysdate(), interval 0 day),'%Y%m%d')) as fc_docref, '' as fc_bustran, '' as fc_suppcode, a.fc_custcode, a.fc_salescode as fc_receipt, 'D' as fc_bttipe, 
   'AR' as fc_tipe, '' as fc_bank, '' as fc_giro, '219301' as fc_gltrans, '219401' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
   concat('JUAL TUNAI SALES',fc_salesname) as fc_note, a.fc_gdcode as fc_iocode, 
   case when SUM(case when fc_brgcode like '%GALO%' then 1 else 0 end ) > 0 then '1'
   else 0 end as fc_costctr, 'IDR' as fc_mu, fm_pay_actual as fm_value1, fm_pay_actual as fm_value2, 
   '0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
   '' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
   '0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
   
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join t_invdtl d on a.fc_branch = d.fc_branch and a.fc_invno = d.fc_invno 
left outer join t_sales c on a.fc_salescode = c.fc_salescode and a.fc_branch = c.fc_branch
where a.fc_branch = '2001' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') 
and b.fc_status_payment = 'CASH'
group by a.fc_invno ;

INSERT INTO t_bankmst
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_tipe, fc_docref, 
   fd_docdate, fd_postdate, fd_inputdate, fc_inputby, fc_mu, fm_rate, fc_gltrans, 
   fc_cr, fm_value1, fm_value2, fc_note, fc_status, fc_tax, fc_udf1, fc_udf2, 
   fc_udf3, fc_udf4, fc_udf5, fm_udf1, fm_udf2, fm_udf3, fm_udf4, fm_udf5)
Select 
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  concat('AD-',a.fc_invno) as fc_docno, 'AR' as fc_tipe, 
   concat('BN-',a.fc_salescode,date_format(date_add(sysdate(), interval 0 day),'%Y%m%d')) as fc_docref, a.fd_docdate, a.fd_postdate, a.fd_inputdate, a.fc_salescode as fc_inputby, 'IDR' as fc_mu, 
   '1' as fm_rate, '219301' as fc_gltrans, 'D' as fc_cr, fm_pay_actual as fm_value1, fm_pay_actual as fm_value2, '' as fc_note, 'R' as  fc_status, 
   '1' as fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
   '0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
where a.fc_branch = '2001' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') 
and b.fc_status_payment = 'CASH';

#BN
INSERT INTO t_bankdtl
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_no, fc_docref, 
   fc_bustran, fc_suppcode, fc_custcode, fc_receipt, fc_bttipe, fc_tipe, 
   fc_bank, fc_giro, fc_gltrans, fc_glvs, fc_cr, fd_duedate, fc_note, fc_iocode, 
   fc_costctr, fc_mu, fm_value1, fm_value2, fm_sisa1, fm_sisa2, fc_status, 
   fn_kmawal, fn_kmakhir, fn_volume, fc_bbm, fc_tujuan, fc_sopircode, fc_bankgirocode, 
   fc_bankasal, fc_giroasal, fm_valueasal, fc_docrefbkk, fc_noref)
Select 
   '0002' as fc_compcode, a.fc_branch, fc_year, fc_periode, concat('BN-',a.fc_salescode,date_format(date_add(sysdate(), interval 0 day),'%Y%m%d')) as fc_docno, '01' as fc_no, 
   '' as fc_docref, 'CUSTOMER A/R Clearing' as fc_bustran, '' as fc_suppcode, '' as fc_custcode, a.fc_salescode as fc_receipt, 'R' as fc_bttipe, 
   'BI' as fc_tipe, '' as fc_bank, '' as fc_giro, '111101' as fc_gltrans, '219301' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
   concat('JUAL TUNAI SALES',fc_salesname) as fc_note, a.fc_gdcode as fc_iocode, 
   case when SUM(case when fc_brgcode like '%GALO%' then 1 else 0 end ) > 0 then '1'
   else 0 end as fc_costctr, 'IDR' as fc_mu, SUM(fm_pay_actual) as fm_value1, SUM(fm_pay_actual) as fm_value2, 
   '0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
   '' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
   '0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
   
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join t_invdtl d on a.fc_branch = d.fc_branch and a.fc_invno = d.fc_invno 
left outer join t_sales c on a.fc_salescode = c.fc_salescode and a.fc_branch = c.fc_branch
where a.fc_branch = '2001' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') 
and b.fc_status_payment = 'CASH'
Group by a.fc_salescode; 

INSERT INTO t_bankmst
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_tipe, fc_docref, 
   fd_docdate, fd_postdate, fd_inputdate, fc_inputby, fc_mu, fm_rate, fc_gltrans, 
   fc_cr, fm_value1, fm_value2, fc_note, fc_status, fc_tax, fc_udf1, fc_udf2, 
   fc_udf3, fc_udf4, fc_udf5, fm_udf1, fm_udf2, fm_udf3, fm_udf4, fm_udf5)
Select 
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode, concat('BN-',a.fc_salescode,date_format(date_add(sysdate(), interval 0 day),'%Y%m%d')) as fc_docno, 'BI' as fc_tipe, 
   '' as fc_docref, a.fd_docdate, a.fd_postdate, a.fd_inputdate, a.fc_salescode as fc_inputby, 'IDR' as fc_mu, 
   '1' as fm_rate, '219301' as fc_gltrans, 'D' as fc_cr, SUM(fm_pay_actual) as fm_value1, SUM(fm_pay_actual) as fm_value2, '' as fc_note, 'R' as  fc_status, 
   '1' as fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
   '0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
where a.fc_branch = '2001' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add(sysdate(), interval 0 day),'%Y%m%d') 
and b.fc_status_payment = 'CASH'
Group by a.fc_salescode;