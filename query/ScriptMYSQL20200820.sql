use db_sfa

create table t_middletier_function(
fc_functionkode varchar(30) ,
fc_functionname varchar(30) ,
primary key (fc_functionkode)
)

create table t_request_middletier (
fn_requestnum bigint not null auto_increment,
fc_branch varchar(10),
fc_functionkode varchar(100) ,
fd_requestdate DATETIME,
fc_status varchar(30) not null default 'WAITING',
primary key(fn_requestnum,fc_branch,fc_functionkode)
)

select * from t_request_middletier where fc_status = 'WAITING'
order by fn_requestnum asc 

insert into t_request_middletier values 
(NULL,'2001','EXPORT_DEVICE_CSV',now(),'WAITING'),
(NULL,'2001','TEST FUNCTION',now(),'WAITING'),
(NULL,'2001','TEST FUNCTION',now(),'WAITING'),
(NULL,'2001','TEST FUNCTION',now(),'WAITING')

create table t_log_msg_middletier (
fn_requestnum,
fd_datelog DATETIME ,
fc_message text,
primary key(fn_requestid)
)

CREATE TABLE `t_trxtype_middletier` (
  `fc_branch` varchar(6) NOT NULL DEFAULT '',
  `fc_trxid` varchar(20) NOT NULL DEFAULT '',
  `fn_trxnumber` tinyint(4) NOT NULL DEFAULT '0',
  `fc_trxcode` varchar(100) NOT NULL DEFAULT '',
  `fv_trxdescription` varchar(200) DEFAULT '',
  `fc_additiiona1` varchar(20) DEFAULT '',
  `fc_additional2` varchar(20) DEFAULT '',
  `fc_additional3` varchar(20) DEFAULT '',
  fl_visible bool not null default false, 
  PRIMARY KEY (`fc_branch`,`fc_trxid`,`fc_trxcode`),
  KEY `idx1_t_trxtype` (`fc_branch`,`fc_trxid`,`fc_trxcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

select fv_trxdescription from t_trxtype_middletier 
where fc_trxid = "MIDDLETIER_PROCESS" and fc_trxcode = "PROCESS_STATUS"

select * from db_sfa.t_customer_edit ;
select * from t_customer_longlat ;

delete * from t_noo_user 

select * from db_dltisnow.t_sales where fc_branch = :branch and fc_salescode in ("0001","3614","B020") ;





