use d_transaksi
DECLARE 
    @cBranch VARCHAR(10), 
	@vCustcode VARCHAR(30),
	@vLongtitude varchar(75),
	@vLatitude varchar(75);

DECLARE cursor_custlonglat CURSOR
FOR select cbranch,vCustcode,vlatitude,vlongitude from t_customerCoordinate ;

OPEN cursor_custlonglat;

FETCH NEXT FROM cursor_custlonglat INTO 
    @cBranch , 
	@vCustcode ,
	@vLatitude ,
	@vLongtitude;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	IF EXISTS (
            select * from [mysql]...[db_sfa.t_customer_longlat] where fc_branch = @cBranch and fc_custcode = @vCustcode
            )
    BEGIN
        UPDATE [mysql]...[db_sfa.t_customer_longlat]
        SET fc_latitude = @vLatitude,
			fc_longtitude = @vLongtitude
        WHERE fc_branch = @cBranch and fc_custcode = @vCustcode;
    END
    ELSE
    BEGIN
        INSERT INTO [mysql]...[db_sfa.t_customer_longlat] (fc_branch,fc_custcode,fc_latitude,fc_longtitude)
        select cbranch,vCustcode,vlatitude,vlongitude from t_customerCoordinate
		where cbranch = @cBranch and vcustcode = @vCustcode;
    END        

	FETCH NEXT FROM cursor_custlonglat INTO 
		@cBranch , 
		@vCustcode ,
		@vLatitude ,
		@vLongtitude;

END;

CLOSE cursor_custlonglat;

DEALLOCATE cursor_custlonglat;