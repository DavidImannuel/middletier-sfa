use d_transaksi

IF OBJECT_ID('tempdb.dbo.#x_diskon_pricelist', 'U') IS NOT NULL
DROP TABLE #x_diskon_pricelist; 
CREATE TABLE #x_diskon_pricelist (
  cbranch varchar(20) NOT NULL DEFAULT '',
  cjenis varchar(20) NOT NULL DEFAULT '',
  ccustcode varchar(15) NOT NULL DEFAULT '',
  corgcode char(2) NOT NULL DEFAULT '',
  cchanelcode char(2) NOT NULL DEFAULT '',
  cbrgcode varchar(50) NOT NULL DEFAULT '',
  csatcode char(5) NOT NULL DEFAULT '',
  nqvldon decimal(6,0) NOT NULL DEFAULT '0',
  nqvlduntil decimal(6,0) NOT NULL DEFAULT '0',
  ndiscpcent1 decimal(5,2) NOT NULL DEFAULT '0.00',
  ndiscpcent2 decimal(5,2) NOT NULL DEFAULT '0.00',
  mdiscamount1 decimal(20,3) NOT NULL DEFAULT '0.000',
  mdiscamount2 decimal(20,3) NOT NULL DEFAULT '0.000',
  dvalidon datetime NOT NULL DEFAULT '0001-01-01 01:01:01',
  dvaliduntil datetime NOT NULL DEFAULT '0001-01-01 01:01:01'
);

DECLARE 
    @branch varchar(10),
    @Custcode VARCHAR(20),
    @cbrgcode varchar(50),
    @nqty integer,
    @periode VARCHAR(10),
  @satuan VARCHAR(10);
    
    Select @periode = FORMAT(GETDATE(),'yyyyMMdd') , @branch = '2001';

--Diskon Global

DECLARE cursor_diskonglobal CURSOR
FOR 
    Select * from (
    Select b.cCustcode , a.cbrgcode , a.nQVldOn , a.csatcode from t_DiscPromo a
    inner join (
     select top 1 with ties a.cChanelcode , a.cAreacode , a.ccustcode , b.cBranch from t_customer a
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where b.cBranch = '2001' 
    order by row_number() over (partition by a.cChanelcode  order by NEWID())
    ) b on a.cOrgCode = b.cAreacode and a.cChanelCode = b.cChanelcode 
    where dValidOn <= GETDATE() and dValidUntil >= GETDATE() and cStatus = '1' 
  ) x where not exists (select * from t_DiscCusPromo y 
  inner join t_customercomp z on y.cCustCode = z.cCustcode where z.cBranch = '2001' and x.cCustCode = y.cCustCode and x.cBrgCode = y.cBrgCode)

OPEN cursor_diskonglobal;
        FETCH NEXT FROM cursor_diskonglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_diskon_pricelist ( 
                   cCustCode, cOrgcode, cChanelCode, cBrgCode, cSatCode, nQVldOn, nQVldUntil,
nDiscPCent1, nDiscPCent2,mDiscAmount1,mDiscAmount2,dValidOn,dValidUntil)
        EXEC pr_SLSDiscAddCost '0002',@Custcode,'',@cbrgcode,@periode,'Discount',@nqty,@satuan

        FETCH NEXT FROM cursor_diskonglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
    END;

CLOSE cursor_diskonglobal;

DEALLOCATE cursor_diskonglobal;

Update #x_diskon_pricelist set cjenis = 'Default' Where cbranch = '2001';

delete from [mysql]...[db_sfa.t_disc_pricelist] where cbranch = '2001';
insert into [mysql]...[db_sfa.t_disc_pricelist]
select * from #x_diskon_pricelist;