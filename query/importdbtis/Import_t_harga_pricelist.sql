use d_transaksi

IF OBJECT_ID('tempdb.dbo.#x_harga_pricelist', 'U') IS NOT NULL
DROP TABLE #x_harga_pricelist; 
CREATE TABLE #x_harga_pricelist (
  cbranch varchar(20) NOT NULL DEFAULT '',
  cjenis varchar(20) NOT NULL DEFAULT '',
  ccustcode varchar(15) NOT NULL DEFAULT '',
  cAreacode varchar(20) NOT NULL DEFAULT '',
  cChanelCode varchar(20) NOT NULL DEFAULT '',
  cbrgcode varchar(50) NOT NULL DEFAULT '', 
  ccondition char(20) NOT NULL DEFAULT '',
  nVldon decimal(6,0) NOT NULL DEFAULT '0',
  nVlduntil decimal(6,0) NOT NULL DEFAULT '0',
  dValidOn datetime,
  dValidUntil datetime ,
  nVldOnQ1 decimal(6,0) NOT NULL DEFAULT '0',
  nVlduntilQ2 decimal(6,0) NOT NULL DEFAULT '0',
  csatcode char(5) NOT NULL DEFAULT '',
  cstatus char(1) NOT NULL DEFAULT '',
  mprice decimal(20,2) NOT NULL DEFAULT '0.000',
  ndiscpcent1 decimal(5,2) NOT NULL DEFAULT '0.00',
  ndiscpcent2 decimal(5,2) NOT NULL DEFAULT '0.00',
  mdiscamount1 decimal(20,2) NOT NULL DEFAULT '0.000',
  mdiscamount2 decimal(5,2) NOT NULL DEFAULT '0.000',
  cbnscode varchar(25) NOT NULL DEFAULT '',
  nqtybns decimal(6,0) NOT NULL DEFAULT '0',
  csatbns char(5) NOT NULL DEFAULT ''
);

DECLARE 
    @branch varchar(10),
    @Custcode VARCHAR(20),
    @cbrgcode varchar(50),
    @nqty integer,
    @periode VARCHAR(10),
  @satuan VARCHAR(10);
    
    Select @periode = FORMAT(GETDATE(),'yyyyMMdd') , @branch = '2001';

DECLARE cursor_hargacust CURSOR
FOR 
    Select a.cCustcode , cBrgcode from t_HrgCustSpecify a with (nolock)
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where cBranch = '2001' and cvalid = '1' and cStatus = '1' and dValidOn <= GETDATE() and dValidUntil >= GETDATE()

OPEN cursor_hargacust;
        FETCH NEXT FROM cursor_hargacust INTO 
            @Custcode, 
            @cbrgcode;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_harga_pricelist ( 
                   cCustCode,cBrgCode,cCondition,nVldOn,nVldUntil,cSatCode,cStatus,mPrice,
           nDiscPCent1,nDiscPCent2,mDiscAmount1, mDiscAmount2,cBnsCode, nQtyBns, cSatBns )
        exec [pr_SLSHrgBrgCustQtyNew] '0002',@Custcode,@cbrgcode,'20200823', '1',''

        FETCH NEXT FROM cursor_hargacust INTO 
            @Custcode, 
            @cbrgcode;
    END;

CLOSE cursor_hargacust;

DEALLOCATE cursor_hargacust;

--dapatkan harga customer Global
DECLARE cursor_hargaglobal CURSOR
FOR 
    Select * from (
    Select b.cCustcode , a.cbrgcode , a.nVldOnQ1 from t_hargadtl a
    inner join (
     select top 1 with ties a.cChanelcode , a.cAreacode , a.ccustcode , b.cBranch from t_customer a
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where b.cBranch = '2001' 
    order by row_number() over (partition by a.cChanelcode  order by NEWID())
    ) b on a.cOrgCode = b.cAreacode and a.cChanelCode = b.cChanelcode 
    where dValidOn <= GETDATE() and dValidUntil >= GETDATE() and cStatus = '1' 
  ) x where not exists (select * from t_HrgCustSpecify y 
  inner join t_customercomp z on y.cCustCode = z.cCustcode where z.cBranch = '2001' and x.cCustCode = y.cCustCode and x.cBrgCode = y.cBrgCode)

OPEN cursor_hargaglobal;

        FETCH NEXT FROM cursor_hargaglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty;


WHILE @@FETCH_STATUS = 0
    BEGIN

        insert into #x_harga_pricelist ( 
                   cCustCode, cAreacode, cChanelCode, cBrgCode, cSatCode, mPrice , dValidOn,
           dValidUntil, nVldOnQ1, nVldUntilQ2, nDiscPCent1, nDiscPCent2,
           mDiscAmount1, cStatus, cCondition, mDiscAmount2, cBnsCode, nQtyBns, cSatBns )
        exec [pr_SLSHrgBrgCustQtyNew] '0002',@Custcode,@cbrgcode,@periode, @nqty,''

        FETCH NEXT FROM cursor_hargaglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty;

    END;
  
CLOSE cursor_hargaglobal;

DEALLOCATE cursor_hargaglobal;

Update #x_harga_pricelist set cbranch = @branch , cjenis = case when cAreacode = '' and cChanelCode = '' then 'Cust_Khusus' else 'Default' end;

delete from [mysql]...[db_sfa.t_harga_pricelist] where cbranch = '2001' ;
insert into [mysql]...[db_sfa.t_harga_pricelist]
select * from #x_harga_pricelist;