use d_transaksi

IF OBJECT_ID('tempdb.dbo.#x_harga_pricelist', 'U') IS NOT NULL
DROP TABLE #x_harga_pricelist; 
CREATE TABLE #x_harga_pricelist (
  cbranch varchar(20) NOT NULL DEFAULT '',
  cjenis varchar(20) NOT NULL DEFAULT '',
  ccustcode varchar(15) NOT NULL DEFAULT '',
  cAreacode varchar(20) NOT NULL DEFAULT '',
  cChanelCode varchar(20) NOT NULL DEFAULT '',
  cbrgcode varchar(50) NOT NULL DEFAULT '', 
  ccondition char(20) NOT NULL DEFAULT '',
  nVldon decimal(6,0) NOT NULL DEFAULT '0',
  nVlduntil decimal(6,0) NOT NULL DEFAULT '0',
  dValidOn datetime,
  dValidUntil datetime ,
  nVldOnQ1 decimal(6,0) NOT NULL DEFAULT '0',
  nVlduntilQ2 decimal(6,0) NOT NULL DEFAULT '0',
  csatcode char(5) NOT NULL DEFAULT '',
  cstatus char(1) NOT NULL DEFAULT '',
  mprice decimal(20,2) NOT NULL DEFAULT '0.000',
  ndiscpcent1 decimal(5,2) NOT NULL DEFAULT '0.00',
  ndiscpcent2 decimal(5,2) NOT NULL DEFAULT '0.00',
  mdiscamount1 decimal(20,2) NOT NULL DEFAULT '0.000',
  mdiscamount2 decimal(5,2) NOT NULL DEFAULT '0.000',
  cbnscode varchar(25) NOT NULL DEFAULT '',
  nqtybns decimal(6,0) NOT NULL DEFAULT '0',
  csatbns char(5) NOT NULL DEFAULT ''
);

IF OBJECT_ID('tempdb.dbo.#x_diskon_pricelist', 'U') IS NOT NULL
DROP TABLE #x_diskon_pricelist; 
CREATE TABLE #x_diskon_pricelist (
  cbranch varchar(20) NOT NULL DEFAULT '',
  cjenis varchar(20) NOT NULL DEFAULT '',
  ccustcode varchar(15) NOT NULL DEFAULT '',
  corgcode char(2) NOT NULL DEFAULT '',
  cchanelcode char(2) NOT NULL DEFAULT '',
  cbrgcode varchar(50) NOT NULL DEFAULT '',
  csatcode char(5) NOT NULL DEFAULT '',
  nqvldon decimal(6,0) NOT NULL DEFAULT '0',
  nqvlduntil decimal(6,0) NOT NULL DEFAULT '0',
  ndiscpcent1 decimal(5,2) NOT NULL DEFAULT '0.00',
  ndiscpcent2 decimal(5,2) NOT NULL DEFAULT '0.00',
  mdiscamount1 decimal(20,3) NOT NULL DEFAULT '0.000',
  mdiscamount2 decimal(20,3) NOT NULL DEFAULT '0.000',
  dvalidon datetime NOT NULL DEFAULT '0001-01-01 01:01:01',
  dvaliduntil datetime NOT NULL DEFAULT '0001-01-01 01:01:01'
);

IF OBJECT_ID('tempdb.dbo.#x_bonusqty_pricelist', 'U') IS NOT NULL
DROP TABLE #x_bonusqty_pricelist; 
CREATE TABLE #x_bonusqty_pricelist (
  cbranch varchar(20) NOT NULL DEFAULT '',
  cjenis varchar(20) NOT NULL DEFAULT '', 
  ccustcode varchar(15) NOT NULL DEFAULT '',
  corgcode char(2) NOT NULL DEFAULT '',
  cchanelcode char(2) NOT NULL DEFAULT '',  
  cno int NOT NULL DEFAULT '0',
  cbrgcode varchar(50) NOT NULL DEFAULT '',
  csatcode char(5) NOT NULL DEFAULT '',
  nqvldon decimal(6,0) NOT NULL DEFAULT '0',
  nqvlduntil decimal(6,0) NOT NULL DEFAULT '0',
  cmultiple char(1) NOT NULL DEFAULT '',
  cbrgcode2 varchar(50) NOT NULL DEFAULT '',
  csatcode2 char(5) NOT NULL DEFAULT '',
  nqtybns decimal(6,0) NOT NULL DEFAULT '0',
  ccek char(3) NOT NULL DEFAULT '');

IF OBJECT_ID('tempdb.dbo.#x_bonusval_pricelist', 'U') IS NOT NULL
DROP TABLE #x_bonusval_pricelist; 
CREATE TABLE #x_bonusval_pricelist (
  cbranch varchar(20) NOT NULL DEFAULT '',
  cAreacode varchar(20) NOT NULL DEFAULT '',
  cChanelCode varchar(20) NOT NULL DEFAULT '',
  cbrgcode varchar(50) NOT NULL DEFAULT '', 
  csatcode char(5) NOT NULL DEFAULT '',
  cmrkcode char(5) NOT NULL DEFAULT '',
  mvalue1 decimal(20,2) NOT NULL DEFAULT '0.000',
  mvalue2 decimal(20,2) NOT NULL DEFAULT '0.000',
  nQtyBns decimal(6,0) NOT NULL DEFAULT '0',
  crepeat char(3) NOT NULL DEFAULT ''
);

IF OBJECT_ID('tempdb.dbo.#x_cust_bill', 'U') IS NOT NULL
DROP TABLE #x_cust_bill; 
CREATE TABLE #x_cust_bill (
  cbranch varchar(20) NOT NULL DEFAULT '',
  ccustcode varchar(20) NOT NULL DEFAULT ''
);

DECLARE 
    @branch varchar(10),
    @Custcode VARCHAR(20),
    @cbrgcode varchar(50),
    @nqty integer,
    @periode VARCHAR(10),
  @satuan VARCHAR(10),
    @cBonus char(1),
    @cBonusValue char(1),
    @cDisc char(1),
    @cDiscMix char(1),
  @setHargaBillGalon char(1),
  @HargaBillGalon char(1);
    
    Select @periode = FORMAT(GETDATE(),'yyyyMMdd') , @branch = '2001';
    Select @cBonus = cBonus , @cBonusValue = cBonusValue , @cDisc = cDisc , @cDiscMix = cDiscMix  from t_setup where cbranch = '2001';


Insert into #x_cust_bill Select cBranch , cCustcode from t_billmst where cbranch = '2001' and  cStatus = 'R'  Group by cBranch , cCustcode;

DECLARE cursor_hargacust CURSOR
FOR 
    Select a.cCustcode , cBrgcode , cSatCode from t_HrgCustSpecify a with (nolock)
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where cBranch = '2001' and cvalid = '1' and cStatus = '1' and CONVERT(char(8),dValidUntil,112)>=CONVERT(char(8),GETDATE(),112)

OPEN cursor_hargacust;
        FETCH NEXT FROM cursor_hargacust INTO 
            @Custcode, 
            @cbrgcode,
      @satuan;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_harga_pricelist ( 
                   cCustCode,cBrgCode,cCondition,nVldOn,nVldUntil,cSatCode,cStatus,mPrice,
           nDiscPCent1,nDiscPCent2,mDiscAmount1, mDiscAmount2,cBnsCode, nQtyBns, cSatBns )
        exec [pr_SLSHrgBrgCustQtyNew] '0002',@Custcode,@cbrgcode,@periode, '1',@satuan

        FETCH NEXT FROM cursor_hargacust INTO 
            @Custcode, 
            @cbrgcode,
      @satuan;
    END;

CLOSE cursor_hargacust;

DEALLOCATE cursor_hargacust;

DECLARE cursor_hargaglobal CURSOR
FOR 
    Select * from (
    Select b.cCustcode , a.cbrgcode , a.nVldOnQ1 , a.cSatCode from t_hargadtl a
    inner join (
     select top 1 with ties a.cChanelcode , a.cAreacode , a.ccustcode , b.cBranch from t_customer a
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where b.cBranch = '2001' 
    order by row_number() over (partition by a.cChanelcode  order by NEWID())
    ) b on a.cOrgCode = b.cAreacode and a.cChanelCode = b.cChanelcode 
    where CONVERT(char(8),dValidUntil,112)>=CONVERT(char(8),GETDATE(),112) and cStatus = '1' 
  ) x where not exists (select * from t_HrgCustSpecify y 
  inner join t_customercomp z on y.cCustCode = z.cCustcode where z.cBranch = '2001' and x.cCustCode = y.cCustCode and x.cBrgCode = y.cBrgCode)

OPEN cursor_hargaglobal;

        FETCH NEXT FROM cursor_hargaglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
      @satuan;


WHILE @@FETCH_STATUS = 0
    BEGIN

        insert into #x_harga_pricelist ( 
                   cCustCode, cAreacode, cChanelCode, cBrgCode, cSatCode, mPrice , dValidOn,
           dValidUntil, nVldOnQ1, nVldUntilQ2, nDiscPCent1, nDiscPCent2,
           mDiscAmount1, cStatus, cCondition, mDiscAmount2, cBnsCode, nQtyBns, cSatBns )
        exec [pr_SLSHrgBrgCustQtyNew] '0002',@Custcode,@cbrgcode,@periode, @nqty,@satuan

        FETCH NEXT FROM cursor_hargaglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
      @satuan;

    END;
  
CLOSE cursor_hargaglobal;

DEALLOCATE cursor_hargaglobal;

Update #x_harga_pricelist set cbranch = @branch , cjenis = case when cAreacode = '' and cChanelCode = '' then 'Cust_Khusus' else 'Default' end;
Update #x_harga_pricelist set cCustcode = 'Default' where cjenis = 'Default';

Select @HargaBillGalon = cHarga_BillGalon from t_setup where cbranch = '2001';
if @HargaBillGalon = 1 begin

    DECLARE cursor_galonggratisisi CURSOR
    FOR 

        Select b.cCustcode , a.cbrgcode , a.nVldOnQ1  from t_hargadtl_galon a
        inner join (
         select top 1 with ties a.cChanelcode , a.cAreacode , a.ccustcode , b.cBranch from t_customer a
        left outer join t_customercomp b on a.cCustcode = b.cCustcode
        where b.cBranch = '2001' 
        order by row_number() over (partition by a.cChanelcode  order by NEWID())
        ) b on a.cOrgCode = b.cAreacode and a.cChanelCode = b.cChanelcode 
        where CONVERT(char(8),dValidUntil,112)>=CONVERT(char(8),GETDATE(),112) and cStatus = '1' 
      
    OPEN cursor_galonggratisisi;
            FETCH NEXT FROM cursor_galonggratisisi INTO 
                @Custcode, 
                @cbrgcode,
          @nqty;
    WHILE @@FETCH_STATUS = 0
        BEGIN
    
        insert into #x_harga_pricelist ( 
            cCustCode, cAreacode, cChanelCode, cBrgCode, cSatCode, mPrice , dValidOn,
            dValidUntil, nVldOnQ1, nVldUntilQ2, nDiscPCent1, nDiscPCent2,
            mDiscAmount1, cStatus, cCondition, mDiscAmount2, cBnsCode, nQtyBns, cSatBns )            
        EXEC [pr_SLSHrgGalon] '0002',@Custcode,@cbrgcode,@periode,'BE';

            FETCH NEXT FROM cursor_galonggratisisi INTO 
                @Custcode, 
                @cbrgcode,
          @nqty;
        END;
    
    CLOSE cursor_galonggratisisi;
    
    DEALLOCATE cursor_galonggratisisi;
    
    Update #x_harga_pricelist set cbranch = @branch , cjenis = 'Galon Beli Strata' , ccustcode = '' where cjenis = '';
end;

    Select @setHargaBillGalon = cSet_Harga_BillGalon from t_setup where cbranch = '2001';
    if @setHargaBillGalon = 1 begin
        insert into #x_harga_pricelist ( 
                 cbranch , cjenis ,  cBrgCode, cSatCode, mPrice )   
        
      Select  @branch , 'Galon Beli' , cBrgcodeMap, 'PCS' ,CAST(coalesce(cHarga_BillGalon_BE,0) as INT) as mValue from t_setup a
      cross join (select distinct cBrgcodeMap from t_barangmap) b
      where cbranch = '2001' union all
          
      Select  @branch , 'Galon Jaminan' , cBrgcodeMap , 'PCS' ,CAST(coalesce(cHarga_BillGalon_JA,0) as INT) as mValue from t_setup a
      cross join (select distinct cBrgcodeMap from t_barangmap) b
      
      where cbranch = '2001';
    end else begin
        insert into #x_harga_pricelist ( 
                 cbranch , cjenis , cBrgCode, cSatCode, mPrice   )   
        Select @branch , concat('Galon ',cketerangan) as cJenis , cBrgcodeMap , 'PCS' , mValue from t_returntipe a
      cross join (select distinct cBrgcodeMap from t_barangmap) b
      
      where ctipe in ('BE','JA')





 
end;

DECLARE cursor_diskonglobal CURSOR
FOR 
    Select * from (
    Select b.cCustcode , a.cbrgcode , a.nQVldOn , a.csatcode from t_DiscPromo a
    inner join (
     select top 1 with ties a.cChanelcode , a.cAreacode , a.ccustcode , b.cBranch from t_customer a
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where b.cBranch = '2001' 
    order by row_number() over (partition by a.cChanelcode  order by NEWID())
    ) b on a.cOrgCode = b.cAreacode and a.cChanelCode = b.cChanelcode 
    where CONVERT(char(8),dValidUntil,112)>=CONVERT(char(8),GETDATE(),112) and cStatus = '1' 
  ) x where not exists (select * from t_DiscCusPromo y 
  inner join t_customercomp z on y.cCustCode = z.cCustcode where z.cBranch = '2001' and x.cCustCode = y.cCustCode and x.cBrgCode = y.cBrgCode)

OPEN cursor_diskonglobal;
        FETCH NEXT FROM cursor_diskonglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_diskon_pricelist ( 
                   cCustCode, cOrgcode, cChanelCode, cBrgCode, cSatCode, nQVldOn, nQVldUntil,
nDiscPCent1, nDiscPCent2,mDiscAmount1,mDiscAmount2,dValidOn,dValidUntil)
        EXEC pr_SLSDiscAddCost '0002',@Custcode,'',@cbrgcode,@periode,'Discount',@nqty,@satuan

        FETCH NEXT FROM cursor_diskonglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
    END;

CLOSE cursor_diskonglobal;

DEALLOCATE cursor_diskonglobal;

Update #x_diskon_pricelist set cjenis = 'Default' , ccustcode = 'Default' , cbranch = '2001' Where cbranch = '';

DECLARE cursor_diskoncust CURSOR
FOR 
  select a.cCustCode ,a. cBrgCode , a.nqvldon , a.csatcode from t_DiscCusPromo a
    left outer join t_customer b on a.cCustcode = b.cCustcode and a.cStatus = b.cStatus
  left outer join t_customercomp c on a.cCustcode = c.cCustcode
  inner join t_DiscPromo d on a.cBrgCode = d.cBrgCode and b.cAreacode = d.cOrgCode and b.cChanelcode = d.cChanelCode
    where cBranch = '2001' and a.cStatus = '1' and  CONVERT(char(8),a.dValidUntil,112)>=CONVERT(char(8),GETDATE(),112) and 
  CONVERT(char(8),d.dValidUntil,112)>=CONVERT(char(8),GETDATE(),112)
  and not exists( SELECT cCustCode,cBrgCode 
      FROM t_HrgCustSpecify x with (nolock)
      WHERE (cValid = '1') AND (cStatus = '1')
      AND (x.cCustCode=a.cCustCode) 
      AND (x.cBrgCode =a.cBrgCode) 
      and a.dValidOn between convert(char(8),x.dValidOn,112) and convert(char(8),x.dValidUntil,112) 
      )

OPEN cursor_diskoncust;
        FETCH NEXT FROM cursor_diskoncust INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_diskon_pricelist (                    
       cCustCode, corgcode, cChanelCode, cBrgCode, csatcode, nQVldOn, nQVldUntil,
       nDiscPCent1, nDiscPCent2,mDiscAmount1,mDiscAmount2,dValidOn,dValidUntil
   )
        EXEC pr_SLSDiscAddCost '0002',@Custcode,'',@cbrgcode,@periode,'DISCMIX',@nqty,@satuan

        FETCH NEXT FROM cursor_diskoncust INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
    END;

CLOSE cursor_diskoncust;

DEALLOCATE cursor_diskoncust;

Update #x_diskon_pricelist set cjenis = 'DISCMIX' , cbranch = '2001' Where cjenis = '';


DECLARE cursor_diskoncust CURSOR
FOR 
    Select a.cCustCode , a.cBrgCode , a.nqvldon , a.csatcode    from t_DiscCusPromo a with (nolock)
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where b.cBranch = '2001' and a.cStatus = '1' and CONVERT(char(8),a.dValidUntil,112)>=CONVERT(char(8),GETDATE(),112);


OPEN cursor_diskoncust;
        FETCH NEXT FROM cursor_diskoncust INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_diskon_pricelist (                    
       cCustCode, cBrgCode, cSatCode , nQVldOn, nQVldUntil, nDiscPCent1, 
       nDiscPCent2,mDiscAmount1,mDiscAmount2,dValidOn,dValidUntil  )
        EXEC pr_SLSDiscAddCost '0002',@Custcode,'',@cbrgcode,@periode,'Discount',@nqty,@satuan

        FETCH NEXT FROM cursor_diskoncust INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
    END;

CLOSE cursor_diskoncust;

DEALLOCATE cursor_diskoncust;

Update #x_diskon_pricelist set cjenis = 'DISKON_CUST' , cbranch = '2001' Where  cjenis = '';

  Insert into #x_bonusval_pricelist (cAreacode,cChanelCode,cmrkcode,mvalue1,mvalue2,cbrgcode,csatcode,nQtyBns,crepeat , cbranch)
  select distinct cOrgCode , cChanelCode , cMrkcode , mValue1 , mValue2 ,  cBrgCode , cSatCode , nQtyBns , cRepeat , '2001'
  from t_BonusValue
  where cOrgcode = '25';

  Update #x_bonusval_pricelist set  cbranch = '2001' Where  cbranch = '';

DECLARE cursor_bonuscust CURSOR
FOR 
    Select a.cCustCode , cBrgCode , nqvldon , csatcode   from t_BonusCusProd a with (nolock)
    left outer join t_customercomp b on a.cCustcode = b.cCustcode
    where cBranch = '2001' and cStatus = '1' and CONVERT(char(8),a.dValidUntil,112)>=CONVERT(char(8),GETDATE(),112)

OPEN cursor_bonuscust;
        FETCH NEXT FROM cursor_bonuscust INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_bonusqty_pricelist ( 
                   cCustCode,cNo,cBrgCode, cSatCode, nQVldOn, nQVldUntil, cMultiple, cBrgCode2,
                cSatCode2, nQtyBns, cCek )
        EXEC pr_SLSBonusExtraNew '0002',@Custcode,@cbrgcode,@periode, 'BONUSPROD',@nqty,@branch,@satuan

        FETCH NEXT FROM cursor_bonuscust INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
    END;

CLOSE cursor_bonuscust;

DEALLOCATE cursor_bonuscust;

Update #x_bonusqty_pricelist set cjenis = 'BONUS_CUST' , cbranch = '2001' Where  cjenis = '';

DECLARE cursor_bonusglobal CURSOR
FOR 
    Select * from (
       Select b.cCustcode , a.cbrgcode , a.nQVldOn , a.csatcode from t_BonusProd a
       inner join (
           select top 1 with ties a.cChanelcode , a.cAreacode , a.ccustcode , b.cBranch from t_customer a
           left outer join t_customercomp b on a.cCustcode = b.cCustcode
           where b.cBranch = '2001' 
           order by row_number() over (partition by a.cChanelcode  order by NEWID())
       ) b on a.cOrgCode = b.cAreacode and a.cChanelCode = b.cChanelcode 
       where CONVERT(char(8),a.dValidUntil,112)>=CONVERT(char(8),GETDATE(),112) and cStatus = '1' and cDistrict=@branch
    ) x 
  where not exists (
        select * from t_BonuscusProd y 
        inner join t_customercomp z on y.cCustCode = z.cCustcode where z.cBranch = '2001' and x.cCustCode = y.cCustCode and x.cBrgCode = y.cBrgCode
  )
    group by x.cCustcode , x.cBrgCode , x.nQVldOn , x.cSatCode

OPEN cursor_bonusglobal;
        FETCH NEXT FROM cursor_bonusglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
WHILE @@FETCH_STATUS = 0
    BEGIN


        insert into #x_bonusqty_pricelist ( 
                   cCustCode, corgcode, cChanelCode,cNo, cBrgCode, cSatCode, nQVldOn, nQVldUntil,
                 cMultiple, cBrgCode2,cSatCode2,nQtyBns,cCek)
        EXEC pr_SLSBonusExtraNew '0002',@Custcode,@cbrgcode,@periode, 'BONUSPROD',@nqty,@branch,@satuan

        FETCH NEXT FROM cursor_bonusglobal INTO 
            @Custcode, 
            @cbrgcode,
            @nqty,
            @satuan;
    END;

CLOSE cursor_bonusglobal;

DEALLOCATE cursor_bonusglobal;
Update #x_bonusqty_pricelist set cjenis = 'DEFAULT' , cbranch = '2001' , ccustcode = 'DEFAULT'  Where  cjenis = '';

EXEC('TRUNCATE TABLE db_sfa.t_cust_bill') AT MYSQL;
insert into [mysql]...[db_sfa.t_cust_bill]
select * from #x_cust_bill;

EXEC('TRUNCATE TABLE db_sfa.t_harga_pricelist') AT MYSQL;
insert into [mysql]...[db_sfa.t_harga_pricelist]
select * from #x_harga_pricelist;

EXEC('TRUNCATE TABLE db_sfa.t_disc_pricelist') AT MYSQL;
insert into [mysql]...[db_sfa.t_disc_pricelist]
select * from #x_diskon_pricelist;

EXEC('TRUNCATE TABLE db_sfa.t_bonusqty_pricelist') AT MYSQL;
insert into [mysql]...[db_sfa.t_bonusqty_pricelist]
select * from #x_bonusqty_pricelist;

EXEC('TRUNCATE TABLE db_sfa.t_bonusval_pricelist') AT MYSQL;
insert into [mysql]...[db_sfa.t_bonusval_pricelist]
select * from #x_bonusval_pricelist;