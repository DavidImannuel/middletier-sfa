delete a from t_sodtl a
inner join t_somst b on a.fc_branch = b.fc_branch and a.fc_sono = b.fc_sono and b.fc_salescode = '5413' and date_format(b.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
where a.fc_branch = '2053';
delete from t_somst where fc_salescode = '5413' and fc_branch = '2053' and date_format(fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d');

delete a from t_dodtl a
inner join t_domst b on a.fc_branch = b.fc_branch and a.fc_dono = b.fc_dono and b.fc_salescode = '5413' and date_format(b.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
where a.fc_branch = '2053';
delete from t_domst where fc_salescode = '5413' and fc_branch = '2053' and date_format(fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d');

delete a from t_billdtl a
inner join t_billmst b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno and b.fc_salescode = '5413' and date_format(b.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
where a.fc_branch = '2053';
delete from t_billmst where fc_salescode = '5413' and fc_branch = '2053' and  date_format(fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d');

delete a from t_movebrgdtl a
inner join t_movebrgmst b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno and b.fc_inputby = '5413' and date_format(b.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
where a.fc_branch = '2053';
delete from t_movebrgmst where fc_inputby = '5413' and fc_branch = '2053' and date_format(fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d');

#SO Jika Ada
INSERT INTO t_somst
  (fc_Branch, fc_SONo, fc_Year, fc_Periode, fd_Docdate, fd_Postdate, fd_Inputdate, 
   fd_Delvdate, fc_Inputby, fc_tipe, fc_POSO, fn_TOP, fc_Salescode, fc_CompCode, 
   fc_Areacode, fc_Chanelcode, fc_DivCode, fc_DistCode, fc_MU, fm_Rate, 
   fc_Custbayar, fc_CustKrm, fc_Gdcode, fc_keterangan, fn_disc1, fn_disc2, 
   fm_Brutto1, fm_Disc1, fm_Netto1, fm_DPP1, fm_PPn1, fm_Brutto2, fm_Disc2, 
   fm_Netto2, fm_DPP2, fm_PPn2, fc_AppBy, fd_Appdate, fc_Tax, fc_Status, 
   fc_TipeGM, fc_SJRef, fc_AppBy1, fd_Appdate1, fc_PreSONo)
Select 
a.fc_branch , concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) ,   
a.fc_year , date_format(a.fd_inputdate,'%m') , date_format(a.fd_docdate,'%Y-%m-%d 00:00:00') , a.fd_postdate , a.fd_inputdate , a.fd_duedate as fd_delvdate, 
a.fc_Inputby , 
case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end , '' as fc_POSO , 
coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) as fn_top , 
a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , a.fc_mu , a.fm_rate , 
a.fc_custbayar , a.fc_CustKrm , a.fc_Gdcode , concat('SFA ',a.fc_keterangan) as fc_keterangan , 
ABS(coalesce((SUM(b.fm_netto1) - SUM(b.fm_brutto1)) * 100 / SUM(b.fm_brutto1),0)) as fn_disc1 , 0 as fn_disc2 , 
SUM(b.fm_brutto1) , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc1 , 
SUM(b.fm_netto1) , SUM(b.fm_dpp1) , SUM(b.fm_ppn1) , SUM(b.fm_brutto1) as fm_brutto2 , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc2 , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'' as fc_appby , NULL as fd_appdate ,  '1' as fc_tax , 'D' as fc_status , '601' as fc_tipeGM , '' as fc_sjref , 
'' as fc_appby1 , NULL as fd_appdate1 , '' as fc_presono 
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') and b.fc_type = 'TO' 
and a.fc_tipe = 'TO'
group by a.fc_branch , a.fc_invno , b.fc_type;

insert into t_domst 
(fc_branch , fc_dono , fc_sono , fc_year , fc_periode , fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_tipe , 
fn_top , fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode , fc_mu , fm_rate , 
fc_custbayar , fc_CustKrm , fc_Gdcode , fc_keterangan , fn_disc1 , fn_disc2 , fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_appby , fd_appdate , fc_postgoods , fc_tax , fc_status , fc_tipeGM , fc_sjref)
Select 
a.fc_branch , concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) , 
concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
		 else 'DR'
end ) ,  
a.fc_year , date_format(a.fd_inputdate,'%m') , date_format(a.fd_docdate,'%Y-%m-%d 00:00:00') , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
case when a.fc_tipe = 'Reversal' then '632' 
     when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'     
		 else 'DR'
end as fc_type, 
coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) as fn_top , 
a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , a.fc_mu , a.fm_rate , 
a.fc_custbayar , a.fc_CustKrm , a.fc_Gdcode , concat('SFA ',a.fc_keterangan) as fc_keterangan  , 
ABS(coalesce((SUM(b.fm_netto1) - SUM(b.fm_brutto1)) * 100 / SUM(b.fm_brutto1),0)) as fn_disc1 , 0 as fn_disc2 , 
SUM(b.fm_brutto1) , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc1 , 
SUM(b.fm_netto1) , SUM(b.fm_dpp1) , SUM(b.fm_ppn1) , SUM(b.fm_brutto1) as fm_brutto2 , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc2 , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'DAVID' as fc_appby , '2020-12-31' as fd_appdate , '0' as fc_postgoods , '1' as fc_tax , 'P' as fc_status , '601' as fc_tipeGM , '' as fc_sjref 
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') and b.fc_type = 'TO' 
and a.fc_tipe = 'TO'
group by a.fc_branch , a.fc_invno , b.fc_type;

#SJ
insert into t_domst 
(fc_branch , fc_dono , fc_sono , fc_year , fc_periode , fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_tipe , 
fn_top , fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode , fc_mu , fm_rate , 
fc_custbayar , fc_CustKrm , fc_Gdcode , fc_keterangan , fn_disc1 , fn_disc2 , fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_appby , fd_appdate , fc_postgoods , fc_tax , fc_status , fc_tipeGM , fc_sjref)
Select 
a.fc_branch , concat(a.fc_invno,'-',
case when a.fc_tipe = 'Reversal' then 'DRC' 
     when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Kembali' then '631'
     when b.fc_type = 'Galon Tukar' then 'GTK'     
     when b.fc_type = 'Galon Extra Isi' then 'DG2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'       
		 else 'DR'
end ) , 
concat(a.fc_invno,'-',
case when a.fc_tipe = 'Reversal' then 'DRC'
     when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
     when b.fc_type = 'Galon Kembali' then 'DR'
     when b.fc_type = 'Galon Tukar' then 'DR' 
     when b.fc_type = 'Galon Extra Isi' then 'DG2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'           
		 else 'DR'
end ) ,  
a.fc_year , date_format(a.fd_inputdate,'%m') , date_format(a.fd_docdate,'%Y-%m-%d 00:00:00') , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
case when a.fc_tipe = 'Reversal' then 'DRC' 
     when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Kembali' then '631'
     when b.fc_type = 'Galon Tukar' then '631'      
     when b.fc_type = 'Galon Extra Isi' then 'DG'
     when b.fc_type = 'Air Extra Isi' then 'DR'      
		 else 'DR'
end as fc_type, 
coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) as fn_top , 
a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , a.fc_mu , a.fm_rate , 
coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custbayar , a.fc_CustKrm , a.fc_Gdcode , concat('SFA ',a.fc_keterangan) as fc_keterangan  , 
ABS(coalesce((SUM(b.fm_netto1) - SUM(b.fm_brutto1)) * 100 / SUM(b.fm_brutto1),0)) as fn_disc1 , 0 as fn_disc2 , 
SUM(b.fm_brutto1) , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc1 , 
SUM(b.fm_netto1) , SUM(b.fm_dpp1) , SUM(b.fm_ppn1) , SUM(b.fm_brutto1) as fm_brutto2 , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc2 , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'DAVID' as fc_appby , '2020-12-31' as fd_appdate , '1' as fc_postgoods , '1' as fc_tax , 'B' as fc_status , 
case when b.fc_type = 'Galon Kembali' then '631' 
     when b.fc_type = 'Galon Tukar' then '631' else '601' end as fc_tipeGM , '' as fc_sjref 
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
 and b.fc_type in ('Normal' , 'Air Extra Isi' , 'Galon Kembali' , 'Galon Tukar') and a.fc_tipe in ('Normal','Reversal')
group by a.fc_branch , a.fc_invno , b.fc_type;

#ReturnSJ
insert into t_domst 
(fc_branch , fc_dono , fc_sono , fc_year , fc_periode , fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_tipe , 
fn_top , fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode , fc_mu , fm_rate , 
fc_custbayar , fc_CustKrm , fc_Gdcode , fc_keterangan , fn_disc1 , fn_disc2 , fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_appby , fd_appdate , fc_postgoods , fc_tax , fc_status , fc_tipeGM , fc_sjref)
Select 
a.fc_branch , concat(a.fc_invno,'-',
case when a.fc_tipe = 'Reversal' then 'RDRC' 
     when b.fc_type = 'Galon Beli' then 'RDG'  
     when b.fc_type = 'Galon Jamin' then 'RDI'
     when b.fc_type = 'Galon Kembali' then 'R631'
     when b.fc_type = 'Galon Tukar' then 'RGTK'     
     when b.fc_type = 'Galon Extra Isi' then 'RDG2'
     when b.fc_type = 'Air Extra Isi' then 'RDR2'       
		 else 'RDR'
end ) , 
a.fc_sjref as fc_sono ,  
a.fc_year , date_format(a.fd_inputdate,'%m') , date_format(a.fd_docdate,'%Y-%m-%d 00:00:00') , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
'DRC' as fc_type, 
coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) as fn_top , 
a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , a.fc_mu , a.fm_rate , 
coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custbayar , a.fc_CustKrm , a.fc_Gdcode , concat('SFA ',a.fc_keterangan) as fc_keterangan  , 
ABS(coalesce((SUM(b.fm_netto1) - SUM(b.fm_brutto1)) * 100 / SUM(b.fm_brutto1),0)) as fn_disc1 , 0 as fn_disc2 , 
SUM(b.fm_brutto1) , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc1 , 
SUM(b.fm_netto1) , SUM(b.fm_dpp1) , SUM(b.fm_ppn1) , SUM(b.fm_brutto1) as fm_brutto2 , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc2 , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'DAVID' as fc_appby , '2020-12-31' as fd_appdate , '1' as fc_postgoods , '1' as fc_tax , 'B' as fc_status , 
'611' as fc_tipeGM , '' as fc_sjref 
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
 and b.fc_type in ('Normal' , 'Air Extra Isi' , 'Galon Kembali' , 'Galon Tukar') and a.fc_tipe = 'RETURN' and fn_qty_return > 0 
group by a.fc_branch , a.fc_invno , b.fc_type;

#ReturnBill
insert into t_billmst 
(fc_branch , fc_docno , fc_docref , fc_year , fc_periode , 
fc_tipe , 
fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode ,  
fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_ppn , fd_duedate , 
fc_custcode , fc_suppcode , fc_MUcode , fm_rate , fn_top , fc_keterangan , 
fn_disc1 , fn_disc2 , 
fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_tax , fc_return , fc_status , fc_sopircode)
Select 
a.fc_branch , concat(a.fc_invno,'-',
case when a.fc_tipe = 'Reversal' then 'RDRC'
     when b.fc_type = 'Galon Beli' then 'RDG'  
     when b.fc_type = 'Galon Jamin' then 'RDI'
     when b.fc_type = 'Galon Extra Isi' then 'RDG2'
     when b.fc_type = 'Air Extra Isi' then 'RDR2'
		 else 'RDR'
end ) , 
SUBSTR(a.fc_invno,10,20) as docref , 
a.fc_year , date_format(a.fd_inputdate,'%m') , 
'DRC' as fc_type, 
a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , 
date_format(a.fd_docdate,'%Y-%m-%d 00:00:00') , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
'1' as fc_ppn , 
coalesce(date_add(a.fd_inputdate, interval coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) day),'2020-12-31') as fd_duedate , 
coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custcode , '' as fc_suppcode , 'IDR' as fc_MU , '1' as fm_rate , 
coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) as fn_top , 
concat('SFA ',a.fc_keterangan) as fc_keterangan  , 
ABS(coalesce((SUM(b.fm_netto1) - SUM(b.fm_brutto1)) * 100 / SUM(b.fm_brutto1),0)) as fn_disc1 , 0 as fn_disc2 , 
SUM(b.fm_brutto1) , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc1 , 
SUM(b.fm_netto1) , SUM(b.fm_dpp1) , SUM(b.fm_ppn1) , SUM(b.fm_brutto1) as fm_brutto2 , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc2 , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'1' as fc_tax , 
'602' as fc_return ,  'C' as fc_status , a.fc_sopircode as fc_sopircode
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
and b.fc_type in ('Normal','Galon Jamin','Galon Beli', 'Air Extra Isi' , 'Galon Extra Isi') and a.fc_tipe = 'RETURN' and fn_qty_return > 0 
group by a.fc_branch , a.fc_invno , b.fc_type;

insert into t_billmst 
(fc_branch , fc_docno , fc_docref , fc_year , fc_periode , 
fc_tipe , 
fc_salescode , fc_compcode , fc_areacode , fc_chanelcode , fc_divcode , fc_distcode ,  
fd_docdate , fd_postdate , fd_inputdate  , 
fc_Inputby , 
fc_ppn , fd_duedate , 
fc_custcode , fc_suppcode , fc_MUcode , fm_rate , fn_top , fc_keterangan , 
fn_disc1 , fn_disc2 , 
fm_brutto1 , fm_disc1 , fm_netto1 , fm_dpp1 , fm_ppn1 ,
fm_brutto2 , fm_disc2 , fm_netto2 , fm_dpp2 , fm_ppn2 , fc_tax , fc_return , fc_status , fc_sopircode)
Select 
a.fc_branch , concat(a.fc_invno,'-',
case when a.fc_tipe = 'Reversal' then 'DRC'
     when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Extra Isi' then 'DG2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'
		 else 'DR'
end ) , 
concat(a.fc_invno,'-',
case when a.fc_tipe = 'Reversal' then 'DRC' 
     when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
     when b.fc_type = 'Galon Extra Isi' then 'DR2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'       
		 else 'DR'
end ) as docref , 
a.fc_year , date_format(a.fd_inputdate,'%m') , 
case when a.fc_tipe = 'Reversal' then 'DRC' 
     when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Extra Isi' then 'DG'
     when b.fc_type = 'Air Extra Isi' then 'DR'     
		 else 'DR'
end as fc_type, 
a.fc_salescode , '0002' as fc_compcode , a.fc_areacode , a.fc_chanelcode , a.fc_divcode , a.fc_distcode , 
date_format(a.fd_docdate,'%Y-%m-%d 00:00:00') , a.fd_postdate , a.fd_inputdate , 
a.fc_Inputby , 
'1' as fc_ppn , 
coalesce(date_add(a.fd_inputdate, interval coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) day),'2020-12-31') as fd_duedate , 
coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custcode , '' as fc_suppcode , 'IDR' as fc_MU , '1' as fm_rate , 
coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) as fn_top , 
concat('SFA ',a.fc_keterangan) as fc_keterangan  , 
ABS(coalesce((SUM(b.fm_netto1) - SUM(b.fm_brutto1)) * 100 / SUM(b.fm_brutto1),0)) as fn_disc1 , 0 as fn_disc2 , 
SUM(b.fm_brutto1) , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc1 , 
SUM(b.fm_netto1) , SUM(b.fm_dpp1) , SUM(b.fm_ppn1) , SUM(b.fm_brutto1) as fm_brutto2 , 
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc2 , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,
'1' as fc_tax , 
case when SUM(case when b.fc_brgcode like '%GALO%' then 1 else 0 end ) > 0 then '1'
   else 0 end as fc_return ,  'R' as fc_status , a.fc_sopircode as fc_sopircode
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
and b.fc_type in ('Normal','Galon Jamin','Galon Beli', 'Air Extra Isi' , 'Galon Extra Isi') and a.fc_tipe in ('Normal','Reversal')
group by a.fc_branch , a.fc_invno , b.fc_type;

INSERT INTO t_movebrgmst
  (fc_Branch, fc_Docno, fc_Year, fc_Periode, fc_Tipe, fc_DocRef, fd_Docdate, 
   fd_Postdate, fd_Inputdate, fc_InputBy, fc_NoSJ, fc_NoExpd, fc_NoCont, 
   fc_PPN, fd_DueDate, fc_Custcode, fc_Suppcode, fc_MuCode, fm_Rate, fn_TOP, 
   fc_GdFrom, fc_GdDest, fc_Keterangan, fn_Disc1, fn_Disc2, fm_Brutto1, 
   fm_Netto1, fm_Invoice1, fm_Disc1, fm_DPP1, fm_PPn1, fm_Brutto2, fm_Netto2, 
   fm_Invoice2, fm_Disc2, fm_DPP2, fm_PPn2, fc_Status, fc_Tcode)
Select
   '2053' as fc_Branch, 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Extra Isi' then 'DG2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'      
		 else 'DR'
  end )as fc_Docno, a.fc_Year, a.fc_Periode,
  case when b.fc_type = 'Galon Beli' then 'DG'  
       when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Extra Isi' then 'DG'
     when b.fc_type = 'Air Extra Isi' then 'DR'        
       else 'DR'
  end as fc_Tipe, 
  concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
     when b.fc_type = 'Galon Jamin' then 'DR'
     when b.fc_type = 'Galon Extra Isi' then 'DR2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'      
		 else 'DR'
end )as fc_DocRef , date_format(a.fd_docdate,'%Y-%m-%d 00:00:00'), a.fd_Postdate, a.fd_Inputdate, a.fc_salescode as fc_InputBy, 
  '' as fc_NoSJ, '' as fc_NoExpd, '' as fc_NoCont, '1' as fc_PPN, 
coalesce(date_add(a.fd_inputdate, interval coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) day),'2020-12-31') as fd_duedate ,
coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custcode , 
  '' as fc_Suppcode, 'IDR' as fc_MuCode, 
   '1' as fm_Rate, 
coalesce(case when coalesce(z.fn_top , 0) = 0 then y.fn_top else z.fn_top end , a.fn_top) as fn_top , 
'' as fc_GdFrom, '' as fc_GdDest, concat('SFA ',a.fc_keterangan) as fc_Keterangan, 
ABS(coalesce((SUM(b.fm_netto1) - SUM(b.fm_brutto1)) * 100 / SUM(b.fm_brutto1),0)) as fn_disc1 , 0 as fn_disc2 , 
SUM(b.fm_brutto1) , SUM(b.fm_netto1) , 0 as fm_invoice1 ,
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc1 , 
SUM(b.fm_dpp1) , SUM(b.fm_ppn1) , SUM(b.fm_brutto1) as fm_brutto2 , 0 as fm_invoice2 ,
ABS((SUM(b.fm_netto1) - SUM(b.fm_brutto1))) as fm_disc2 , SUM(b.fm_netto1) AS fm_netto2 ,
SUM(b.fm_dpp1) as fm_dpp2, SUM(b.fm_ppn1) as fm_ppn2,   
    'R' as fc_Status, '' as fc_Tcode
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') and 
b.fc_type in ('Galon Jamin','Galon Beli','Galon Extra Isi') and a.fc_tipe in ('Normal' , 'RETURN')
group by a.fc_branch , a.fc_invno , b.fc_type;

INSERT INTO t_sodtl
  (fc_Branch, fc_SOno, fc_BrgCode, fc_No, fc_Extra, fn_qty1, fn_qty2, fc_Satcode, 
   fn_Extra1, fn_Extra2, fc_GdCode, fn_Harga1, fn_Harga2, fn_Disc1, fn_Disc2, 
   fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, fm_PPN1, fm_Disc2_1, 
   fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPN2, fc_Reason, fc_BrgRef, 
   fc_GrpRef, fn_QtyRef, fc_PromoRef)
Select
     a.fc_branch , concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
		 else 'DR'
end ) ,  

   b.fc_BrgCode, b.fc_No, 
   case when b.fc_type = 'Bonus' Then '2' else '1' end as fc_Extra, 
   b.fn_qty - fn_qty_return as fn_qty1, 
   (b.fn_qty - fn_qty_return) * coalesce(c.fn_convert,0) as fn_qty2, b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2, b.fc_GdCode, b.fn_Harga, 
   coalesce(b.fn_harga / c.fn_convert , 0) as fn_Harga2, 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * 100 / b.fn_harga , 0 ) as fn_Disc1 , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * 100 / b.fn_harga , 0 ) as fn_Disc2  , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_2  ,   
   b.fm_brutto1 ,  
   b.fm_netto1 , b.fm_dpp1 , b.fm_ppn1 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_2  ,    
   b.fm_brutto1 as fm_brutto2 , 
   b.fm_netto1 AS fm_netto2 ,
   b.fm_dpp1 as fm_dpp2, b.fm_ppn1 as fm_ppn2,   
   
   '' as fc_Reason , '' as fc_BrgRef , '' as fc_grpref , '0' as fn_qtyRef , '' as fc_promoRef
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join t_satuan c on a.fc_branch  = c.fc_branch and b.fc_brgcode = c.fc_brgcode and b.fc_satcode = c.fc_satcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
and a.fc_tipe = 'TO';

#DO
INSERT INTO t_dodtl
  (fc_Branch, fc_DOno, fc_BrgCode, fc_Batch, fc_No, fc_Extra, fn_qty1, 
   fn_qty2, fc_Satcode, fn_Extra1, fn_Extra2, fc_GdCode, fn_Harga1, fn_Harga2, 
   fn_Disc1, fn_Disc2, fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, 
   fm_PPN1, fm_Disc2_1, fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPN2, 
   fc_ReasonRtr)

Select
   b.fc_Branch, concat(b.fc_invno,'-',
   case when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Extra Isi' then 'DG2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'       
		 else 'DR'
   end ) , b.fc_BrgCode, 
   case when b.fc_type = 'Galon Kembali' then 'UN'
     when b.fc_type = 'Galon Tukar' then 'UN'
   else  '1' end as fc_Batch, 
   b.fc_No, 
   case when b.fc_type = 'Bonus' Then '2'  
        when b.fc_type = 'Air Extra Isi' Then '3'
        when b.fc_type = 'Bonus Tambahan' Then '3'
   else '1' end as fc_Extra , 
   b.fn_qty - fn_qty_return as fn_qty1, 
   (b.fn_qty - fn_qty_return) * coalesce(c.fn_convert,0) as fn_qty2, b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2, b.fc_GdCode, b.fn_Harga, 
   coalesce(b.fn_harga / c.fn_convert ,0) as fn_Harga2, 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * 100 / b.fn_harga , 0 ) as fn_Disc1 , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * 100 / b.fn_harga , 0 ) as fn_Disc2  , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_2  ,   
   b.fm_brutto1 ,  
   b.fm_netto1 , b.fm_dpp1 , b.fm_ppn1 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_2  ,     
   b.fm_brutto1 as fm_brutto2 , 
   b.fm_netto1 AS fm_netto2 ,
   b.fm_dpp1 as fm_dpp2, b.fm_ppn1 as fm_ppn2, 
    '' as fc_ReasonRtr
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join t_satuan c on a.fc_branch  = c.fc_branch and b.fc_brgcode = c.fc_brgcode and b.fc_satcode = c.fc_satcode
left outer join db_dltisnow.t_barang d on b.fc_brgcode = d.fc_brgcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') and a.fc_tipe = 'TO';

#SJ
INSERT INTO t_dodtl
  (fc_Branch, fc_DOno, fc_BrgCode, fc_Batch, fc_No, fc_Extra, fn_qty1, 
   fn_qty2, fc_Satcode, fn_Extra1, fn_Extra2, fc_GdCode, fn_Harga1, fn_Harga2, 
   fn_Disc1, fn_Disc2, fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, 
   fm_PPN1, fm_Disc2_1, fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPN2, 
   fc_ReasonRtr)

Select
   b.fc_Branch, concat(b.fc_invno,'-',
   case when a.fc_tipe = 'Reversal' then 'DRC' 
     when b.fc_type = 'Galon Beli' then 'DG'  
     when b.fc_type = 'Galon Jamin' then 'DI'
     when b.fc_type = 'Galon Kembali' then '631'
     when b.fc_type = 'Galon Tukar' then 'GTK'
     when b.fc_type = 'Galon Extra Isi' then 'DG2'
     when b.fc_type = 'Air Extra Isi' then 'DR2'       
		 else 'DR'
   end )  , b.fc_BrgCode, 
   case 
     when b.fc_type = 'Galon Beli' then 'BE'  
     when b.fc_type = 'Galon Jamin' then 'JA'
     when b.fc_type = 'Galon Kembali' then 'UN'
     when b.fc_type = 'Galon Tukar' then 'TU'
     when b.fc_type = 'Galon Extra Isi' then 'BE'     
		 else '1'
   end as fc_Batch,  
   b.fc_No, 
   case when b.fc_type = 'Bonus' Then '2'  
        when b.fc_type = 'Air Extra Isi' Then '3'
        when b.fc_type = 'Bonus Tambahan' Then '3'
   else '1' end as fc_Extra , 
   ABS(b.fn_qty - fn_qty_return) as fn_qty1, 
   ABS(b.fn_qty - fn_qty_return) * coalesce(c.fn_convert,0) as fn_qty2, b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2, b.fc_GdCode, b.fn_Harga, 
   coalesce(b.fn_harga / c.fn_convert ,0) as fn_Harga2,
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * 100 / b.fn_harga , 0 ) as fn_Disc1 , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * 100 / b.fn_harga , 0 ) as fn_Disc2  , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_2  ,     
   b.fm_brutto1 ,  
   b.fm_netto1 , b.fm_dpp1 , b.fm_ppn1 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_2  ,      
   b.fm_brutto1 as fm_brutto2 , 
   b.fm_netto1 AS fm_netto2 ,
   b.fm_dpp1 as fm_dpp2, b.fm_ppn1 as fm_ppn2, 
    '' as fc_ReasonRtr
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join t_satuan c on a.fc_branch  = c.fc_branch and b.fc_brgcode = c.fc_brgcode and b.fc_satcode = c.fc_satcode
left outer join db_dltisnow.t_barang d on b.fc_brgcode = d.fc_brgcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
 and b.fc_type in ( 'Normal' , 'Bonus Tambahan', 'Bonus' , 'Air Extra Isi' , 'GALON KEMBALI' , 'Galon Tukar') and a.fc_tipe in ('Normal','Reversal');

#Return
INSERT INTO t_dodtl
  (fc_Branch, fc_DOno, fc_BrgCode, fc_Batch, fc_No, fc_Extra, fn_qty1, 
   fn_qty2, fc_Satcode, fn_Extra1, fn_Extra2, fc_GdCode, fn_Harga1, fn_Harga2, 
   fn_Disc1, fn_Disc2, fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, 
   fm_PPN1, fm_Disc2_1, fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPN2, 
   fc_ReasonRtr)

Select
   b.fc_Branch, concat(b.fc_invno,'-',
   case when a.fc_tipe = 'Reversal' then 'RDRC' 
     when b.fc_type = 'Galon Beli' then 'RDG'  
     when b.fc_type = 'Galon Jamin' then 'RDI'
     when b.fc_type = 'Galon Kembali' then 'R631'
     when b.fc_type = 'Galon Tukar' then 'RGTK'
     when b.fc_type = 'Galon Extra Isi' then 'RDG2'
     when b.fc_type = 'Air Extra Isi' then 'RDR2'       
		 else 'RDR'
   end )  , b.fc_BrgCode, 
   case 
     when b.fc_type = 'Galon Beli' then 'BE'  
     when b.fc_type = 'Galon Jamin' then 'JA'
     when b.fc_type = 'Galon Kembali' then 'UN'
     when b.fc_type = 'Galon Tukar' then 'TU'
     when b.fc_type = 'Galon Extra Isi' then 'BE'     
		 else '1'
   end as fc_Batch,  
   b.fc_No, 
   case when b.fc_type = 'Bonus' Then '2'  
        when b.fc_type = 'Air Extra Isi' Then '3'
        when b.fc_type = 'Bonus Tambahan' Then '3'
   else '1' end as fc_Extra , 
   fn_qty_return as fn_qty1, 
   fn_qty_return * coalesce(c.fn_convert,0) as fn_qty2, b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2, b.fc_GdCode, b.fn_Harga, 
   coalesce(b.fn_harga / c.fn_convert ,0) as fn_Harga2,
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * 100 / b.fn_harga , 0 ) as fn_Disc1 , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * 100 / b.fn_harga , 0 ) as fn_Disc2  , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * fn_qty_return  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * fn_qty_return , 0 )  as fm_disc1_2  ,     
   b.fm_brutto1 ,  
   b.fm_netto1 , b.fm_dpp1 , b.fm_ppn1 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * fn_qty_return  , 0 )  as fm_disc2_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * fn_qty_return  , 0 )  as fm_disc2_2  ,      
   b.fm_brutto1 as fm_brutto2 , 
   b.fm_netto1 AS fm_netto2 ,
   b.fm_dpp1 as fm_dpp2, b.fm_ppn1 as fm_ppn2, 
    '' as fc_ReasonRtr
from t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join t_satuan c on a.fc_branch  = c.fc_branch and b.fc_brgcode = c.fc_brgcode and b.fc_satcode = c.fc_satcode
left outer join db_dltisnow.t_barang d on b.fc_brgcode = d.fc_brgcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
 and b.fc_type in ( 'Normal' , 'Bonus Tambahan', 'Bonus' , 'Air Extra Isi' , 'GALON KEMBALI' , 'Galon Tukar') and a.fc_tipe = 'RETURN' and fn_qty_return > 0;

#Return BIll
INSERT INTO t_billdtl
  (fc_Branch, fc_Docno, fc_No, fc_DocRef, fc_Year, fc_Periode, fc_Brgcode, 
   fc_TipeCode, fc_Grpcode, fc_Valcode, fc_ValTipe, fc_PriceCtr, fc_Prdcode, 
   fc_Mrkcode, fc_Jnscode, fc_Sizcode, fc_MdlCode, fc_GdCode, fc_Batch, 
   fd_Postdate, fc_Suppcode, fc_Custcode, fc_CostCTR, fc_Extra, fn_Qty1, 
   fn_Qty2, fc_Satcode, fn_Extra1, fn_Extra2, fn_Harga1, fn_Harga2, fn_Disc1, 
   fn_Disc2, fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, fm_PPN1, 
   fm_Disc2_1, fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPn2, fn_SisaGalon, 
   fn_SisaQtyCN, fn_SisaValCN)
Select 
   b.fc_Branch, concat(a.fc_invno,'-',
   case when a.fc_tipe = 'Reversal' then 'RDRC' 
        when b.fc_type = 'Galon Beli' then 'RDG'  
        when b.fc_type = 'Galon Jamin' then 'RDI'
        when b.fc_type = 'Galon Extra Isi' then 'RDG2'
        when b.fc_type = 'Air Extra Isi' then 'RDR2'        
        else 'RDR' 
   end ) as fc_docno , b.fc_No, concat(a.fc_invno,'-',
   case when b.fc_type = 'Galon Beli' then 'RDR'  
        when b.fc_type = 'Galon Jamin' then 'RDR'
        when b.fc_type = 'Galon Extra Isi' then 'RDR2'
        when b.fc_type = 'Air Extra Isi' then 'RDR2'         
		 else 'RDR'
   end ) as fc_docref , 
   date_format('2020-12-31','%Y') as fc_Year, date_format('2020-12-31','%m') as fc_Periode, b.fc_Brgcode, 
   b.fc_TipeCode, b.fc_Grpcode, b.fc_Valcode, b.fc_ValTipe, b.fc_PriceCtr, b.fc_Prdcode, 
   b.fc_Mrkcode, b.fc_Jnscode, b.fc_Sizcode, b.fc_MdlCode, b.fc_GdCode, 
  case when b.fc_type = 'Galon Beli' then 'BE'  
       when b.fc_type = 'Galon Jamin' then 'JA'
       when b.fc_type = 'Galon Extra Isi' then 'BE'       
       else '1'
   end as fc_Batch,
   a.fd_Postdate , '' as fc_Suppcode, 
   coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custcode , 
   coalesce(d.fc_costctr,'') as fc_CostCTR, 
   case when b.fc_type = 'Bonus' Then '2'  
        when b.fc_type = 'Air Extra Isi' Then '3'
        when b.fc_type = 'Bonus Tambahan' Then '3'
   else '1' end as fc_Extra, 
   fn_qty_return as fn_qty1, 
   fn_qty_return * coalesce(c.fn_convert,0) as fn_qty2,  b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2 , b.fn_Harga, coalesce(b.fn_harga / c.fn_convert ,0) as fn_Harga2, 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * 100 / b.fn_harga , 0 ) as fn_Disc1 , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * 100 / b.fn_harga , 0 ) as fn_Disc2  , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * fn_qty_return  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * fn_qty_return  , 0 )  as fm_disc1_2  ,     
   b.fm_brutto1 ,  
   b.fm_netto1 , b.fm_dpp1 , b.fm_ppn1 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) *  fn_qty_return  , 0 )  as fm_disc2_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) *  fn_qty_return  , 0 )  as fm_disc2_2  ,      
   b.fm_brutto1 as fm_brutto2 , 
   b.fm_netto1 AS fm_netto2 ,
   b.fm_dpp1 as fm_dpp2, b.fm_ppn1 as fm_ppn2,    
   0 as fn_SisaGalon, 0 as fn_SisaQtyCN, 0 as fn_SisaValCN
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join t_satuan c on a.fc_branch  = c.fc_branch and b.fc_brgcode = c.fc_brgcode and b.fc_satcode = c.fc_satcode
left outer join db_dltisnow.t_barang d on b.fc_brgcode = d.fc_brgcode
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
and a.fc_tipe = 'RETURN' and fn_qty_return > 0 and b.fc_type in ('Normal','Bonus','Bonus Tambahan','Galon Jamin','Galon Beli', 'Air Extra Isi' , 'Galon Extra Isi') ;

INSERT INTO t_billdtl
  (fc_Branch, fc_Docno, fc_No, fc_DocRef, fc_Year, fc_Periode, fc_Brgcode, 
   fc_TipeCode, fc_Grpcode, fc_Valcode, fc_ValTipe, fc_PriceCtr, fc_Prdcode, 
   fc_Mrkcode, fc_Jnscode, fc_Sizcode, fc_MdlCode, fc_GdCode, fc_Batch, 
   fd_Postdate, fc_Suppcode, fc_Custcode, fc_CostCTR, fc_Extra, fn_Qty1, 
   fn_Qty2, fc_Satcode, fn_Extra1, fn_Extra2, fn_Harga1, fn_Harga2, fn_Disc1, 
   fn_Disc2, fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, fm_PPN1, 
   fm_Disc2_1, fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPn2, fn_SisaGalon, 
   fn_SisaQtyCN, fn_SisaValCN)
Select 
   b.fc_Branch, concat(a.fc_invno,'-',
   case when a.fc_tipe = 'Reversal' then 'DRC' 
        when b.fc_type = 'Galon Beli' then 'DG'  
        when b.fc_type = 'Galon Jamin' then 'DI'
        when b.fc_type = 'Galon Extra Isi' then 'DG2'
        when b.fc_type = 'Air Extra Isi' then 'DR2'        
        else 'DR' 
   end ) as fc_docno , b.fc_No, concat(a.fc_invno,'-',
   case when b.fc_type = 'Galon Beli' then 'DR'  
        when b.fc_type = 'Galon Jamin' then 'DR'
        when b.fc_type = 'Galon Extra Isi' then 'DR2'
        when b.fc_type = 'Air Extra Isi' then 'DR2'         
		 else 'DR'
   end ) as fc_docref , 
   date_format('2020-12-31','%Y') as fc_Year, date_format('2020-12-31','%m') as fc_Periode, b.fc_Brgcode, 
   b.fc_TipeCode, b.fc_Grpcode, b.fc_Valcode, b.fc_ValTipe, b.fc_PriceCtr, b.fc_Prdcode, 
   b.fc_Mrkcode, b.fc_Jnscode, b.fc_Sizcode, b.fc_MdlCode, b.fc_GdCode, 
  case when b.fc_type = 'Galon Beli' then 'BE'  
       when b.fc_type = 'Galon Jamin' then 'JA'
       when b.fc_type = 'Galon Extra Isi' then 'BE'       
       else '1'
   end as fc_Batch,
   a.fd_Postdate , '' as fc_Suppcode, 
   coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custcode , 
   coalesce(d.fc_costctr,'') as fc_CostCTR, 
   case when b.fc_type = 'Bonus' Then '2'  
        when b.fc_type = 'Air Extra Isi' Then '3'
        when b.fc_type = 'Bonus Tambahan' Then '3'
   else '1' end as fc_Extra, 
   ABS(b.fn_qty - fn_qty_return) as fn_qty1, 
   ABS(b.fn_qty - fn_qty_return) * coalesce(c.fn_convert,0) as fn_qty2,  b.fc_Satcode, '0' as fn_Extra1, '0' as fn_Extra2 , b.fn_Harga, coalesce(b.fn_harga / c.fn_convert ,0) as fn_Harga2, 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * 100 / b.fn_harga , 0 ) as fn_Disc1 , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * 100 / b.fn_harga , 0 ) as fn_Disc2  , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_2  ,     
   b.fm_brutto1 ,  
   b.fm_netto1 , b.fm_dpp1 , b.fm_ppn1 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_2  ,      
   b.fm_brutto1 as fm_brutto2 , 
   b.fm_netto1 AS fm_netto2 ,
   b.fm_dpp1 as fm_dpp2, b.fm_ppn1 as fm_ppn2,    
   0 as fn_SisaGalon, 0 as fn_SisaQtyCN, 0 as fn_SisaValCN
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join t_satuan c on a.fc_branch  = c.fc_branch and b.fc_brgcode = c.fc_brgcode and b.fc_satcode = c.fc_satcode
left outer join db_dltisnow.t_barang d on b.fc_brgcode = d.fc_brgcode
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
and a.fc_tipe in ('Normal' , 'Reversal') and b.fc_type in ('Normal','Bonus','Bonus Tambahan','Galon Jamin','Galon Beli', 'Air Extra Isi' , 'Galon Extra Isi') ;

INSERT INTO t_movebrgdtl
  (fc_Branch, fc_Docno, fc_DocRef, fc_Year, fc_Periode, fc_No, fc_Tipe, 
   fc_Brgcode, fc_GdCode, fc_Batch, fc_BrgName, fc_NoRef, fd_Inputdate, 
   fc_Suppcode, fc_Custcode, fc_CostCTR, fc_CR, fc_Satcode, fc_Extra, fn_Qty1, 
   fn_Qty2, fn_Extra1, fn_Extra2, fn_Harga1, fn_Harga2, fn_Disc1, fn_Disc2, 
   fm_Disc1_1, fm_Disc1_2, fm_Brutto1, fm_Netto1, fm_DPP1, fm_PPN1, fm_Disc2_1, 
   fm_Disc2_2, fm_Brutto2, fm_Netto2, fm_DPP2, fm_PPn2, fn_NewAvg, fm_NewStd, 
   fm_NewValue, fn_QtySaldo, fn_QtySaldo1, fn_QtySaldo2, fn_AVGSaldo, fn_StdSaldo, 
   fn_ValueSaldo, fc_InsTipe)
Select
   b.fc_Branch, 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
         when b.fc_type = 'Galon Jamin' then 'DI'
         when b.fc_type = 'Galon Extra Isi' then 'DG2'
         when b.fc_type = 'Air Extra Isi' then 'DR2'          
		 else 'DR'
   end ) as fc_docno , 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DR'  
         when b.fc_type = 'Galon Jamin' then 'DR'
         when b.fc_type = 'Galon Extra Isi' then 'DR2'
         when b.fc_type = 'Air Extra Isi' then 'DR2'          
		 else 'DR'
   end ) as fc_docref , a.fc_Year, a.fc_Periode, b.fc_No, 
   case when b.fc_type = 'Galon Beli' then 'DG'  
       when b.fc_type = 'Galon Jamin' then 'DI'
         when b.fc_type = 'Galon Extra Isi' then 'DG'
         when b.fc_type = 'Air Extra Isi' then 'DR'        
       else 'DR'
   end as fc_Tipe, 
   b.fc_Brgcode, coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_GdCode, 
  case when b.fc_type = 'Galon Beli' then 'BE'  
       when b.fc_type = 'Galon Jamin' then 'JA'
       when b.fc_type = 'Galon Extra Isi' then 'BE'       
       else ''
   end as fc_Batch, '' as  fc_BrgName,  '' as fc_NoRef, a.fd_Inputdate, 
   '' as fc_Suppcode, coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custbayar , '' as fc_CostCTR, 'C' as fc_CR, b.fc_Satcode, case when b.fc_type = 'Bonus' Then '2' else '1' end  as fc_Extra, 
   b.fn_qty - fn_qty_return as fn_qty1, 
   (b.fn_qty - fn_qty_return) * coalesce(c.fn_convert,0) as fn_qty2 , '0' as fn_Extra1, '0' as fn_Extra2 , b.fn_Harga, coalesce(b.fn_harga / c.fn_convert ,0) as fn_Harga2, 
   ((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * 100 / b.fn_harga as fn_Disc1 , 
   ((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * 100 / b.fn_harga as fn_Disc2 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc1_2  ,     
   b.fm_brutto1 ,  
   b.fm_netto1 , b.fm_dpp1 , b.fm_ppn1 , 
   coalesce(((b.fn_Harga * b.fn_Disc1 / 100) + b.fm_disc1_1) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_1  , 
   coalesce(((b.fn_Harga * b.fn_Disc2 / 100) + b.fm_disc1_2) * (b.fn_qty - fn_qty_return)  , 0 )  as fm_disc2_2  ,      
   b.fm_brutto1 as fm_brutto2 , 
   b.fm_netto1 AS fm_netto2 ,
   b.fm_dpp1 as fm_dpp2, b.fm_ppn1 as fm_ppn2,     
   '0' as fn_NewAvg, '0' as fm_NewStd, '0' as fm_NewValue, '0' as fn_QtySaldo, 
   '0' as fn_QtySaldo1, '0' as fn_QtySaldo2, '0' as fn_AVGSaldo, '0' as fn_StdSaldo, '0' as fn_ValueSaldo, 
   'AV' as fc_InsTipe
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join t_satuan c on a.fc_branch  = c.fc_branch and b.fc_brgcode = c.fc_brgcode and b.fc_satcode = c.fc_satcode
left outer join db_dltisnow.t_barang d on b.fc_brgcode = d.fc_brgcode 
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') and 
b.fc_type in ('Galon Jamin','Galon Beli','Galon Extra Isi') and a.fc_tipe in ('Normal' , 'RETURN');

delete a from t_paymentdtl a
inner join t_paymentmst b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno and b.fd_inputby = '5413' and date_format(b.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
where a.fc_branch = '2053';
delete from t_paymentmst where fd_inputby = '5413' and fc_branch = '2053' and date_format(fd_docdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d');

delete a from t_bankdtl a
inner join t_bankmst b on a.fc_branch = b.fc_branch and a.fc_docno = b.fc_docno and b.fc_inputby = '5413' and date_format(b.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
where a.fc_branch = '2053';
delete from t_bankmst where fc_inputby = '5413' and fc_branch = '2053' and date_format(fd_docdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d');

INSERT INTO t_paymentdtl
  (fc_branch, fc_docno, fc_docref, fc_noref, fc_no, fc_gltrans, fc_pk, 
   fc_cr, fc_mu, fm_rate, fm_netto1, fm_netto2, fm_pay1, fm_pay2, fc_status, 
   fc_cashdisc, fm_cashdisc1, fm_cashdisc2)
Select 
   a.fc_branch, 
   concat('REC-',a.fc_invno) as fc_docno, 
   concat(a.fc_invno,'-',case when b.fc_type = 'Galon Beli' then 'DG'  
       when b.fc_type = 'Galon Jamin' then 'DI' 
       when b.fc_type = 'Galon Extra Isi' then 'DG2'
       when b.fc_type = 'Air Extra Isi' then 'DR2'       
       else 'DR'
   end ) as fc_docref,    
   '' as fc_noref,  '01' as fc_no, 
   case when b.fc_type = 'Galon Beli' then 'DG'  
      when b.fc_type = 'Galon Jamin' then 'DI' else 'DR'
   end  as fc_gltrans, 
   '' as fc_pk, 'D' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(b.fm_netto1) as fm_netto1, SUM(b.fm_netto1) as fm_netto2, 
   SUM(b.fm_netto1) as fm_pay1, SUM(b.fm_netto1) as fm_pay2, 
   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from t_invpay a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join t_invmst c on a.fc_branch = c.fc_branch and a.fc_invno = c.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and a.fc_status_payment = 'CASH'
and b.fc_type in ('Normal','Galon Jamin','Galon Beli', 'Galon Extra Isi') 
and a.fm_pay_actual > 0
and c.fc_tipe in ('Normal','RETURN')
group by a.fc_branch , a.fc_invno , b.fc_type;

INSERT INTO t_paymentdtl
  (fc_branch, fc_docno, fc_docref, fc_noref, fc_no, fc_gltrans, fc_pk, 
   fc_cr, fc_mu, fm_rate, fm_netto1, fm_netto2, fm_pay1, fm_pay2, fc_status, 
   fc_cashdisc, fm_cashdisc1, fm_cashdisc2)
Select 
   a.fc_branch, 
    concat('REC-',a.fc_invno) as fc_docno,     
   concat('AD-',a.fc_invno) as fc_docref, 
   '' as fc_noref,  '01' as fc_no, 
   'AR'  as fc_gltrans, 
   '' as fc_pk, 'C' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, fm_netto as fm_netto1, fm_netto as fm_netto2, 
   fm_pay_actual as fm_pay1, fm_pay_actual as fm_pay2, 
   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from t_invpay a
left outer join t_invmst c on a.fc_branch = c.fc_branch and a.fc_invno = c.fc_invno
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and a.fc_status_payment = 'CASH' and c.fc_tipe in ('Normal','RETURN')
and a.fm_pay_actual > 0
Group by a.fc_branch , a.fc_salescode ,  a.fc_invno;

INSERT INTO t_paymentmst
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_docref, fc_tipe, 
   fc_suppcode, fc_custcode, fd_docdate, fd_postdate, fd_inputdate, fd_inputby, 
   fc_mucode, fm_rate, fm_value1, fm_value2, fc_note, fc_status, fc_tax, 
   fc_plantbayar)
Select
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  
   concat('REC-',a.fc_invno) as fc_docno, 
    '' as fc_docref, 
   'D' as fc_tipe, a.fc_suppcode, coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) , date_format(a.fd_docdate,'%Y-%m-%d 00:00:00'), a.fd_postdate, a.fd_inputdate, 
   a.fc_salescode as fd_inputby, 'IDR' as fc_mucode, '1' as fm_rate, fm_pay_actual as fm_value1, fm_pay_actual as fm_value2, concat('JUAL TUNAI SALES',fc_salesname) as fc_note, 
   'R' as fc_status, '1' as fc_tax, '' as fc_plantbayar
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join db_dltisnow.t_sales c on a.fc_salescode = c.fc_salescode and a.fc_branch = c.fc_branch
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and  a.fc_branch = '2053' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and b.fm_pay_actual > 0
and a.fc_tipe in ('Normal','RETURN') and b.fc_status_payment = 'CASH' ;
#AD

INSERT INTO t_bankdtl
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_no, fc_docref, 
   fc_bustran, fc_suppcode, fc_custcode, fc_receipt, fc_bttipe, fc_tipe, 
   fc_bank, fc_giro, fc_gltrans, fc_glvs, fc_cr, fd_duedate, fc_note, fc_iocode, 
   fc_costctr, fc_mu, fm_value1, fm_value2, fm_sisa1, fm_sisa2, fc_status, 
   fn_kmawal, fn_kmakhir, fn_volume, fc_bbm, fc_tujuan, fc_sopircode, fc_bankgirocode, 
   fc_bankasal, fc_giroasal, fm_valueasal, fc_docrefbkk, fc_noref)
Select 
   '0002' as fc_compcode, a.fc_branch, fc_year, fc_periode,  concat('AD-',a.fc_invno) as fc_docno, '01' as fc_no, 
   concat('BN-',a.fc_salescode,date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')) as fc_docref, '' as fc_bustran, '' as fc_suppcode, 
   coalesce(case when coalesce(z.fc_custcode , '') = '' then a.fc_custbayar else z.fc_custcode end , a.fc_custbayar) as fc_custcode , a.fc_salescode as fc_receipt, 'D' as fc_bttipe, 
   'AR' as fc_tipe, '' as fc_bank, '' as fc_giro, '219301' as fc_gltrans, '219401' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
   concat('JUAL TUNAI SALES',fc_salesname) as fc_note, a.fc_gdcode as fc_iocode, 
   coalesce(d.fc_costctr,'0') as fc_costctr, 'IDR' as fc_mu, fm_pay_actual as fm_value1, fm_pay_actual as fm_value2, 
   '0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
   '' as fc_tujuan, a.fc_sopircode as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
   '0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
   
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join (
     Select case when SUM(case when fc_brgcode like '%GALO%' then 1 else 0 end ) > 0 then '1'
            else 0 end as fc_costctr , fc_branch , fc_invno
     from t_invdtl where fc_brgcode like '%GALO%' group by fc_branch , fc_invno
) d on a.fc_branch = d.fc_branch and a.fc_invno = d.fc_invno 
left outer join db_dltisnow.t_sales c on a.fc_salescode = c.fc_salescode and a.fc_branch = c.fc_branch
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and a.fc_tipe in ('Normal','RETURN') 
and b.fc_status_payment = 'CASH'
and b.fm_pay_actual > 0;

INSERT INTO t_bankmst
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_tipe, fc_docref, 
   fd_docdate, fd_postdate, fd_inputdate, fc_inputby, fc_mu, fm_rate, fc_gltrans, 
   fc_cr, fm_value1, fm_value2, fc_note, fc_status, fc_tax, fc_udf1, fc_udf2, 
   fc_udf3, fc_udf4, fc_udf5, fm_udf1, fm_udf2, fm_udf3, fm_udf4, fm_udf5)
Select 
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  concat('AD-',a.fc_invno) as fc_docno, 'AR' as fc_tipe, 
   concat('BN-',a.fc_salescode,date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')) as fc_docref, date_format(a.fd_docdate,'%Y-%m-%d 00:00:00'), a.fd_postdate, a.fd_inputdate, a.fc_salescode as fc_inputby, 'IDR' as fc_mu, 
   '1' as fm_rate, '219301' as fc_gltrans, 'D' as fc_cr, fm_pay_actual as fm_value1, fm_pay_actual as fm_value2, '' as fc_note, 'R' as  fc_status, 
   '1' as fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
   '0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and b.fc_status_payment = 'CASH'
and b.fm_pay_actual > 0;

#BN
INSERT INTO t_bankdtl
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_no, fc_docref, 
   fc_bustran, fc_suppcode, fc_custcode, fc_receipt, fc_bttipe, fc_tipe, 
   fc_bank, fc_giro, fc_gltrans, fc_glvs, fc_cr, fd_duedate, fc_note, fc_iocode, 
   fc_costctr, fc_mu, fm_value1, fm_value2, fm_sisa1, fm_sisa2, fc_status, 
   fn_kmawal, fn_kmakhir, fn_volume, fc_bbm, fc_tujuan, fc_sopircode, fc_bankgirocode, 
   fc_bankasal, fc_giroasal, fm_valueasal, fc_docrefbkk, fc_noref)
Select 
   '0002' as fc_compcode, a.fc_branch, fc_year, fc_periode, concat('BN-',a.fc_salescode,date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')) as fc_docno, '01' as fc_no, 
   '' as fc_docref, 'CUSTOMER A/R Clearing' as fc_bustran, '' as fc_suppcode, '' as fc_custcode, a.fc_salescode as fc_receipt, 'R' as fc_bttipe, 
   'BI' as fc_tipe, '' as fc_bank, '' as fc_giro, '111101' as fc_gltrans, '219301' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
   concat('JUAL TUNAI SALES',fc_salesname) as fc_note, a.fc_gdcode as fc_iocode, 
   coalesce(d.fc_costctr,'0') as fc_costctr, 'IDR' as fc_mu, SUM(fm_pay_actual) as fm_value1, SUM(fm_pay_actual) as fm_value2, 
   '0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
   '' as fc_tujuan, a.fc_sopircode as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
   '0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
   
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join (
     Select case when SUM(case when fc_brgcode like '%GALO%' then 1 else 0 end ) > 0 then '1'
            else 0 end as fc_costctr , fc_branch , fc_invno
     from t_invdtl where fc_brgcode like '%GALO%' group by fc_branch , fc_invno
) d on a.fc_branch = d.fc_branch and a.fc_invno = d.fc_invno
left outer join db_dltisnow.t_sales c on a.fc_salescode = c.fc_salescode and a.fc_branch = c.fc_branch 
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and a.fc_tipe in ('Normal','RETURN')
and b.fc_status_payment = 'CASH'
and b.fm_pay_actual > 0
Group by a.fc_salescode; 

INSERT INTO t_bankmst
  (fc_compcode, fc_branch, fc_year, fc_periode, fc_docno, fc_tipe, fc_docref, 
   fd_docdate, fd_postdate, fd_inputdate, fc_inputby, fc_mu, fm_rate, fc_gltrans, 
   fc_cr, fm_value1, fm_value2, fc_note, fc_status, fc_tax, fc_udf1, fc_udf2, 
   fc_udf3, fc_udf4, fc_udf5, fm_udf1, fm_udf2, fm_udf3, fm_udf4, fm_udf5)
Select 
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode, concat('BN-',a.fc_salescode,date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')) as fc_docno, 'BI' as fc_tipe, 
   '' as fc_docref, date_format(a.fd_docdate,'%Y-%m-%d 00:00:00'), a.fd_postdate, a.fd_inputdate, a.fc_salescode as fc_inputby, 'IDR' as fc_mu, 
   '1' as fm_rate, '111101' as fc_gltrans, 'D' as fc_cr, SUM(fm_pay_actual) as fm_value1, SUM(fm_pay_actual) as fm_value2, '' as fc_note, 'R' as  fc_status, 
   '1' as fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
   '0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from t_invmst a
left outer join t_invpay b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno 
left outer join db_dltisnow.t_customer y on a.fc_custcode = y.fc_custcode
left outer join db_dltisnow.t_customer z on y.fc_custtagih = z.fc_custcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and date_format(a.fd_postdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and a.fc_tipe in ('Normal','RETURN') 
and b.fc_status_payment = 'CASH'
and b.fm_pay_actual > 0
Group by a.fc_salescode;
#TAG
#Ambil Tang
delete from t_tagdtl where  fc_branch = '2053' and  fc_gdcode = '101' and fc_tagno in 
( select  fc_tagno from t_tagmst where fc_salescode = '5413' and fc_branch = '2053' and fc_loktujuan = '101' and date_format(fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d'));

delete from t_tagmst where fc_salescode = '5413' and  fc_branch = '2053' and fc_loktujuan = '101' and date_format(fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') and fc_ket like '%SFA TAG TURUN%';

INSERT INTO t_tagmst
  (fc_branch, fc_Tagno, fc_Tipe, fc_Year, fc_Periode, fc_CostCTR, fc_Docref, 
   fd_Docdate, fd_Postdate, fd_Inputdate, fc_InputBy, fd_Editdate, fc_Editby, 
   fc_Branchasal, fc_LokAsal, fc_Branchtujuan, fc_LokTujuan, fc_Ket, fc_Status, 
   fc_OrderType, fc_shift, fc_salescode, fc_supircode, fc_stkno, fc_platno)
Select
   a.fc_branch, concat(a.fc_salescode,'-',date_format(a.fd_Docdate , '%Y%m%d')) , '311' as fc_Tipe, 
   date_format(a.fd_Docdate , '%Y') as fc_Year, date_format(a.fd_Docdate , '%m') as fc_Periode, '' as fc_CostCTR, 
   '' as fc_Docref, a.fd_Docdate as fd_Docdate,   a.fd_Postdate as fd_Postdate,  a.fd_Inputdate as fd_Inputdate,  a.fc_salescode as fc_InputBy,  
   '2020-12-31' as fd_Editdate, 
   a.fc_salescode as fc_Editby, a.fc_branch as fc_Branchasal, a.fc_gdcode as fc_LokAsal, a.fc_branch as fc_Branchtujuan, '101' as fc_LokTujuan, 
  left(concat( 'SFA TAG TURUN ',b.fc_salesname,' ',fc_gdname),40) as fc_Ket, 'R' as fc_Status, '' as fc_OrderType, '0' as fc_shift, a.fc_salescode, a.fc_sopircode, 
   '' as fc_stkno, c.fc_gdname as fc_platno
From t_lkhmst x
inner join t_invmst a on x.fc_branch = a.fc_branch and x.fc_salescode = a.fc_salescode and date_format(a.fd_Docdate,'%Y%m%d') = date_format(x.fd_datelkh,'%Y%m%d') and a.fc_tipe in ('Normal' , 'Reversal')
left outer join db_dltisnow.t_sales b on a.fc_branch = b.fc_branch and a.fc_salescode = b.fc_salescode
left outer join db_dltisnow.t_gudang c on a.fc_gdcode = c.fc_gdcode and a.fc_branch = c.fc_branch
where  x.fc_salescode = '5413' and x.fc_branch = '2053' and date_format(x.fd_datelkh,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and a.fc_tipe in ('Normal','Reversal') 
limit 1;

#Return DO
INSERT INTO t_tagmst
  (fc_branch, fc_Tagno, fc_Tipe, fc_Year, fc_Periode, fc_CostCTR, fc_Docref, 
   fd_Docdate, fd_Postdate, fd_Inputdate, fc_InputBy, fd_Editdate, fc_Editby, 
   fc_Branchasal, fc_LokAsal, fc_Branchtujuan, fc_LokTujuan, fc_Ket, fc_Status, 
   fc_OrderType, fc_shift, fc_salescode, fc_supircode, fc_stkno, fc_platno)
Select
   a.fc_branch, concat(a.fc_salescode,'-',date_format(a.fd_Docdate , '%Y%m%d')) , '311' as fc_Tipe, 
   date_format(a.fd_Docdate , '%Y') as fc_Year, date_format(a.fd_Docdate , '%m') as fc_Periode, '' as fc_CostCTR, 
   '' as fc_Docref, a.fd_Docdate as fd_Docdate,   a.fd_Postdate as fd_Postdate,  a.fd_Inputdate as fd_Inputdate,  a.fc_salescode as fc_InputBy,  
   '2020-12-31' as fd_Editdate, 
   a.fc_salescode as fc_Editby, a.fc_branch as fc_Branchasal, a.fc_gdcode as fc_LokAsal, a.fc_branch as fc_Branchtujuan, '101' as fc_LokTujuan, 
  left(concat( 'SFA TAG TURUN ',b.fc_salesname,' ',fc_gdname),40) as fc_Ket, 'R' as fc_Status, '' as fc_OrderType, '0' as fc_shift, a.fc_salescode, a.fc_sopircode, 
   '' as fc_stkno, c.fc_gdname as fc_platno
From t_lkhmst x
inner join t_invmst a on x.fc_branch = a.fc_branch and x.fc_salescode = a.fc_salescode and date_format(a.fd_Docdate,'%Y%m%d') = date_format(x.fd_datelkh,'%Y%m%d') and a.fc_tipe = 'RETURN'
inner join t_invdtl y on a.fc_branch = y.fc_branch and a.fc_invno = y.fc_invno
left outer join db_dltisnow.t_sales b on a.fc_branch = b.fc_branch and a.fc_salescode = b.fc_salescode
left outer join db_dltisnow.t_gudang c on a.fc_gdcode = c.fc_gdcode and a.fc_branch = c.fc_branch 
where  x.fc_salescode = '5413' and x.fc_branch = '2053' and date_format(x.fd_datelkh,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and fn_qty_return > 0
Group by x.fc_branch , x.fc_salescode
limit 1;

Delete from t_tagdtl_temp where  fc_branch = '2053' and  fc_userid = 'DAVID';
INSERT INTO t_tagdtl_temp
Select a.fc_branch , 'DAVID' as fc_userid , 'TAG' as fc_jenis , a.fc_salescode , b.fc_brgcode , 
coalesce(SUM(b.fn_qty1 * case when fc_satcode = fc_satcode2 then d.fn_qty2 else d.fn_qty1 end),0) as QtyNaik  ,  0 as QtyJual  ,   0 as QtyReturn  ,   
coalesce(fc_satcode1,'') as fc_satcode ,   a.fc_lokasal as fc_gdcode ,  '1' as fc_batch 
from t_tagmst a 
left outer join t_tagdtl b on a.fc_tagno = b.fc_tagno and a.fc_branch = b.fc_branch
left outer join db_dltisnow.t_barang c on b.fc_brgcode = c.fc_brgcode
left outer join db_dltisnow.t_mdlsatuan d on c.fc_mdlcode = d.fc_mdlcode
where a.fc_branch = '2053' and date_format(a.fd_docdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')
and a.fc_lokasal = '101'  and a.fc_salescode = '5413' and a.fc_status = 'T'
group by a.fc_branch , fc_salescode , b.fc_brgcode , b.fc_satcode;

INSERT INTO t_tagdtl_temp 
Select a.fc_branch , 'DAVID' as fc_userid , 'Invoice' as fc_jenis , a.fc_salescode , b.fc_brgcode , 
0 as QtyTAG , 
coalesce(SUM(case when a.fc_tipe = 'REVERSAL' then 0 else fn_qty  * (case when fc_satcode = fc_satcode2 then d.fn_qty2 else d.fn_qty1 end ) end),0) as QtyTerjual,
coalesce(SUM(case when a.fc_tipe = 'REVERSAL' then fn_qty  * (case when fc_satcode = fc_satcode2 then d.fn_qty2 else d.fn_qty1 end) else fn_qty_return  * (case when fc_satcode = fc_satcode2 then d.fn_qty2 else d.fn_qty1 end )end),0) as QtyReturn ,
coalesce(fc_satcode1,'') as fc_satcode ,   a.fc_gdcode as fc_gdcode ,  '1' as fc_batch 
from t_invmst a 
left outer join t_invdtl b on a.fc_invno = b.fc_invno  and a.fc_branch = b.fc_branch
left outer join db_dltisnow.t_barang c on b.fc_brgcode = c.fc_brgcode
left outer join db_dltisnow.t_mdlsatuan d on c.fc_mdlcode = d.fc_mdlcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and  date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')   
and a.fc_tipe in ('Normal' , 'Reversal')
and b.fc_type in ('Normal', 'Air Extra Isi' , 'Bonus' , 'Galon Tukar' , 'Bonus Tambahan')
group by a.fc_branch , a.fc_salescode , fc_brgcode , fc_satcode ;

INSERT INTO t_tagdtl_temp
select a.fc_branch , 'DAVID' as fc_userid , 'GALON' as fc_jenis , 
 a.fc_salescode , d.fc_brgcode , 
0 as QtyTAG , 
0 as QtyJual , 
SUM(ABS(b.fn_qty - b.fn_qty_return) * e.fn_qty2) as QtyReturn , 
coalesce(fc_satcode1,'') as fc_satcode ,   a.fc_gdcode as fc_gdcode , 'UN' as fc_batch 
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_barangmap c on b.fc_brgcode = c.fc_brgcode
left outer join db_dltisnow.t_barang d on c.fc_brgmap = d.fc_brgcode
left outer join db_dltisnow.t_mdlsatuan e on d.fc_mdlcode = e.fc_mdlcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and  date_format(a.fd_Inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') 
and a.fc_tipe in ('Normal' , 'Reversal')
and b.fc_type = 'Galon Kembali'
Group by fc_salescode , b.fc_brgcode ;

#Return DO

INSERT INTO t_tagdtl_temp
Select a.fc_branch , 'DAVID' as fc_userid , 'Invoice' as fc_jenis , a.fc_salescode , b.fc_brgcode , 
0 as QtyTAG , 
coalesce(SUM(case when a.fc_tipe = 'REVERSAL' then 0 else fn_qty  * (case when fc_satcode = fc_satcode2 then d.fn_qty2 else d.fn_qty1 end ) end),0) as QtyTerjual,
coalesce(SUM(case when a.fc_tipe = 'REVERSAL' then fn_qty  * (case when fc_satcode = fc_satcode2 then d.fn_qty2 else d.fn_qty1 end) else fn_qty_return  * (case when fc_satcode = fc_satcode2 then d.fn_qty2 else d.fn_qty1 end )end),0) as QtyReturn ,
coalesce(fc_satcode1,'') as fc_satcode ,   a.fc_gdcode as fc_gdcode ,  '1' as fc_batch 
from t_invmst a 
left outer join t_invdtl b on a.fc_invno = b.fc_invno  and a.fc_branch = b.fc_branch
left outer join db_dltisnow.t_barang c on b.fc_brgcode = c.fc_brgcode
left outer join db_dltisnow.t_mdlsatuan d on c.fc_mdlcode = d.fc_mdlcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and  date_format(a.fd_inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')  and a.fc_tipe = 'RETURN'  
and b.fc_type in ('Normal', 'Air Extra Isi' , 'Bonus' , 'Galon Tukar' , 'Bonus Tambahan')
group by a.fc_branch , a.fc_salescode , fc_brgcode , fc_satcode ;

INSERT INTO t_tagdtl_temp
select a.fc_branch , 'DAVID' as fc_userid , 'GALON' as fc_jenis , 
 a.fc_salescode , d.fc_brgcode , 
0 as QtyTAG , 
0 as QtyJual , 
SUM(ABS(b.fn_qty - b.fn_qty_return) * e.fn_qty2) as QtyReturn , 
coalesce(fc_satcode1,'') as fc_satcode ,   a.fc_gdcode as fc_gdcode , 'UN' as fc_batch 
From t_invmst a
left outer join t_invdtl b on a.fc_branch = b.fc_branch and a.fc_invno = b.fc_invno
left outer join db_dltisnow.t_barangmap c on b.fc_brgcode = c.fc_brgcode
left outer join db_dltisnow.t_barang d on c.fc_brgmap = d.fc_brgcode
left outer join db_dltisnow.t_mdlsatuan e on d.fc_mdlcode = e.fc_mdlcode
where a.fc_salescode = '5413' and a.fc_branch = '2053' and  date_format(a.fd_Inputdate,'%Y%m%d') = date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d') and a.fc_tipe = 'RETURN' 
and b.fc_type = 'Galon Kembali'
Group by fc_salescode , b.fc_brgcode ;

#Masukan TAGDTL Qty Besar
delete from t_tagdtl where  fc_branch = '2053' and fc_tagno = concat('5413','-',date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d'));

SET @row_number = 0; 
insert into t_tagdtl
select * from (
select a.fc_branch , concat(a.fc_salescode,'-',date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')) ,  
'101' as fc_gdcode , a.fc_brgcode , a.fc_batch as fc_batch , RIGHT( CONCAT('0',(@row_number:=@row_number + 1) ),2 )AS  fc_no , 
(SUM(fn_qty_tag) - SUM(fn_qty_jual) + SUM(fn_qty_return)) div c.fn_qty2 as QtyTurun , 
(SUM(fn_qty_tag) - SUM(fn_qty_jual) + SUM(fn_qty_return)) div c.fn_qty2 * c.fn_qty2 as QtyTurun2 , 
c.fc_satcode2 , '' as fc_GL , 0 as fm_price
from t_tagdtl_temp a
left outer join db_dltisnow.t_barang b on a.fc_brgcode = b.fc_brgcode
left outer join db_dltisnow.t_mdlsatuan c on b.fc_mdlcode = c.fc_mdlcode
where a.fc_salescode = '5413' and  a.fc_branch = '2053' and fc_userid = 'DAVID' 
group by fc_branch , fc_salescode  , a.fc_brgcode , c.fc_satcode2 ) a
where QtyTurun <> 0;
#Masukan TAGDTL Qty Kecil
insert into t_tagdtl
select * from (
select a.fc_branch , concat(a.fc_salescode,'-',date_format(date_add('2020-12-31', interval 0 day),'%Y%m%d')) ,  
'101' as fc_gdcode , a.fc_brgcode , a.fc_batch as fc_batch , RIGHT( CONCAT('0',(@row_number:=@row_number + 1) ),2 )AS  fc_no , 
(SUM(fn_qty_tag) - SUM(fn_qty_jual) + SUM(fn_qty_return)) mod c.fn_qty2 as QtyTurun , 
(SUM(fn_qty_tag) - SUM(fn_qty_jual) + SUM(fn_qty_return)) mod c.fn_qty2 * c.fn_qty1 as QtyTurun2 , 
c.fc_satcode1 as fc_satcode , '' as fc_GL , 0 as fm_price
from t_tagdtl_temp a
left outer join db_dltisnow.t_barang b on a.fc_brgcode = b.fc_brgcode
left outer join db_dltisnow.t_mdlsatuan c on b.fc_mdlcode = c.fc_mdlcode
where a.fc_salescode = '5413' and  a.fc_branch = '2053' and fc_userid = 'DAVID' 
group by fc_branch , fc_salescode ,  a.fc_brgcode , c.fc_satcode2) a
where QtyTurun <> 0;

call pr_upd_fm_pay_paymentdtl;

