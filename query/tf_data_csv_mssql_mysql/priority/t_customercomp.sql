-- sql server
EXEC XP_CMDSHELL 'net use S: \\192.168.100.16\Data /USER:administrator'
declare @TSQL varchar(8000),@cBranch char(4),@DirOutput varchar(100);
select @cBranch = :branch
select @DirOutput = 'S:\\'+@cBranch+'-t_customercomp.csv' 
select @TSQL = 'bcp "
select replace(cCustcode,''\'',''\\'') ,cCompcode, cBranch
from d_transaksi..t_customercomp where cBranch = '+ @cBranch +'
" queryout "'+@DirOutput+'" -T -c -t "|" '
select @TSQL =  REPLACE(REPLACE(@TSQL, CHAR(13), ' '), CHAR(10), ' ')
EXEC XP_CMDSHELL @TSQL

-- mysql
LOAD data INFILE  :csvfile replace -- csvfile = 'D:\\Data\\2001-t_customercomp.csv'
INTO TABLE db_dltisnow.t_customercomp 
FIELDS TERMINATED BY '|' ENCLOSED BY '"' 
LINES TERMINATED BY '\r\n'
(fc_custcode,fc_compcode,fc_branch);
