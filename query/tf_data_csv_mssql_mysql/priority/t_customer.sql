-- sql server
EXEC XP_CMDSHELL 'net use S: \\192.168.100.16\Data /USER:administrator'
declare @TSQL varchar(8000),@cBranch char(4),@DirOutput varchar(100);
select @cBranch = :branch
select @DirOutput = 'S:\\'+@cBranch+'-t_customer.csv' 
select @TSQL = 'bcp "
select replace(a.cCustcode,''\'',''\\''),replace(a.cNama,''\'',''\\''),replace(a.cAlamat1,''\'',''\\''),replace(a.cAlamat2,''\'',''\\''),
a.cPropinsi,a.cKota,a.cKecamatan,a.cDesa,a.cNegara,
a.cKodepos,a.cPhone,a.cFax,replace(a.cContact,''\'',''\\''),a.cEmail,a.cPKP,
replace(a.cKeterangan,''\'',''\\''),a.nLimitNota,a.nLimitTotal,a.nSO,a.nDO,a.nAR,a.nPDC,a.cInputBy,a.dInputdate,a.cEditBy,
a.dEditdate,a.nDisc,a.nTOP,a.cSalesCode,a.cCompCode,
a.cAreacode,a.cChanelcode,a.cDivCode,a.cDistCode,a.cStatus,a.cGL,a.cCOL,a.cGrpCode,
a.cHP,a.cAR_Delv,a.cDT,a.cRDT,a.nQtyDokUnblock,a.cKunjungan,a.dStartjdwl,a.
cCusttagih,a.cCashDisc,a.cCodetoko,a.nTop_Toleransi,a.cNIK,a.cPaspor,a.cTunaiTransfer,a.cTaxReq 
from d_transaksi..t_Customer a 
inner join d_transaksi..t_customercomp b on a.ccustcode = b.ccustcode
where b.cBranch = '+ @cBranch +'
" queryout "'+@DirOutput+'" -T -c -t "|" '
select @TSQL =  REPLACE(REPLACE(@TSQL, CHAR(13), ' '), CHAR(10), ' ')
EXEC XP_CMDSHELL @TSQL
EXEC XP_CMDSHELL 'Dir S:'

-- mysql
LOAD data INFILE  :csvfile replace --:csvfile = D:\\Data\\2001-t_customer.csv
INTO TABLE db_dltisnow.t_customer 
FIELDS TERMINATED BY '|' ENCLOSED BY '"' 
LINES TERMINATED BY '\r\n'
(fc_custcode,fc_nama,fc_alamat1,fc_alamat2,fc_propinsi,fc_kota,fc_kecamatan,fc_desa,fc_negara,
fc_kodepos,fc_phone,fc_fax,fc_contact,fc_email,fc_pkp,fc_keterangan,fn_limitnota,fn_limittotal,
fn_so,fn_do,fn_ar,fn_pdc,fc_inputby,fd_inputdate,fc_editby,fd_editdate,fn_disc,
fn_top,fc_salescode,fc_compcode,fc_areacode,fc_chanelcode,fc_divcode,fc_distcode,
fc_status,fc_gl,fc_col,fc_grpcode,fc_hp,fc_ar_delv,fc_dt,fc_rdt,fn_qtydokunblock,
fc_kunjungan,@fd_startjdwl,fc_custtagih,fc_cashdisc,fc_codetoko,fn_top_toleransi,fc_nik,
fc_paspor,fc_tunaitransfer,fc_taxreq)
set fd_startjdwl = nullif(trim(@fd_startjdwl),'')

