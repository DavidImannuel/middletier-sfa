-- sql server
EXEC XP_CMDSHELL 'net use S: \\192.168.100.16\Data /USER:administrator'
declare @TSQL varchar(8000)
select @TSQL = 'bcp "
select top 40000 cCustcode,quotename(cNama,''""''),replace(cAlamat1,''\'',''\\''),cAlamat2,cPropinsi,cKota,cKecamatan,cDesa,cNegara,cKodepos,cPhone,cFax,cContact,cEmail,cPKP,
cKeterangan,nLimitNota,nLimitTotal,nSO,nDO,nAR,nPDC,cInputBy,dInputdate,cEditBy,dEditdate,nDisc,nTOP,cSalesCode,cCompCode,
cAreacode,cChanelcode,cDivCode,cDistCode,cStatus,cGL,cCOL,cGrpCode,cHP,cAR_Delv,cDT,cRDT,nQtyDokUnblock,cKunjungan,dStartjdwl,
cCusttagih,cCashDisc,cCodetoko,nTop_Toleransi,cNIK,cPaspor,cTunaiTransfer,cTaxReq 
from d_transaksi..t_Customer
" queryout "S:\\t_customer.csv" -T -c -t "|" '
select @TSQL =  REPLACE(REPLACE(@TSQL, CHAR(13), ''), CHAR(10), '')
EXEC XP_CMDSHELL @TSQL
EXEC XP_CMDSHELL 'Dir S:'

-- mysql
LOAD data INFILE  'D:\\Data\\t_customer.csv' replace
INTO TABLE db_dltisnow_tes.t_customer 
FIELDS TERMINATED BY '|' ENCLOSED BY '"' -- escaped by '\\'
LINES TERMINATED BY '\r\n'
(fc_custcode,fc_nama,fc_alamat1,@fc_alamat2,@fc_propinsi,@fc_kota,@fc_kecamatan,@fc_desa,@fc_negara,
@fc_kodepos,@fc_phone,@fc_fax,@fc_contact,@fc_email,@fc_pkp,@fc_keterangan,@fn_limitnota,@fn_limittotal,
@fn_so,@fn_do,@fn_ar,@fn_pdc,@fc_inputby,@fd_inputdate,@fc_editby,@fd_editdate,@fn_disc,
@fn_top,@fc_salescode,@fc_compcode,@fc_areacode,@fc_chanelcode,@fc_divcode,@fc_distcode,
@fc_status,@fc_gl,@fc_col,@fc_grpcode,@fc_hp,@fc_ar_delv,@fc_dt,@fc_rdt,@fn_qtydokunblock,
@fc_kunjungan,@fd_startjdwl,@fc_custtagih,@fc_cashdisc,@fc_codetoko,@fn_top_toleransi,@fc_nik,
@fc_paspor,@fc_tunaitransfer,@fc_taxreq)
set fd_startjdwl = nullif(trim(@fd_startjdwl),'')

