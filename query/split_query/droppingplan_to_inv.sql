select * from t_DroppingPlanMst;
select * from t_DroppingPlanDTL;
-- invmst
insert into [mysql]...[db_sfa.t_invmst]
select 
d.cBranch,concat(CONVERT(varchar,a.dPostdate,112),'-',d.cDocno) Docno,d.cYear,d.cPeriode,d.dDocdate,d.dPostdate,
d.dInputdate,d.cInputBy,'DO' fc_tipe,1 fn_top,d.cSalesCode,d.cCompCode,
d.cAreacode,d.cChanelcode,d.cDivCode,d.cDistCode,d.cPPN,d.dDueDate,
'' fc_poso,d.cCustcode,d.cSuppcode,'IDR' fc_MU,1 fc_rate,c.cCustbayar,c.cCustkrm,
a.cgdCodeAsal,d.cKeterangan,d.nDisc1,d.nDisc2,
d.mBrutto1,d.mDisc1,d.mNetto1,d.mDPP1,d.mPPN1,
d.mBrutto2,d.mDisc2,d.mNetto2,d.mDPP2,d.mPPN2,
c.cAppBy,c.dAppDate,c.cPOstgoods,c.cTax,'' fc_Status,
c.cTipegm,d.cDocRef,d.cReturn,d.Csopircode, '' fc_presono
from t_droppingplanmst a
left join t_droppingplandtl b on a.vDocno = b.vDocno and a.cbranch = b.cbranch
left join t_domst c on b.cSJNO = c.cDono and a.cbranch = c.cbranch
left join t_billmst d on b.cInvoiceNo = d.cDocno and a.cbranch = d.cbranch

-- invdtl
select
d.cBranch,concat(CONVERT(varchar,a.dPostdate,112),'-',d.cDocno) Docno,d.cNo,d.cBrgcode,d.cTipecode,
d.cGrpcode,d.cValcode,d.cValtipe,d.cPricectr,d.cPrdcode,d.cMrkCode,
d.cjnscode,d.csizcode,
d.cMdlCode,'Normal' fc_type,d.nQty1,'0' fn_qtyreturn,d.cSatcode,
d.cGdCode,cast(d.nHarga1 as numeric(20,3)),cast(d.nDisc1 as numeric(20,3)),cast(d.nDisc2 as numeric(20,3)),cast(d.mdisc1_1 as numeric(20,3) ),cast(d.mdisc1_2 as numeric(20,3)),
cast(d.mBrutto1 as numeric(20,3)),cast(d.mNetto1 as numeric(20,3)),cast(d.mDPP1 as numeric(20,3)), cast(d.mPPn1 as numeric(20,3) )
from t_droppingplanmst a
left join t_droppingplandtl b on a.vDocno = b.vDocno and a.cbranch = b.cBranch
left join t_billmst c on b.cInvoiceNo = c.cDocno and b.cBranch = c.cBranch 
left join t_billdtl d on d.cDOcno = c.cDocno and c.cBranch =  d.cBranch 

-- import tis
insert into [mysql]...[db_sfa.t_invmst]
select 
d.cBranch,concat(CONVERT(varchar,a.dPostdate,112),'-',d.cDocno) Docno,d.cYear,d.cPeriode,d.dDocdate,d.dPostdate,
d.dInputdate,d.cInputBy,'DO' fc_tipe,1 fn_top,d.cSalesCode,d.cCompCode,
d.cAreacode,d.cChanelcode,d.cDivCode,d.cDistCode,d.cPPN,d.dDueDate,
'' fc_poso,d.cCustcode,d.cSuppcode,'IDR' fc_MU,1 fc_rate,c.cCustbayar,c.cCustkrm,
a.cgdCodeAsal,d.cKeterangan,d.nDisc1,d.nDisc2,
d.mBrutto1,d.mDisc1,d.mNetto1,d.mDPP1,d.mPPN1,
d.mBrutto2,d.mDisc2,d.mNetto2,d.mDPP2,d.mPPN2,
c.cAppBy,c.dAppDate,c.cPOstgoods,c.cTax,'' fc_Status,
c.cTipegm,d.cDocRef,d.cReturn,d.Csopircode, '' fc_presono
from t_droppingplanmst a
left join t_droppingplandtl b on a.vDocno = b.vDocno and a.cbranch = b.cbranch
left join t_domst c on b.cSJNO = c.cDono and a.cbranch = c.cbranch
left join t_billmst d on b.cInvoiceNo = d.cDocno and a.cbranch = d.cbranch
left join [mysql]...[db_sfa.t_invmst] e on concat(CONVERT(varchar,a.dPostdate,112),'-',d.cDocno) = e.fc_invno
where format(a.dPostdate,'yyyyMMdd') = format(cast( :date as date),'yyyyMMdd') and e.fc_invno is null;

insert into [mysql]...[db_sfa.t_invdtl]
select
d.cBranch,concat(CONVERT(varchar,a.dPostdate,112),'-',d.cDocno) Docno,d.cNo,d.cBrgcode,d.cTipecode,
d.cGrpcode,d.cValcode,d.cValtipe,d.cPricectr,d.cPrdcode,d.cMrkCode,
d.cjnscode,d.csizcode,
d.cMdlCode,'Normal' fc_type,d.nQty1,'0' fn_qtyreturn,d.cSatcode,
d.cGdCode,cast(d.nHarga1 as numeric(20,3)),cast(d.nDisc1 as numeric(20,3)),cast(d.nDisc2 as numeric(20,3)),cast(d.mdisc1_1 as numeric(20,3) ),cast(d.mdisc1_2 as numeric(20,3)),
cast(d.mBrutto1 as numeric(20,3)),cast(d.mNetto1 as numeric(20,3)),cast(d.mDPP1 as numeric(20,3)), cast(d.mPPn1 as numeric(20,3) )
from t_droppingplanmst a
left join t_droppingplandtl b on a.vDocno = b.vDocno and a.cbranch = b.cBranch
left join t_billmst c on b.cInvoiceNo = c.cDocno and b.cBranch = c.cBranch 
left join t_billdtl d on d.cDOcno = c.cDocno and c.cBranch =  d.cBranch 
left join [mysql]...[db_sfa.t_invdtl] e on concat(CONVERT(varchar,a.dPostdate,112),'-',d.cDocno) = e.fc_invno
where format(a.dPostdate,'yyyyMMdd') = format(cast( :date as date),'yyyyMMdd') and e.fc_invno is null;
