select * from t_lhpmst where date_format(fd_inputdate,'%Y%m%d') = date_format( date_add( sysdate() , interval -1 day),'%Y%m%d')  ;
select * from t_lhpdtl a 
where exists ( select fc_docno from t_lhpmst b where date_format(fd_inputdate,'%Y%m%d') = date_format( date_add( sysdate() , interval -1 day),'%Y%m%d') and a.fc_Docno = b.fc_docno);

#tipe BI
#bankmst BI 
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) fc_docno,'BI' fc_tipe,
'' fc_docref,a.fd_Docdate,a.fd_postdate,a.fd_inputdate,a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'111101' fc_gltrans,'D' fc_cr,sum(a.fm_Pay) fm_value1,
sum(a.fm_Pay) fm_value2,'' fc_note,'R' fcstatus, 1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
   '0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from t_lhpmst a 
where date_format(a.fd_docdate,'%Y%m%d') = date_format( date_add( sysdate() , interval 0 day),'%Y%m%d')  
group by concat('BN-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')),a.fc_branch;
#bankdtl BI
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('BN-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) fc_docno,'BI' fc_tipe,'01' fc_no,
'' fc_docref,'CUSTOMER AR/CLEARING' cBusTrans, '' as fc_suppcode, '' as fc_custcode, a.fc_collector as fc_receipt, 'R' as fc_bttipe, 
'BI' as fc_tipe, '' as fc_bank, '' as fc_giro, '111101' as fc_gltrans, '219301' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN SALES',b.fc_salesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, SUM(fm_pay) as fm_value1, SUM(fm_pay) as fm_value2, 
'0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from t_lhpmst a 
left join db_dltisnow.t_sales b on a.fc_collector = b.fc_salescode
where date_format(a.fd_docdate,'%Y%m%d') = date_format( date_add( sysdate() , interval 0 day),'%Y%m%d') 
group by concat('BI-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) ,a.fc_branch;

#bank AR
#bank mst
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP/',trim(b.fc_docref),'/',a.fc_custcode,date_format(a.fd_docdate,'%Y%m%d') ) fc_docno,'AR' fc_tipe,
concat('BN-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) fc_docref,a.fd_Docdate,a.fd_postdate,a.fd_inputdate,a.fc_Collector,
'IDR' fc_MU,'1' fm_Rate,'219301' fc_gltrans,'D' fc_cr,sum(b.fm_Pay) fm_value1,
sum(b.fm_Pay) fm_value2,'' fc_note,'R' fcstatus, 1 fc_tax, '' as fc_udf1, '' as fc_udf2, '' as fc_udf3, '' as fc_udf4, '' as fc_udf5, '0' as fm_udf1, 
   '0' as fm_udf2, '0' as fm_udf3, '0' as fm_udf4, '0' as fm_udf5
from t_lhpmst a
left join t_lhpdtl b on a.fc_docno = b.fc_docno 
-- where date_format(a.fd_docdate,'%Y%m%d') = date_format( date_add( sysdate() , interval 0 day),'%Y%m%d')  
group by concat('AD-LHP/',trim(b.fc_docref),'/',a.fc_custcode,date_format(a.fd_docdate,'%Y%m%d') ) ,a.fc_branch ;
#bank dtl
select 
'0002' fc_compcode,a.fc_branch,a.fc_year,a.fc_periode,
concat('AD-LHP/',trim(b.fc_docref),'/',a.fc_custcode,date_format(a.fd_docdate,'%Y%m%d') ) fc_docno,'BI' fc_tipe,'01' fc_no,
concat('BN-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) fc_docref,
'' cBusTrans, '' as fc_suppcode, '' as fc_custcode, a.fc_collector as fc_receipt, 'D' as fc_bttipe, 
'AR' as fc_tipe, '' as fc_bank, '' as fc_giro, '219301' as fc_gltrans, '219401' as fc_glvs, 'C' as fc_cr, '1900-01-01 00:00:00' as fd_duedate, 
concat('PENAGIHAN SALES',b.fc_salesname) as fc_note, '' as fc_iocode, 
'' fc_costctr, 'IDR' as fc_mu, SUM(b.fm_pay) as fm_value1, SUM(b.fm_pay) as fm_value2, 
'0' as fm_sisa1, '0' as fm_sisa2, 'R' as fc_status, '0' as fn_kmawal, '0' as fn_kmakhir, '0' as fn_volume, '' as fc_bbm, 
'' as fc_tujuan, '' as fc_sopircode, '' as fc_bankgirocode, '' as fc_bankasal, '' as fc_giroasal, 
'0' as fm_valueasal, '' as fc_docrefbkk, '' as fc_noref
from t_lhpmst a 
left join db_dltisnow.t_sales b on a.fc_collector = b.fc_salescode
left join t_lhpdtl b on a.fc_docno = b.fc_docno 
where date_format(a.fd_docdate,'%Y%m%d') = date_format( date_add( sysdate() , interval -1 day),'%Y%m%d') 
group by concat('AD-LHP/',trim(b.fc_docref),'/',a.fc_custcode,date_format(a.fd_docdate,'%Y%m%d') ),a.fc_branch;

#payment
-- paymentmst
Select
   '0002' as fc_compcode, a.fc_branch, a.fc_year, a.fc_periode,  
   concat('REC-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) fc_docno, 
    '' as fc_docref, 
   'D' as fc_tipe, '' fc_suppcode, a.fc_custcode, a.fd_docdate, a.fd_postdate, a.fd_inputdate, 
   a.fc_collector as fd_inputby, 'IDR' as fc_mucode, '1' as fm_rate, fm_pay as fm_value1, fm_pay as fm_value2, concat('PENAGIHAN SALES ',fc_salesname) as fc_note, 
   'R' as fc_status, '1' as fc_tax, '' as fc_plantbayar
from t_lhpmst a
left outer join t_sales b on a.fc_collector = b.fc_salescode and a.fc_branch = b.fc_branch
group by concat('REC-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')),a.fc_branch;

-- paymentdtl bill
Select 
a.fc_branch, 
    concat('REC-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) fc_docno, 
     b.fc_Docref fc_docref, 
   '' as fc_noref,  '01' as fc_no, 
    b.fc_Tipe fc_gltrans, 
   '' as fc_pk, 'D' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, SUM(b.fm_netto) as fm_netto1, SUM(b.fm_netto) as fm_netto2, 
   SUM(b.fm_pay) as fm_pay1, SUM(b.fm_pay) as fm_pay2, 
   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from 
t_lhpmst a
left join t_lhpdtl b on a.fc_docno = b.fc_docno and a.fc_branch = b.fc_branch
group by concat('REC-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')),b.fc_Docref,a.fc_branch;

-- paymentdtl ad
Select 
   a.fc_branch, 
concat('REC-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')) fc_docno, 
  concat('AD-LHP/',trim(b.fc_docref),'/',a.fc_custcode,date_format(a.fd_docdate,'%Y%m%d') ) fc_docref, 
   '' as fc_noref,  '01' as fc_no, 
   'AR'  as fc_gltrans, 
   '' as fc_pk, 'C' as fc_cr, 'IDR' as fc_mu, '1' as fm_rate, b.fm_netto as fm_netto1, b.fm_netto as fm_netto2, 
   b.fm_pay as fm_pay1, b.fm_pay as fm_pay2, 
   'R' as fc_status, 0 as fc_cashdisc, 0 as fm_cashdisc1, 0 as fm_cashdisc2
from t_lhpmst a
left join t_lhpdtl b on a.fc_docno = b.fc_docno and a.fc_branch = b.fc_branch
group by concat('REC-LHP',a.fc_collector,date_format(a.fd_docdate,'%Y%m%d')),b.fc_docref,a.fc_branch;





