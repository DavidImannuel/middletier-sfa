use d_transaksi

IF OBJECT_ID('tempdb.dbo.#tempCustNO', 'U') IS NOT NULL
DROP TABLE #tempCustNO; 
CREATE TABLE #tempCustNO ( cCustno varchar(50) )

DECLARE 	
@custcodetis varchar(20),@fc_custcode varchar(20) ,@fc_custnama varchar(50),@year varchar(4),@month varchar(2)  ;

select @year = FORMAT(GETDATE(),'yyyy');
select @month = FORMAT(GETDATE(),'MM');
insert into #tempCustNO EXEC pr_nomor_CustCode '2001','CUST',@year,@month;
select * from #tempCustNO
select @custcodetis=cCustno from #tempCustNO;
DECLARE cursor_noo CURSOR
FOR 
select  
fc_custcode ,fc_custnama 
from [MYSQL]...[db_sfa.t_noo] where fc_statusconfirm = 'Y';
OPEN cursor_noo;

FETCH NEXT FROM cursor_noo INTO 
@fc_custcode ,@fc_custnama ;

WHILE @@FETCH_STATUS = 0
BEGIN

	
	update [MYSQL]...[db_sfa.t_noo] set fc_custcode = @custcodetis where fc_custcode = @fc_custcode;
	update t_nomor set nDocno = nDocno + 1 where cDocument = 'CUST' and cPeriode =@month and cyear=@year and cBranch = '2001';

	insert into #tempCustNO exec pr_nomor_CustCode '2001','CUST',@year,@month;
	select * from #tempCustNO;
	select @custcodetis=cCustno from #tempCustNO;

FETCH NEXT FROM cursor_noo INTO 
@fc_custcode ,@fc_custnama ;

END;

CLOSE cursor_noo;

DEALLOCATE cursor_noo;



