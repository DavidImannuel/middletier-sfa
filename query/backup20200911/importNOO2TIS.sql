insert into t_customer 
(ccustcode,cnama,calamat1,calamat2,cpropinsi,ckota,ckecamatan,cdesa,cnegara,
ckodepos,cphone,cfax,ccontact,cemail,cpkp,cketerangan,nlimitnota,
nlimittotal,nso,ndo,
nar,npdc,cinputby,dinputdate,ceditby,deditdate,ndisc,ntop,
csalescode,ccompcode,careacode,cchanelcode,cdivcode,cdistcode,
cstatus,cgl,ccol,cgrpcode,chp,car_delv,cdt,crdt,nqtydokunblock,ckunjungan,dstartjdwl,
ccusttagih,ccashdisc,ccodetoko,ntop_toleransi,cnik,cpaspor,ctunaitransfer,ctaxreq)
select  
fc_custcode,fc_custnama ,fc_alamat1 , '' as fc_alamat2 , fc_propinsi ,fc_kota ,fc_kecamatan, 
fc_desa ,fc_negara ,fc_kodepos ,fc_phone , '' as fc_fax , fc_contact ,fc_email ,'0' as fc_pkp , 'NOO SFA' , 0 as nlimitnota ,
0 as fn_limittotal, 0 as fn_so, 0 as fn_do,
0 as fn_ar, 0 as fn_pdc, fc_salescode as fc_inputby , GETDATE() , 'CHRIS' as fc_editby, GETDATE() as fd_editdate , 0 as fn_disc , 0 as fn_top ,
fc_userid, '0002' as fc_compcode ,'25' as fc_areacode, fc_chanelcode , '60' as fc_divcode ,'24' as fc_distcode , 
'1' as fc_status, '1200000000' as fc_gl, 'KASIR1' as fc_col,'1001' as fc_grpcode, '' as fc_hp, '1' as fc_ar_delv, '0' as fc_dt, '0' as fc_rdt, '0' as fn_qtydokunblock, '0' as fc_kunjungan, '1900-01-01 00:00:00' as fd_startjdwl,
'' as fc_custtagih, '0' as fc_cashdisc, '' as fc_codetoko, '0' as fn_top_toleransi, '' as fc_nik, '' as fc_paspor, '' as fc_tunaitransfer, '0' fc_taxreq
from [MYSQL]...[db_sfa.t_noo] where fc_statusconfirm = 'Y';


-- import custcomp,custtax,longlat
use d_transaksi
DECLARE 
	@cCompcode varchar(4),@cBranch VARCHAR(4), @cCustcode VARCHAR(30),@cCustnama VARCHAR(30),
	@cAlamat VARCHAR(60),@cNPWP VARCHAR(30),@cNPPKP VARCHAR(30),@cLongitude VARCHAR(30),@cLangitude VARCHAR(30);
DECLARE cursorvar CURSOR
FOR select fc_compcode,fc_branch,fc_custcode,fc_custnama,fc_alamat1,fc_npwp,fc_nppkp ,fc_longtitude,fc_latitude from [MYSQL]...[db_sfa.t_noo] where fc_statusconfirm = 'Y';

OPEN cursorvar;

FETCH NEXT FROM cursorvar INTO 
    @cCompcode ,@cBranch , @cCustcode ,@cCustnama ,
	@cAlamat ,@cNPWP ,@cNPPKP ,@cLongitude ,@cLangitude ;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	IF EXISTS ( select * from t_customer where cCustcode = @cCustcode )
	begin
		insert into t_customercomp select fc_custcode,fc_compcode,fc_branch from [MYSQL]...[db_sfa.t_noo] 
			where fc_custcode = @cCustcode and fc_branch = @cBranch;
		insert into t_custtax select fc_custcode,fc_custnama,fc_alamat1,'00.000.000.0.000.000','00.000.000.0.000.000',getdate(),NULL 
			from [MYSQL]...[db_sfa.t_noo] 
			where fc_custcode = @cCustcode and fc_branch = @cBranch and fc_statusconfirm = 'Y';
		insert into t_customercoordinate select fc_branch,fc_custcode,fc_longtitude,fc_latitude from [MYSQL]...[db_sfa.t_noo] 
			where fc_custcode = @cCustcode and fc_branch = @cBranch and fc_statusconfirm = 'Y';
		delete from [MYSQL]...[db_sfa.t_noo] where fc_custcode = @cCustcode and fc_branch = @cBranch and fc_statusconfirm = 'Y';
	end

	FETCH NEXT FROM cursorvar INTO 
		 @cCompcode ,@cBranch , @cCustcode ,@cCustnama ,@cAlamat ,@cNPWP ,@cNPPKP ,@cLongitude ,@cLangitude ;

END;

CLOSE cursorvar;

DEALLOCATE cursorvar;
