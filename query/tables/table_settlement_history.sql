use d_sfa
create table t_SFA_SOHis (
	fc_branch char(4),
	fc_SfaNo varchar(50), -- no dari sfa
	fc_TisNo varchar(50), -- no dari TIS (hasil exec pr_nomor)
	fc_salescode varchar(10), -- salescode
	fc_inputby varchar(10), -- jaga" mengambil userid admin yang me nyettlement
	fd_TransDate datetime , -- tanggal transaksi
	fd_PorcessDate datetime, -- tanggal proses settlement
	fc_keterangan varchar(50), -- keterangan transaksi
	primary key (fc_branch,fc_SfaNo,fc_TisNo)
)
create table t_SFA_DOHis (
	fc_branch char(4),
	fc_SfaNo varchar(50), -- no dari sfa
	fc_TisNo varchar(50), -- no dari TIS (hasil exec pr_nomor)
	fc_salescode varchar(10), -- salescode
	fc_inputby varchar(10), -- jaga" mengambil userid admin yang di settlement
	fd_TransDate datetime , -- tanggal transaksi
	fd_PorcessDate datetime, -- tanggal proses settlement
	fc_keterangan varchar(50), -- keterangan transaksi
	primary key (fc_branch,fc_SfaNo,fc_TisNo)
)
create table t_SFA_BillHis (
	fc_branch char(4),
	fc_SfaNo varchar(50), -- no dari sfa
	fc_TisNo varchar(50), -- no dari TIS (hasil exec pr_nomor)
	fc_salescode varchar(10), -- salescode
	fc_inputby varchar(10), -- jaga" mengambil userid admin yang di settlement
	fd_TransDate datetime , -- tanggal transaksi
	fd_PorcessDate datetime, -- tanggal proses settlement
	fc_keterangan varchar(50), -- keterangan transaksi
	primary key (fc_branch,fc_SfaNo,fc_TisNo)
)

create table t_SFA_BankHis (
	fc_branch char(4),
	fc_SfaNo varchar(50), -- no dari sfa
	fc_TisNo varchar(50), -- no dari TIS (hasil exec pr_nomor)
	fc_salescode varchar(10), -- salescode
	fc_inputby varchar(10), -- jaga" mengambil userid admin yang di settlement
	fd_TransDate datetime , -- tanggal transaksi
	fd_PorcessDate datetime, -- tanggal proses settlement
	fc_keterangan varchar(50), -- keterangan transaksi
	primary key (fc_branch,fc_SfaNo,fc_TisNo)
)

create table t_SFA_PaymentHis (
	fc_branch char(4),
	fc_SfaNo varchar(50), -- no dari sfa
	fc_TisNo varchar(50), -- no dari TIS (hasil exec pr_nomor)
	fc_salescode varchar(10), -- salescode
	fc_inputby varchar(10), -- jaga" mengambil userid admin yang di settlement
	fd_TransDate datetime , -- tanggal transaksi
	fd_PorcessDate datetime, -- tanggal proses settlement
	fc_keterangan varchar(50), -- keterangan transaksi
	primary key (fc_branch,fc_SfaNo,fc_TisNo)
)

DROP TABLE d_sfa..t_SFA_TAGHis

create table t_SFA_TAGHis (
	fc_branch char(4),
	fc_SfaNo varchar(50), -- no dari sfa
	fc_TisNo varchar(50), -- no dari TIS (hasil exec pr_nomor)
	fc_salescode varchar(10), -- salescode
	fc_inputby varchar(10), -- jaga" mengambil userid admin yang di settlement
	fd_TransDate datetime , -- tanggal transaksi
	fd_PorcessDate datetime, -- tanggal proses settlement
	fc_keterangan varchar(50), -- keterangan transaksi
	primary key (fc_branch,fc_SfaNo,fc_TisNo)
)

create table t_SFA_MoveBrgHis (
	fc_branch char(4),
	fc_SfaNo varchar(50), -- no dari sfa
	fc_TisNo varchar(50), -- no dari TIS (hasil exec pr_nomor)
	fc_salescode varchar(10), -- salescode
	fc_inputby varchar(10), -- jaga" mengambil userid admin yang di settlement
	fd_TransDate datetime , -- tanggal transaksi
	fd_PorcessDate datetime, -- tanggal proses settlement
	fc_keterangan varchar(50), -- keterangan transaksi
	primary key (fc_branch,fc_SfaNo,fc_TisNo)
)
