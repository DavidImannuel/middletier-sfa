CREATE TABLE db_dltisnow.t_DroppingPlanMst(
	fc_Branch char(4) NOT NULL,
	fc_Docno varchar(12) NOT NULL,
	fc_Salescode1 char(4) NOT NULL,
	fc_Salescode2 char(4) NOT NULL,
	fc_Tagno char(12) NOT NULL,
	fc_GdCodeAsal char(4) NOT NULL,
	fc_GdCodeTujuan char(4) NOT NULL,
	fc_Keterangan varchar(150) NULL,
	fd_Postdate datetime NOT NULL,
	fd_Inputdate datetime NOT NULL,
	fc_InputBy varchar(10) NOT NULL,
	fc_ApproveDate datetime NOT NULL,
	fc_ApprovedBy varchar(10) NOT NULL,
	fc_Status char(1) NOT NULL,
	primary key(fc_branch,fc_docno)
);

CREATE TABLE db_dltisnow.t_DroppingPlanDtl (
	fc_Branch char(4) NOT NULL,
	fc_Docno varchar(12) NOT NULL,
	fc_SJNo char(12) NOT NULL,
	fc_InvoiceNo char(12) NOT NULL,
	fm_Value double(20,3) NOT NULL,
 	primary key(fc_branch,fc_docno,fc_sjno,fc_InvoiceNo)
);

CREATE TABLE db_dltisnow.t_tagihanmst (
	fc_Branch char(4) NOT NULL,
	fc_Docno varchar(20) NOT NULL,
	fc_Custcode varchar(15) NOT NULL,
	fc_Year char(4) NOT NULL,
	fc_Periode char(2) NOT NULL,
	fd_Docdate datetime NOT NULL,
	fd_Postdate datetime NOT NULL,
	fd_Inputdate datetime NOT NULL,
	fc_InputBy varchar(10) NOT NULL,
	fm_Netto1 double(20,3) NOT NULL,
	fm_Netto2 double(20,3) NOT NULL,
	fm_Pay1 double(20,3) NOT NULL,
	fm_Pay2 double(20,3) NOT NULL,
	fm_SisaCN double(20,3) NOT NULL,
	fc_Status char(1) NOT NULL,
	fc_Bank varchar(15) NULL,
	fc_NoRek varchar(50) NULL,
	fd_Approvedate datetime NOT NULL,
	fc_ApproveBy varchar(10) NOT NULL,
	fd_Confirmdate datetime NOT NULL,
	fc_ConfirmBy varchar(10) NOT NULL,
	primary key (fc_branch,fc_docno,fc_custcode,fc_year,fc_periode)
);

CREATE TABLE db_dltisnow.t_tagihandtl(
	fc_Branch char(4) NOT NULL,
	fc_Docno varchar(20) NOT NULL,
	fc_Docref varchar(20) NOT NULL,
	fc_Tipe char(3) NOT NULL,
	fd_Docdate datetime NOT NULL,
	fd_Postdate datetime NOT NULL,
	fd_Inputdate datetime NOT NULL,
	fd_Duedate datetime NOT NULL,
	fc_Mu char(3) NOT NULL,
	fm_Rate double(20,3) NOT NULL,
	fn_TOP tinyint NOT NULL,
	fc_CR char(1) NOT NULL,
	fm_Netto1 double(20,3) NOT NULL,
	fm_Netto2 double(20,3) NOT NULL,
	fm_Pay1 double(20,3) NOT NULL,
	fm_Pay2 double(20,3) NOT NULL,
	fm_SisaCN double(20,3) NOT NULL,
	fc_Status char(1) NOT NULL,
	primary key(fc_branch,fc_docno,fc_docref,fc_tipe)
);

CREATE TABLE db_dltisnow.t_tagihanrkpmst (
	fc_Branch char(4) NOT NULL,
	fc_Docno varchar(12) NOT NULL,
	fc_Year char(4) NOT NULL,
	fc_Periode char(2) NOT NULL,
	fd_Docdate datetime NOT NULL,
	fd_Inputdate datetime NOT NULL,
	fd_Postdate datetime NOT NULL,
	fc_InputBy varchar(10) NOT NULL,
	fc_Collector varchar(10) NOT NULL,
	fc_Status char(1) NOT NULL,
	fc_Tax char(1) NOT NULL,
	primary key(fc_branch,fc_docno,fc_year,fc_periode)
);

CREATE TABLE db_dltisnow.t_tagihanrkpdtl (
	fc_Branch char(4) NOT NULL,
	fc_Docno varchar(12) NOT NULL,
	fc_Docref varchar(12) NOT NULL,
	fn_No int NOT NULL,
	fc_Status char(1) NOT NULL,
	primary key(fc_branch,fc_docno,fc_docref)
);



create table t_lhp (
	fc_branch chr(4) , -- kode branch
	fc_Docno varchar(20) , -- no dokumen lhp
	fc_custcode varchar(30),-- code customer
	fm_pay double(20,3),-- jumlah yg dibayar
	fc_paymethod varchar(20),-- method bayar
	fd_transdate datetime,-- tgl transaksi
primary key (fc_branch,dc_docno)
)


--  table buat mas imam
CREATE TABLE db_dltisnow.t_lhpmst (
	fc_Branch char(4) NOT NULL, #kode depo (disiapkan dari tis)
	fc_Docno varchar(20) NOT NULL, #no lhp (disiapkan dari tis)
	fc_Custcode varchar(15) NOT NULL, #no customer (disiapkan dari tis)
	fc_Year char(4) NOT NULL, # yyyy (disiapkan dari tis)
	fc_Periode char(2) NOT NULL, # mm (disiapkan dari tis)
	fd_Docdate datetime NOT NULL, #Tanggal yyyy-mm-dd (disiapkan dari tis)
	fd_Postdate datetime NOT NULL, #Tanggal yyyy-mm-dd (disiapkan dari tis)
	fd_Inputdate datetime NOT NULL, #Tanggal yyyy-mm-dd hh:nn:ss (diedit mas imam)
	fc_Collector varchar(10) NOT NULL, #collectornya (disiapkan dari tis)
	fm_Netto double(20,3) NOT NULL,#total uang yang ditagih di dtl (disiapkan dari tis)
	fm_Pay double(20,3) NOT NULL, #total uang yg dibayar customer di dtl (diisi di mas imam)
	fc_Status char(1) NOT NULL DEFAULT '', #F = blm transaksi T sudah transaksi
	fc_PayType varchar(10) NOT NULL #cash /transfer NB : sementara cash dl aja mas
	fc_Bank varchar(15) NULL DEFAULT '', #bank  (disiapkan dari tis)
	fc_NoRek varchar(50) NULL DEFAULT '', #No rekening (disiapkan dari tis)
	primary key (fc_branch,fc_docno,fc_custcode,fc_year,fc_periode)
);

CREATE TABLE db_dltisnow.t_lhpdtl(
	fc_Branch char(4) NOT NULL, #kode depo (disiapkan dari tis)
	fc_Docno varchar(20) NOT NULL,#no lhp (disiapkan dari tis)
	fc_Docref varchar(20) NOT NULL,#no bill (disiapkan dari tis)
	fc_Tipe char(3) NOT NULL,#tipe ('NORMAL','GALON BELI','GALON JAMIN') (disiapkan dari tis)
	fd_Docdate datetime NOT NULL,#Tanggal yyyy-mm-dd (disiapkan dari tis)
	fd_Postdate datetime NOT NULL, #Tanggal yyyy-mm-dd (disiapkan dari tis)
	fd_Inputdate datetime NOT NULL,#Tanggal yyyy-mm-dd hh:nn:ss
	fd_Duedate datetime NOT NULL, #tanggal batas waktu pembayaran (disiapkan dari tis)
	fm_Netto double(20,3) NOT NULL,#Tanggal yyyy-mm-dd (disiapkan dari tis)
	fm_Pay double(20,3) NOT NULL,#uang yg dibayar customer (diisi mas imam)
	fc_Status char(1) NOT NULL DEFAULT '',#F / T
	primary key(fc_branch,fc_docno,fc_docref,fc_tipe)
);
