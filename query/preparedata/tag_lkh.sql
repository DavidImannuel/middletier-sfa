delete from t_tagmst where fc_tagno= 'TGN1B01120201010';
delete from t_tagdtl where fc_tagno= 'TGN1B01120201010';
delete from t_tagmst where fc_tagno= 'TGN2B01120201010';
delete from t_tagdtl where fc_tagno= 'TGN2B01120201010';
delete from t_tagmst where fc_tagno= 'TGN3B01120201010';
delete from t_tagdtl where fc_tagno= 'TGN3B01120201010';

-- tag 1
insert into t_tagmst
select fc_branch,concat('TGN1',fc_salescode,date_format(fd_docdate,'%Y%m%d')),fc_Tipe,fc_Year,fc_Periode,fc_CostCTR,fc_Docref,sysdate(),fd_Postdate,
fd_Inputdate,fc_InputBy,fd_Editdate,fc_Editby,fc_Branchasal,fc_LokAsal,fc_Branchtujuan,
fc_LokTujuan,fc_Ket,fc_Status,fc_OrderType,fc_shift,fc_salescode,fc_supircode,fc_stkno,fc_platno
from t_tagmst where fc_tagno = 'TG0000152749';
insert into t_tagdtl
select fc_branch,'TGN1B01120201010',fc_Gdcode,fc_Brgcode,fc_Batch,fc_No,fn_qty1,fn_qty2,fc_Satcode,fc_GL,fm_price
from t_tagdtl where fc_tagno = 'TG0000152749';
-- tag 2
insert into t_tagmst
select fc_branch,'TGN2B01120201010',fc_Tipe,fc_Year,fc_Periode,fc_CostCTR,fc_Docref,sysdate(),fd_Postdate,
fd_Inputdate,fc_InputBy,fd_Editdate,fc_Editby,fc_Branchasal,fc_LokAsal,fc_Branchtujuan,
fc_LokTujuan,fc_Ket,fc_Status,fc_OrderType,fc_shift,fc_salescode,fc_supircode,fc_stkno,fc_platno
from t_tagmst where fc_tagno = 'TG0000152753';
insert into t_tagdtl
select fc_branch,'TGN2B01120201010',fc_Gdcode,fc_Brgcode,fc_Batch,fc_No,60.00,60.00,fc_Satcode,fc_GL,fm_price
from t_tagdtl where fc_tagno = 'TG0000152753';

-- tag 3
insert into t_tagmst
select fc_branch,'TGN3B01120201010',fc_Tipe,fc_Year,fc_Periode,fc_CostCTR,fc_Docref,sysdate(),fd_Postdate,
fd_Inputdate,fc_InputBy,fd_Editdate,fc_Editby,fc_Branchasal,fc_LokAsal,fc_Branchtujuan,
fc_LokTujuan,fc_Ket,fc_Status,fc_OrderType,fc_shift,fc_salescode,fc_supircode,fc_stkno,fc_platno
from t_tagmst where fc_tagno = 'TG0000152753';

insert into t_tagdtl
select fc_branch,'TGN3B01120201010',fc_Gdcode,fc_Brgcode,fc_Batch,fc_No,40.00,40.00,fc_Satcode,fc_GL,fm_price
from t_tagdtl where fc_tagno = 'TG0000152753'
union all
select '2001', 'TGN3B01120201010', '364', 'CLE-SMART-220-IDM', '1', '13', '10.00', '240.00', 'PACK', '', '0.000'
union all
select '2001', 'TGN3B01120201010', '364', 'CLE-SMART-220-IDM', '2', '13', '150.00', '150.00', 'PCS', '', '0.000'
union all
select '2001', 'TGN3B01120201010', '364', 'IND-500-8+ALK', '3', '13', '10.00', '120.00', 'BOX', '', '0.000';

select * from t_tagmst where fc_tagno= 'TGN1B01120201010';
select * from t_tagdtl where fc_tagno= 'TGN1B01120201010';

select * from t_tagmst where fc_tagno= 'TGN2B01120201010';
select * from t_tagdtl where fc_tagno= 'TGN2B01120201010';


delete from t_tagmst where fc_tagno= 'TGN1B01120201010';
delete from t_tagdtl where fc_tagno= 'TGN1B01120201010';

delete from t_tagmst where fc_tagno= 'TGN2B01120201010';
delete from t_tagdtl where fc_tagno= 'TGN2B01120201010';
delete from t_tagmst where fc_tagno= 'TGN3B01120201010';
delete from t_tagdtl where fc_tagno= 'TGN3B01120201010';

-- stk
insert into t_stkmst
select fc_branch,'STKB011201010','LKHB011201010',fc_salescode,fc_drivercode,
fc_daystk,fc_shift,sysdate(),fd_dateinput,fc_userid,fc_status
from t_stkmst where fc_salescode = 'B011';

insert into t_stkdtl
select 'STKB011201010',fn_rownum,fc_custcode,fc_sono,fc_docref,
fd_datevisit,fc_status,fc_desc_kunjungan
from t_stkdtl where fc_stkno = 'STK.20.00021';

insert into t_lkhmst
select 
fc_branch,'STKB011201010','LKHB011201010',fc_salescode,fc_drivercode,fc_shift,fd_dateinput,date_format(sysdate(),'%Y-%m-%d 00:00:00'),'F'
from t_lkhmst where fc_lkhno = '2001-20201008-1-B011';


insert into t_lkhdtl
select fc_branch,'LKHB011201010',fn_rownum,fc_custcode,sysdate(),"0000-00-00 01:01:01","0000-00-00 01:01:01",
'','F' 
from t_lkhdtl where fc_lkhno = '2001-20201008-1-B011';

delete from t_lkhmst where fc_lkhno = 'LKHB011201010'
