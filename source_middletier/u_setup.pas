unit u_setup;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Buttons,
  StdCtrls, ComCtrls, CheckLst, DBGrids, DBCtrls, RichMemo, AbZipper, AbUnzper,
  db,ZConnection, ZSqlProcessor, ZDataset, strutils, FileUtil, DateTimePicker, ftpsend;

type

  { Tf_setup }

  Tf_setup = class(TForm)
    Button1: TButton;
    Button2: TButton;
    CheckBox1: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    DBNavigator4: TDBNavigator;
    DBNavigator5: TDBNavigator;
    dsqt_exportcsv_dbtis: TDataSource;
    dsqt_importcsv_dbtis: TDataSource;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    DBMemo2: TDBMemo;
    DBMemo3: TDBMemo;
    DBMemo4: TDBMemo;
    DBMemo5: TDBMemo;
    DBNavigator2: TDBNavigator;
    DBNavigator3: TDBNavigator;
    dst_importdbtis: TDataSource;
    dst_importdltisnow: TDataSource;
    dst_exportcsvsales: TDataSource;
    DBGrid1: TDBGrid;
    DBMemo1: TDBMemo;
    DBNavigator1: TDBNavigator;
    Edit1: TEdit;
    Edit2: TEdit;
    GroupBox1: TGroupBox;
    Memo1: TMemo;
    Memo2: TMemo;
    PageControl: TPageControl;
    PageControl1: TPageControl;
    Panel1: TPanel;
    Panel10: TPanel;
    Panel2: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    qt_exportcsvsalesfc_additional1: TStringField;
    qt_exportcsvsalesfc_additional2: TStringField;
    qt_exportcsvsalesfc_desc: TStringField;
    qt_exportcsvsalesfc_query: TMemoField;
    qt_exportcsvsalesfc_querytarget: TStringField;
    qt_exportcsvsalesfc_querytype: TStringField;
    qt_exportcsvsalesfl_status: TSmallintField;
    qt_exportcsvsalesfn_no: TLongintField;
    qt_exportcsv_dbtisfc_additional1: TStringField;
    qt_exportcsv_dbtisfc_additional2: TStringField;
    qt_exportcsv_dbtisfc_desc: TStringField;
    qt_exportcsv_dbtisfc_query: TMemoField;
    qt_exportcsv_dbtisfc_querytarget: TStringField;
    qt_exportcsv_dbtisfc_querytype: TStringField;
    qt_exportcsv_dbtisfl_status: TSmallintField;
    qt_exportcsv_dbtisfn_no: TLongintField;
    qt_importcsv_dbtisfc_additional1: TStringField;
    qt_importcsv_dbtisfc_additional2: TStringField;
    qt_importcsv_dbtisfc_desc: TStringField;
    qt_importcsv_dbtisfc_query: TMemoField;
    qt_importcsv_dbtisfc_querytarget: TStringField;
    qt_importcsv_dbtisfc_querytype: TStringField;
    qt_importcsv_dbtisfl_status: TSmallintField;
    qt_importcsv_dbtisfn_no: TLongintField;
    qt_importdbtisfc_additional1: TStringField;
    qt_importdbtisfc_additional2: TStringField;
    qt_importdbtisfc_desc: TStringField;
    qt_importdbtisfc_query: TMemoField;
    qt_importdbtisfc_querytarget: TStringField;
    qt_importdbtisfc_querytype: TStringField;
    qt_importdbtisfl_status: TSmallintField;
    qt_importdbtisfn_no: TLongintField;
    qt_importdltisnowfc_additional1: TStringField;
    qt_importdltisnowfc_additional2: TStringField;
    qt_importdltisnowfc_desc: TStringField;
    qt_importdltisnowfc_query: TMemoField;
    qt_importdltisnowfc_querytarget: TStringField;
    qt_importdltisnowfc_querytype: TStringField;
    qt_importdltisnowfl_status: TSmallintField;
    qt_importdltisnowfn_no: TLongintField;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheetPrepareData: TTabSheet;
    qt_importdbtis: TZQuery;
    qt_importdltisnow: TZQuery;
    qt_exportcsvsales: TZQuery;
    qt_exportcsv_dbtis: TZQuery;
    qt_importcsv_dbtis: TZQuery;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure DateTimePicker1Enter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qt_exportcsvsalesNewRecord(DataSet: TDataSet);
    procedure qt_exportcsv_dbtisNewRecord(DataSet: TDataSet);
    procedure qt_importcsv_dbtisNewRecord(DataSet: TDataSet);
    procedure qt_importdbtisNewRecord(DataSet: TDataSet);
    procedure qt_importdltisnowNewRecord(DataSet: TDataSet);
  private

  public

  end;

  { TThreadTestQuery }

  TThreadTestQuery = class(TThread)
    private
    protected
      FDBType,FBRanch,FSalescode,FMessage:String;
      FDateparam:TDate;
      procedure Execute; override;
      procedure showLog;
    public
      constructor Create(vDBType,vBranch,vSalescode:String;vDateparam:TDate);virtual;reintroduce;
  end;


var
  f_setup: Tf_setup;

implementation

uses
  u_datamodule,u_threads,u_service;

{$R *.lfm}

{ TThreadTestQuery }

procedure TThreadTestQuery.Execute;
var
  Conn:TZConnection;
  Q:TZQuery;
  SP:TZSQLProcessor;
begin
  if self.FDBType = 'MYSQL' then begin
    Conn:=TZConnection.Create(Nil);
    Conn.Protocol:='mysql';
    Conn.HostName:=f_service.MYSQLHost;
    Conn.User:=f_service.MYSQLUser;
    Conn.Password:=f_service.MYSQLPassword;
    Conn.Database:=f_service.MYSQLDB;
    Conn.LibraryLocation:='';
    Conn.Port:=f_service.MYSQLPort;
    try
      Conn.Connected:=True;
      SP:=TZSQLProcessor.Create(Nil);
      SP.Connection:=Conn;
      SP.Script.Clear;
      SP.Script.Text:=f_setup.Memo2.Text;
      if AnsiContainsStr(SP.Script.Text,':branch') then SP.ParamByName('branch').AsString:=FBranch;
      if AnsiContainsStr(SP.Script.Text,':salescode') then SP.ParamByName('salescode').AsString:=FSalescode;
      if AnsiContainsStr(SP.Script.Text,':date') then SP.ParamByName('date').AsDate:=FDateparam;
      SP.Execute;
      FMessage:='Query Success';
      Synchronize(@showLog);
      if Assigned(Conn) then Conn.Free;
      if Assigned(Q) then Q.Free;
      if Assigned(SP) then SP.Free;
    except
      on E:exception do begin
          FMessage:=E.Message;
          Synchronize(@showLog);
          if Assigned(Conn) then Conn.Free;
          if Assigned(Q) then Q.Free;
          if Assigned(SP) then SP.Free;
      end;
    end;
  end else if self.FDBType = 'SQLSERVER' then begin
     Conn:=TZConnection.Create(Nil);
     Conn.Protocol:='ado';
     Conn.HostName:=f_service.SQLSERVERHost;
     Conn.User:=f_service.SQLSERVERUser;
     Conn.Password:=f_service.SQLSERVERPassword;
     Conn.Database:=f_service.SQLSERVERDB;
     Conn.Port:=f_service.SQLSERVERPort;
    try
      Conn.Connected:=True;
      Q:=TZQuery.Create(Nil);
      Q.Connection:=Conn;
      Q.SQL.Clear;
      Q.SQL.Text:=f_setup.Memo2.Text;
      if AnsiContainsStr(Q.SQL.Text,':branch') then Q.ParamByName('branch').AsString:=FBranch;
      if AnsiContainsStr(Q.SQL.Text,':salescode') then Q.ParamByName('salescode').AsString:=FSalescode;
      if AnsiContainsStr(Q.SQL.Text,':date') then Q.ParamByName('date').AsDate:=FDateparam;
      Q.ExecSQL;
      FMessage:='Query Success';
      Synchronize(@showLog);
      if Assigned(Conn) then Conn.Free;
      if Assigned(Q) then Q.Free;
      if Assigned(SP) then SP.Free;
    except
      on E:exception do begin
          FMessage:=E.Message;
          Synchronize(@showLog);
          if Assigned(Conn) then Conn.Free;
          if Assigned(Q) then Q.Free;
          if Assigned(SP) then SP.Free;
      end;
    end;
  end else begin
      self.FMessage:='INVALID DATABASE';
      Synchronize(@showLog);
  end;
end;

procedure TThreadTestQuery.showLog;
begin
  f_setup.Memo1.Lines.Add(self.FMessage);
end;

constructor TThreadTestQuery.Create(vDBType, vBranch, vSalescode: String;
  vDateparam: TDate);
begin
  FDBType:=vDBType;
  FBranch:=vBranch;
  FSalescode:=vSalescode;
  FDateParam:=vDateParam;
  FreeOnTerminate:=True;
  inherited Create(False);
end;

{ Tf_setup }

procedure Tf_setup.qt_importdbtisNewRecord(DataSet: TDataSet);
begin
  qt_importdbtisfn_no.AsInteger:=qt_importdbtis.RecordCount+1;
  qt_importdbtisfc_querytype.AsString:='IMPORT_DBTIS';
end;

procedure Tf_setup.qt_exportcsvsalesNewRecord(DataSet: TDataSet);
begin
  qt_exportcsvsalesfn_no.AsInteger:=qt_exportcsvsales.RecordCount+1;
  qt_exportcsvsalesfc_querytype.AsString:='EXPORT_CSV';
  qt_exportcsvsalesfc_querytarget.AsString:='SALES';
end;

procedure Tf_setup.qt_exportcsv_dbtisNewRecord(DataSet: TDataSet);
begin
  qt_exportcsv_dbtisfn_no.AsInteger:=qt_exportcsv_dbtis.RecordCount+1;
  qt_exportcsv_dbtisfc_querytype.AsString:='EXPORT_CSV_DBTIS';
end;

procedure Tf_setup.qt_importcsv_dbtisNewRecord(DataSet: TDataSet);
begin
  qt_importcsv_dbtisfn_no.AsInteger:=qt_importcsv_dbtis.RecordCount+1;
  qt_importcsv_dbtisfc_querytype.AsString:='IMPORT_CSV_DBTIS';
end;

procedure Tf_setup.FormCreate(Sender: TObject);
begin
  // code here
end;

procedure Tf_setup.CheckBox1Change(Sender: TObject);
begin
  if CheckBox1.Checked = true then begin
    qt_importdbtisfc_query.ReadOnly:=True;
    qt_importdltisnowfc_query.ReadOnly:=True;
    qt_exportcsvsalesfc_query.ReadOnly:=True;
    qt_exportcsv_dbtisfc_query.ReadOnly:=True;
    qt_importcsv_dbtisfc_query.ReadOnly:=True;
  end else begin
    qt_importdbtisfc_query.ReadOnly:=False;
    qt_importdltisnowfc_query.ReadOnly:=False;
    qt_exportcsvsalesfc_query.ReadOnly:=False;
    qt_exportcsv_dbtisfc_query.ReadOnly:=False;
    qt_importcsv_dbtisfc_query.ReadOnly:=False;
  end;
end;

procedure Tf_setup.Button1Click(Sender: TObject);
begin
  TThreadTestQuery.Create('MYSQL',Edit1.Text,Edit2.Text,DateTimePicker1.Date);
end;

procedure Tf_setup.Button2Click(Sender: TObject);
begin
  TThreadTestQuery.Create('SQLSERVER',Edit1.Text,Edit2.Text,DateTimePicker1.Date);
end;

procedure Tf_setup.DateTimePicker1Enter(Sender: TObject);
begin
  DateTimePicker1.Date:=Now;
end;

procedure Tf_setup.qt_importdltisnowNewRecord(DataSet: TDataSet);
begin
  qt_importdltisnowfn_no.AsInteger:=qt_importdltisnow.RecordCount+1;
  qt_importdltisnowfc_querytype.AsString:='IMPORT_DLTISNOW';
end;

end.

