unit u_setupdb;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons;

type

  { Tf_setupdb }

  Tf_setupdb = class(TForm)
    EdtMssqlUser: TEdit;
    EdtMysqlUser: TEdit;
    EdtMssqlServer: TEdit;
    EdtMssqlDB: TEdit;
    EdtMssqlPass: TEdit;
    EdtMssqlPort: TEdit;
    EdtMysqlServer: TEdit;
    EdtMysqlDB: TEdit;
    EdtMysqlPass: TEdit;
    EdtMysqlPort: TEdit;
    Image1: TImage;
    Image10: TImage;
    Image11: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    BtnTestConMssql: TSpeedButton;
    BtnTestConMysql: TSpeedButton;
    procedure BtnTestConMssqlClick(Sender: TObject);
    procedure BtnTestConMysqlClick(Sender: TObject);
  private

  public

  end;

var
  f_setupdb: Tf_setupdb;

implementation

uses u_datamodule,u_middletier;

{$R *.lfm}

{ Tf_setupdb }

procedure Tf_setupdb.BtnTestConMssqlClick(Sender: TObject);
begin
  with DM.ConnSqlserver do begin
    try
      Connected:=False;
      //User:=EdtMssqlUser.Text;
      //Password:=EdtMssqlPass.Text;
      //HostName:=EdtMssqlServer.Text;
      //Port:=StrToInt(EdtMssqlPort.Text);
      //Database:=EdtMssqlDB.Text;
      Database:='Provider=SQLOLEDB.1;'
               +'Password='+QuotedStr(EdtMssqlPass.Text)+';'
               +'Persist Security Info=True;User ID='+QuotedStr(EdtMssqlUser.Text)+';'
               +'Initial Catalog='+QuotedStr(EdtMssqlDB.Text)+';'
               +'Data Source='+QuotedStr(EdtMssqlServer.Text)+';';
      Connected:=True;
      if Connected then ShowMessage('Connection OK');
    Except
      on E:Exception do begin
         ShowMessage('Connection Failed. Error '+E.Message);
      end;
    end;

  end;
end;

procedure Tf_setupdb.BtnTestConMysqlClick(Sender: TObject);
begin
  with DM.ConnMysql do begin
    try
      Connected:=False;
      User:=EdtMysqlUser.Text;
      Password:=EdtMysqlPass.Text;
      HostName:=EdtMysqlServer.Text;
      Port:=StrToInt(EdtMysqlPort.Text);
      Database:=EdtMysqlDB.Text;
      Connected:=True;
      if Connected then begin
        ShowMessage('Connection OK');
        f_middletier.ActivatedQueryComponent;
      end;
    Except
      on E:Exception do begin
         ShowMessage('Connection Failed. Error '+E.Message);
      end;
    end;

  end;
end;

end.

