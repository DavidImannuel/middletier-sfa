unit u_querystorage;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ZDataset, db;

type

  { TQS }

  TQS = class(TDataModule)
    ds_settlementmysql: TDataSource;
    ds_settlement_query: TDataSource;
    ds_request_middletier: TDataSource;
    qt_importsqlserverbranchfc_desc: TStringField;
    qt_importsqlserverbranchfc_query: TMemoField;
    qt_importsqlserverbranchfc_querytarget: TStringField;
    qt_importsqlserverbranchfc_querytype: TStringField;
    qt_importsqlserverbranchfl_status: TSmallintField;
    qt_importsqlserverbranchfn_no: TLongintField;
    qt_request_middletier: TZQuery;
    qt_request_middletierfc_branch: TStringField;
    qt_request_middletierfc_functionkode: TStringField;
    qt_request_middletierfc_status: TStringField;
    qt_request_middletierfd_dateparam: TDateTimeField;
    qt_request_middletierfd_requestdate: TDateTimeField;
    qt_request_middletierfn_requestnum: TLargeintField;
    qt_settlementmysqlfc_additional1: TStringField;
    qt_settlementmysqlfc_additional2: TStringField;
    qt_settlementmysqlfc_desc: TStringField;
    qt_settlementmysqlfc_query: TMemoField;
    qt_settlementmysqlfc_querytarget: TStringField;
    qt_settlementmysqlfc_querytype: TStringField;
    qt_settlementmysqlfl_status: TSmallintField;
    qt_settlementmysqlfn_no: TLongintField;
    qt_settlement_query: TZQuery;
    qt_settlement_queryfc_additional1: TStringField;
    qt_settlement_queryfc_additional2: TStringField;
    qt_settlement_queryfc_desc: TStringField;
    qt_settlement_queryfc_query: TMemoField;
    qt_settlement_queryfc_querytarget: TStringField;
    qt_settlement_queryfc_querytype: TStringField;
    qt_settlement_queryfl_status: TSmallintField;
    qt_settlement_queryfn_no: TLongintField;
    qt_settlementmysql: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure qt_settlementmysqlNewRecord(DataSet: TDataSet);
    procedure qt_settlement_queryNewRecord(DataSet: TDataSet);
  private

  public

  end;

var
  QS: TQS;

implementation

{$R *.lfm}

{ TQS }

procedure TQS.DataModuleCreate(Sender: TObject);
begin
  qt_request_middletier.Open;
  qt_settlement_query.Open;
end;

procedure TQS.qt_settlementmysqlNewRecord(DataSet: TDataSet);
begin
  qt_settlementmysqlfn_no.AsInteger:=qt_settlementmysql.RecordCount+1;
  qt_settlementmysqlfc_querytype.AsString:='SETTLEMENT_MYSQL';
  qt_settlementmysqlfc_querytarget.AsString:='GENERAL';
end;

procedure TQS.qt_settlement_queryNewRecord(DataSet: TDataSet);
begin
  qt_settlement_queryfn_no.AsInteger:=qt_settlement_query.RecordCount + 1;
  qt_settlement_queryfc_querytype.AsString:='SETTLEMENT_QUERY';
  qt_settlement_queryfc_querytarget.AsString:='GENERAL';
end;

end.

