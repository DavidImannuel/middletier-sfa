unit u_datamodule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Dialogs, Controls, ZConnection, ZDataset,
  ZSqlProcessor, ZStoredProcedure, mssqlconn, odbcconn, sqldb, StdCtrls,
  ExtCtrls, AbUnzper, AbZipper, ftpsend, strutils;

type

  { TDM }

  TDM = class(TDataModule)
    MyZippper: TAbZipper;
    MyUnzipper: TAbUnZipper;
    ImageList: TImageList;
    MyBatch: TZSQLProcessor;
    ConnMysql: TZConnection;
    MyQuery: TZQuery;
    ConnSqlserver: TZConnection;
    PingTimer: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure ReadJSONConfig(filename : String);
    //list middletier helper
    procedure ReOpenDataSet(DS:TZQuery); //zeos component
    procedure RunQuery(Conn:TZConnection;Desc,Query: String); //special procedure for t_query(db_sfa)
    procedure RunQueryWithBranch(Conn:TZConnection;Desc,Query,Branch : String;Date:TDate); //special procedure for t_query(db_sfa)
    procedure ExportRecordToCsv(desc,query,filename,delim:string);
    procedure ExportDataCsvSales(branch,salescode,desc,query,filename,delim:string;Date:TDate);
    procedure ImportCsv(table, filename, delim,fields: string;IgnoreFirstRow:Boolean);
    procedure ZipFile(BaseDir,Filename:String);
    procedure UnZip(ZipFileName,ExtractDir,FileToBeExtract:String);
    Procedure SendFileToFTP(filenameServer,filenameLocal:String);
    Procedure GetFileFromFTP(filenameServer,filenameLocal:String);
    Procedure DeleteFTPFile(filenameServer:String);
    procedure ListAllFilesOnFTP(const vDir:String);
    function RunQueryTransaction(Conn:TZConnection;Desc,Query: String):Boolean;
    function GetFTPFile(filenameServer,filenameLocal:String):Boolean;
    //end helper
    FUNCTION NamaBulan(tglbulan:TDate):String;
    FUNCTION NowX:TDateTime;
    FUNCTION CreateNoByYearYYPrefix(BranchIs : String; DocCode :String; PartCode :String): String;
    function CountingRows(conn:TZConnection;tablename:String):Integer;
    function CreateNoCust(branch,doc,year,month:String):String;
    function GetCurDir(filename:string=''):String;
    function ReadFile(filename:String):string;

  private

  public
     UserId , BranchID,DbMysql,DbSqlserver,LinkedServerMysql : String;
     MyFtp:TFTPSend;

  end;

var
  DM: TDM;

implementation

uses u_setup,JsonTools,u_service;

{$R *.lfm}

{ TDM }

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  BranchID:='2001';
  //instanse FTP Object
  MyFtp:=TFTPSend.Create;
  //ReadJSONConfig('config.json');
  //f_service.qt_request.Open;
  //f_service.DateTimePicker1.Date:=NowX;
end;

procedure TDM.ReadJSONConfig(filename : String);
begin
  With TJsonNode.Create do begin
    LoadFromFile(filename);
    ConnMysql.Connected:=False;
    ConnSqlserver.Connected:=False;
    //mysql
    ConnMysql.HostName:=Find('mysql/hostname').AsString;
    ConnMysql.Port:=StrToInt(Find('mysql/port').AsString);
    ConnMysql.User:=Find('mysql/user').AsString;
    ConnMysql.Password:=Find('mysql/password').AsString;
    ConnMysql.LibraryLocation:=Find('mysql/libraryLocation').AsString;
    ConnMysql.Database:=Find('mysql/database').AsString;
    //sqlserver
    ConnSqlserver.HostName:=Find('sqlserver/hostname').AsString;
    ConnSqlserver.Port:=StrToInt(Find('sqlserver/port').AsString);
    ConnSqlserver.User:=Find('sqlserver/user').AsString;
    ConnSqlserver.Password:=Find('sqlserver/password').AsString;
    ConnSqlserver.Database:=Find('sqlserver/database').AsString;
    //FTP
    MyFtp.TargetHost:=Find('ftp/hostname').AsString;
    MyFtp.TargetPort:=Find('ftp/port').AsString;
    MyFtp.UserName:=Find('ftp/user').AsString;
    MyFtp.Password:=Find('ftp/password').AsString;
    //try to connect all
    if Myftp.Login then begin
      //ShowMessage('FTP connect');
    end else begin
        ShowMessage('FTP cant connect,check username , password & port !!');
        ShowMessage('Application Closed');
        Application.Terminate;
    end;

    try
      ConnSqlserver.Connected:=True;
      ConnMysql.Connected:=True;
    except
       on E:exception do begin
         ShowMessage('MYSQL / SQLSERVER CANT CONNECT . ERROR : '+E.Message);
         ShowMessage('Application Closed');
         Application.Terminate;
       end;
    end;

  end;

end;

procedure TDM.ReOpenDataSet(DS: TZQuery);
begin
  DS.Close;DS.Open;
end;

procedure TDM.RunQuery(Conn:TZConnection;Desc, Query: String);
begin
  With MyQuery do begin
    try
      Connection:=Conn;
      SQL.Clear;
      SQl.Text:=Query;
      ExecSQL;
      //WriteLog(Desc+' success.',f_setup.MemoLog);
    except
       on E:Exception do
        begin
            //WriteLog(Desc+' failed. ERROR : '+E.Message,f_setup.MemoLog);
        end;
    end;

  end;
end;

procedure TDM.RunQueryWithBranch(Conn:TZConnection;Desc,Query,Branch: String;Date:TDate);
begin

  With MyQuery do begin
    try
      Connection:=Conn;
      SQL.Clear;
      SQL.Text:=Query;
      if AnsiContainsStr(Query,':branch') then ParamByName('branch').AsString:=Branch;
      if AnsiContainsStr(Query,':date') then ParamByName('date').AsDate:=Date;
      if AnsiContainsStr(query,':tgl') then ParamByName('tgl').AsString:=FormatDateTime('YYYY-MM-DD',Date);
      ExecSQL;
      //WriteLog(Desc+' success.',f_setup.MemoLog);
    except
       on E:Exception do
        begin
            //WriteLog(Desc+' failed. ERROR : '+E.Message,f_setup.MemoLog);
        end;
    end;

  end;
end;

procedure TDM.ExportRecordToCsv(desc, query, filename, delim: string);
begin
  with MyBatch do begin
    try
      //WriteLog('Run Export CSV : '+desc,f_setup.MemoLog);
      Connection:=ConnMysql;
      if FileExists(filename) then DeleteFile(filename);
      Script.Text:=query+' into outfile '+QuotedStr(filename)
                  +' FIELDS TERMINATED BY '+ QuotedStr(delim);
      Execute;
      //WriteLog('Export CSV with filename '+filename+ ' success.',f_setup.MemoLog);
    except
       on E:Exception do begin
         //WriteLog('Export CSV with filename '+filename+ ' failed. ERROR : '+E.Message,f_setup.MemoLog);
       end;
    end;
  end;
end;

procedure TDM.ExportDataCsvSales(branch,salescode,desc, query, filename,
  delim: string;Date:TDate);
begin
  with MyBatch do begin
    try
      //WriteLog('Run Export CSV : '+desc,f_setup.MemoLog);
      Connection:=ConnMysql;
      if FileExists(filename) then DeleteFile(filename);
      Script.Text:=query+' into outfile '+QuotedStr(filename)
                  +' FIELDS TERMINATED BY '+ QuotedStr(delim)
                  //+'  OPTIONALLY ENCLOSED BY '+QuotedStr('"')
                  //+' LINES TERMINATED BY '+chr(13)+';'
                  ;

      if AnsiContainsStr(query,':branch') then ParamByName('branch').AsString:=branch;
      if AnsiContainsStr(query,':salescode') then ParamByName('salescode').AsString:=salescode;
      if AnsiContainsStr(query,':date') then ParamByName('date').AsDate:=Date;
      Execute;
      //WriteLog('Export CSV with filename '+filename+ ' success.',f_setup.MemoLog);
    except
       on E:Exception do begin
         //WriteLog('Export CSV with filename '+filename+ ' failed. ERROR : '+E.Message,f_setup.MemoLog);
       end;
    end;
  end;
end;

procedure TDM.ImportCsv(table, filename, delim, fields: string;
  IgnoreFirstRow: Boolean);
var
  Processor: TZSQLProcessor;
  CurDir,PathSaveFile : String;
begin
  Processor:=TZSQLProcessor.Create(Self);
  with processor do begin
    try
      Connection:=DM.ConnMysql;
      GetDir (0,CurDir);
      if IgnoreFirstRow then begin
        Script.Text:=' load data local infile '+QuotedStr(filename)
                  +' REPLACE ' //delete jika sudah ada lalu insert
                  +' into table ' + table
                  +' FIELDS TERMINATED BY '+ QuotedStr(delim)
                  +' ENCLOSED BY '+QuotedStr('"')
                  //+' lines terminated by ' +QuotedStr('\r\n')
                  +' lines terminated by '+ QuotedStr(chr(13)) //chr13 = enter jika menggunakan \r atau \n kurang work
                  +' IGNORE 1 LINES '
                  +fields+' ;';
      end else begin
        Script.Text:=' load data local infile '+QuotedStr(filename)
                  +' into table ' + table
                  +' FIELDS TERMINATED BY '+ QuotedStr(delim)
                  +' ENCLOSED BY '+QuotedStr('"')
                  +' lines terminated by ' +QuotedStr(chr(13))
                  +fields+' ;';
      end;
      Execute;
      //WriteLog('Import CSV file on table '+table+ ' success.',f_setup.MemoLog);
    except
       on E:Exception do begin
         //WriteLog('Import CSV file on table '+table+ ' failed. ERROR : '+E.Message,f_setup.MemoLog);
       end;
    end;
    Free;
  end;
end;

procedure TDM.ZipFile(BaseDir, Filename: String);
begin
  try
    MyZippper.FileName:=Filename;
    //if FileExists(Filename) then begin  //gakjalan ?
    //  if DeleteFile(Filename) then WriteLog('File EXIST: '+AnsiReplaceStr(Filename,'\\','\')+' FILE DELETED',f_setup.MemoLog);
    //end;
    //WriteLog('prepare to zip '+AnsiReplaceStr(Filename,'\\','\'),f_setup.MemoLog);
    //name of the directory that we want to zip
    MyZippper.BaseDirectory:=BaseDir;
    MyZippper.AddFiles('*.*',faAnyFile);
    MyZippper.Save;
    MyZippper.CloseArchive;
    //WriteLog('zip file from ' +BaseDir+ ' to '+Filename+' success',f_setup.MemoLog);
 except
    on E:Exception do begin
      //WriteLog('Cannot ZIP : '+BaseDir+' to '+Filename,f_setup.MemoLog);
      //WriteLog('Error : '+E.Message,f_setup.MemoLog);
    end;
 end;
end;

procedure TDM.UnZip(ZipFileName, ExtractDir, FileToBeExtract: String);
begin
  try
    //WriteLog('Creating Directory for '+ AnsiReplaceStr(ZipFileName,'\\','\'),f_setup.MemoLog);
    CreateDir(ExtractDir);
  except
     on E: Exception do begin
       //WriteLog('Error : '+ E.Message,f_setup.MemoLog);
     end;
  end;

  with MyUnzipper do begin
    BaseDirectory:=ExtractDir;
    try
       //WriteLog('Unzip '+ZipFileName,f_setup.MemoLog);
       FileName:=ZipFileName;
       ExtractFiles(FileToBeExtract);
       //UnZip.ExtractFiles('*.csv');
       //WriteLog('Succesfully Unzip ' + AnsiReplaceStr(ZipFileName,'\\','\'),f_setup.MemoLog);
       CloseArchive;
    except
       on E:Exception do begin
         //WriteLog('Error : '+ E.Message,f_setup.MemoLog);
       end;
    end;
  end;
end;

procedure TDM.SendFileToFTP(filenameServer, filenameLocal: String);
begin
  with MyFtp do begin

    try
      if FtpPutFile(TargetHost,TargetPort,filenameServer,filenameLocal,UserName,Password)
         then
           //WriteLog('Succesfully transfer '+AnsiReplaceStr(filenameLocal,'\\','\')+' to FTP server',f_setup.MemoLog)
         else
           //WriteLog('Cannot transfer '+AnsiReplaceStr(filenameLocal,'\\','\')+' to FTP server',f_setup.MemoLog)
           ;
    except
       on E: Exception do begin
         //WriteLog('Cannot transfer '+AnsiReplaceStr(filenameLocal,'\\','\')+' to FTP server',f_setup.MemoLog);
         //WriteLog('Error : '+ E.Message,f_setup.MemoLog);
       end;

    end;

  end;
end;

procedure TDM.GetFileFromFTP(filenameServer, filenameLocal: String);
begin
  with MyFtp do begin

    try
      if FtpGetFile(TargetHost,TargetPort,filenameServer,filenameLocal,UserName,Password) then begin
        //WriteLog('Succesfully download '+filenameLocal+' from FTP server',f_setup.MemoLog);
      end else
      //WriteLog('Cannot download '+filenameLocal+' from FTP server',f_setup.MemoLog)
      ;
    except
       on E: Exception do begin
         //WriteLog('Error : '+ E.Message,f_setup.MemoLog);
         //saat errror hapus file nya
         {
         DeleteFile(filenameLocal);
         }
       end;

    end;

  end;
end;

procedure TDM.DeleteFTPFile(filenameServer: String);
begin
  with MyFtp do begin

    try

      if Login and DeleteFile(filenameServer) then begin
        //WriteLog('Succesfully delete '+filenameServer+' on FTP server',f_setup.MemoLog);
      end else
      //WriteLog('Cannot delete '+filenameServer+' on from FTP server',f_setup.MemoLog)
      ;

    except
       on E: Exception do begin
         //WriteLog('Error : '+ E.Message,f_setup.MemoLog);
       end;

    end;

  end;
end;

procedure TDM.ListAllFilesOnFTP(const vDir: String);
var
  i: integer;
  s: string;
  dirs: TStringList;
begin

  if MyFtp.Login then
    //WriteLog('Listing Dir and File on FTP',f_setup.MemoLog) else WriteLog('Cannot Login FTP',f_setup.MemoLog)
    ;
  dirs := nil;
  try
    MyFtp.List(vDir, false);
    for i := 0 to MyFtp.FtpList.Count - 1 do begin
      s := vDir + '/' + MyFtp.FtpList[i].FileName;
      //WriteLog('-'+s,f_setup.MemoLog);
      if MyFtp.FtpList[i].Directory then begin
        if dirs = nil then
          dirs := TStringList.Create;
        dirs.Add(s);
        //WriteLog('#'+s,f_setup.MemoLog);
      end;
    end;
    if dirs <> nil then begin
      for i := 0 to dirs.Count - 1 do begin
        MyFtp.ChangeWorkingDir(dirs[i]);
        ListAllFilesOnFTP(dirs[i]);
      end;
    end;
  finally
    dirs.Free;
  end;

end;

function TDM.RunQueryTransaction(Conn: TZConnection; Desc, Query: String
  ): Boolean;
begin
  With MyQuery do begin
      Connection:=Conn;
      Connection.StartTransaction;
    try
      SQL.Clear;
      SQl.Text:=Query;
      ExecSQL;
      Result:=True;
      //WriteLog(Desc+' success.',f_setup.MemoLog);
    except
       on E:Exception do
        begin
            Result:=False;
            Connection.Rollback;
            //WriteLog(Desc+' failed. ERROR : '+E.Message,f_setup.MemoLog);
        end;
    end;
  end;
end;

function TDM.GetFTPFile(filenameServer, filenameLocal: String): Boolean;
begin
  with MyFtp do begin
    try
      if FtpGetFile(TargetHost,TargetPort,filenameServer,filenameLocal,UserName,Password) then begin
        //WriteLog('Succesfully download '+filenameLocal+' from FTP server',f_setup.MemoLog);
        Result:=True;
      end else begin
        //WriteLog('Cannot download '+filenameLocal+' from FTP server',f_setup.MemoLog) ;
        Result:=False;
      end;
    except
       on E: Exception do begin
         //WriteLog('Error : '+ E.Message,f_setup.MemoLog);
         Result:=False;
       end;
    end;
  end;
end;

function TDM.NowX:TDateTime;
var q1:TZQuery;
    cValue : String;
begin
   q1:=TZQuery.Create(Application);
   with q1 do begin
      Connection := DM.ConnMysql;
      Sql.Add('select sysdate() as tanggal');
      Open;
      try
           result:=StrToDateTime(FieldByName('tanggal').AsString);
      except
        cValue := Copy(q1.FieldByName('tanggal').AsString,9,2) + '-'+   // DD
                  Copy(q1.FieldByName('tanggal').AsString,6,2) + '-'+   // MM
                  Copy(q1.FieldByName('tanggal').AsString,1,4) + '-'+   // YYYY
                  ' '+
                  Copy(q1.FieldByName('tanggal').AsString,12,2) + ':'+  // TT
                  Copy(q1.FieldByName('tanggal').AsString,15,2) + ':'+  // NN
                  Copy(q1.FieldByName('tanggal').AsString,18,2);        // SS
        ShowMessage(q1.FieldByName('tanggal').AsString);
        ShowMessage(cValue);
        result:=StrToDateTime(cValue);
      end;
   end;
   q1.free;
end;

function TDM.CreateNoByYearYYPrefix(BranchIs : String; DocCode :String; PartCode :String): String;
var
  q                                     : TZQuery;
  Result2Send, vDelimeter, yyIs         : String;

begin
  vDelimeter            := '.';
  yyIs                  := vDelimeter + FormatDateTime('yy',NowX) + vDelimeter;
  q                     := TZQuery.Create(Application);
  With q Do Begin
    Connection                            := DM.ConnMysql;
    SQL.Clear;
    SQL.Text                            :=
        ' select concat(fv_prefix, ' + QuotedStr(yyIs) +  ', ' +
        ' repeat(''0'',fn_count3-( 4 + length(fv_prefix) + length(fn_docno + 1)) ), ' +
        ' fn_docno + 1) as NomorIs ' +
        ' from t_nomor where (fc_branch = '+ QuotedStr(BranchIs)+ ' and fv_document = ' + QuotedStr(DocCode) + ') ' +
        ' and (fv_part = ' + QuotedStr(PartCode)+  ');';
    Open;
  End;
  Result2Send                           := q.FieldByName('NomorIs').AsString;

  With q Do Begin
    SQL.Clear;
    SQL.Add('update t_nomor set fn_docno = fn_docno + 1 ');
    SQL.Add('where (fc_branch = :branchis and fv_document = :docis) and (fv_part = :partis)');
    ParamByName('branchis').Value       := BranchIs;
    ParamByName('docis').Value          := DocCode;
    ParamByName('partis').Value         := PartCode;
    ExecSql;
  End;
  Result                                := Result2Send;
  q.Free;
end;

function TDM.CountingRows(conn: TZConnection; tablename: String): Integer;
var
  q:TZQuery;
begin
  q:=TZQuery.Create(Self);
  with q do begin
    Connection:=conn;
    SQL.Text:='select count(*) as count from '+tablename;
    Open;
    Result:=q.FieldByName('count').AsInteger;
    Free;
  end;
end;

function TDM.CreateNoCust(branch, doc, year, month: String): String;
var
  q:TZQuery;
  p:TZSQLProcessor;
begin
  q:=TZQuery.Create(Self);
  p:=TZSQLProcessor.Create(Self);

  with q do begin
    Connection:=ConnSqlserver;
    SQL.Add('EXEC pr_nomor_CustCode '+QuotedStr(branch)+','+QuotedStr(doc)+','+QuotedStr(year)+','+QuotedStr(month));
    Open;
    Result:=FieldByName('Nomor').AsString;
  end;
end;

function TDM.GetCurDir(filename: string): String;
var
  CurDir:String;
begin
  //GetDir (0,CurDir);
  CurDir:=GetCurrentDir;
  Result:=AnsiReplaceStr(CurDir+PathDelim+filename,'\','\\') // for windows path directory format
end;

function TDM.ReadFile(filename: String): string;
Var S : String;
    C : Char;
    //F : File of char;
    F : TextFile;
begin
  AssignFile(F,filename);
  Reset(F);
  S:='';
  While not Eof(F) and (C<>' ') do
    Begin
    Read (F,C);
    S+=C
    end;
 Writeln;
 CloseFile(F);
 Result:=S;
end;

function TDM.NamaBulan(tglbulan:TDate):String;
begin
     If FormatDateTime('mm',tglbulan) = '01' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Januari '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '02' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Februari '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '03' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Maret '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '04' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' April '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '05' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Mei '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '06' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Juni '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '07' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Juli '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '08' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Agustus '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '09' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' September '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '10' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Oktober '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '11' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' November '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
     If FormatDateTime('mm',tglbulan) = '12' Then Begin
        Result := FormatDateTime('dd',tglbulan)+' Desember '+FormatDateTime('yyyy',tglbulan);
        Exit;
     End;
end;


end.

