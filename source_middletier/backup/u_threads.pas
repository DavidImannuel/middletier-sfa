unit u_threads;

{$mode objfpc}{$H+}

interface

uses
  Classes,StdCtrls, SysUtils,Forms,FileUtil,
  zcomponent,ZConnection,ZDataset,StrUtils,ZSqlProcessor,
  AbUnzper, AbZipper,AbArcTyp,AbZipTyp,AbUtils,
  ftpsend,LazFileUtils;

Type

  { ThreadService }

  ThreadService = class(TThread)
    private
    protected
      ConnMysql,ConnSQLSERVER:TZConnection;
      qt_query,qt_sales,qt_promoimg,MyQuerySQLSERVER:TZQuery;
      MyBatchQueryMYSQL:TZSQLProcessor;
      MyFtp : TFTPSend;
      FBranch,FProcessName,FProcessID,FSalesCode:String;
      FDateParam:TDate;
      FLogMsgType,FLogMsg: String;
      // added variable
      vDeviceID,vSalescode,vCSVFilename,vFtpTempDir,vPromoDir:String;
      ListBox:TListBox;
      procedure Execute;override;
      procedure showLog(vType, vMsg:String);
      procedure ProcessStatus(vType :string);
      function GetCurDir(filename:string=''):String;
    public
      procedure writeLog;
      constructor Create(vBranch,vProcessName,vProcessId:String;vDateParam:TDate);virtual;reintroduce;
  end;

  { TThreadLogMsg }

  TThreadLogMsg = class(TThread)
    private
    protected
      FMsgType,FMsg: String;
      procedure Execute;override;
    public
      procedure writeLog;
      constructor Create(vType,vMsg:String);virtual;reintroduce;
  end;



FUNCTION NowX:TDateTime;

implementation

uses u_setup,u_datamodule,u_service;


function NowX: TDateTime;
var
  c:TZConnection;
  q1:TZQuery;
  cValue : String;
begin

   c:= TZConnection.Create(Application);
   with c do begin
     Protocol:='mysql';
     HostName:=f_service.MYSQLHost;
     User:=f_service.MYSQLUser;
     Password:=f_service.MYSQLPassword;
     Database:=f_service.MYSQLDB;
     LibraryLocation:='';
     Port:=f_service.MYSQLPort;
   end;
   q1:=TZQuery.Create(Application);
   with q1 do begin
      Connection := c;
      Sql.Add('select sysdate() as tanggal');
      Open;
      try
           result:=StrToDateTime(FieldByName('tanggal').AsString);
      except
        cValue := Copy(q1.FieldByName('tanggal').AsString,9,2) + '-'+   // DD
                  Copy(q1.FieldByName('tanggal').AsString,6,2) + '-'+   // MM
                  Copy(q1.FieldByName('tanggal').AsString,1,4) + '-'+   // YYYY
                  ' '+
                  Copy(q1.FieldByName('tanggal').AsString,12,2) + ':'+  // TT
                  Copy(q1.FieldByName('tanggal').AsString,15,2) + ':'+  // NN
                  Copy(q1.FieldByName('tanggal').AsString,18,2);        // SS
        result:=StrToDateTime(cValue);
      end;
   end;
   q1.free;
end;

{ TThreadLogMsg }

procedure TThreadLogMsg.Execute;
begin
 Synchronize(@writeLog);
end;

procedure TThreadLogMsg.writeLog;
begin
  f_service.ShowLog(FMsgType,FMsg);
end;

constructor TThreadLogMsg.Create(vType, vMsg: String);
begin
  FMsgType:=vType;
  FMsg:=vMsg;             
  FreeOnTerminate:=True;
  inherited Create(False);
end;

{ ThreadService }

procedure ThreadService.Execute;
var
  i:Integer;
begin
  {
   NOTE
   untuk delete sql server ke mysql di ganti ke mysql saja

  }
  (* Synchronize MessageLog Example*)
  FLogMsgType:='';
  FLogMsg:='THREAD BRANCH :'+FBranch+' Start';
  Synchronize(@writeLog);
  (* Synchronize MessageLog Example*)
  (* Create Connection Object *)
  (* MYSQL *)
  ConnMYSQL:=TZConnection.Create(Nil);
  with ConnMYSQL do begin
   Protocol:='mysql';
   HostName:=f_service.MYSQLHost;
   User:=f_service.MYSQLUser;
   Password:=f_service.MYSQLPassword;
   Database:=f_service.MYSQLDB;
   LibraryLocation:='';
   Port:=f_service.MYSQLPort;
  end;
  (* SQLSERVER *)
  ConnSQLSERVER:=TZConnection.Create(Nil);
  with ConnSQLSERVER do begin
   Protocol:='ado';
   HostName:=f_service.SQLSERVERHost;
   User:=f_service.SQLSERVERUser;
   Password:=f_service.SQLSERVERPassword;
   Database:=f_service.SQLSERVERDB;
   Port:=f_service.SQLSERVERPort;
  end;
  // try connect MYSQL and SQLSERVER
  try
    ConnMysql.Connected:=True;
    ConnSQLSERVER.Connected:=True;
  except
    on E:exception do begin
     showLog('error',E.Message);
     //destroy all when cant connect
     ConnMysql.Free;
     ConnSQLSERVER.Free;
     Free;
    end;
  end;
  (* Query Component *)
  qt_query:=TZQuery.Create(Nil);
  qt_query.Connection:=ConnMysql;

  qt_sales:=TZQuery.Create(Nil);
  qt_sales.Connection:=ConnMysql;

  qt_promoimg:=TZQuery.Create(Nil);
  qt_promoimg.Connection:=ConnMysql;

  // componnet for execute query
  MyQuerySQLSERVER:=TZQuery.Create(Nil);
  MyQuerySQLSERVER.Connection:=ConnSQLSERVER;

  MyBatchQueryMYSQL:=TZSQLProcessor.Create(Nil);
  MyBatchQueryMYSQL.Connection:=ConnMYSQL;
  (* Query Component *)

  (* FTP *)
  MyFtp:=TFTPSend.Create;
  with MyFtp do begin
    TargetHost:=f_service.FTPHost;
    TargetPort:=f_service.FTPPort;
    UserName:=f_service.FTPUser;
    Password:=f_service.FTPPassword;
  end;
  (* FTP *)

  if FProcessName = 'TRANSFER_DATA_BETWEEN_SERVER' then begin
    showLog('info','TRANSFER_DATA_BETWEEN_SERVER Start');
    ProcessStatus('PROCESS');
    (*EXport CSV DBTIS*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "EXPORT_CSV_DBTIS" and fc_querytarget = "PRIORITY" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      try
        showLog('','Run '+FieldByName('fc_desc').AsString);
        MyQuerySQLSERVER.SQL.Clear;
        MyQuerySQLSERVER.SQL.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyQuerySQLSERVER.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyQuerySQLSERVER.ParamByName('date').AsDate:=FDateParam;
        MyQuerySQLSERVER.ExecSQL;
        showLog('success','IMPORT TIS to DLTISNOW '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
          showLog('error','IMPORT TIS to DLTISNOW '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*EXport CSV DBTIS*)
    (*Import CSV DBTIS*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "IMPORT_CSV_DBTIS" and fc_querytarget = "PRIORITY" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      showLog('','Run '+FieldByName('fc_desc').AsString);
      try
        MyBatchQueryMYSQL.Script.Clear;
        MyBatchQueryMYSQL.Script.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyBatchQueryMYSQL.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyBatchQueryMYSQL.ParamByName('date').AsDate:=FDateParam;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':csvfile')
           then MyBatchQueryMYSQL.ParamByName('csvfile').AsString:=AnsiReplaceStr(FieldByName('fc_additional1').AsString,':branch',FBranch);
        showLog('info','Import CSV '+AnsiReplaceStr(FieldByName('fc_additional1').AsString,':branch',FBranch));
        MyBatchQueryMYSQL.Execute;
        showLog('success','IMPORT DLTISNOW to SFA '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
         showLog('error','IMPORT DLTISNOW to SFA '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*Import CSV DBTIS*)
    (*Import TIS*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "IMPORT_DBTIS" and fc_querytarget = "PRIORITY" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      try
        showLog('','Run '+FieldByName('fc_desc').AsString);
        MyQuerySQLSERVER.SQL.Clear;
        MyQuerySQLSERVER.SQL.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyQuerySQLSERVER.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyQuerySQLSERVER.ParamByName('date').AsDate:=FDateParam;
        MyQuerySQLSERVER.ExecSQL;
        showLog('success','IMPORT TIS to DLTISNOW '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
          showLog('error','IMPORT TIS to DLTISNOW '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*Import TIS*)
    (*Import DLTISNOW*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "IMPORT_DLTISNOW" and fc_querytarget = "PRIORITY" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      showLog('','Run '+FieldByName('fc_desc').AsString);
      try
        MyBatchQueryMYSQL.Script.Clear;
        MyBatchQueryMYSQL.Script.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyBatchQueryMYSQL.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyBatchQueryMYSQL.ParamByName('date').AsDate:=FDateParam;
        MyBatchQueryMYSQL.Execute;
        showLog('success','IMPORT DLTISNOW to SFA '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
         showLog('error','IMPORT DLTISNOW to SFA '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*Import DLTISNOW*)
    ProcessStatus('FINISH');
    showLog('info','TRANSFER_DATA_BETWEEN_SERVER Finish');
  end else if FProcessName = 'TRANSFER_MASTER_DATA' then begin
    showLog('info','TRANSFER_MASTER_DATA Start');
    ProcessStatus('PROCESS');
    (*EXport CSV DBTIS*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "EXPORT_CSV_DBTIS" and fc_querytarget = "MASTER" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      try
        showLog('','Run '+FieldByName('fc_desc').AsString);
        MyQuerySQLSERVER.SQL.Clear;
        MyQuerySQLSERVER.SQL.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyQuerySQLSERVER.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyQuerySQLSERVER.ParamByName('date').AsDate:=FDateParam;
        MyQuerySQLSERVER.ExecSQL;
        showLog('success','IMPORT TIS to DLTISNOW '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
          showLog('error','IMPORT TIS to DLTISNOW '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*EXport CSV DBTIS*)
    (*Import CSV DBTIS*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "IMPORT_CSV_DBTIS" and fc_querytarget = "MASTER" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      showLog('','Run '+FieldByName('fc_desc').AsString);
      try
        MyBatchQueryMYSQL.Script.Clear;
        MyBatchQueryMYSQL.Script.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyBatchQueryMYSQL.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyBatchQueryMYSQL.ParamByName('date').AsDate:=FDateParam;
        MyBatchQueryMYSQL.Execute;
        showLog('success','IMPORT DLTISNOW to SFA '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
         showLog('error','IMPORT DLTISNOW to SFA '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*Import CSV DBTIS*)
    (*Import TIS*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "IMPORT_DBTIS" and fc_querytarget = "MASTER" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      try
        showLog('','Run '+FieldByName('fc_desc').AsString);
        MyQuerySQLSERVER.SQL.Clear;
        MyQuerySQLSERVER.SQL.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyQuerySQLSERVER.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyQuerySQLSERVER.ParamByName('date').AsDate:=FDateParam;
        MyQuerySQLSERVER.ExecSQL;
        showLog('success','TRANSFER MASTER DATA TIS to DLTISNOW '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
          showLog('error','TRANSFER MASTER DATA TIS to DLTISNOW '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*Import TIS*)
    (*Import DLTISNOW*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_query where fc_querytype = "IMPORT_DLTISNOW" and fc_querytarget = "MASTER" and fl_status = 1 order by fn_no ';
     Open;
     First;
     while not EOF do begin
      showLog('','Run '+FieldByName('fc_desc').AsString);
      try
        MyBatchQueryMYSQL.Script.Clear;
        MyBatchQueryMYSQL.Script.Text:=FieldByName('fc_query').AsString;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then MyBatchQueryMYSQL.ParamByName('branch').AsString:=FBranch;
        if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then MyBatchQueryMYSQL.ParamByName('date').AsDate:=FDateParam;
        MyBatchQueryMYSQL.Execute;
        showLog('success','TRANSFER MASTER DLTISNOW to SFA '+FieldByName('fc_desc').AsString);
      except
        on E:Exception do begin
         showLog('error','TRANSFER MASTER DLTISNOW to SFA '+FieldByName('fc_desc').AsString+','+E.Message);
        end;
      end;
      Next;
     end;
    end;
    (*Import DLTISNOW*)
    ProcessStatus('FINISH');
    showLog('info','TRANSFER_MASTER_DATA Finish');
  end else if FProcessName = 'EXPORT_DEVICE_CSV'then begin
    showLog('info','EXPORT_DEVICE_CSV Start');
    ProcessStatus('PROCESS');
    (*EXPORT CSV DEVICE ID*)
    with qt_query do begin
     SQL.Clear;
     SQL.Text:=' select * from t_device_list where fc_branch = :branch and fc_active = "T" ';
     ParamByName('branch').AsString:=FBranch;
     Open;
     First;
     while not EOF do begin
       vDeviceID:=FieldByName('fc_devicecode').AsString;
         with TZSQLProcessor.Create(Nil) do begin
          try
            showLog('','Run Export CSV DEVICE : '+vDeviceID);
            Connection:=ConnMysql;
            if FileExists(GetCurDir('ftp'+PathDelim+vDeviceID+'.csv')) then DeleteFile(GetCurDir('ftp'+PathDelim+vDeviceID+'.csv'));
            Script.Text:='select * from db_sfa.t_device where fc_devicecode = '+QuotedStr(vDeviceID)+' and fc_branch = '+QuotedStr(FBranch)
                        +' into outfile '+QuotedStr(GetCurDir('ftp'+PathDelim+vDeviceID+'.csv'))
                        +' FIELDS TERMINATED BY '+ QuotedStr('|')
                        +' LINES TERMINATED BY '+QuotedStr('\n')
                        ;
            Execute;
            showLog('success','Export CSV with filename '+GetCurDir('ftp'+PathDelim+vDeviceID+'.csv')+ ' success.');
          except
             on E:Exception do begin
              showLog('error','Export CSV with filename '+GetCurDir('ftp'+PathDelim+vDeviceID+'.csv')+ ' failed. ERROR : '+E.Message);
             end;
          end;
          Free;
         end;

        with MyFtp do begin
          try
            if Login then showLog('success','success login ftp') else showLog('error','cant login ftp');
            FtpPutFile(f_service.FTPHost,f_service.FTPPort,'deviceid/'+vDeviceID+'.csv',GetCurDir('ftp'+PathDelim+vDeviceID+'.csv'),f_service.FTPUser,f_service.FTPPassword);
          except
            on E:Exception do begin
               showLog('error',E.Message);
            end;
          end;
        end;
        Next;
     end;
    end;
    (*EXPORT CSV DEVICE ID*)
    ProcessStatus('FINISH');
    showLog('info','EXPORT_DEVICE_CSV Finished');
  end else if FProcessName = 'EXPORT_DATA_CSV' then begin
    showLog('info','EXPORT_DATA_CSV Start, based on LKH : '+DateToStr(FDateParam));
    ProcessStatus('PROCESS');
    (*EXPORT DATA SALES CSV *)
    with qt_sales do begin
      SQl.Clear;
      SQL.Text:=' select a.fc_salescode,a.fc_salesname ' +
                ' from db_dltisnow.t_sales a '+
                ' inner join t_lkhmst b on a.fc_salescode = b.fc_salescode and a.fc_branch = b.fc_branch and a.fc_status = 1 '+
                ' where date_format(fd_datelkh,"%Y%m%d") = date_format(:date,"%Y%m%d") ' +
                ' and b.fc_status != "T" and a.fc_branch = :branch ';
      ParamByName('branch').AsString:=FBranch;
      ParamByName('date').AsDate:=FDateParam;
      Open;
      First;
      while not EOF do begin
       vSalescode:=trim(FieldByName('fc_salescode').AsString);
       (* ftp data dir path *)
       vFtpTempDir:=GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+FBranch+PathDelim+vSalescode);
       (* ftp promo img dir *)
       vPromoDir:=GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+FBranch+PathDelim+vSalescode+PathDelim+'promo'+PathDelim);
       try
         if not DirectoryExists( GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+FBranch) ) then CreateDir(GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+FBranch));
         if DirectoryExists(vFtpTempDir) then begin
          LazFileUtils.RemoveDirUTF8(vFtpTempDir);
          LazFileUtils.CreateDirUTF8(vFtpTempDir);
         end else LazFileUtils.CreateDirUTF8(vFtpTempDir);
         if DirectoryExists(vPromoDir) then begin
          LazFileUtils.RemoveDirUTF8(vPromoDir);
          LazFileUtils.CreateDirUTF8(vPromoDir);
         end else LazFileUtils.CreateDirUTF8(vPromoDir);
       except
         on E:exception do begin
          ShowLog('error',E.Message);
         end;
       end;
       (* EXPORT CSV QUERY *)
       with qt_query do begin
        SQl.Clear;
        SQl.Text:='select * from t_query where fc_querytype = "EXPORT_CSV" and fc_querytarget = "SALES" and fl_status = 1 order by fn_no ';
        Open;
        First;
        while not EOF do begin
         vCSVFilename:=GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+FBranch+PathDelim+vSalescode+PathDelim+FieldByName('fc_additional1').AsString);
         (* EXPORT CSV *)
         with TZSQLProcessor.Create(Nil) do begin
          try
            showLog('info','Run Export CSV SALES '+vSalescode+' : '+FieldByName('fc_desc').AsString );
            showLog('info','CURRENT DIRECTORY DEBUG '+ ExtractFilePath(ParamStr(0)));
            Connection:=ConnMysql;
            if FileExists(vCSVFilename) then DeleteFile(vCSVFilename);
            Script.Text:=FieldByName('fc_query').AsString+' into outfile '+QuotedStr(vCSVFilename)
                        +' FIELDS TERMINATED BY '+ QuotedStr('|')
                        +' LINES TERMINATED BY '+QuotedStr('\n')
                        ;
            if AnsiContainsStr(FieldByName('fc_query').AsString,':branch') then ParamByName('branch').AsString:=FBranch;
            if AnsiContainsStr(FieldByName('fc_query').AsString,':salescode') then ParamByName('salescode').AsString:=vSalescode;
            if AnsiContainsStr(FieldByName('fc_query').AsString,':date') then ParamByName('date').AsDate:=FDateParam;
            Execute;
            showLog('success','Export CSV SALES '+vSalescode+' with filename '+vCSVFilename+ ' success.');
          except
             on E:Exception do begin
              showLog('error','Export CSV SALES '+vSalescode+' with filename '+vCSVFilename+ ' failed. ERROR : '+E.Message);
             end;
          end;
          Free;
         end;
         (* EXPORT CSV *)
         Next;
        end;
       end;
       (* EXPORT CSV QUERY *)
       (* PROMO IMG *)
       with qt_promoimg do begin
        SQl.Clear;
        SQl.Text:='select * from t_promoimg where fc_salescode = :salescode and :date >= fd_validon and :date <= fd_validuntil and fc_branch = :branch ';
        ParamByName('branch').AsString:=FBranch;
        ParamByName('salescode').AsString:=vSalescode;
        ParamByName('date').AsDate:=FDateParam;
        ShowLog('','Get Promo on date : '+FormatDateTime('DD-MM-YYY',FDateParam) + ' and sales : '+vSalescode);
        Open;
        while not EOF do begin
          f_service.ShowLog('','Getting Promo IMG FILENAME : '+FieldByName('fc_imgfilename').AsString);
          with MyFtp do begin
           if Login then begin
            FtpGetFile(
              f_service.FTPHost,
              f_service.FTPPort,
              '/promo/'+FBranch+'/'+FieldByName('fc_imgfilename').AsString,
              vPromoDir+FieldByName('fc_imgfilename').AsString,
              f_service.FTPUser,
              f_service.FTPPassword
              );
           end else showLog('error','cannot login ftp');
          end;
        end;
       end;
       (* PROMO IMG *)
       (* ZIP FILE *)
       // vFtpTempDir:=GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+FBranch+PathDelim+vSalescode);
       if FileExists(vFtpTempDir+'.zip') then DeleteFile(vFtpTempDir+'.zip');
       with TAbZipper.Create(Nil) do begin
         try
            FileName:=vFtpTempDir+'.zip';
            showLog('info','prepare to zip '+AnsiReplaceStr(FileName,'\\','\'));
            //name of the directory that we want to zip
            StoreOptions:=[soRecurse];
            BaseDirectory:=vFtpTempDir;
            AddFiles('*.*',faAnyFile);
            Save;
            CloseArchive;
            showLog('success','zip file from ' +vFtpTempDir+ ' to '+FileName+' success');
          except
          on E:Exception do begin
             showLog('error','Cannot ZIP : '+FileName+','+E.Message);
           end;
         end;
         Free;
       end;
         (*FTP SEND*)
         with MyFtp do begin
          if Login then begin
           if not ChangeWorkingDir('/preparedata/'+FBranch) then CreateDir('/preparedata/'+FBranch);
           if FtpPutFile(
            f_service.FTPHost,
            f_service.FTPPort,
            '/preparedata/'+FBranch+'/'+vSalescode+'.zip',
            vFtpTempDir+'.zip',
            f_service.FTPUser,
            f_service.FTPPassword
           ) then showLog('success','send '+vFtpTempDir+'.zip to ftp') else showLog('error','cant send '+vFtpTempDir+'.zip to ftp');
          end else showLog('error','cant login ftp');
         end;
         (*FTP SEND*)
       (* ZIP FILE *)
       Next;
      end;
    end;
    (*EXPORT DATA SALES CSV *)
    ProcessStatus('FINISH');
    showLog('info','EXPORT_DATA_CSV Finished');
  end else if FProcessName = 'SETTLEMENT' then begin
    showLog('info','SETTLEMENT Start');
    ProcessStatus('PROCESS');
    (*SETTLEMENT CSV SALES*)
    ListBox:=TListBox.Create(Nil); //listbox constructor
     (* SALES QUERY *)
     with qt_sales do begin
      SQl.Clear;
      SQL.Text:=' select a.fc_salescode,a.fc_salesname ' +
                ' from db_dltisnow.t_sales a '+
                ' inner join t_lkhmst b on a.fc_salescode = b.fc_salescode and a.fc_branch = b.fc_branch and a.fc_status = 1 '+
                ' where date_format(fd_datelkh,"%Y%m%d") = date_format(:date,"%Y%m%d") ' +
                ' and b.fc_status != "T" and a.fc_branch = :branch ';
      ParamByName('branch').AsString:=FBranch;
      ParamByName('date').AsDate:=FDateParam;
      Open;
      First;
      while not EOF do begin
       vSalescode:=FieldByName('fc_salescode').AsString;

       if not DirectoryExists( GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch) ) then CreateDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch);
       if not MyFtp.Login then showLog('error','cant login ftp');
       // IF can Download / Return true
        with MyFtp do begin
          if  FtpGetFile(
           f_service.FTPHost,
           f_service.FTPPort,
           '/upload/'+FBranch+'/'+vSalescode+'.zip',
           GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch+PathDelim+vSalescode+'.zip'),
           f_service.FTPUser,
           f_service.FTPPassword
          ) then
          begin
            showLog('success','download success file salescode : '+vSalescode);
            { backup ftp file }
            //create dir backup
            if not DirectoryExists( GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',NowX)) )
               then  LazFileUtils.CreateDirUTF8( GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',NowX)) );

            if not DirectoryExists( GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',NowX)+PathDelim+FBranch) )
               then  LazFileUtils.CreateDirUTF8( GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',NowX)+PathDelim+FBranch) );
            // get ftp file and move it to backup
            with MyFtp do begin
              FtpGetFile(
               f_service.FTPHost,
               f_service.FTPPort,
               '/upload/'+FBranch+'/'+vSalescode+'.zip',
               GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',NowX)+PathDelim+FBranch+PathDelim+vSalescode+'.zip'),
               f_service.FTPUser,
               f_service.FTPPassword
              )
            end;
            //after get and backup ftp file then delete it
            with MyFtp do begin
             if Login then DeleteFile('/upload/'+FBranch+'/'+vSalescode+'.zip') else showLog('error','cant login ftp');
            end;
            //unzip csv file
            try
              showLog('info','Creating Directory for '+ AnsiReplaceStr(GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch+PathDelim+vSalescode),'\\','\'));
              LazFileUtils.DeleteFileUTF8(GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch+PathDelim+vSalescode));
              LazFileUtils.CreateDirUTF8(GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch+PathDelim+vSalescode));
              with TAbUnZipper.Create(Nil) do begin
                BaseDirectory:=GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch+PathDelim+vSalescode);
                try
                   FileName:=GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch+PathDelim+vSalescode+'.zip');
                   showLog('info','Unzip '+FileName);
                   ExtractFiles('*.csv');
                   showLog('success','Succesfully Unzip ' + AnsiReplaceStr(FileName,'\\','\'));
                   CloseArchive;
                except
                   on E:Exception do begin
                      showLog('error','Cannot Unzip : '+FileName+','+E.Message);
                   end;
                end;
                Free;
              end;
            except
               on E: Exception do begin
                 showLog('error',E.Message);
               end;
            end;
            ListBox.Items.Clear;
            ListBox.Items:= FindAllFiles(GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+FBranch+PathDelim+vSalescode),'*.csv',True) ;
            ShowLog('info','Found :'+IntToStr(ListBox.Items.Count) +' File');
            for i:=0 to ListBox.Items.Count-1 do begin
              //ListBox.ItemIndex:=i;
              ShowLog('info','import csv '+ListBox.Items[i]);
              (*IMPORT CSV*)
              with TZSQLProcessor.Create(Nil) do begin
                try
                  Connection:=ConnMysql;
                  Script.Text:=' load data local infile '
                              +QuotedStr(AnsiReplaceStr(ListBox.Items[i],'\','\\'))
                              +' REPLACE ' //delete jika sudah ada lalu insert
                              +' into table ' + 'db_sfa.'+ExtractFileNameWithoutExt( ExtractFileName(ListBox.Items[i]) )
                              +' FIELDS TERMINATED BY '+ QuotedStr('|')
                              +' ENCLOSED BY '+QuotedStr('"')
                              +' lines terminated by '+ QuotedStr(chr(13)) //chr13 = enter jika menggunakan \r atau \n kurang work
                              +' IGNORE 1 LINES '
                              +' ;';
                  Execute;
                  showLog('success','Import CSV file on table '+ExtractFileNameWithoutExt( ExtractFileName(ListBox.Items[i]) )+ ' success.');
                except
                   on E:Exception do begin
                     showLog('error','Import CSV file on table '+ExtractFileNameWithoutExt( ExtractFileName(ListBox.Items[i]) )+ ' failed. ERROR : '+E.Message);
                   end;
                end;
                Free;
              end;
              (*IMPORT CSV*)
            end;
          end else ShowLog('error','cant download '+ vSalescode +' File');
        end;
       Next;
      end;
     end;
    ListBox.Free; //destructor listbox
    (*SETTLEMENT CSV SALES*)
    ProcessStatus('FINISH');
    showLog('info','SETTLEMENT Finished');
  end else if FProcessName = 'TES' then begin
     showLog('',AnsiReplaceStr('D:\\Data\\2001-t_customercomp.csv',':branch',FBranch));
  end;

  //free database object
  ConnSQLSERVER.Free;
  ConnMysql.Free;
  //free query object
  qt_query.Free;
  qt_sales.Free;
  qt_promoimg.Free;
  MyQuerySQLSERVER.Free;
  MyBatchQueryMYSQL.Free;

  //Free;
end;

procedure ThreadService.showLog(vType, vMsg: String);
begin
  FLogMsgType:=vType;
  FLogMsg:=vMsg;
  Synchronize(@writeLog);
end;

procedure ThreadService.ProcessStatus(vType: string);
begin
  if vType = 'PROCESS' then begin
    try
      with MyBatchQueryMYSQL do begin
       Script.Clear;
       Script.Text:='update t_request_middletier set fc_status = :status where fn_requestnum = :fn_requestnum ';
       ParamByName('status').AsString:='PROCESS';
       ParamByName('fn_requestnum').AsInteger:=StrToInt(FProcessID);
       Execute;
      end;
    except
      on E:Exception do begin
       showLog('error',e.Message);
      end;
    end;
  end else if vType = 'FINISH' then begin
    with MyBatchQueryMYSQL do begin
     Script.Clear;
     Script.Text:='update t_request_middletier set fc_status = :status where fn_requestnum = :fn_requestnum ';
     ParamByName('status').AsString:='FINISH';
     ParamByName('fn_requestnum').AsInteger:=StrToInt(FProcessID);
     try
     Execute;
     showLog('finish','THREAD BRANCH :'+FBranch+' Finished');
     except
       on E:Exception do begin
        showLog('error','THREAD BRANCH :'+FBranch+' Cannot Update Status,'+E.Message);
       end;
     end;
    end;
  end;
end;

function ThreadService.GetCurDir(filename: string): String;
begin
    Result:=AnsiReplaceStr(ExtractFilePath(ParamStr(0))+filename,'\','\\') // for windows path directory format
end;

procedure ThreadService.writeLog;
begin
  f_service.WriteLog(FLogMsgType,FLogMsg,FBranch,FProcessID);
end;

constructor ThreadService.Create(vBranch, vProcessName, vProcessId: String;
  vDateParam: TDate);
begin
  FBranch:=vBranch;
  FProcessName:=vProcessName;
  FProcessID:=vProcessId;
  FDateParam:=vDateParam; 
  FreeOnTerminate:=True;
  inherited Create(False);
end;

end.

