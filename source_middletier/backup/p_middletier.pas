program p_middletier;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, laz_synapse, datetimectrls, abbrevia, zcomponent, u_setup,
  u_datamodule, u_threads, JsonTools, u_service
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Title:='p_middletier';
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(Tf_service, f_service);
  Application.CreateForm(Tf_setup, f_setup);
  Application.Run;
end.

