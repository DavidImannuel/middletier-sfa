unit u_service;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DB, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls, Buttons, DBGrids, DateTimePicker, ZDataset, RichMemo, ftpsend,
  JsonTools, ZConnection,ZSqlProcessor;

const
  TIMER_INTERVAL = 3000;

type

  { Tf_service }

  Tf_service = class(TForm)
    BtnPower: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    ComboBox1: TComboBox;
    ds_processedrequest: TDataSource;
    DateTimePicker1: TDateTimePicker;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    ds_request: TDataSource;
    Edit1: TEdit;
    EditBranch: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    LabelPowerDesciption: TLabel;
    LabelTimeRunning: TLabel;
    LblAppPath: TLabel;
    lblCpuCount: TLabel;
    LblMYSQLHost: TLabel;
    LblMYSQLUSER: TLabel;
    LblMYSQLPASS: TLabel;
    LblFTPHost: TLabel;
    LblFTPUSER: TLabel;
    LblFTPPASS: TLabel;
    LblMYSQLPort: TLabel;
    LblFTPPORT: TLabel;
    LblSQLSERVERHOST: TLabel;
    LblSQLSERVERUSER: TLabel;
    LblSQLSERVERPASS: TLabel;
    LblSQLSERVERPort: TLabel;
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel2: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    qt_request: TZQuery;
    qt_requestfc_branch: TStringField;
    qt_requestfc_functionkode: TStringField;
    qt_requestfc_status: TStringField;
    qt_requestfd_dateparam: TDateTimeField;
    qt_requestfd_requestdate: TDateTimeField;
    qt_requestfn_requestnum: TLargeintField;
    RichMemoLog: TRichMemo;
    SpeedButton1: TSpeedButton;
    StatusBar1: TStatusBar;
    TimerService: TTimer;
    qt_processedrequest: TZQuery;
    qt_processedrequestfc_branch: TStringField;
    qt_processedrequestfc_functionkode: TStringField;
    qt_processedrequestfc_status: TStringField;
    qt_processedrequestfd_dateparam: TDateField;
    qt_processedrequestfd_requestdate: TDateTimeField;
    qt_processedrequestfn_requestnum: TLargeintField;
    procedure BtnPowerClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TimerServiceStartTimer(Sender: TObject);
    procedure TimerServiceTimer(Sender: TObject);
    procedure ReadJSONConfig(filename:String);
  private

  public
    MYSQLHost,MYSQLUser,MYSQLPassword,MYSQLLibLoc,MYSQLDB:String;
    MYSQLPort:Integer;
    SQLSERVERHost,SQLSERVERUser,SQLSERVERPassword,SQLSERVERDB:String;
    SQLSERVERPort:Integer;
    FTPHost,FTPUser,FTPPassword,FTPPort:String;
    MyFtp:TFTPSend;
    procedure WriteLog(vType, vMessage, vBranch, vProcessID: String);
    procedure ShowLog(vType, vMessage: String);

  end;

var
  f_service: Tf_service;

implementation
uses u_datamodule,u_threads,u_setup;

{$R *.lfm}


{ Tf_service }

procedure Tf_service.BtnPowerClick(Sender: TObject);
begin
  if TimerService.Enabled then begin
    TimerService.Enabled:=False;
    LabelPowerDesciption.Caption:='Service is stop';
    BtnPower.ImageIndex:=1;
  end else if not TimerService.Enabled then begin
    TimerService.Enabled:=True;
    LabelPowerDesciption.Caption:='Service is running';
    BtnPower.ImageIndex:=0;
  end;
end;

procedure Tf_service.Button1Click(Sender: TObject);
begin
  if ( trim(Edit1.Text) <> '' ) then begin
    ReadJSONConfig(Edit1.Text);
    qt_request.Open;
    qt_processedrequest.Open;
  end else ShowMessage('harap load config json terlebih dahulu');
end;

procedure Tf_service.Button2Click(Sender: TObject);
var
  ConnMYSQL:TZConnection;
begin
  ConnMYSQL:=TZConnection.Create(Self);
  with ConnMYSQL do begin
   Protocol:='mysql';
   HostName:=f_service.MYSQLHost;
   User:=f_service.MYSQLUser;
   Password:=f_service.MYSQLPassword;
   Database:=f_service.MYSQLDB;
   LibraryLocation:='';
   Port:=f_service.MYSQLPort;
  end;
  with DM.MyBatch do begin
    Connection:=ConnMysql;
    Script.Clear;
    Script.Text:='insert into t_request_middletier(fc_branch,fc_functionkode,fd_requestdate,fc_status,fd_dateparam) '
                +' values( :fc_branch,:fc_functionkode,:fd_requestdate,:fc_status,:fd_dateparam )';
    ParamByName('fc_branch').AsString:=EditBranch.Text;
    ParamByName('fc_functionkode').AsString:=ComboBox1.Items.Strings[ComboBox1.ItemIndex];
    ParamByName('fd_requestdate').AsDateTime:=DM.NowX;
    ParamByName('fc_status').AsString:='WAITING';
    ParamByName('fd_dateparam').AsDate:=DateTimePicker1.Date;
    Execute;
    ConnMYSQL.Free;
  end;
end;

procedure Tf_service.Button3Click(Sender: TObject);
begin
  if OpenDialog1.Execute then begin
      OpenDialog1.Filter:='JSON Config|*.json;*.JSON';
      edit1.Text:=OpenDialog1.FileName;
  end;
end;

procedure Tf_service.Button4Click(Sender: TObject);
begin
   u_threads.ThreadService.Create('2001',
            'TES',
            '123',
            now);
end;

procedure Tf_service.FormCreate(Sender: TObject);
begin
  (* instanse FTP Object *)
  MyFtp:=TFTPSend.Create;
  TimerService.Interval:=TIMER_INTERVAL;
  LblAppPath.Caption:=ExtractFilePath(ParamStr(0));
  lblCpuCount.Caption:='CPU COUNT : '+IntToStr(GetCPUCount);
end;

procedure Tf_service.SpeedButton1Click(Sender: TObject);
begin
  //.Show make visible to true
  f_setup.Show;
end;

procedure Tf_service.TimerServiceStartTimer(Sender: TObject);
begin
  ShowLog('','Service Start');
end;

procedure Tf_service.TimerServiceTimer(Sender: TObject);
begin
  qt_request.Close;qt_request.Open;
  qt_processedrequest.Close;qt_processedrequest.Open;
  if f_service.qt_request.RecordCount = 0 then begin
      qt_request.Close;qt_request.Open;
      qt_processedrequest.Close;qt_processedrequest.Open;
      LabelTimeRunning.Caption:=TimeToStr(Now);
  end else begin
    TimerService.Enabled:=False;

    with f_service.qt_request do begin
      First;
      While not EOF do begin

        try
          u_threads.ThreadService.Create(qt_requestfc_branch.AsString,
            qt_requestfc_functionkode.AsString,
            qt_requestfn_requestnum.AsString,
            qt_requestfd_dateparam.AsDateTime);
        Except
           on E:exception do begin
             u_threads.TThreadLogMsg.Create('error',E.Message);
           end;
        end;
        WriteLog('','RUN THREAD ON BRANCH : '+qt_requestfc_branch.AsString+' PROCESS : '+qt_requestfc_functionkode.AsString+' PROCESS ID : '+qt_requestfn_requestnum.AsString
                         ,qt_requestfc_branch.AsString,qt_requestfn_requestnum.AsString);
        Next;
      end;
    end;

    try
     qt_request.Close;qt_request.Open;
     TimerService.Enabled:=True;
    except
       on E:Exception do begin
         WriteLog('error',E.Message,'','');
       end;
    end;
  end;
end;

procedure Tf_service.ReadJSONConfig(filename: String);
begin
  With TJsonNode.Create do begin
    LoadFromFile(filename);
    //mysql
    MYSQLHost:=Find('mysql/hostname').AsString;
    MYSQLPort:=StrToInt(Find('mysql/port').AsString);
    MYSQLUser:=Find('mysql/user').AsString;
    MYSQLPassword:=Find('mysql/password').AsString;
    MYSQLLibLoc:=Find('mysql/libraryLocation').AsString;
    MYSQLDB:=Find('mysql/database').AsString;
    //sqlserver
    SQLSERVERHost:=Find('sqlserver/hostname').AsString;
    SQLSERVERPort:=StrToInt(Find('sqlserver/port').AsString);
    SQLSERVERUser:=Find('sqlserver/user').AsString;
    SQLSERVERPassword:=Find('sqlserver/password').AsString;
    SQLSERVERDB:=Find('sqlserver/database').AsString;
    //FTP
    FTPHost:=Find('ftp/hostname').AsString;
    FTPPort:=Find('ftp/port').AsString;
    FTPUser:=Find('ftp/user').AsString;
    FTPPassword:=Find('ftp/password').AsString;
    MyFtp.TargetHost:=FTPHost;
    MyFtp.TargetPort:=FTPPort;
    MyFtp.UserName:=FTPUser;
    MyFtp.Password:=FTPPassword;

    //display on label
    LblMYSQLHost.Caption:=MYSQLHost;
    LblMYSQLUSER.Caption:=MYSQLUser;
    LblMYSQLPort.Caption:=IntToStr(MYSQLPort);
    LblMYSQLPASS.Caption:=MYSQLPassword;

    LblSQLSERVERHOST.Caption:=SQLSERVERHost;
    LblSQLSERVERUSER.Caption:=SQLSERVERUser;
    LblSQLSERVERPort.Caption:=IntToStr(SQLSERVERPort);
    LblSQLSERVERPASS.Caption:=SQLSERVERPassword;

    LblFTPHost.Caption:=FTPHost;
    LblFTPUSER.Caption:=FTPUser;
    LblFTPPASS.Caption:=FTPPassword;
    LblFTPPORT.Caption:=FTPPort;

    //try to connect all

    try
     with DM.ConnMysql do begin
       Connected:=False;
       HostName:=MYSQLHost;
       Port:=MYSQLPort;
       User:=MYSQLUser;
       Password:=MYSQLPassword;
       Database:=MYSQLDB;
       Connected:=True;


     //open setup query

       if Assigned(f_setup) then begin
         f_setup.qt_importdbtis.Open;
         f_setup.qt_importdltisnow.Open;
         f_setup.qt_exportcsvsales.Open;
         f_setup.qt_exportcsv_dbtis.Open;
         f_setup.qt_importcsv_dbtis.Open;
         f_setup.CheckBox1Change(Self);
       end;

     end;

     with DM.ConnSqlserver do begin
       Connected:=False;
       HostName:=SQLSERVERHost;
       Port:=SQLSERVERPort;
       User:=SQLSERVERUser;
       Password:=SQLSERVERPassword;
       Database:=SQLSERVERDB;
       Connected:=True;
     end;

    except
       on E:exception do begin
         MessageDlg('Error','MYSQL / SQL SERVER CANT CONNECT . ERROR : '+E.Message+', The Application Can''t be Open,'+sLineBreak +'Please Check JSON Config FIle. ',mtError,[mbOK],0);
       end;
    end;

    if Myftp.Login then begin
      //ShowMessage('FTP connect');
        //if Assigned(MyFtp) then MyFtp.Free;
    end else begin
        MessageDlg('Error','FTP cant connect,check USERNAME , PASSWORD & PORT !!, The Application Can''t be Open, '+sLineBreak +'Please Check JSON Config FIle. ',mtError,[mbOK],0);
    end;

  end;
end;

procedure Tf_service.WriteLog(vType, vMessage,vBranch,vProcessID: String);
begin
  with RichMemoLog do begin
    if vType = 'info' then begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+']['+vBranch+']['+vProcessID+'] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clBlue);
    end else if vType = 'success' then begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+']['+vBranch+']['+vProcessID+'][SUCCESS] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clGreen);
    end else if vType = 'error' then begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+']['+vBranch+']['+vProcessID+'][ERROR] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clRed);
    end else begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+']['+vBranch+']['+vProcessID+'] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clBlack);
    end;
  end;
end;

procedure Tf_service.ShowLog(vType, vMessage: String);
begin
  with RichMemoLog do begin
    if vType = 'info' then begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+'] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clBlue);
    end else if vType = 'success' then begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+'][SUCCESS] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clGreen);
    end else if vType = 'error' then begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+'][ERROR] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clRed);
    end else begin
       Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+'] '+vMessage);
       //SetRangeColor(Length(Lines.Text) - Length(Lines[Lines.Count - 1]) - Lines.Count - 1, Length(Lines[Lines.Count - 1]),clBlack);
    end;
  end;
end;

end.

