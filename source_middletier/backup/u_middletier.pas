unit u_middletier;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Buttons,
  StdCtrls, ComCtrls, CheckLst, DBGrids, DBCtrls, RichMemo, AbZipper, AbUnzper,
  db, ZSqlProcessor, ZDataset, strutils, FileUtil, DateTimePicker, ftpsend;

type

  { Tf_middletier }

  Tf_middletier = class(TForm)
    CheckBox1: TCheckBox;
    DateTimePickerPrepareData: TDateTimePicker;
    DateTimePickerSettlement: TDateTimePicker;
    ds_branch_tis: TDataSource;
    DBGridSettlementQuery: TDBGrid;
    DBGridQueryImportDBTIS8: TDBGrid;
    DBLookupComboBox1: TDBLookupComboBox;
    DBMemo8: TDBMemo;
    DBMemo9: TDBMemo;
    DBNavigator8: TDBNavigator;
    DBNavigator9: TDBNavigator;
    ds_branchsales: TDataSource;
    DBGrid4: TDBGrid;
    ds_exportcsvsales: TDataSource;
    DBGridQueryImportDBTIS4: TDBGrid;
    ds_queryimportdltisnowbranch: TDataSource;
    DBGridQueryImportDBTIS2: TDBGrid;
    DBGridQueryImportDBTIS3: TDBGrid;
    DBMemo5: TDBMemo;
    DBNavigator5: TDBNavigator;
    ds_queryimportdltisnowgeneral: TDataSource;
    DBGridQueryImportDBTIS1: TDBGrid;
    DBMemo4: TDBMemo;
    DBNavigator4: TDBNavigator;
    ds_importsqlserverbranch: TDataSource;
    ds_importsqlservergeneral: TDataSource;
    DBGridQueryImportDBTIS: TDBGrid;
    DBGridSalesExportCSV: TDBGrid;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    DBMemo3: TDBMemo;
    DBNavigator1: TDBNavigator;
    DBNavigator2: TDBNavigator;
    DBNavigator3: TDBNavigator;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    LabelDate1: TLabel;
    LabelPowerDesciption2: TLabel;
    ListBox1: TListBox;
    MemoLog: TMemo;
    DBGridQueryImportDBDLTISNOW1: TDBGrid;
    ds_sales_csv: TDataSource;
    ds_device_list: TDataSource;
    DBGridExportDeviceUser: TDBGrid;
    DateTimePicker1: TDateTimePicker;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    dsqt_trxlist: TDataSource;
    dsqt_trxtype: TDataSource;
    Label1: TLabel;
    Label10: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    LabelDate: TLabel;
    LabelDate2: TLabel;
    BtnPower: TBitBtn;
    Image1: TImage;
    LabelPowerDesciption: TLabel;
    LabelPowerDesciption1: TLabel;
    LabelTimeRunning: TLabel;
    MemoLog1: TMemo;
    PageControl: TPageControl;
    PageControl1: TPageControl;
    PageControl2: TPageControl;
    PageControl3: TPageControl;
    PageControl4: TPageControl;
    PageControl5: TPageControl;
    PageControl6: TPageControl;
    Panel1: TPanel;
    HomePanel: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel2: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel30: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel39: TPanel;
    Panel4: TPanel;
    Panel40: TPanel;
    Panel41: TPanel;
    Panel43: TPanel;
    Panel44: TPanel;
    Panel45: TPanel;
    Panel46: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    ProgressBar1: TProgressBar;
    ProgressBar3: TProgressBar;
    ProgressBar4: TProgressBar;
    ProgressBar5: TProgressBar;
    ProgressBar6: TProgressBar;
    ProgressBar7: TProgressBar;
    ProgressBar8: TProgressBar;
    ProgressBar9: TProgressBar;
    ProgressBarExportDeviceUser: TProgressBar;
    PSetup: TPanel;
    qt_branchsalesfc_branch: TStringField;
    qt_branchsalesfc_compcode: TStringField;
    qt_branchsalesfc_salescode: TStringField;
    qt_branchsalesfc_salesname: TStringField;
    qt_branchsalesfc_status: TStringField;
    qt_device_listfc_active: TStringField;
    qt_device_listfc_branch: TStringField;
    qt_device_listfc_devicecode: TStringField;
    qt_device_listfc_userid: TStringField;
    qt_exportcsvsalesfc_additional1: TStringField;
    qt_exportcsvsalesfc_additional2: TStringField;
    qt_exportcsvsalesfc_desc: TStringField;
    qt_exportcsvsalesfc_query: TMemoField;
    qt_exportcsvsalesfc_querytarget: TStringField;
    qt_exportcsvsalesfc_querytype: TStringField;
    qt_exportcsvsalesfl_status: TSmallintField;
    qt_exportcsvsalesfn_no: TLongintField;
    qt_importdltisnowbranchfc_desc: TStringField;
    qt_importdltisnowbranchfc_query: TMemoField;
    qt_importdltisnowbranchfc_querytarget: TStringField;
    qt_importdltisnowbranchfc_querytype: TStringField;
    qt_importdltisnowbranchfl_status: TSmallintField;
    qt_importdltisnowbranchfn_no: TLongintField;
    qt_importdltisnowgeneralfc_desc: TStringField;
    qt_importdltisnowgeneralfc_query: TMemoField;
    qt_importdltisnowgeneralfc_querytarget: TStringField;
    qt_importdltisnowgeneralfc_querytype: TStringField;
    qt_importdltisnowgeneralfl_status: TSmallintField;
    qt_importdltisnowgeneralfn_no: TLongintField;
    qt_importsqlservergeneralfc_desc: TStringField;
    qt_importsqlservergeneralfc_query: TMemoField;
    qt_importsqlservergeneralfc_querytarget: TStringField;
    qt_importsqlservergeneralfc_querytype: TStringField;
    qt_importsqlservergeneralfl_status: TSmallintField;
    qt_importsqlservergeneralfn_no: TLongintField;
    qt_sales_csvfc_salescode: TStringField;
    qt_sales_csvfc_salesname: TStringField;
    qt_importsqlserverbranchfc_additional1: TStringField;
    qt_importsqlserverbranchfc_additional2: TStringField;
    qt_importsqlserverbranchfc_desc: TStringField;
    qt_importsqlserverbranchfc_query: TMemoField;
    qt_importsqlserverbranchfc_querytarget: TStringField;
    qt_importsqlserverbranchfc_querytype: TStringField;
    qt_importsqlserverbranchfl_status: TSmallintField;
    qt_importsqlserverbranchfn_no: TLongintField;
    qt_trxlist: TZQuery;
    qt_trxlistfc_trxid: TStringField;
    qt_trxtype: TZQuery;
    qt_trxtypefc_branch: TStringField;
    qt_trxtypefc_trxcode: TStringField;
    qt_trxtypefc_trxid: TStringField;
    qt_trxtypefn_trxnumber: TSmallintField;
    qt_trxtypefv_trxdescription: TStringField;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    Splitter1: TSplitter;
    Splitter3: TSplitter;
    StatusBar1: TStatusBar;
    TabSheet1: TTabSheet;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet15: TTabSheet;
    TabSheet16: TTabSheet;
    TabSheet17: TTabSheet;
    TabSheet18: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    TabSheetPrepareData: TTabSheet;
    TabSheetSetup: TTabSheet;
    TabSheetHome: TTabSheet;
    Timer: TTimer;
    qt_device_list: TZReadOnlyQuery;
    qt_sales_csv: TZQuery;
    qt_importsqlservergeneral: TZQuery;
    qt_importdltisnowgeneral: TZQuery;
    qt_importdltisnowbranch: TZQuery;
    qt_exportcsvsales: TZQuery;
    qt_branchsales: TZQuery;
    qt_branch_tis: TZQuery;
    qt_branch_tiscBranch: TStringField;
    qt_branch_tiscCompCode: TStringField;
    qt_branch_tiscDesc: TStringField;
    qt_branch_tiscNo: TStringField;
    qt_branch_tiscStatus: TStringField;
    qt_branch_tisdLastTrans: TDateTimeField;
    qt_importsqlserverbranch: TZQuery;
    qt_promoimg: TZQuery;
    qt_promoimgfc_branch: TStringField;
    qt_promoimgfc_deskripsi: TStringField;
    qt_promoimgfc_imgfilename: TStringField;
    qt_promoimgfc_salescode: TStringField;
    qt_promoimgfd_upldate: TDateTimeField;
    qt_promoimgfd_validon: TDateTimeField;
    qt_promoimgfd_validuntil: TDateTimeField;
    procedure BtnPowerClick(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure dsqt_trxlistDataChange(Sender: TObject; Field: TField);
    procedure ds_branch_tisDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qt_exportcsvsalesNewRecord(DataSet: TDataSet);
    procedure qt_importdltisnowbranchNewRecord(DataSet: TDataSet);
    procedure qt_importdltisnowgeneralNewRecord(DataSet: TDataSet);
    procedure qt_importsqlserverbranchNewRecord(DataSet: TDataSet);
    procedure qt_importsqlservergeneralNewRecord(DataSet: TDataSet);
    procedure qt_trxtypeNewRecord(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure TimerStartTimer(Sender: TObject);
    procedure TimerStopTimer(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure WriteLog(message:String);
    procedure TransferDataBeetweenMYSQL(TMysql,Query : String); //procedure to transfer data from beetween mysql
    function GetCurDir(filename:string=''):String;
    //preparedata
    procedure ActivatedQueryComponent;
    procedure importDBTISGENERAL;
    procedure importDBTISBRANCH(branch:string;vDate:TDate);
    procedure importDLTISNOWGENERAL;
    procedure importDLTISNOWBRANCH(branch:string;vDate:TDate);
    procedure ExportDeviceCSV(branch:string);
    procedure ExportCSVSales(branch:string;vDate:TDate);
    procedure ZeroPositionProgessBarPrepareData;
    //settlement
    procedure Settlement(vBranch:string);
    procedure SettlementQuery(branch:string;vDate:TDate);
    //thread
    procedure runPrepareDatathread;
    procedure runPrepareDeviceIDCsvThread;
    procedure runPrepareDataCsvThread;
    procedure runSettlementThread;
  private

  public

    MyFtp:TFTPSend;
    VThreadPrepareData : TThread;

  end;


  { ThreadTransferData }

  ThreadTransferData = class(TThread)
    protected
      procedure Execute; override;
  end;

  { ThreadPrepareDeviceIDCSV }

  ThreadPrepareDeviceIDCSV = class(TThread)
    protected
      procedure Execute; override;
  end;

  { ThreadPrepareDataCSV }

  ThreadPrepareDataCSV = class(TThread)
    protected
      procedure Execute; override;
  end;

  { ThreadSettlement }

  ThreadSettlement = class(TThread)
    protected
      procedure Execute; override;
  end;


var
  f_middletier: Tf_middletier;

implementation

uses
  u_datamodule,u_querystorage,u_threads;

{$R *.lfm}

{ ThreadPrepareDataCSV }

procedure ThreadPrepareDataCSV.Execute;
begin
  with f_middletier do begin
    ExportCSVSales(qt_branch_tiscBranch.AsString,DateTimePickerPrepareData.Date);
  end;
end;

{ ThreadPrepareDeviceIDCSV }

procedure ThreadPrepareDeviceIDCSV.Execute;
begin
  f_middletier.ExportDeviceCSV(f_middletier.qt_branch_tiscBranch.AsString);
end;

{ ThreadSettlement }

procedure ThreadSettlement.Execute;
begin
  f_middletier.Settlement(f_middletier.qt_branch_tiscBranch.AsString);
end;


{ ThreadTransferData }

procedure ThreadTransferData.Execute;
begin

 //Import from tis to dltisnow
 with f_middletier do begin
   importDBTISGENERAL;
   importDBTISBRANCH(qt_branch_tiscBranch.AsString,DM.NowX);
   importDLTISNOWGENERAL;
   importDLTISNOWBRANCH(qt_branch_tiscBranch.AsString,DM.NowX);
    //ZeroPositionProgessBarPrepareData;
 end;

end;


{ Tf_middletier }

procedure Tf_middletier.BtnPowerClick(Sender: TObject);
begin
  if Timer.Enabled then begin
    Timer.Enabled:=False;
    //DM.PingTimer.Enabled:=False;
    LabelPowerDesciption.Caption:='Service is stop';
    StatusBar1.Panels[2].Text:='STOP';
    BtnPower.ImageIndex:=1;

  end else if not Timer.Enabled then begin

    Timer.Enabled:=True;
    //DM.PingTimer.Enabled:=True;
    LabelPowerDesciption.Caption:='Service is running';
    StatusBar1.Panels[2].Text:='RUNNING';
    BtnPower.ImageIndex:=0;
  end;
end;

procedure Tf_middletier.CheckBox1Change(Sender: TObject);
begin
  if CheckBox1.Checked then begin
    qt_importsqlserverbranchfc_query.ReadOnly:=True;
    qt_importsqlservergeneralfc_query.ReadOnly:=True;
    qt_importdltisnowgeneralfc_query.ReadOnly:=True;
    qt_importdltisnowbranchfc_query.ReadOnly:=True;
    qt_exportcsvsalesfc_query.ReadOnly:=True;
    qt_importsqlserverbranchfc_query.ReadOnly:=true;
    QS.qt_settlement_queryfc_query.ReadOnly:=True;
  end else begin
    qt_importsqlserverbranchfc_query.ReadOnly:=False;
    qt_importsqlservergeneralfc_query.ReadOnly:=False;
    qt_importdltisnowgeneralfc_query.ReadOnly:=False;
    qt_importdltisnowbranchfc_query.ReadOnly:=False;
    qt_exportcsvsalesfc_query.Readonly:=False;
    qt_importsqlserverbranchfc_query.ReadOnly:=False;
    QS.qt_settlement_queryfc_query.ReadOnly:=False;
  end;
end;

procedure Tf_middletier.dsqt_trxlistDataChange(Sender: TObject; Field: TField);
var vQuery : String;
begin
    vQuery:=qt_trxtype.SQL.Text;
    With qt_trxtype do begin
      Close;
      SQl.Clear;
      SQl.Text:=vQuery;
      ParamByName('fc_trxid').AsString:=qt_trxlistfc_trxid.AsString;
      Open;
    end;
end;

procedure Tf_middletier.ds_branch_tisDataChange(Sender: TObject; Field: TField);
begin
  with qt_device_list do begin
   Close;
   ParamByName('branch').AsString:=qt_branch_tiscBranch.AsString;
   Open;
 end;
 with qt_sales_csv do begin
   Close;
   ParamByName('branch').AsString:=qt_branch_tiscBranch.AsString;
   Open;
 end;
 with qt_branchsales do begin
   Close;
   ParamByName('branch').AsString:=qt_branch_tiscBranch.AsString;
   Open;
 end;
end;

procedure Tf_middletier.FormCreate(Sender: TObject);
begin
  LabelDate.Caption:=DM.NamaBulan(DM.NowX);
  DateTimePicker1.DateTime:=DM.NowX;
  CheckBox1Change(Sender);
  ActivatedQueryComponent;
end;

procedure Tf_middletier.FormShow(Sender: TObject);
begin
  //ShowMessage('MIDDLETIER START');
end;

procedure Tf_middletier.qt_exportcsvsalesNewRecord(DataSet: TDataSet);
begin
  qt_exportcsvsalesfn_no.AsInteger:=qt_exportcsvsales.RecordCount + 1;
  qt_exportcsvsalesfc_querytype.AsString:='EXPORT_CSV';
  qt_exportcsvsalesfc_querytarget.AsString:='SALES_CAN';
end;

procedure Tf_middletier.qt_importdltisnowbranchNewRecord(DataSet: TDataSet);
begin
  qt_importdltisnowbranchfn_no.AsInteger:=qt_importdltisnowbranch.RecordCount+1;
  qt_importdltisnowbranchfc_querytype.AsString:='IMPORT_DLTISNOW';
  qt_importdltisnowbranchfc_querytarget.AsString:='BRANCH';
end;

procedure Tf_middletier.qt_importdltisnowgeneralNewRecord(DataSet: TDataSet);
begin
  qt_importdltisnowgeneralfn_no.AsInteger:=qt_importdltisnowgeneral.RecordCount + 1;
  qt_importdltisnowgeneralfc_querytype.AsString:='IMPORT_DLTISNOW';
  qt_importdltisnowgeneralfc_querytarget.AsString:='GENERAL';
end;

procedure Tf_middletier.qt_importsqlserverbranchNewRecord(DataSet: TDataSet);
begin
  qt_importsqlserverbranchfn_no.AsInteger:=qt_importsqlserverbranch.RecordCount + 1;
  qt_importsqlserverbranchfc_querytype.AsString:='IMPORT_SQLSERVER';
  qt_importsqlserverbranchfc_querytarget.AsString:='BRANCH';
end;

procedure Tf_middletier.qt_importsqlservergeneralNewRecord(DataSet: TDataSet);
begin
  qt_importsqlservergeneralfn_no.AsInteger:=qt_importsqlservergeneral.RecordCount + 1;
  qt_importsqlservergeneralfc_querytype.AsString:='IMPORT_SQLSERVER';
  qt_importsqlservergeneralfc_querytarget.AsString:='GENERAL';
end;

procedure Tf_middletier.qt_trxtypeNewRecord(DataSet: TDataSet);
begin
  qt_trxtypefc_branch.AsString:='002';
  qt_trxtypefc_trxid.AsString:=qt_trxlistfc_trxid.AsString;
end;

procedure Tf_middletier.SpeedButton1Click(Sender: TObject);
begin
  qt_trxlist.Close;qt_trxlist.Open;
  qt_trxtype.Close;qt_trxtype.Open;
end;

procedure Tf_middletier.SpeedButton2Click(Sender: TObject);
begin
  if MessageDlg('Are you Sure want to Prepare Data Now',mtConfirmation,mbYesNo,0) = mrYes then begin
     runPrepareDatathread;
  end;
end;

procedure Tf_middletier.SpeedButton3Click(Sender: TObject);
begin
  qt_device_list.Close;qt_device_list.Open;
end;

procedure Tf_middletier.SpeedButton4Click(Sender: TObject);
begin
  runSettlementThread;
end;

procedure Tf_middletier.SpeedButton5Click(Sender: TObject);
begin
  runPrepareDeviceIDCsvThread;
end;

procedure Tf_middletier.SpeedButton6Click(Sender: TObject);
begin
  runPrepareDataCsvThread;
end;

procedure Tf_middletier.SpeedButton8Click(Sender: TObject);
begin
  u_threads.RunSettlementQuery;
end;

procedure Tf_middletier.SpeedButton9Click(Sender: TObject);
begin
  u_threads.RunImportDBTISGeneral;
end;

procedure Tf_middletier.TimerStartTimer(Sender: TObject);
begin
  DM.WriteLog('Middletier is start.',MemoLog);
end;

procedure Tf_middletier.TimerStopTimer(Sender: TObject);
begin
  DM.WriteLog('Middletier is stop.',MemoLog);
end;

procedure Tf_middletier.TimerTimer(Sender: TObject);
var
  vRequestNo:Integer;
begin
  //WriteLog('Checking time.....');
  try

    if not DM.ConnMysql.Connected then begin
       DM.ConnMysql.Reconnect;
    end else begin
      DM.ConnMysql.Ping;
      LabelTimeRunning.Caption:=TimeToStr(DM.NowX);
      StatusBar1.Panels[3].Text:=FormatDateTime('yyyy-mm-dd hh:nn:ss',DM.NowX);
    end;

  except
     on E:exception do begin
       DM.WriteLog('ERROR : ' + E.Message,MemoLog);
     end;
  end;
  //if FormatDateTime('HH:nn:ss',DM.NowX) = TimeToStr(DateTimePicker1.Time) then begin
  //  runPrepareDatathread;
  //end;
  with QS do begin

    if qt_request_middletier.RecordCount = 0 then begin
       qt_request_middletier.Close;
       qt_request_middletier.Open;
    end else begin

      Timer.Enabled:=False;

      with u_threads.ThreadMiddletierRequest.Create(True) do begin
        FreeOnTerminate:=True;Start;
        WaitFor;TImer.Enabled:=True;
      end;

    end;

  end;

end;

procedure Tf_middletier.WriteLog(message: String);
begin
  MemoLog.Lines.Add('['+FormatDateTime('DD-MM-YYYY HH:nn:ss',DM.NowX)+'] '+message);
end;

procedure Tf_middletier.TransferDataBeetweenMYSQL(TMysql, Query: String);
var
  SQLProcessor : TZSQLProcessor;
begin
  SQLProcessor := TZSQLProcessor.Create(Self);
  With SQLProcessor do begin
    try
      Connection:=DM.ConnMysql;
      Script.Clear;
      Script.Text:=Query;
      Execute;
      WriteLog('Query for '+TMysql+'(sfa) success.');
    except
       on E:Exception do
        begin
            WriteLog('Query for '+TMysql+' (sfa) failed. ERROR : '+E.Message);
        end;
    end;
    Free;
  end;
end;

function Tf_middletier.GetCurDir(filename: string): String;
var
  CurDir:String;
begin
  //GetDir (0,CurDir);
  CurDir:=GetCurrentDir;
  Result:=AnsiReplaceStr(CurDir+PathDelim+filename,'\','\\') // for windows path directory format
end;

procedure Tf_middletier.ActivatedQueryComponent;
begin
  DM.ReOpenDataSet(qt_trxlist);
  DM.ReOpenDataSet(qt_trxtype);
  qt_device_list.Open;
  qt_branch_tis.Open;
  DM.ReOpenDataSet(qt_importsqlservergeneral);
  DM.ReOpenDataSet(qt_importsqlserverbranch);
  DM.ReOpenDataSet(qt_importdltisnowgeneral);
  Dm.ReOpenDataSet(qt_importdltisnowbranch);
  DM.ReOpenDataSet(qt_exportcsvsales);
  DM.ReOpenDataSet(QS.qt_settlementmysql);
  DM.ReOpenDataSet(QS.qt_settlement_query);

end;

procedure Tf_middletier.ExportDeviceCSV(branch:string);
begin
  DBGridExportDeviceUser.Enabled:=False;
  Dm.WriteLog('job start. export device user to CSV db_sfa(MYSQL) on Branch '+ branch,MemoLog);
  ProgressBarExportDeviceUser.Position:=0;

  with qt_device_list do begin
    Close;
    ParamByName('branch').AsString:=branch;
    Open;
  end;

  ProgressBarExportDeviceUser.Max:=qt_device_list.RecordCount;

  qt_device_list.First;
   while not qt_device_list.EOF do begin

     DM.ExportRecordToCsv('t_device',
       'select * from db_sfa.t_device where fc_devicecode = '+QuotedStr(qt_device_listfc_devicecode.AsString)
       +' and fc_branch = '+QuotedStr(branch),
       DM.GetCurDir('ftp'+PathDelim+qt_device_listfc_devicecode.AsString+'.csv'),'|');

     DM.SendFileToFTP('deviceid/'+qt_device_listfc_devicecode.AsString+'.csv',DM.GetCurDir('ftp'+PathDelim+qt_device_listfc_devicecode.AsString+'.csv'));

     qt_device_list.Next;
     ProgressBarExportDeviceUser.Position:=ProgressBarExportDeviceUser.Position+1;
   end;

   DM.WriteLog('job finish. export device user to CSV db_sfa(MYSQL) finish.',MemoLog);
   DM.WriteLog('========================================================================================================================',MemoLog);
   DBGridExportDeviceUser.Enabled:=True;
end;

procedure Tf_middletier.ExportCSVSales(branch:string;vDate:TDate);
var
  Dir,PromoDir,CSVFilename:String;
begin

  DBGridSalesExportCSV.Enabled:=False;
  DM.WriteLog('job start. export CSV db_sfa(MYSQL) on Branch : '+branch+' with date param '+FormatDateTime('DD-MM-YYY',vDate),MemoLog);

  qt_sales_csv.Close;
  qt_sales_csv.ParamByName('branch').AsString:=branch;
  qt_sales_csv.Open;

  ProgressBar3.Position:=0;
  ProgressBar3.Max:=qt_sales_csv.RecordCount;

  qt_sales_csv.First;
  while not qt_sales_csv.EOF do begin

    // ftp data dir path
    Dir:=DM.GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+qt_sales_csvfc_salescode.AsString);
    // ftp promo img dir
    PromoDir:=DM.GetCurDir('ftp'+PathDelim+'preparedata'+PathDelim+qt_sales_csvfc_salescode.AsString+PathDelim+'promo');

    try
      DM.WriteLog('Creating CSV Data Directory '+ Dir,MemoLog);
      CreateDir(Dir);
      DM.WriteLog('Creating Promo IMG Data Directory '+ Dir,MemoLog);
      CreateDir(PromoDir);
    except
       on E: Exception do begin
         Dm.WriteLog('Error : '+ E.Message,MemoLog);
       end;
    end;

      qt_exportcsvsales.First;
       while not qt_exportcsvsales.EOF do begin
         if qt_exportcsvsalesfl_status.AsInteger <> 0 then begin

           CSVFilename:=DM.GetCurDir('ftp'+PathDelim+'preparedata'
                                   +PathDelim+qt_sales_csvfc_salescode.AsString
                                   +PathDelim+qt_exportcsvsalesfc_additional1.AsString) ;

           DM.ExportDataCsvSales(branch,
               qt_sales_csvfc_salescode.AsString
               , qt_exportcsvsalesfc_desc.AsString,
               qt_exportcsvsalesfc_query.AsString,
             CSVFilename,'|',vDate);

         end else
             //WriteLog(qt_exportcsvsalesfc_desc.AsString,+' query not active')
             ;
         qt_exportcsvsales.Next;
       end;

       // NEED TESTING
       with qt_promoimg do begin
         Close;
         ParamByName('branch').AsString:=branch;
         ParamByName('salescode').AsString:=qt_sales_csvfc_salescode.AsString;
         ParamByName('date').AsDate:=vDate;
         Open;

         Dm.WriteLog('Get Promo on date : '+FormatDateTime('DD-MM-YYY',vDate) + ' and sales : '+qt_sales_csvfc_salescode.AsString,MemoLog);

         while not EOF do begin
           Dm.WriteLog('Getting Promo IMG FILENAME : '+qt_promoimgfc_imgfilename.AsString,MemoLog);
           DM.GetFTPFile('/promo/'+branch+'/'+qt_promoimgfc_imgfilename.AsString,PromoDir+qt_promoimgfc_imgfilename.AsString);
         end;

       end;


       if DeleteFile(DM.GetCurDir('ftp'+PathDelim+qt_sales_csvfc_salescode.AsString+'.zip') )
          then
              //DM.WriteLog('File EXIST FILE DELETED',f_middletier.MemoLog)
              ;

       DM.ZipFile(Dir,DM.GetCurDir('ftp'+PathDelim+qt_sales_csvfc_salescode.AsString+'.zip'));

       DM.SendFileToFTP('preparedata/'+qt_sales_csvfc_salescode.AsString+'.zip',DM.GetCurDir('ftp'+PathDelim+qt_sales_csvfc_salescode.AsString+'.zip'));

    qt_sales_csv.Next;
    ProgressBar3.Position:=ProgressBar3.Position+1;
  end;
   DM.WriteLog('job finish. export CSV db_sfa(MYSQL) finish.',MemoLog);
   DM.WriteLog('========================================================================================================================',MemoLog);
   DBGridSalesExportCSV.Enabled:=True;
end;

procedure Tf_middletier.ZeroPositionProgessBarPrepareData;
begin
  ProgressBar1.Position:=0;
  ProgressBar3.Position:=0;
  ProgressBarExportDeviceUser.Position:=0;
end;

procedure Tf_middletier.Settlement(vBranch:string);
var
  i:Integer;
begin

  with f_middletier do begin

    DBGridQueryImportDBDLTISNOW1.Enabled:=False;

    DM.WriteLog('Job Start. Downloading File from FTP and extract it',MemoLog);

    With qt_branchsales do begin
      Close;
      //untuk testing
      Sql.Clear;
      //SQL.Text:='select * from db_dltisnow.t_sales where fc_branch = :branch and fc_salescode in ("B015") ;';
      SQL.Text:='select * from db_dltisnow.t_sales where fc_branch = :branch ;';
      //untuk testing
      ParamByName('branch').AsString:=vBranch;
      Open;
    end;

    ProgressBar4.Position:=0;
    ProgressBar4.Max:=qt_branchsales.RecordCount;

    qt_branchsales.First;

    while not qt_branchsales.EOF do begin

      // IF can Download / DM.ftpGetFile retunr true
      if ( DM.GetFTPFile(
         '/upload/'+qt_branchsalesfc_salescode.AsString+'.zip',
         Dm.GetCurDir('ftptemp'+PathDelim+qt_branchsalesfc_salescode.AsString+'.zip')
        ) )then
      begin

        DM.GetFileFromFTP(
          '/upload/'+qt_branchsalesfc_salescode.AsString+'.zip',
          Dm.GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+qt_branchsalesfc_salescode.AsString+'.zip')
        );
        { backup ftp file }

        //create dir backup
        if not DirectoryExists(
           Dm.GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',DM.NowX) )
           ) then CreateDir( Dm.GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',DM.NowX) ) );

        // get ftp file and move it to backup
        DM.GetFileFromFTP(
          '/upload/'+qt_branchsalesfc_salescode.AsString+'.zip',
          Dm.GetCurDir( 'backup'+pathDelim+'CSV'+FormatDateTime('yyyymmdd',DM.NowX)+PathDelim+qt_branchsalesfc_salescode.AsString+'.zip')
        );

        //after get and backup ftp file then delete it
        DM.DeleteFTPFile('/upload/'+qt_branchsalesfc_salescode.AsString+'.zip');

        //unzip csv file
        DM.UnZip(
              DM.GetCurDir('ftp'+pathDelim+'downloadftp'+PathDelim+qt_branchsalesfc_salescode.AsString+'.zip'),
              DM.GetCurDir('ftp'+pathDelim+'settlement'+PathDelim+qt_branchsalesfc_salescode.AsString),
        '*.csv');

        ListBox1.Items.Clear;
        ListBox1.Items:= FindAllFiles(
          DM.GetCurDir('ftp'+pathDelim+'settlement'+PathDelim+qt_branchsalesfc_salescode.AsString)
          ,'*.csv',True) ;

        Dm.WriteLog('Found :'+IntToStr(ListBox1.Items.Count) +' File',MemoLog);
        for i:=0 to ListBox1.Items.Count-1 do begin
          ListBox1.ItemIndex:=i;
          DM.WriteLog( ListBox1.Items[i] ,f_middletier.MemoLog);
          DM.ImportCsv('db_sfa.'+ExtractFileNameWithoutExt( ExtractFileName(ListBox1.Items[i]) )
              ,AnsiReplaceStr(ListBox1.Items[i],'\','\\'),'|','',True);
        end;

      end else DM.WriteLog('cant download '+ qt_branchsalesfc_salesname.AsString + ' ( '+qt_branchsalesfc_salescode.AsString+' ) File',MemoLog);

      qt_branchsales.Next;
      ProgressBar4.Position:=ProgressBar4.Position+1;
    end;

    DBGridQueryImportDBDLTISNOW1.Enabled:=True;

  end;

end;

procedure Tf_middletier.SettlementQuery(branch: string; vDate: TDate);
begin
  // MYSQL
  QS.qt_settlementmysql.Close;
  QS.qt_settlementmysql.Open;

  DBGridQueryImportDBTIS8.Enabled:=False;

  QS.qt_settlementmysql.First;

  WriteLog('job start. Run Settlement Query (MYSQL)');
  ProgressBar6.Position:=0;
  ProgressBar6.Max:=QS.qt_settlementmysql.RecordCount;

   while not QS.qt_settlementmysql.EOF do begin
     if QS.qt_settlementmysqlfl_status.AsInteger <> 0 then begin
         DM.RunQueryWithBranch(DM.ConnSqlserver,
           QS.qt_settlementmysqlfc_desc.AsString,
           QS.qt_settlementmysqlfc_query.AsString,
           branch,vDate);
     end else
         //do nothing
         ;
     QS.qt_settlementmysql.Next;
     ProgressBar6.Position:=ProgressBar6.Position+1;
   end;

   WriteLog('job finish. Run Settlement Query finish. (MYSQL)');
   WriteLog('========================================================================================================================');
   DBGridQueryImportDBTIS8.Enabled:=True;

  //SQL SERVER
  QS.qt_settlement_query.Close;
  QS.qt_settlement_query.Open;

  DBGridSettlementQuery.Enabled:=False;

  QS.qt_settlement_query.First;

  WriteLog('job start. Run Settlement Query (SQLSERVER)');
  ProgressBar5.Position:=0;
  ProgressBar5.Max:=QS.qt_settlement_query.RecordCount;

   while not QS.qt_settlement_query.EOF do begin
     if QS.qt_settlement_queryfl_status.AsInteger <> 0 then begin
         DM.RunQueryWithBranch(DM.ConnSqlserver,
           QS.qt_settlement_queryfc_desc.AsString,
           QS.qt_settlement_queryfc_query.AsString,
           branch,vDate);
     end else
         //do nothing
         ;
     QS.qt_settlement_query.Next;
     ProgressBar5.Position:=ProgressBar5.Position+1;
   end;

   WriteLog('job finish. Run Settlement Query finish. (SQLSERVER)');
   WriteLog('========================================================================================================================');
   DBGridSettlementQuery.Enabled:=True;
end;

procedure Tf_middletier.runPrepareDatathread;
begin
  with ThreadTransferData.Create(True) do begin
    FreeOnTerminate:=True;Start;
  end;
end;

procedure Tf_middletier.runPrepareDeviceIDCsvThread;
begin
  With ThreadPrepareDeviceIDCSV.Create(True) do begin
    FreeOnTerminate:=true;Start;
  end;
end;

procedure Tf_middletier.runPrepareDataCsvThread;
begin
  With ThreadPrepareDataCSV.Create(True) do begin
    FreeOnTerminate:=true;Start;
  end;
end;

procedure Tf_middletier.runSettlementThread;
begin
  With ThreadSettlement.Create(True) do begin
    FreeOnTerminate:=true;Start;
  end;
end;

procedure Tf_middletier.importDBTISGENERAL;
begin
  qt_importsqlservergeneral.Close;
  qt_importsqlservergeneral.Open;

  DBGridQueryImportDBTIS.Enabled:=False;

  WriteLog('job start. Import SQL SERVER GENERAL QUERY');
  ProgressBar1.Position:=0;
  ProgressBar1.Max:=qt_importsqlservergeneral.RecordCount;

   while not qt_importsqlservergeneral.EOF do begin
     if qt_importsqlservergeneralfl_status.AsInteger <> 0 then begin
         DM.RunQuery(DM.ConnSqlserver,
           qt_importsqlservergeneralfc_desc.AsString,
           qt_importsqlservergeneralfc_query.AsString);
     end else
         //do nothing
         ;
     qt_importsqlservergeneral.Next;
     ProgressBar1.Position:=ProgressBar1.Position+1;
   end;

   WriteLog('job finish. Import SQL SERVER GENERAL QUERY finish.');
   WriteLog('========================================================================================================================');
   DBGridQueryImportDBTIS.Enabled:=True;

end;

procedure Tf_middletier.importDBTISBRANCH(branch:string;vDate:TDate);
begin
  qt_importsqlserverbranch.Close;
  qt_importsqlserverbranch.Open;

  DBGridQueryImportDBTIS1.Enabled:=False;

  WriteLog('job start. Import SQL SERVER GENERAL BRANCH on Branch : '+branch+' with date param '+FormatDateTime('DD-MM-YYY',vDate) );
  ProgressBar9.Position:=0;
  ProgressBar9.Max:=qt_importsqlserverbranch.RecordCount;

   while not qt_importsqlserverbranch.EOF do begin
     if qt_importsqlserverbranchfl_status.AsInteger <> 0 then begin
         DM.RunQueryWithBranch(DM.ConnSqlserver,
           qt_importsqlserverbranchfc_desc.AsString,
           qt_importsqlserverbranchfc_query.AsString,
           branch,vDate);
     end else
         //do nothing
         ;
     qt_importsqlserverbranch.Next;
     ProgressBar9.Position:=ProgressBar9.Position+1;
   end;

   WriteLog('job finish. Import SQL SERVER GENERAL BRANCH finish.');
   WriteLog('========================================================================================================================');
   DBGridQueryImportDBTIS1.Enabled:=True;
end;

procedure Tf_middletier.importDLTISNOWGENERAL;
begin
  qt_importdltisnowgeneral.Close;
  qt_importdltisnowgeneral.Open;

  DBGridQueryImportDBTIS2.Enabled:=False;

  WriteLog('job start. Import DB_DLTISNOW GENERAL');
  ProgressBar8.Position:=0;
  ProgressBar8.Max:=qt_importdltisnowgeneral.RecordCount;

   while not qt_importdltisnowgeneral.EOF do begin
     if qt_importdltisnowgeneralfl_status.AsInteger <> 0 then begin
         DM.RunQuery(DM.ConnMysql,
           qt_importdltisnowgeneralfc_desc.AsString,
           qt_importdltisnowgeneralfc_query.AsString);
     end else
         //do nothing
         ;
     qt_importdltisnowgeneral.Next;
     ProgressBar9.Position:=ProgressBar9.Position+1;
   end;

   WriteLog('job finish. Import DB_DLTISNOW GENERAL finish.');
   WriteLog('========================================================================================================================');
   DBGridQueryImportDBTIS2.Enabled:=True;
end;

procedure Tf_middletier.importDLTISNOWBRANCH(branch:string;vDate:TDate);
begin
  qt_importdltisnowbranch.Close;
  qt_importdltisnowbranch.Open;

  DBGridQueryImportDBTIS3.Enabled:=False;

  DM.WriteLog('job start. Import DB_DLTISNOW BRANCH on Branch : '+branch+' with date param '+FormatDateTime('DD-MM-YYY',vDate),MemoLog);
  ProgressBar7.Position:=0;
  ProgressBar7.Max:=qt_importdltisnowbranch.RecordCount;

  qt_importdltisnowbranch.First;
   while not qt_importdltisnowbranch.EOF do begin
     if qt_importdltisnowbranchfl_status.AsInteger <> 0 then begin
         DM.RunQueryWithBranch(DM.ConnMysql,
           qt_importdltisnowbranchfc_desc.AsString,
           qt_importdltisnowbranchfc_query.AsString,
           branch,vDate);
     end else
         //do nothing
         ;
     qt_importdltisnowbranch.Next;
     ProgressBar7.Position:=ProgressBar7.Position+1;
   end;

   WriteLog('job finish. Import DB_DLTISNOW BRANCH finish.');
   WriteLog('========================================================================================================================');
   DBGridQueryImportDBTIS3.Enabled:=True;
end;

end.

